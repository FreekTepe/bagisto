<?php

// add routes for which to exclude the bliss css sheets. (so that only architect css is active)
return ['marketplace.account.products.edit-assign', 'marketplace.account.products.search', 'discogs-csv-import.index', 'discogs-csv-import.custom_csv_matches', 'discogs-csv-import.custom_csv_matches.single_file', 'discogs-csv-import.custom_csv_matches.single_row'];
