<?php

return [
    'admin' => [
        [
            'key' => 'marketplace',
            'name' => 'marketplace::app.admin.layouts.marketplace',
            'route' => 'admin.marketplace.sellers.index',
            'sort' => 2,
            'icon-class' => 'marketplace-icon',
        ], [
            'key' => 'marketplace.sellers',
            'name' => 'marketplace::app.admin.layouts.sellers',
            'route' => 'admin.marketplace.sellers.index',
            'sort' => 1
        ], [
            'key' => 'marketplace.products',
            'name' => 'marketplace::app.admin.layouts.products',
            'route' => 'admin.marketplace.products.index',
            'sort' => 2
        ], [
            'key' => 'marketplace.reviews',
            'name' => 'marketplace::app.admin.layouts.seller-reviews',
            'route' => 'admin.marketplace.reviews.index',
            'sort' => 3
        ], [
            'key' => 'marketplace.orders',
            'name' => 'marketplace::app.admin.layouts.orders',
            'route' => 'admin.marketplace.orders.index',
            'sort' => 3
        ], [
            'key' => 'marketplace.transactions',
            'name' => 'marketplace::app.admin.layouts.transactions',
            'route' => 'admin.marketplace.transactions.index',
            'sort' => 3
        ], [
            'key' => 'marketplace.bulkupload',
            'name' => 'bulkupload::app.admin.bulk-upload.bulk-upload',
            'route' => 'admin.marketplace.bulk-upload.index',
            'sort' => 6
        ], [
            'key' => 'marketplace.bulkupload.upload-files',
            'name' => 'bulkupload::app.admin.bulk-upload.upload-files',
            'route' => 'admin.marketplace.bulk-upload.index',
            'sort' => 1
        ], [
            'key' => 'marketplace.bulkupload.run-profile',
            'name' => 'bulkupload::app.admin.bulk-upload.run-profile',
            'route' => 'admin.marketplace.run-profile.index',
            'sort' => 2
        ],  [
            'key' => 'marketplace.dataflow-profile',
            'name' => 'bulkupload::app.admin.bulk-upload.bulk-upload-dataflow-profile',
            'route' => 'admin.marketplace.dataflow-profile.index',
            'sort' => 7
        ],  [
            'key' => 'marketplace.dataflow-profile',
            'name' => 'Format rules',
            'route' => 'shipping.format.rules',
            'sort' => 7
        ]
    ],

    'customer' => [
        // ACCOUNT
        [
            'key'   => 'account',
            'name'  => 'shop::app.layouts.my-account',
            'route' => 'customer.profile.index',
            'icon' => '',
            'sort'  => 1,
        ], [
            'key'   => 'account.profile',
            'name'  => 'shop::app.layouts.profile',
            'route' => 'customer.profile.index',
            'icon' => 'pe-7s-id',
            'sort'  => 1,
        ], [
            'key'   => 'account.address',
            'name'  => 'shop::app.layouts.address',
            'route' => 'customer.address.index',
            'icon' => 'pe-7s-map-marker',
            'sort'  => 2,
        ], [
            'key'   => 'account.reviews',
            'name'  => 'shop::app.layouts.reviews',
            'route' => 'customer.reviews.index',
            'icon' => 'pe-7s-like2',
            'sort'  => 3,
        ], [
            'key'   => 'account.wishlist',
            'name'  => 'shop::app.layouts.wishlist',
            'route' => 'customer.wishlist.index',
            'icon' => 'pe-7s-like',
            'sort'  => 4,
        ],
        [
            'key'   => 'account.compare',
            'name'  => 'velocity::app.customer.compare.text',
            'route' => 'velocity.customer.product.compare',
            'icon' => 'pe-7s-look',
            'sort'  => 5,
        ],
        [
            'key'   => 'account.orders',
            'name'  => 'shop::app.layouts.orders',
            'route' => 'customer.orders.index',
            'icon' => 'pe-7s-cart',
            'sort'  => 6,
        ], [
            'key'   => 'account.downloadables',
            'name'  => 'shop::app.layouts.downloadable-products',
            'route' => 'customer.downloadable_products.index',
            'icon' => 'pe-7s-cloud-download',
            'sort'  => 7,
        ],

        // MARKETPLACE
        [
            'key' => 'marketplace',
            'name' => 'marketplace::app.shop.layouts.marketplace',
            'route' => 'marketplace.account.seller.edit',
            'icon' => '',
            'sort' => 2
        ], [
            'key' => 'marketplace.seller',
            'name' => 'Shop Profile',
            'route' => 'marketplace.account.seller.edit',
            'icon' => 'pe-7s-id',
            'sort' => 3
        ], [
            'key' => 'marketplace.dashboard',
            'name' => 'marketplace::app.shop.layouts.dashboard',
            'route' => 'marketplace.account.dashboard.index',
            'icon' => 'pe-7s-graph2',
            'sort' => 1
        ], [
            'key' => 'marketplace.products',
            'name' => 'marketplace::app.shop.layouts.products',
            'route' => 'marketplace.account.products.index',
            'icon' => 'pe-7s-box2',
            'sort' => 4
        ], [
            'key' => 'marketplace.orders',
            'name' => 'marketplace::app.shop.layouts.orders',
            'route' => 'marketplace.account.orders.index',
            'icon' => 'pe-7s-rocket',
            'sort' => 5
        ], [
            'key' => 'marketplace.transactions',
            'name' => 'marketplace::app.shop.layouts.transactions',
            'route' => 'marketplace.account.transactions.index',
            'icon' => 'pe-7s-cash',
            'sort' => 6
        ], [
            'key' => 'marketplace.reviews',
            'name' => 'marketplace::app.shop.layouts.reviews',
            'route' => 'marketplace.account.reviews.index',
            'icon' => 'pe-7s-like2',
            'sort' => 7
        ],


        [
            'key' => 'marketplace.csvImport',
            'name' => 'CSV imports',
            'route' => 'discogs-csv-import.index',
            'icon' => 'pe-7s-cloud-upload',
            'sort' => 8
        ],
        [
            'key' => 'marketplace.policies',
            'name' => 'Shipping policies',
            'route' => 'shipping.policies',
            'icon' => 'pe-7s-box1',
            'sort' => 9
        ],
        // [
        //     'key' => 'shipping.policies',
        //     'name' => 'Shipping policies',
        //     'route' => 'shipping.policies',
        //     'icon' => 'pe-7s-box1',
        //     'sort' => 9
        // ],
        // [
        //     'key' => 'marketplace.matchedCustomCsvImport',
        //     'name' => 'Custom CSV product matches',
        //     'route' => 'discogs-csv-import.custom_csv_matches',
        //     'icon' => 'pe-7s-like2',
        //     'sort' => 9
        // ],
        [
            'key' => 'marketplace.mptablerate',
            'name' => 'marketplace_tablerate_shipping::app.admin.layouts.manage-shipping-rates',
            'route' => 'shop.marketplace.tablerate.rates.index',
            'icon' => 'pe-7s-close',
            'sort' => 11
        ], [
            'key' => 'marketplace.superset',
            'name' => 'marketplace_tablerate_shipping::app.admin.layouts.manage-superset-rates',
            'route' => 'shop.marketplace.tablerate.super_set_rates.index',
            'icon' => 'pe-7s-close',
            'sort' => 12
        ], 

        // 
    ]
];
