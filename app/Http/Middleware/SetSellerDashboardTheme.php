<?php

namespace App\Http\Middleware;

use Closure;
use Webkul\Theme\Facades\Themes;

class SetSellerDashboardTheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Themes::set('vinylheaven-seller-portal-theme');
        return $next($request);
    }
}
