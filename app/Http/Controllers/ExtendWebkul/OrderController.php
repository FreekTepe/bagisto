<?php

    namespace App\Http\ExtendWebkul\Controllers;

    use Webkul\Marketplace\Http\Controllers\Shop\Account\Sales\OrderController as OrderBaseController;
    use Webkul\Marketplace\Repositories\SellerRepository;
    use Webkul\Marketplace\Repositories\OrderRepository;

    class OrderController extends OrderBaseController
    {

        /**
         * Create a new controller instance.
         *
         * @param  Webkul\Marketplace\Repositories\SellerRepository $sellerRepository
         * @param  Webkul\Marketplace\Repositories\OrderRepository  $orderRepository
         * @return void
         */
        public function __construct(
            SellerRepository $sellerRepository,
            OrderRepository $orderRepository
        ) {
            parent::__construct($sellerRepository, $orderRepository);
        }

        /**
         * Show the view for the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function view($id)
        {
            themes()->set('vinylheaven-seller-portal-theme');

            $seller = $this->sellerRepository->findOneWhere([
                'customer_id' => auth()->guard('customer')->user()->id
            ]);

            $sellerOrder = $this->orderRepository->findOneWhere([
                'order_id' => $id,
                'marketplace_seller_id' => $seller->id
            ]);

            if (!$sellerOrder) {
                abort('404');
            }

            return view($this->_config['view'], compact('sellerOrder'));
        }

    }