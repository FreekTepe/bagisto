<?php

namespace App\Http\Controllers;

use App\Jobs\DeleteAllProductsJob;
use App\Models\ExtendWebkul\Seller;
use App\Repositories\SellerRepository;
use App\Exports\MarketplaceProductsExport;
use Webkul\Marketplace\Http\Controllers\Shop\Account\ProductController as BaseProductController;


class MarketplaceProductController extends BaseProductController
{
    // Destroy all marketplace seller products
    public function destroy_all()
    {
        $sellerRepository = resolve(SellerRepository::class);
        $seller = $sellerRepository->getSellerByUserId(auth()->guard('customer')->user()->id);

        // dispatch job
        DeleteAllProductsJob::dispatch($seller);

        // foreach ($seller->marketplace_products as $marketplace_product) {
        //     if ($marketplace_product) {
        //         if ($marketplace_product->is_owner == 1) {
        //             $this->sellerProduct->delete($marketplace_product->id);
        //             $this->product->delete($marketplace_product->product_id);
        //         } else {
        //             $this->sellerProduct->delete($marketplace_product->id);
        //         }
        //     }
        // }

        session()->flash('success', 'Your products are being deleted, this may take a while.');
        return back();
    }

    public function download_csv()
    {
        
        $sellerRepository = resolve(SellerRepository::class);
        $seller = $sellerRepository->getSellerByUserId(auth()->guard('customer')->user()->id);

        // dd($seller->marketplace_products[12]->product->artists);
        return \Excel::download(new MarketplaceProductsExport($seller->marketplace_products), 'products.csv');
    }
}
