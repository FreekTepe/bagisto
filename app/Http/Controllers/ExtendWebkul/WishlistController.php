<?php

namespace App\Http\ExtendWebkul\Controllers;

use Webkul\Product\Repositories\ProductRepository;
use Webkul\Customer\Repositories\WishlistRepository;
use App\Repositories\WishlistRepository as ExtendWishlistRepository;
use Webkul\Customer\Http\Controllers\WishlistController as WishlistBaseController;

class WishlistController extends WishlistBaseController
{

    protected $extendWishlistRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Customer\Repositories\WishlistRepository  $wishlistRepository
     * @param  \Webkul\Product\Repositories\ProductRepository  $productRepository
     * @param  \App\Repositories\WishlistRepository  $extendWishlistRepository
     * @return void
     */
    public function __construct(
        WishlistRepository $wishlistRepository,
        ProductRepository $productRepository,
        ExtendWishlistRepository $extendWishlistRepository

    ) {
        $this->extendWishlistRepository = $extendWishlistRepository;
        parent::__construct($wishlistRepository, $productRepository);
    }

    public function index()
    {
        $wishlistItems = $this->extendWishlistRepository->getCustomerWhishlist(8); // param = pagiantion
        return view($this->_config['view'])->with('items', $wishlistItems);
    }
}
