<?php

namespace App\Http\ExtendWebkul\Controllers;

use App\Models\ExtendWebkul\MarketplaceProduct;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Marketplace\Http\Controllers\Shop\Controller;
use Webkul\Marketplace\Repositories\OrderRepository;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Sales\Repositories\OrderItemRepository as BaseOrderItem;
use Webkul\Sales\Repositories\ShipmentRepository;
use Webkul\Marketplace\Http\Controllers\Shop\Account\Sales\ShipmentController as BaseShipmentController;


class ShipmentController extends BaseShipmentController
{

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\OrderRepository    $order
     * @param  Webkul\Marketplace\Repositories\SellerRepository   $seller
     * @param  Webkul\Sales\Repositories\OrderItemRepository      $baseOrderItem
     * @param  Webkul\Marketplace\Repositories\ShipmentRepository $shipment
     * @return void
     */
    public function __construct(
        OrderRepository $order,
        SellerRepository $seller,
        BaseOrderItem $baseOrderItem,
        ShipmentRepository $shipment
    ) {
        parent::__construct($order, $seller, $baseOrderItem, $shipment);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $orderId
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $orderId)
    {

        themes()->set('vinylheaven-seller-portal-theme');

        $seller = $this->seller->findOneWhere([
            'customer_id' => auth()->guard('customer')->user()->id
        ]);

        $sellerOrder = $this->order->findOneWhere([
            'order_id' => $orderId,
            'marketplace_seller_id' => $seller->id
        ]);


        if (!$sellerOrder->canShip()) {
            session()->flash('error', 'Order shipment creation is not allowed.');

            return redirect()->back();
        }

        $this->validate(request(), [
            'shipment.carrier_title' => 'required',
            'shipment.track_number' => 'required',
            'shipment.source' => 'required',
            'shipment.items.*.*' => 'required|numeric|min:0',
        ]);

        $data = array_merge(request()->all(), [
            'vendor_id' => $sellerOrder->marketplace_seller_id
        ]);

        if (!$this->isInventoryValidate($data)) {
            session()->flash('error', 'Requested quantity is invalid or not available.');

            return redirect()->back();
        }

        // Update the marketplace_product.qty's
        $this->updateMarketplaceProductInventories($data);

        // inventory->qty is updated here.
        $this->shipment->create(array_merge($data, [
            'order_id' => $orderId
        ]));

        session()->flash('success', 'Shipment created successfully.');

        return redirect()->route($this->_config['redirect'], $orderId);
    }

    /**
     * Update the marketplace.qty values based on marketplace_products and order quantities in order items.
     *
     * @param array $data
     * @return boolean
     */
    public function updateMarketplaceProductInventories(&$data)
    {
        $valid = false;

        foreach ($data['shipment']['items'] as $itemId => $inventorySource) {
            if ($qty = $inventorySource[$data['shipment']['source']]) {

                $orderItem = $this->baseOrderItem->find($itemId);
                $order_qty = $orderItem->qty_ordered;
                $marketplace_product = MarketplaceProduct::findOrFail($orderItem->additional['seller_info']['product_id']);
                $old_qty = $marketplace_product->qty;
                $marketplace_product->qty = $old_qty - $order_qty;
                $marketplace_product->qty;
                $marketplace_product->save();

                $valid = true;
            } else {
                unset($data['shipment']['items'][$itemId]);
            }
        }
        return $valid;
    }
}
