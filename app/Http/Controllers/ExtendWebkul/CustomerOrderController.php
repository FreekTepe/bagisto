<?php

use App\Models\ExtendWebkul\Order;

    namespace App\Http\ExtendWebkul\Controllers;

    use Webkul\Sales\Repositories\OrderRepository;
    use Webkul\Sales\Repositories\InvoiceRepository;
    use Webkul\Marketplace\Repositories\SellerRepository;
    use Webkul\Sales\Models\Order;
    use App\Models\ExtendWebkul\Order as MarketplaceOrder;

    use Webkul\Shop\Http\Controllers\OrderController as OrderBaseController;

    class CustomerOrderController extends OrderBaseController
    {

        protected $sellerRepository;

        /**
         * Create a new controller instance.
         *
         * @param  Webkul\Marketplace\Repositories\SellerRepository $sellerRepository
         * @param  Webkul\Marketplace\Repositories\OrderRepository  $orderRepository
         * @return void
         */
        public function __construct(
            OrderRepository $orderRepository,
            InvoiceRepository $invoiceRepository,
            SellerRepository $sellerRepository
        ) {
            parent::__construct($orderRepository, $invoiceRepository);
            $this->sellerRepository = $sellerRepository;
        }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        themes()->set('vinylheaven-seller-portal-theme');

        $order_ids = Order::where('customer_id', auth()->guard('customer')->user()->id)->get()->pluck('id');
        // dd($order_ids);
        $orders = MarketplaceOrder::whereIn('order_id', $order_ids)->get();
        
        return view($this->_config['view'], compact('orders'));
    }

    /**
     * Show the view for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function view($id)
    {
        themes()->set('vinylheaven-seller-portal-theme');

        $order = MarketplaceOrder::find($id);
        if (!$order) {
            abort(404);
        }

        return view($this->_config['view'], compact('order'));
    }
     

    }