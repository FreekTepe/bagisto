<?php

namespace App\Http\ExtendWebkul\Controllers;

use Webkul\Marketplace\Http\Controllers\Shop\Account\DashboardController as BaseDashboardController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Marketplace\Http\Controllers\Shop\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Marketplace\Repositories\OrderRepository;
use Webkul\Marketplace\Repositories\OrderItemRepository;
use Webkul\Product\Repositories\ProductInventoryRepository;

/**
 * Dashboard controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DashboardController extends BaseDashboardController
{

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository       $sellerRepository
     * @param  Webkul\Marketplace\Repositories\OrderRepository        $orderRepository
     * @param  Webkul\Marketplace\Repositories\OrderItemRepository    $orderItemRepository
     * @param  Webkul\Product\Repositories\ProductInventoryRepository $productInventoryRepository
     * @return void
     * 
     */
    public function __construct(
        SellerRepository $sellerRepository,
        OrderRepository $orderRepository,
        OrderItemRepository $orderItemRepository,
        ProductInventoryRepository $productInventoryRepository
    ) {
        parent::__construct($sellerRepository, $orderRepository, $orderItemRepository, $productInventoryRepository);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        themes()->set('vinylheaven-seller-portal-theme');

        $isSeller = $this->sellerRepository->isSeller(auth()->guard('customer')->user()->id);

        if (!$isSeller) {
            return redirect()->route('marketplace.account.seller.create');
        }

        $this->setStartEndDate();

        $this->seller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $statistics = [
            'total_orders' =>  [
                'previous' => $previous = $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->lastStartDate)
                        ->where('marketplace_orders.created_at', '<=', $this->lastEndDate);
                })->count(),
                'current' => $current = $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->startDate)
                        ->where('marketplace_orders.created_at', '<=', $this->endDate);
                })->count(),
                'progress' => $this->getPercentageChange($previous, $current)
            ],
            'total_sales' =>  [
                'previous' => $previous = $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->lastStartDate)
                        ->where('marketplace_orders.created_at', '<=', $this->lastEndDate);
                })->sum('base_seller_total'),
                'current' => $current = $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->startDate)
                        ->where('marketplace_orders.created_at', '<=', $this->endDate);
                })->sum('base_seller_total') - $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->startDate)
                        ->where('marketplace_orders.created_at', '<=', $this->endDate);
                })->sum('base_grand_total_refunded') + $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->startDate)
                        ->where('marketplace_orders.created_at', '<=', $this->endDate);
                })->sum('base_commission_invoiced'),
                'progress' => $this->getPercentageChange($previous, $current)
            ],
            'avg_sales' =>  [
                'previous' => $previous = $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->lastStartDate)
                        ->where('marketplace_orders.created_at', '<=', $this->lastEndDate);
                })->avg('base_seller_total'),
                'current' => $current = $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->startDate)
                        ->where('marketplace_orders.created_at', '<=', $this->endDate);
                })->avg('base_seller_total') - $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->startDate)
                        ->where('marketplace_orders.created_at', '<=', $this->endDate);
                })->avg('base_grand_total_refunded') + $this->orderRepository->scopeQuery(function ($query) {
                    return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                        ->where('marketplace_orders.created_at', '>=', $this->startDate)
                        ->where('marketplace_orders.created_at', '<=', $this->endDate);
                })->avg('base_commission_invoiced'),
                'progress' => $this->getPercentageChange($previous, $current)
            ],
            'top_selling_products' => $this->getTopSellingProducts(),
            'customer_with_most_sales' => $this->getCustomerWithMostSales(),
            'stock_threshold' => rand(10,100)
        ];

        foreach (core()->getTimeInterval($this->startDate, $this->endDate) as $interval) {
            $statistics['sale_graph']['label'][] = $interval['start']->format('d M');

            $total = $this->orderRepository->scopeQuery(function ($query) use ($interval) {
                return $query->where('marketplace_orders.marketplace_seller_id', $this->seller->id)
                    ->where('marketplace_orders.created_at', '>=', $interval['start'])
                    ->where('marketplace_orders.created_at', '<=', $interval['end']);
            })->sum('base_seller_total');

            $statistics['sale_graph']['total'][] = $total;
            $statistics['sale_graph']['formated_total'][] = core()->formatBasePrice($total);
        }

        return view($this->_config['view'], compact('statistics'))->with(['startDate' => $this->startDate, 'endDate' => $this->endDate]);
    }
}
