<?php

namespace App\Http\ExtendWebkul\Controllers;

use Webkul\Marketplace\Http\Controllers\Shop\Account\SellerController as SellerBaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Marketplace\Http\Controllers\Shop\Controller;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Marketplace\Http\Requests\SellerForm;
use App\Repositories\SellerRepository as CustomSellerRepository;

/**
 * Marketplace seller page controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SellerController extends SellerBaseController
{

    /**
     * SellerRepository object
     *
     * @var array
     */
    protected $custom_seller_repository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository $seller
     * @return void
     */
    public function __construct(SellerRepository $seller_repository, CustomSellerRepository $custom_seller_repository)
    {
        $this->custom_seller_repository = $custom_seller_repository;
        parent::__construct($seller_repository);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\Marketplace\Http\Requests\SellerForm $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $isSeller = $this->custom_seller_repository->isSeller(auth()->guard('customer')->user()->id);
        $data = request()->all();

        if(!isset($data['paypal_allowed'])){ $data['paypal_allowed'] = 0; }
        if(!isset($data['bank_allowed'])){ $data['bank_allowed'] = 0; }
        if(!isset($data['cash_allowed'])){ $data['cash_allowed'] = 0; }
        if(!isset($data['pickup_allowed'])){ $data['pickup_allowed'] = 0; }
        if(!isset($data['shop_enabled'])){ $data['shop_enabled'] = 0; }

        $data['url'] = str_slug($data['shop_title'], "-");

        if (!$isSeller) {
            return redirect()->route('marketplace.account.seller.create');
        }

        // do a file check
        try {
            $logo = $this->custom_seller_repository->checkFormFile($data, "logo");
            $banner = $this->custom_seller_repository->checkFormFile($data, "banner");
        } catch (\Throwable $th) {
            session()->flash('error', $th->getMessage());
            return redirect()->route($this->_config['redirect']);
        }

        // Update profile details
        \DB::beginTransaction();
        try {

            // try seller model update
            $this->custom_seller_repository->update($data, $id);

            // try file updates
            if ($logo) {
                $this->custom_seller_repository->uploadImage($logo, "logo", $id);
            }

            if ($banner) {
                $this->custom_seller_repository->uploadImage($banner, "banner", $id);
            }

            \DB::commit();

            session()->flash('success', 'Your profile was updated successfully.');
            return redirect()->route($this->_config['redirect']);
        } catch (\Throwable $th) {

            \DB::rollBack();
            session()->flash('error', $th->getMessage());
            return redirect()->route($this->_config['redirect']);
        }
    }
}
