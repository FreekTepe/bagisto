<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->validate([
            'seller_id' => 'required',
            'customer_id' => 'required',
            'order_id' => 'required',
            'sender' => 'required|string', // (seller or customer) 
            'message' => 'nullable', // either a message or status_change
            'status_change' => 'nullable'
        ]);

        Message::create($data);

        if($data['sender'] == 'customer'){
            return redirect(route('customer.orders.view', $data['order_id']));
        }

        return redirect(route('marketplace.account.orders.view', $data['order_id']));
    }

}
