<?php

namespace App\Http\Controllers\Api;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\ExtendWebkul\Seller;
use App\Http\Controllers\Controller;

class StatisticsController extends Controller
{

    public function orderprices(Request $request, $seller_id)
    {

        $data = $request->validate([
            'year' => 'string',
            'months' => 'array' 
        ]);


        $seller = Seller::find($seller_id);

        $query = $seller->orders()
                         ->whereYear('created_at', $data['year']);

        if(isset($data['months'])){
            $query->whereIn(\DB::raw('month(created_at)'), $data['months']);
        }
                         
        $orders = $query->get();

        // Group by month
        $grouped_orders = $orders->groupBy(function($d) {
            return Carbon::parse($d->created_at)->format('m');
        });


        $prices = [
            "01" => 0,
            "02" => 0,
            "03" => 0,
            "04" => 0,
            "05" => 0,
            "06" => 0,
            "07" => 0,
            "08" => 0,
            "09" => 0,
            "10" => 0,
            "11" => 0,
            "12" => 0
        ];
        foreach ($grouped_orders as $month => $orders) {
            $prices[$month] = $orders->pluck('base_grand_total')->sum();
        }

        return response()->json(['data' => array_values($prices)], 200);


        // return total order price per month within range
    }
}
