<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateProductsToElastic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vhelastic:migrate-products-to-elastic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the bagsito products to te elastic search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $product_model = app('App\Models\ExtendWebkul\Product');

        $products = $product_model->getProductsForCatalog(0);
        $products_saved = 0;

        foreach ($products as $product_index => $product) {
            $product = $product_model->with(['variants', 'variants.inventories'])->findOrFail($product->product_id);

            if ($product->save()) {
                $products_saved++;
            }
        }

        echo 'Total products found: ' . count($products) . ' Total products saved: ' . $products_saved;
        return 'Total products found: ' . count($products) . ' Total products indexed in the elasticsearch node: ' . $products_saved;
    }
}
