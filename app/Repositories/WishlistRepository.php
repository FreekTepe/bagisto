<?php

namespace App\Repositories;

use Webkul\Customer\Repositories\WishlistRepository as WishlistBaseRepository;

class WishlistRepository extends WishlistBaseRepository
{
    /**
     * get customer wishlist Items.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getCustomerWhishlist($paginate = 8)
    {
        return $this->model->where([
            'channel_id'  => core()->getCurrentChannel()->id,
            'customer_id' => auth()->guard('customer')->user()->id,
        ])->paginate($paginate);
    }
}
