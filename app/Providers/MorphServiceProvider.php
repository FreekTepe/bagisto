<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use VinylHeaven\CustomAttributeType\Models\ArtistGroup;
use VinylHeaven\CustomAttributeType\Models\ArtistSolo;

class MorphServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'group' => ArtistGroup::class,
            'solo' => ArtistSolo::class,
        ]);
    }
}
