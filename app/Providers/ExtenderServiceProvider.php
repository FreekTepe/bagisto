<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class ExtenderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Extend Webkul core models
        // Core Model => Overriding Model
        \Extender::extendCoreModels([
            \Webkul\Product\Models\Product::class => \App\Models\ExtendWebkul\Product::class,
            \Webkul\Product\Models\ProductAttributeValue::class => \App\Models\ExtendWebkul\ProductAttributeValue::class,
            \Webkul\Marketplace\Models\Product::class => \App\Models\ExtendWebkul\MarketplaceProduct::class,
            \Webkul\Customer\Models\Customer::class => \App\Models\ExtendWebkul\Customer::class,
            \Webkul\Marketplace\Models\Seller::class => \App\Models\ExtendWebkul\Seller::class,
            \Webkul\Checkout\Models\Cart::class => \App\Models\ExtendWebkul\Cart::class,
            \Webkul\Sales\Models\Order::class => \App\Models\ExtendWebkul\Order::class,
        ]);

        // divert Webkul routes to custom controller methods
        /**
         * webkul route name => controller@ method()
         */
        $this->app->booted(function () {
            \Extender::divertRoutes([
                'shop.productOrCategory.index' => 'VinylHeaven\Catalog\Http\Controllers\CatalogController@productOrCategory',
                'shop.search.index' => 'App\Http\ExtendWebkul\Controllers\SearchController@index',
                'shop.checkout.onepage.index' => 'VinylHeaven\Checkout\Http\Controllers\OnepageController@index',
                'shop.checkout.save-order' => 'VinylHeaven\Checkout\Http\Controllers\OnepageController@saveOrder',
                'cart.add' => 'VinylHeaven\Checkout\Http\Controllers\CartController@add',
                'customer.wishlist.index' => 'App\Http\ExtendWebkul\Controllers\WishlistController@index',
                'marketplace.account.products.search' => 'App\Http\ExtendWebkul\Controllers\AssignProductController@index',
                'marketplace.account.seller.update' => 'App\Http\ExtendWebkul\Controllers\SellerController@update',
                'marketplace.account.shipments.store' => 'App\Http\ExtendWebkul\Controllers\ShipmentController@store',
                'marketplace.account.dashboard.index' => 'App\Http\ExtendWebkul\Controllers\DashboardController@index',
                'marketplace.sellers.product.index' => 'VinylHeaven\Catalog\Http\Controllers\CatalogController@sellerProduct',
                // 'marketplace.account.orders.view' => 'App\Http\ExtendWebkul\Controllers\OrderController@view',
                // 'customer.orders.view' => 'App\Http\ExtendWebkul\Controllers\CustomerOrderController@view',
                // 'customer.orders.index' => 'App\Http\ExtendWebkul\Controllers\CustomerOrderController@index',
                // 'marketplace.account.products.update' => 'App\Http\ExtendWebkul\Controllers\AssignProductController@update',
            ]);

            // here we can apply middleware to a list bagisto routes
            \Extender::applyMiddleware([
                'marketplace.account.products.edit-assign' => 'set.theme.sellerdashboard',
                'shop.marketplace.tablerate.rates.index' => 'set.theme.sellerdashboard',
                'shop.marketplace.tablerate.super_set_rates.index' => 'set.theme.sellerdashboard',
                'customer.wishlist.index' => 'set.theme.sellerdashboard',
                // 'marketplace.account.products.edit-assign' => 'set.theme.sellerdashboard'
            ]);
        });

        // Here we extend the $attributeTypeFields attribute that gets referenced statically in Webkul\Product\Repositories\ProductAttributeValueRepository
        \Webkul\Product\Models\ProductAttributeValue::$attributeTypeFields = [
            'text'        => 'text_value',
            'textarea'    => 'text_value',
            'price'       => 'float_value',
            'boolean'     => 'boolean_value',
            'select'      => 'integer_value',
            'multiselect' => 'text_value',
            'datetime'    => 'datetime_value',
            'date'        => 'date_value',
            'file'        => 'text_value',
            'image'       => 'text_value',
            'checkbox'    => 'text_value',
            'tracklist'   => 'json_value',
            'artists'     => 'boolean_value',
            'labels'      => 'boolean_value',
            'companies'   => 'json_value',
            'identifiers' => 'json_value',
            'formats'     => 'json_value'
        ];
    }
}
