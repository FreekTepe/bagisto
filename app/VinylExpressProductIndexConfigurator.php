<?php

namespace App;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class VinylExpressProductIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'vinylexpress_product';

    /**
     * @var array
     */
    protected $settings = [];
}
