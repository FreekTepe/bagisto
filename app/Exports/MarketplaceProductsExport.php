<?php

namespace App\Exports;

use App\Models\ExtendWebkul\MarketPlaceProduct;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class MarketplaceProductsExport implements FromView
{

    protected $marketplace_products;

    public function __construct($marketplace_products)
    {
        $this->marketplace_products = $marketplace_products;
    }

    public function view(): View
    {
        return view('vendor.exports.marketplace-products', [
            'marketplace_products' => $this->marketplace_products
        ]);
    }
}