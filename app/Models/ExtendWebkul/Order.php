<?php

namespace App\Models\ExtendWebkul;

use App\Message;
use Webkul\Sales\Models\Order as OrderBaseModel;

class Order extends OrderBaseModel
{
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
