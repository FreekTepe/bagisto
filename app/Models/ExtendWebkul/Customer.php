<?php

namespace App\Models\ExtendWebkul;

// the package Model to override
use Webkul\Customer\Models\Customer as CustomerBaseModel;

class Customer extends CustomerBaseModel
{
    public function seller()
    {
        return $this->hasOne('App\Models\ExtendWebkul\Seller');
    }
}
