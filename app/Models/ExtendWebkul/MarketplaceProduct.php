<?php

namespace App\Models\ExtendWebkul;

use DB;
use Webkul\Marketplace\Models\Product as BaseProduct;

class MarketplaceProduct extends BaseProduct
{
    /**
     * Set the touches attribute for the ScoutElasticSearch driver
     */
    protected $touches = ['product'];

    public function product()
    {
        return $this->belongsTo('App\Models\ExtendWebkul\Product');
    }

    protected $fillable = [
        'condition',
        'description',
        'price',
        'shipping_price',
        'qty',
        'marketplace_seller_id',
        'parent_id',
        'product_id',
        'is_owner',
        'is_approved',
        'private_comments',
        'sleeve_condition',
        'storage_location',
        'width',
        'height',
        'length',
        'weight',
        'listing_id'
    ];

    public function findAllBySeller($seller, $limit = 33)
    {
        $params = request()->input();
        $limit = isset($params['limit']) ? $params['limit'] : $limit;
        $offset = isset($params['page']) ? ($limit * $params['page']) : 0;
        $sort = isset($params['sort']) ? $params['sort'] : 'created_at';
        $order = isset($params['order']) ? $params['order'] : 'asc';

        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) use ($seller, $params, $limit, $offset, $sort, $order) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.*')
                ->addSelect(DB::raw('IF( product_flat.special_price_from IS NOT NULL
                            AND product_flat.special_price_to IS NOT NULL , IF( NOW( ) >= product_flat.special_price_from
                            AND NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , IF( product_flat.special_price_from IS NULL , IF( product_flat.special_price_to IS NULL , IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , IF( NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) ) , IF( product_flat.special_price_to IS NULL , IF( NOW( ) >= product_flat.special_price_from, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , product_flat.price ) ) ) AS price'))
                ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
                ->leftJoin('marketplace_products', 'product_flat.product_id', '=', 'marketplace_products.product_id')
                ->where('product_flat.status', 1)
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->whereNotNull('product_flat.url_key')
                ->where('marketplace_products.marketplace_seller_id', $seller->id)
                ->where('marketplace_products.is_approved', 1);

            $qb->addSelect(DB::raw('(CASE WHEN marketplace_products.is_owner = 0 THEN marketplace_products.price ELSE product_flat.price END) AS price'));

            $queryBuilder = $qb->leftJoin('product_flat as flat_variants', function ($qb) use ($channel, $locale) {
                $qb->on('product_flat.id', '=', 'flat_variants.parent_id')
                    ->where('flat_variants.channel', $channel)
                    ->where('flat_variants.locale', $locale);
            });

            if ($limit && $limit > 0) {
                $qb->offset($offset)
                    ->limit($limit);
            }

            if (isset($params['sort'])) {
                $attribute = $this->attribute->findOneByField('code', $params['sort']);

                if ($params['sort'] == 'price') {
                    $qb->orderBy($attribute->code, $params['order']);
                } else {
                    $qb->orderBy($params['sort'] == 'created_at' ? 'product_flat.created_at' : $attribute->code, $params['order']);
                }
            }

            return $qb->groupBy('product_flat.id');
        })->get();

        return $results;
    }

    public function countAllBySeller($seller)
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) use ($seller) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.*')
                ->addSelect(DB::raw('IF( product_flat.special_price_from IS NOT NULL
                            AND product_flat.special_price_to IS NOT NULL , IF( NOW( ) >= product_flat.special_price_from
                            AND NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , IF( product_flat.special_price_from IS NULL , IF( product_flat.special_price_to IS NULL , IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , IF( NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) ) , IF( product_flat.special_price_to IS NULL , IF( NOW( ) >= product_flat.special_price_from, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , product_flat.price ) ) ) AS price'))
                ->leftJoin('marketplace_products', 'product_flat.product_id', '=', 'marketplace_products.product_id')
                ->where('product_flat.status', 1)
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->whereNotNull('product_flat.url_key')
                ->where('marketplace_products.marketplace_seller_id', $seller->id)
                ->where('marketplace_products.is_approved', 1);

            $qb->addSelect(DB::raw('(CASE WHEN marketplace_products.is_owner = 0 THEN marketplace_products.price ELSE product_flat.price END) AS price'));



            return $qb->groupBy('product_flat.id');
        })->get();

        return count($results);
    }

    /**
     * Returns latests added marketplace/seller products
     *
     * @return Collection
     */
    public function getLatestProducts($limit = 8)
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($limit) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                    ->addSelect('product_flat.*')
                    ->orderBy('created_at', 'desc')
                    ->offset(0)
                    ->limit($limit);
        })->get();

		return $results;
    }

    /**
     * @param integer $qty
     *
     * @return bool
     */
    public function haveSufficientQuantity($qty)
    {
        $total = 0;

        $channelInventorySourceIds = core()->getCurrentChannel()
                ->inventory_sources()
                ->where('status', 1)
                ->pluck('id');

        foreach ($this->product->inventories as $inventory) {
            $index = $channelInventorySourceIds->search($inventory->inventory_source_id);
            
            if (is_numeric($index) && $this->seller->id == $inventory->vendor_id) {
                $total_before = $total;
                $total += $inventory->qty;
            }
            
        }

        if (!$total) {
            return false;
        }

        $orderedInventory = $this->product->ordered_inventories()
                ->where('channel_id', core()->getCurrentChannel()->id)
                ->first();
                
        if ($orderedInventory) {
            $total -= $orderedInventory->qty;
        }
            
        return $qty <= $total ? true : false;
    }

    public function getOneWhere($product_id)
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($product_id) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                    ->addSelect('product_flat.*')
                    ->leftJoin('marketplace_products', 'marketplace_products.product_id', '=', 'product_flat.product_id')
                    ->where('product_flat.product_id', $product_id);
        })->get();

		return isset($results[0]) ? $results[0] : null;
    }

    public function getWidthAttribute()
    {
        return !is_null($this->attributes['width']) ? $this->attributes['width'] : $this->product->width;
    }

    public function getHeightAttribute()
    {
        return !is_null($this->attributes['height']) ? $this->attributes['height'] : $this->product->height;
    }

    public function getLengthAttribute()
    {
        return !is_null($this->attributes['length']) ? $this->attributes['length'] : $this->product->length;
    }

    public function getWeightAttribute()
    {
        return !is_null($this->attributes['weight']) ? $this->attributes['weight'] : $this->product->weight;
    }


    public function getShippingPrice($customer = null)
    {
        $ip = $this->getRealIpAddr();
        return str_replace(',', '.', app('VinylHeaven\Shipping\Repositories\ShippingPriceRepository')->getShippingPrice($this, $ip, $customer));
    }

    private function getRealIpAddr(){
        if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function getNewestProducts($limit = 32)
    {
        return app('App\Models\ExtendWebkul\MarketplaceProduct')
            ->orderBy('created_at', 'desc')
            ->offset(0)
            ->limit($limit)
            ->get();
    }
}