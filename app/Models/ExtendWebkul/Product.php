<?php

namespace App\Models\ExtendWebkul;

// the package Model to override
use Illuminate\Database\Eloquent\Model;
use Webkul\Product\Models\Product as ProductBaseModel;
use Webkul\Product\Repositories\ProductFlatRepository;
use App\Models\ExtendWebkul\MarketplaceProduct;
use VinylHeaven\CustomAttributeType\Models\Artist;
use VinylHeaven\CustomAttributeType\Models\Label;

use App\VinylExpressProductIndexConfigurator;
use ScoutElastic\Searchable;

use App\Models\FetchedImageRelease;
use VinylHeaven\Shipping\Models\Rule;

class Product extends ProductBaseModel
{

    use Searchable;

    protected $indexConfigurator = VinylExpressProductIndexConfigurator::class;

    protected $searchRules = [];

    public function toSearchableArray()
    {
        $product_array = $this->toArray();
        $marketplace_products = [];
        $root_category_id = null;
        $prices = [];
        $artists = [];
        $formats = [];

        if (!empty($this->marketplace_product)) {
            foreach ($this->marketplace_product as $mp_product) {
                $marketplace_products[] = [
                    'id' => $mp_product->id,
                    'price' => $mp_product->price,
                    'condition' => $this->getElasticSearchConditionGrading($mp_product->condition),
                    'sleeve_condition' => $this->getElasticSearchConditionGrading($mp_product->sleeve_condition),
                    'qty' => $mp_product->qty
                ];
                $prices[] = $mp_product->price;
            }
        }

        foreach ($this->categories as $category) {
            if ($category->parent_id == null) {
                $root_category_id = $category->id;
            }
        }

        foreach ($this->artists as $artist) {
            $artists[] = [
                'artist_id' => $artist->artist_id,
                'name' => $artist->name
            ];
        }

        if (!empty($this->formats)) {
            foreach ($this->formats as $format) {
                if (!empty($format['name']) || !empty($format['descriptions'])) {
                    $format_descriptions = [];
                    if (!empty($format['descriptions'])){
                        foreach ($format['descriptions'] as $description) {
                            $format_descriptions[] = [
                                'name' => $description
                            ];
                        }
                    }
                    $formats[] = [
                        'name' => $format['name'],
                        'descriptions' => $format_descriptions
                    ];
                }
            }
        }

        $product_array['root_category_id'] = $root_category_id; 
        $product_array['marketplace_products'] = $marketplace_products;
        $product_array['artists'] = $artists;
        $product_array['formats'] = $formats;

        /**
         * Hier moet nog formats bij
         * In het zoek resultaat de filters aanpassen naar wat er mogelijk is in het resultaat
         * VIN 110 bekijken ivm memory leak
         * 
         */
        
        $product_elastic_array = [
            'id' => $product_array['id'],
            'name' => $product_array['name'],
            'description' => $product_array['description'],
            'price' => !empty($product_array['price']) ? $product_array['price'] : 0.00,
            'min_price' => count($prices) ? min($prices) : 0.00,
            'max_price' => count($prices) ? max($prices) : 0.00,
            'special_price' => !empty($product_array['special_price']) ? $product_array['special_price'] : 0.00,
            'root_category_id' => $product_array['root_category_id'],
            'marketplace_products' => $product_array['marketplace_products'],
            'tracklist' => json_decode($product_array['tracklist']),
            'artists' => $product_array['artists'],
            'formats' => $product_array['formats'],
            'created_at' => $product_array['created_at'],
            'updated_at' => $product_array['updated_at']
        ];
        //dd($this->artists->count(), $this->artists, $this->formats, $product_elastic_array);

        // $product_elastic_array = [
        //     'id' => $product_array['id'],
        //     'name' => $product_array['name'],
        //     'root_category_id' => $product_array['root_category_id'],
        //     'marketplace_products' => $product_array['marketplace_products'],
        //     'tracklist' => json_decode($product_array['tracklist']),
        //     'artists' => $product_array['artists'],
        //     'created_at' => $product_array['created_at'],
        //     'updated_at' => $product_array['updated_at']
        // ];
        
        return $product_elastic_array;
    }

    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'name' => [
                'type' => 'text',
                'fields' => [
                    'sort' => [
                        'type' => 'keyword'
                    ],
                    'autocomplete' => [
                        'type' => 'search_as_you_type'
                    ]
                ],
            ],
            'description' => [
                'type' => 'text'
            ],
            'price' => [
                'type' => 'double'
            ],
            'special_price' => [
                'type' => 'double'
            ],
            'min_price' => [
                'type' => 'double'
            ],
            'max_price' => [
                'type' => 'double'
            ],
            'root_category_id' => [
                'type' => 'integer'
            ],
            'marketplace_products' => [
                'type' => 'nested',
                'properties' => [
                    'price' => [
                        'type' => 'double'
                    ],
                    'condition' => [
                        'type' => 'text'
                    ],
                    'sleeve_condition' => [
                        'type' => 'text'
                    ],
                    'marketplace_seller_id' => [
                        'type' => 'integer'
                    ],
                    'qty' => [
                        'type' => 'integer'
                    ],
                ],
            ],
            'tracklist' => [
                'type' => 'nested',
                'properties' => [
                    "position" => [
                        'type' => 'text'
                    ],
                    "title" => [
                        'type' => 'text'
                    ],
                    "duration" => [
                        'type' => 'text'
                    ],
                    "artists" => [
                        'type' => 'nested',
                        'properties' => [
                            "id" => [
                                'type' => 'integer'
                            ],
                            "name" => [
                                'type' => 'text'
                            ],
                            "role" => [
                                'type' => 'text'
                            ],
                        ],
                    ],
                ],
            ],
            'artists' => [
                'type' => 'nested',
                'properties' => [
                    'artist_id' => [
                        'type' => 'integer'
                    ],
                    'name' => [
                        'type' => 'text',
                        'fields' => [
                            'sort' => [
                                'type' => 'keyword'
                            ],
                            'autocomplete' => [
                                'type' => 'search_as_you_type'
                            ],
                        ],
                    ]
                ]
            ],
            'formats' => [
                'type' => 'nested',
                'properties' => [
                    'name' => [
                        'type' => 'text'
                    ],
                    'descriptions' => [
                        'type' => 'nested',
                        'properties' => [
                            'name' => [
                                'type' => 'text'
                            ]
                        ]
                    ]
                ]
            ],
            'created_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss'
            ],
            'updated_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss'
            ],
        ]
    ];
    

    protected $casts = [
        'tracklist' => 'array',
        'identifiers' => 'array',
        'companies' => 'array',
        'formats' => 'array'
    ];

    protected $fillable = [
        'type',
        'attribute_family_id',
        'sku',
        'parent_id',
    ];

    public function getProductsForCatalog($limit = 33)
    {
        $params = request()->input();
        $limit = isset($params['limit']) ? $params['limit'] : $limit;
        $offset = isset($params['page']) ? ($limit * $params['page']) : 0;
        $sort = isset($params['sort']) ? $params['sort'] : 'created_at';
        $order = isset($params['order']) ? $params['order'] : 'asc';

        $results = app(ProductFlatRepository::class)->scopeQuery(function ($query) use ($limit, $offset, $sort, $order) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.*')
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->where('product_flat.status', 1)
                ->whereNotNull('product_flat.url_key');

            if ($limit && $limit > 0) {
                $qb->offset($offset)
                    ->limit($limit);
            }


            if (isset($sort)) {
                $attribute = app('Webkul\Attribute\Repositories\AttributeRepository')->findOneByField('code', $sort);

                if ($sort == 'price') {
                    if ($attribute->code == 'price') {
                        $qb->orderBy('min_price', $order);
                    } else {
                        $qb->orderBy($attribute->code, $order);
                    }
                } else {
                    $qb->orderBy($sort == 'created_at' ? 'product_flat.created_at' : $attribute->code, $order);
                }
            }

            return $qb->groupBy('product_flat.id');
        })->get();

        return $results;
    }

    public function getTotalProductsAmountForCatalog()
    {
        $params = request()->input();
        $limit = isset($params['limit']) ? $params['limit'] : 33;
        $offset = isset($params['page']) ? ($limit * $params['page']) : 0;
        $sort = isset($params['sort']) ? $params['sort'] : 'created_at';
        $order = isset($params['order']) ? $params['order'] : 'asc';

        $results = app(ProductFlatRepository::class)->scopeQuery(function ($query) use ($limit, $offset, $sort, $order) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.id')
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->where('product_flat.status', 1)
                ->whereNotNull('product_flat.url_key');


            if (isset($sort)) {
                $attribute = app('Webkul\Attribute\Repositories\AttributeRepository')->findOneByField('code', $sort);

                if ($sort == 'price') {
                    if ($attribute->code == 'price') {
                        $qb->orderBy('min_price', $order);
                    } else {
                        $qb->orderBy($attribute->code, $order);
                    }
                } else {
                    $qb->orderBy($sort == 'created_at' ? 'product_flat.created_at' : $attribute->code, $order);
                }
            }

            return $qb->groupBy('product_flat.id');
        })->get();

        return count($results);
    }


    public function rules()
    {
        return $this->belongsToMany(Rule::class);
    }

    public function images()
    {
        return $this->hasMany(FetchedImageRelease::class, 'release_id', 'sku');
    }

    // Relationships fetched_image_releases
    public function labels()
    {
        return $this->belongsToMany(Label::class);
    }

    public function artists()
    {
        return $this->belongsToMany(Artist::class);
    }

    public function marketplace_product()
    {
        return $this->hasMany(MarketplaceProduct::class, 'product_id', 'id')->orderBy('price', 'asc');
    }

    public function marketplaceProducts()
    {
        return $this->marketplace_product;
    }

    public function marketplaceProductsWhere($where, $first = false)
    {
        if ($first) {
            return $this->hasMany(MarketplaceProduct::class, 'product_id', 'id')->where($where)->first();
        } else {
            return $this->hasMany(MarketplaceProduct::class, 'product_id', 'id')->where($where)->get();
        }
    }

    public function getMarketplaceProductsFromPrice()
    {
        if ($this->marketplace_product()->count()) {
            return isset($this->marketplace_product()->get()[0]) ? core()->currency(core()->convertPrice($this->marketplace_product()->get()[0]->price, core()->getBaseCurrencyCode(), 'EUR')) : core()->currency(0.00);
        }
        return core()->currency(0.00);
    }

    public function getElasticSearchConditionGrading($condition)
    {
        return stristr($this->getConditionGrading($condition), '+') ? str_replace('+', 'P', $this->getConditionGrading($condition)) : $this->getConditionGrading($condition);
    }

    public function getConditionGradingBackgroundClass($condition_grading)
    {
        foreach ($this->getAllConditionGradings() as $grading_index => $grading) {
            if ($grading['grading'] == $condition_grading) {
                return $grading['class'];
            }
        }
    }

    public function getSleeveConditionGrading($condition)
    {

        if (stristr($condition, '(M)')) {
            return 'M';
        }
        elseif (stristr($condition, 'Generic')) {
            return 'Generic';
        } elseif (stristr($condition, 'Not Graded')) {
            return 'Not Graded';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'NM';
        } elseif (stristr($condition, 'VG+')) {
            return 'VG+';
        } elseif (stristr($condition, 'VG')) {
            return 'VG';
        } elseif (stristr($condition, 'G+')) {
            return 'G+';
        } elseif (stristr($condition, 'G')) {
            return 'G';
        } elseif (stristr($condition, 'P')) {
            return 'P';
        }
        return '?';
    }

    public function getConditionGrading($condition)
    {
        if (stristr($condition, '(M)')) {
            return 'M';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'NM';
        } elseif (stristr($condition, 'VG+')) {
            return 'VG+';
        } elseif (stristr($condition, 'VG')) {
            return 'VG';
        } elseif (stristr($condition, 'G+')) {
            return 'G+';
        } elseif (stristr($condition, 'G')) {
            return 'G';
        } elseif (stristr($condition, 'P')) {
            return 'P';
        }

        return '?';
    }

    public function getSleeveConditionText($condition)
    {
        if (stristr($condition, '(M)')) {
            return 'Mint';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'Near Mint';
        } elseif (stristr($condition, 'VG+')) {
            return 'Very Good Plus';
        } elseif (stristr($condition, 'VG')) {
            return 'Very Good';
        } elseif (stristr($condition, 'G+')) {
            return 'Good Plus';
        } elseif (stristr($condition, 'G')) {
            return 'Good';
        } elseif (stristr($condition, 'P')) {
            return 'Poor';
        }

        return 'Unknown';
    }

    public function getConditionText($condition)
    {
        if (stristr($condition, '(M)')) {
            return 'Mint';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'Near Mint';
        } elseif (stristr($condition, 'VG+')) {
            return 'Very Good Plus';
        } elseif (stristr($condition, 'VG')) {
            return 'Very Good';
        } elseif (stristr($condition, 'G+')) {
            return 'Good Plus';
        } elseif (stristr($condition, 'G')) {
            return 'Good';
        } elseif (stristr($condition, 'P')) {
            return 'Poor';
        }

        return 'Unknown';
    }

    public function getAllConditionGradings()
    {
        return [
            [
                'grading' => 'M',
                'class' => 'bg-grading-m',
                'label_class' => '',
                'text' => 'Mint'
            ],
            [
                'grading' => 'NM',
                'class' => 'bg-grading-nm',
                'label_class' => '',
                'text' => 'Near Mint'
            ],
            [
                'grading' => 'VG+',
                'class' => 'bg-grading-vgp',
                'label_class' => '',
                'text' => 'Very Good Plus'
            ],
            [
                'grading' => 'VG',
                'class' => 'bg-grading-vg',
                'label_class' => '',
                'text' => 'Very Good'
            ],
            [
                'grading' => 'G+',
                'class' => 'bg-grading-gp',
                'label_class' => '',
                'text' => 'Good Plus'
            ],
            [
                'grading' => 'G',
                'class' => 'bg-grading-g',
                'label_class' => '',
                'text' => 'Good'
            ],
            [
                'grading' => 'P',
                'class' => 'bg-grading-p',
                'label_class' => '',
                'text' => 'Poor'
            ]
        ];
    }

    public function getPriceAttribute()
    {
        $product = app('Webkul\Product\Models\ProductFlat')->where('product_id', $this->id)->first();
        return isset($product->price) ? $product->price : null;
    }

    public function getPrimaryImage()
    {
        if ($image = $this->images()->where('image_type', 'primary')->first()) {
            return $image->Url();
        }
        return $this->getProductImagePlaceholder();
    }

    public function getProductImagePlaceholder()
    {
        return asset('themes/vinylexpress/assets/images/Default-Product-Image.png');
    }

    public function getReleaseYear()
    {
        $release_date_split = explode('-', $this->release_date);

        if (count($release_date_split) > 1) {
            foreach ($release_date_split as $item_index => $item) {
                if (strlen($item) == 4) {
                    return $item;
                }
            }
        } else {
            return isset($release_date_split[0]) ? $release_date_split[0] : $release_date;
        }
    }

    public function hasArtistName()
    {
        return isset($this->artists[0]) && !empty($this->artists[0]['name']);
    }

    public function getArtistName()
    {
        return isset($this->artists[0]['name']) ? $this->artists[0]['name'] : '';
    }

    public function hasFormatName()
    {
        return isset($this->formats[0]) && !empty($this->formats[0]['name']);
    }

    public function getFormatName()
    {
        return isset($this->formats[0]) ? $this->formats[0]['name'] : false;
    }

    public function hasCompanyName()
    {
        return isset($this->companies[0]) && !empty($this->companies[0]['name']);
    }

    public function getCompanyName()
    {
        return isset($this->companies[0]) ? $this->companies[0]['name'] : false;
    }

    public function hasGenre()
    {
        return isset($this->categories[0]) && !empty($this->categories[0]['name']);
    }

    public function getGenre()
    {
        return isset($this->categories[0]) ? $this->categories[0]['name'] : false;
    }

    public function getWidthAttribute()
    {
        return count($this->rules) > 0 ? $this->rules()->first()->width : null;
    }

    public function getHeightAttribute()
    {
        return count($this->rules) > 0 ? $this->rules()->first()->height : null;
    }

    public function getLengthAttribute()
    {
        return count($this->rules) > 0 ? $this->rules()->first()->length : null;
    }

    public function getWeightAttribute()
    {
        return count($this->rules) > 0 ? $this->rules()->first()->weight : null;
    }

    public function formatsFormated()
    {
        $formats_formated = [];

        foreach ($this->formats as $format) {
            $format_string = '';

            if (!empty($format['qty'])) {
                $format_string .= $format['qty'] . ' ';
            }

            $format_string .= $format['name'];

            if (!empty($format['descriptions'])) {
                foreach ($format['descriptions'] as $description) {
                    $format_string .= ' ' . $description;
                }
            }

            $formats_formated[] = $format_string;
        }
        return $formats_formated;
    }
}
