<?php

namespace App\Models\ExtendWebkul;

// the package Model to override
use Webkul\Product\Models\ProductAttributeValue as ProductAttributeValueBaseModel;

class ProductAttributeValue extends ProductAttributeValueBaseModel
{

    /**
     * Set the touches attribute for the ScoutElasticSearch driver
     */
    protected $touches = ['product'];

    public function product()
    {
        return $this->belongsTo('App\Models\ExtendWebkul\Product');
    }
}
