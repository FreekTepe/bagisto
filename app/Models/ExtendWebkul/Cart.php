<?php

    namespace App\Models\ExtendWebkul;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\Event;
    use Webkul\Tax\Helpers\Tax;
    use Webkul\Checkout\Models\Cart as CartBaseModel;
    use Webkul\Customer\Repositories\CustomerAddressRepository;
    use Webkul\Checkout\Repositories\CartAddressRepository;
    use Webkul\Customer\Models\CustomerAddress;
    use Webkul\Checkout\Models\CartAddress;
    use Webkul\Checkout\Models\CartShippingRateProxy;
    use Webkul\Checkout\Models\CartAddressProxy;
    use Webkul\Checkout\Models\CartItemProxy;
    

    use App\Models\ExtendWebkul\Seller;

    class Cart extends CartBaseModel
    {
        /**
         * Get the relevant items
         * if the cart instance is a bagisto cart then return all of the relevant items of the bagisto cart
         * if the cart is a seller cart return the items that are relevant items of the seller cart instance
         * 
         * @return \Illuminate\Database\Eloquent\Collection
         */
        public function items() {
            if ($this->isSellerCart()) {
                //dd(__FILE__,  app('Webkul\Checkout\Repositories\CartItemRepository')->where([['cart_id', '=', $this->vinylheaven_cart_parent_id], ['seller_id', '=', $this->seller->id]])->get());
                return $this->hasMany(CartItemProxy::modelClass(), 'cart_id', 'vinylheaven_cart_parent_id')->whereNull('parent_id')->where([['seller_id', '=', $this->seller->id]]);
            }

            return $this->hasMany(CartItemProxy::modelClass())->whereNull('parent_id');
        }

        /**
         * Check if the cart has cart items
         * 
         * @return Boolean
         */
        public function hasItems()
        {
            return ($this->items()->count()) ? true : false;
        }

        /**
         * Gets all of the seller carts that belongs to the cart
         * 
         * @return \Illuminate\Database\Eloquent\Collection
         */
        public function seller_carts()
        {
            return $this->hasMany(self::class, 'vinylheaven_cart_parent_id', 'id')->where(['is_active' => 1]);
        }

        /**
         * Checks if the cart has seller carts
         * 
         * @return Boolean
         */
        public function hasSellerCarts()
        {
            return ($this->seller_carts()->get()->count()) ? true : false;
        }

        /**
         * pritified function of model function seller_carts
         * 
         * @return \Illuminate\Database\Eloquent\Collection
         */
        public function sellerCarts()
        {
            return $this->seller_carts()->get();
        }

        /**
         * Check if there are cart items that belong to this cart
         * if there are items there needs to be an seller cart created that has the seller id that is set in the additional info of the item
         * if the seller cart is not created return true
         * if all of the seller cart(s) are created return false
         * 
         * @return Boolean
         */
        public function needsSellerCartsCreated()
        {
            $needs_seller_carts_created = false;
            foreach ($this->items()->get() as $item) {
                if ($item->seller_id == 0) {
                    $item->seller_id = $item->additional['seller_info']['seller_id'];
                    $item->save();
                }

                if (!$this->hasSellerCart($item->additional['seller_info']['seller_id'])) {
                    $needs_seller_carts_created = true;
                }
            }

            return $needs_seller_carts_created;
        }

        /**
         * We need to create seller cart(s) loop thru the cart items and check if we have the seller cart
         * if not create that specific seller cart
         */
        public function createSellerCarts()
        {
            foreach ($this->items()->get() as $item) {
                if (!$this->hasSellerCart($item->additional['seller_info']['seller_id'])) {
                    $this->createSellerCart($item->additional['seller_info']['seller_id']);
                }
            }
        }

        /**
         * Check if the cart is an seller cart and not an bagsito cart
         */
        public function isSellerCart()
        {
            return ($this->seller_id != 0 && $this->is_vinylheaven_cart == 1 && $this->vinylheaven_cart_parent_id != 0) ? true : false;
        }

        /**
         * Check if the cart has the specific seller cart
         * 
         * @param int | $seller_id
         * 
         * @return Boolean
         */
        public function hasSellerCart($seller_id)
        {
            if (empty($seller_id)) {
                return false;
            }

            return ($this->seller_carts()->where(['seller_id' => $seller_id, 'is_active' => 1, 'vinylheaven_cart_parent_id' => $this->id])->first()) ? true : false;
        }

        /**
         * Gets the specific requested seller cart
         * 
         * @param int | $seller_id
         * 
         * @return Boolean
         */
        public function getSellerCart($seller_id)
        {
            if (empty($seller_id)) {
                return;
            }

            return $this->seller_carts()->where(['seller_id' => $seller_id, 'is_active' => 1, 'vinylheaven_cart_parent_id' => $this->id])->first();
        }

        /**
         * Create an seller cart
         */
        public function createSellerCart($seller_id)
        {
            $cartRepository = app('Webkul\Checkout\Repositories\CartRepository');

            $cart_data = [
                'is_active' => 1,
                'vinylheaven_cart_parent_id' => $this->id,
                'is_vinylheaven_cart' => 1,
                'seller_id' => $seller_id,
                'channel_id' => core()->getCurrentChannel()->id,
                'global_currency_code' => core()->getBaseCurrencyCode(),
                'base_currency_code' => core()->getBaseCurrencyCode(),
                'channel_currency_code' => core()->getChannelBaseCurrencyCode(),
                'cart_currency_code' => core()->getCurrentCurrencyCode(),
                'items_count' => 1,
            ];

            $sellerCart = $cartRepository->create($cart_data);
        }

        /**
         * Get the seller that belongs to this cart
         * 
         * @return \App\Models\ExtendWebkul\Seller
         */
        public function seller()
        {
            return $this->hasOne(Seller::class, 'id', 'seller_id');
        }

        /**
         * Calculated the totals of the cart instance and store to the database
         */
        public function collectTotals()
        {
            $this->validateItems();
            $this->calculateItemsTax();

            $this->grand_total = $this->base_grand_total = 0;
            $this->sub_total = $this->base_sub_total = 0;
            $this->tax_total = $this->base_tax_total = 0;
            $this->discount_amount = $this->base_discount_amount = 0;

            foreach ($this->items()->get() as $item) {
                $this->discount_amount += $item->discount_amount;
                $this->base_discount_amount += $item->base_discount_amount;

                $this->sub_total = (float)$this->sub_total + $item->total;
                $this->base_sub_total = (float)$this->base_sub_total + $item->base_total;
            }

            $this->tax_total = Tax::getTaxTotal($this, false);
            $this->base_tax_total = Tax::getTaxTotal($this, true);

            if (is_float($this->base_tax_total)) {
                if ($this->base_tax_total > 0) {
                    $this->base_tax_total = (string) $this->base_tax_total;
                } else {
                    $this->base_tax_total = '0.0000';
                }
            }

            $this->grand_total = $this->sub_total + $this->tax_total - $this->discount_amount;
            $this->base_grand_total = $this->base_sub_total + $this->base_tax_total - $this->base_discount_amount;

            if ($shipping = $this->selected_shipping_rate) {
                $this->grand_total = (float)$this->grand_total + $shipping->price - $shipping->discount_amount;
                $this->base_grand_total = (float)$this->base_grand_total + $shipping->base_price - $shipping->base_discount_amount;

                $this->discount_amount += $shipping->discount_amount;
                $this->base_discount_amount += $shipping->base_discount_amount;
            }

            $quantities = 0;

            foreach ($this->items()->get() as $item) {
                $quantities = $quantities + $item->quantity;
            }

            $this->items_count = $this->items->count();
            $this->items_qty = $quantities;
            $this->save();
        }

        /**
         * To validate if the product information is changed by admin and the items have been added to the cart before it.
         *
         * @return bool
         */
        public function validateItems()
        {
            $cartItemRepository = app('Webkul\Checkout\Repositories\CartItemRepository');

            foreach ($this->items()->get() as $item) {
                $response = $item->product->getTypeInstance()->validateCartItem($item);

                if ($response) {
                    return;
                }

                $price = ! is_null($item->custom_price) ? $item->custom_price : $item->base_price;

                $cartItemRepository->update([
                    'price'      => core()->convertPrice($price),
                    'base_price' => $price,
                    'total'      => core()->convertPrice($price * $item->quantity),
                    'base_total' => $price * $item->quantity,
                ], $item->id);
            }

            return true;
        }

        /**
         * Calculates cart items tax
         *
         * @return void
         */
        public function calculateItemsTax(): void
        {
            foreach ($this->items()->get() as $item) {
                $taxCategory = app('Webkul\Tax\Repositories\TaxCategoryRepository')->find($item->product->tax_category_id);

                if (! $taxCategory) {
                    continue;
                }
                
                if ($item->product->getTypeInstance()->isStockable()) {
                    $address = $this->shipping_address;
                } else {
                    $address = $this->billing_address;
                }

                if ($address === null && auth()->guard('customer')->check()) {
                    $address = auth()->guard('customer')->user()->addresses()
                        ->where('default_address', 1)->first();
                }

                if ($address === null) {
                    $address = new class() {
                        public $country;

                        public $postcode;

                        function __construct()
                        {
                            $this->country = strtoupper(config('app.default_country'));
                        }
                    };
                }

                $taxRates = $taxCategory->tax_rates()->where([
                    'country' => $address->country,
                ])->orderBy('tax_rate', 'desc')->get();

                $item = $this->setItemTaxToZero($item);

                if ($taxRates->count()) {
                    foreach ($taxRates as $rate) {
                        $haveTaxRate = false;

                        if ($rate->state != '' && $rate->state != $address->state) {
                            continue;
                        }

                        if (! $rate->is_zip) {
                            if ($rate->zip_code == '*' || $rate->zip_code == $address->postcode) {
                                $haveTaxRate = true;
                            }
                        } else {
                            if ($address->postcode >= $rate->zip_from && $address->postcode <= $rate->zip_to) {
                                $haveTaxRate = true;
                            }
                        }

                        if ($haveTaxRate) {
                            $item->tax_percent = $rate->tax_rate;
                            $item->tax_amount = ($item->total * $rate->tax_rate) / 100;
                            $item->base_tax_amount = ($item->base_total * $rate->tax_rate) / 100;

                            break;
                        }
                    }
                }

                $item->save();
            }
        }

        /**
         * Set Item tax to zero.
         *
         * @param  \Webkul\Checkout\Contracts\CartItem  $item
         * @return \Webkul\Checkout\Contracts\CartItem
         */
        protected function setItemTaxToZero(CartItem $item): CartItem
        {
            $item->tax_percent = 0;
            $item->tax_amount = 0;
            $item->base_tax_amount = 0;

            return $item;
        }

        public function itemsQty()
        {
            return round($this->items_qty);
        }
    }