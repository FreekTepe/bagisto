<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ExtendWebkul\Product;

use Aws\S3\S3Client;

class FetchedImageRelease extends Model
{
    protected $table = 'fetched_image_releases';

    public function product()
    {
        return $this->belongsTo(Product::class, 'sku', 'release_id');
    }

    public function Url()
    {
        if ($this->downloaded == 't') {
            return 'https://release-images.eu-central-1.linodeobjects.com/' . $this->file_name;
        } else {
            return $this->addToObjectStore();
        }
    }

    public function addToObjectStore()
    {
        $object_store_config = [
            'region' => config('objectstore.region'),
            'version' => config('objectstore.version'),
            'endpoint' => config('objectstore.auth.url'),
            'credentials' => new \Aws\Credentials\Credentials(
                config('objectstore.access.key'),
                config('objectstore.secret.key')
            ),
        ];

        $object_store = new S3Client($object_store_config);

        $result = $object_store->putObject([
            'Bucket' => 'release-images',
            'ACL' => 'public-read',
            'Key' => $this->file_name,
            'SourceFile' =>  $this->getImageFromDiscogs() 
        ]);
        
        $this->downloaded = 't';
        $this->save();

        //dd('RESULT FOR ADDING IMAGE TO OBJECT STORE', $result, $this->file_name, $this);
        return $result['ObjectURL'];
    }

    public function getImageFromDiscogs()
    {
        $image_file = storage_path('app/' . $this->file_name);
        $file_open = fopen($image_file, 'w+');
        $image_curl_handler = curl_init($this->url);
        //curl_setopt($image_curl_handler, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time
        curl_setopt($image_curl_handler, CURLOPT_FILE, $file_open);
        curl_setopt($image_curl_handler, CURLOPT_USERAGENT, $this->getUseragent());

        $curl_execute = curl_exec($image_curl_handler);

        curl_close($image_curl_handler);
        fclose($file_open);

        return $image_file; 
    }

    public function getUseragent()
    {
        $useragents = [
            "Mozilla/5.0 (iPad; CPU iPhone OS 12_1_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.2 Safari/605.1.15",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0",
        ];

        return $useragents[rand(0, 3)];
    }
}