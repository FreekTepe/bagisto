<?php

namespace App;

use App\Models\ExtendWebkul\Seller;
use App\Models\ExtendWebkul\Customer;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    public static $PAYED = 100;
    public static $PAYMENT_CANCLED = 101;

    public static $INVOICED = 200;
    public static $INVOICE_CANCLED = 201;

    public static $SHIPPED = 300;
    public static $SHIPMENT_CANCLED = 301;

    protected $fillable = ["seller_id", "customer_id","order_id","sender","message","status_change"];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function render()
    {

        if(is_null($this->message) && !is_null($this->status)){
            // render a status update
            return 
            "<div class='chat-box'>
                status blabla
            </div>
            <small class='opacity-6'>
                <i class='fa fa-calendar-alt mr-1'></i>".
                \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans()
            ."</small>";
        }elseif (!is_null($this->message) && is_null($this->status)) {
            // render a message
            return 
            "<div class='chat-box'>
                $this->message
            </div>
            <small class='opacity-6'>
                <i class='fa fa-calendar-alt mr-1'></i>".
                \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans()
            ."</small>";
        }

        // return 
        // '<div class="chat-box">
        //     But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system.
        // </div>
        // <small class="opacity-6">
        //     <i class="fa fa-calendar-alt mr-1"></i>
        //     11:01 AM | Yesterday
        // </small>';

    }

}
