<?php

namespace App\DataGrids;

use DB;
use Webkul\Ui\DataGrid\DataGrid;
use App\Repositories\SellerRepository;

/**
 * Product Data Grid class
 *
 * @author Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DiscogsCsvFilesDataGrid extends DataGrid
{
    /**
     * @var integer
     */
    protected $index = 'id';

    /**
     * @var string
     */
    protected $sortOrder = 'desc'; //asc or desc

    /**
     * SellerRepository object
     *
     * @var Object
     */
    protected $sellerRepository;

    /**
     * paginate the collection or not
     *
     * @var bool
     */
    protected $paginate = true;

    /**
     * If paginated then value of pagination.
     *
     * @var int
     */
    protected $itemsPerPage = 10;

    /**
     * Create a new repository instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository $sellerRepository
     * @return void
     */
    public function __construct(SellerRepository $sellerRepository)
    {
        parent::__construct();

        $this->sellerRepository = $sellerRepository;
    }

    public function prepareQueryBuilder()
    {
        $seller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $queryBuilder =  DB::table('media')
            ->where('collection_name', 'dicogscsv')
            ->where('model_id', $seller->id)
            ->distinct();

        $this->addFilter('id', 'id');
        $this->addFilter('file_name', 'file_name');
        $this->addFilter('created_at', 'created_at');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => 'id',
            'type' => 'number',
            'searchable' => false,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'file_name',
            'label' => 'File name',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Created at',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);
    }

    public function prepareActions()
    {
//         $this->addAction([
//             'type' => 'Show',
//             'method' => 'GET',
//             'route' => 'discogs-csv-import.custom_csv_matches.single_file',
//             'icon' => 'icon eye-lg-icon'
//         ]);

         $this->addAction([
             'type' => 'Delete',
             'method' => 'GET',
             'route' => 'discogs-csv-import.delete',
             'icon' => 'icon trash-icon'
         ]);
    }
}
