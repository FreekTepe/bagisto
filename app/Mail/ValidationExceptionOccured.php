<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer;
use Symfony\Component\ErrorHandler\Exception\FlattenException;


class ValidationExceptionOccured extends Mailable
{
    use Queueable, SerializesModels;

    protected $exception;
    public $css;
    public $content;
    public $validation_errors;
    public $request_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($exception, $request_data)
    {
        $this->exception = $exception;
        $this->request_data = $request_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->validation_errors = $this->exception->errors();
        $e = FlattenException::create($this->exception);

        $handler = new HtmlErrorRenderer(true); // boolean, true raises debug flag...
        $this->css = $handler->getStylesheet();
        $this->content = $handler->getBody($e);

        return $this->view('emails.exception')
            ->from('info@vinylheaven.com', 'Vinyl Heaven')
            ->subject('Exception: ' . \Request::fullUrl())
            ->replyTo('info@vinylheaven.com', 'Vinyl Heaven');
    }
}
