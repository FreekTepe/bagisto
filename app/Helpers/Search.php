<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Search 
{
    public function getTotalPaginationPages($products_count)
    {
        return ceil(($products_count / $this->getLimit()));
    }

    public function getLimit()
    {
        $params = request()->input();

        return isset($params['limit']) ? $params['limit'] : 33;
    }

    public function getCurrentPage()
    {
        $params = request()->input();

        return isset($params['page']) ? $params['page'] : 1;
    }

    public function getPageRange()
    {
        return [
            'start' => $this->getCurrentPage(),
            'max' => ($this->getCurrentPage() + 10)
        ];
    }

    public function getPaginationUrl($page_number)
    {
        return request()->fullUrlWithQuery([
            'page'  => $page_number,
        ]);
    }

    public function getPaginationNextPageUrl($products_count)
    {
        if ($this->getCurrentPage() == $products_count) {
            return '#';
        } elseif ($this->getTotalPaginationPages($products_count) == 1) { 
            return '#';
        } else {
            return request()->fullUrlWithQuery([
                'page' => ($this->getCurrentPage() + 1)
            ]);
        }
    }

    public function getPaginationPreviousPageUrl()
    {
        if ($this->getCurrentPage() == 1) {
            return '#';
        } else {
            return request()->fullUrlWithQuery([
                'page' => ($this->getCurrentPage() - 1)
            ]);
        }
    }

    public function getFromPrice($marketplace_products)
    {
        if (count($marketplace_products)) {
            return isset($marketplace_products[0]) ? core()->currency($marketplace_products[0]['price']) : core()->currency(0.00);
        }
        
        return core()->currency(0.00);
    }

    public function getFilteredByElements()
    {
        $params = request()->input();
        $filtered_by_elements = [];

        if (isset($params['price_min'])) {
            $filtered_by_elements['price_min'] = [
                'label' => 'price min',
                'value' => core()->currency($params['price_min'])
            ];
        }

        if (isset($params['price_max'])) {
            $filtered_by_elements['price_max'] = [
                'label' => 'price max',
                'value' => core()->currency($params['price_max'])
            ];
        }

        if (isset($params['condition'])) {
            $filtered_by_elements['condition'] = [
                'label' => 'condition',
                'value' => $params['condition']
            ];
        }

        if (isset($params['sleeve_condition'])) {
            $filtered_by_elements['sleeve_condition'] = [
                'label' => 'sleeve condition',
                'value' => $params['sleeve_condition']
            ];
        }

        if (isset($params['genre'])) {
            $filtered_by_elements['genre'] = [
                'label' => 'genre',
                'value' => $this->getGenreNames()
            ];
        }

        if (isset($params['format'])) {
            $filtered_by_elements['format'] = [
                'label' => 'format',
                'value' => $params['format']
            ];
        }

        return $filtered_by_elements;
    }

    public function getGenreNames()
    {
        $names = '';

        if (request()->filled('genre')) {
            $genres_query = $this->sanitizeUrlString(request()->get('genre'));
            $genres = $this->getGenres();

            foreach ($genres_query as $genre_index => $genre) {
                if (!$genre_index) {
                    $names = $genres[$genre]->name;
                } else {
                    $names .= ', ' . $genres[$genre]->name;
                }
            }
        }

        return $names;
    }

    public function getFilteredByClearAllUrl()
    {
        $term = '';
        if (request()->has('term')) {
            return request()->url() . '?term=' . request()->get('term');
        } else {
            return request()->url(); 
        }
        
    }

    public function hasCondition()
    {
        return request()->has('condition');
    }

    public function getConditionContainerClass()
    {
        if (request()->has('condition')) {
            return '';
        } else {
            return 'hide';
        }
    }

    public function getSleeveConditionContainerClass()
    {
        if (request()->has('sleeve_condition')) {
            return '';
        } else {
            return 'hide';
        }
    }

    public function getConditionContainerDisplay()
    {
        if (request()->has('condition')) {
            return '';
        } else {
            return "display: none;";
        }
    }

    public function getSleeveConditionContainerDisplay()
    {
        if (request()->has('sleeve_condition')) {
            return '';
        } else {
            return "display: none;";
        }
    }

    public function getConditionFilters()
    {
        $params = request()->input();
        $grading_filters = $this->getAllConditionGradings();
        
        if (isset($params['condition'])) {
            $conditions = $this->sanitizeUrlString($params['condition']);
            foreach ($conditions as $condition_index => $condition) {
                $conditions[$condition_index] = $this->getElasticSearchConditionGrading($condition);
            }
            
            foreach ($grading_filters as $grading_index => $grading) {
                if (in_array($this->getElasticSearchConditionGrading($grading['grading']), $conditions)) {
                    $grading_filters[$grading_index]['label_class'] = 'active';
                }
            }
        }
        //dd($conditions, $grading_filters);
        return $grading_filters;
    }

    public function getSleeveConditionFilters()
    {
        $params = request()->input();
        $grading_filters = $this->getAllConditionGradings();
        
        if (isset($params['sleeve_condition'])) {
            $conditions = $this->sanitizeUrlString($params['sleeve_condition']);
            foreach ($conditions as $condition_index => $condition) {
                $conditions[$condition_index] = $this->getElasticSearchConditionGrading($condition);
            }
            
            foreach ($grading_filters as $grading_index => $grading) {
                if (in_array($this->getElasticSearchConditionGrading($grading['grading']), $conditions)) {
                    $grading_filters[$grading_index]['label_class'] = 'active';
                }
            }
        }
        //dd($conditions, $grading_filters);
        return $grading_filters;
    }

    public function getElasticSearchConditionGrading($condition)
    {
        return stristr($this->getConditionGrading($condition), '+') ? str_replace('+', 'P', $this->getConditionGrading($condition)) : $this->getConditionGrading($condition);
    }

    public function getConditionGrading($condition)
    {
        if (stristr($condition, 'M') && !stristr($condition, 'NM') && !stristr($condition, 'M-')) {
            return 'M';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'NM';
        } elseif (stristr($condition, 'VG+')) {
            return 'VG+';
        } elseif (stristr($condition, 'VG')) {
            return 'VG';
        } elseif (stristr($condition, 'G+')) {
            return 'G+';
        } elseif (stristr($condition, 'G')) {
            return 'G';
        } elseif (stristr($condition, 'P')) {
            return 'P';
        }

        return '?';
    }

    public function getConditionGradingBackgroundClass($condition_grading)
    {
        foreach ($this->getAllConditionGradings() as $grading_index => $grading) {
            if ($grading['grading'] == $condition_grading) {
                return $grading['class'];
            }
        }
    }

    public function getAllConditionGradings()
    {
        return [
            [
                'grading' => 'M',
                'class' => 'bg-grading-m',
                'label_class' => '',
                'text' => 'Mint'
            ],
            [
                'grading' => 'NM',
                'class' => 'bg-grading-nm',
                'label_class' => '',
                'text' => 'Near Mint'
            ],
            [
                'grading' => 'VG+',
                'class' => 'bg-grading-vgp',
                'label_class' => '',
                'text' => 'Very Good Plus'
            ],
            [
                'grading' => 'VG',
                'class' => 'bg-grading-vg',
                'label_class' => '',
                'text' => 'Very Good'
            ],
            [
                'grading' => 'G+',
                'class' => 'bg-grading-gp',
                'label_class' => '',
                'text' => 'Good Plus'
            ],
            [
                'grading' => 'G',
                'class' => 'bg-grading-g',
                'label_class' => '',
                'text' => 'Good'
            ],
            [
                'grading' => 'P',
                'class' => 'bg-grading-p',
                'label_class' => '',
                'text' => 'Poor'
            ]
        ];
    }

    public function getConditionUrl($condition)
    {
        $params = request()->input();
        if (isset($params['condition'])) {
            $conditions = $this->sanitizeUrlString($params['condition']);
            if (in_array($condition, $conditions)) {
                $condition_query = implode(',', array_filter($conditions, function($value, $key) use ($condition) {
                    return $this->getElasticSearchConditionGrading($value) != $this->getElasticSearchConditionGrading($condition);
                }, ARRAY_FILTER_USE_BOTH));

                if (!empty($condition_query)) {
                    return request()->fullUrlWithQuery([
                        'condition'  => $condition_query,
                    ]);
                } else {
                    return $this->getUrlWithOutCondition();
                }
            } else {
                return request()->fullUrlWithQuery([
                    'condition'  => $params['condition'] . ',' . $condition,
                ]);
            }
        } else {
            return request()->fullUrlWithQuery([
                'condition'  => $condition,
            ]);
        }
    }

    public function getSleeveConditionUrl($condition)
    {
        $params = request()->input();
        if (isset($params['sleeve_condition'])) {
            $conditions = $this->sanitizeUrlString($params['sleeve_condition']);
            if (in_array($condition, $conditions)) {
                $condition_query = implode(',', array_filter($conditions, function($value, $key) use ($condition) {
                    return $this->getElasticSearchConditionGrading($value) != $this->getElasticSearchConditionGrading($condition);
                }, ARRAY_FILTER_USE_BOTH));

                if (!empty($condition_query)) {
                    return request()->fullUrlWithQuery([
                        'sleeve_condition'  => $condition_query,
                    ]);
                } else {
                    return $this->getUrlWithOutSleeveCondition();
                }
            } else {
                return request()->fullUrlWithQuery([
                    'sleeve_condition'  => $params['sleeve_condition'] . ',' . $condition,
                ]);
            }
        } else {
            return request()->fullUrlWithQuery([
                'sleeve_condition'  => $condition,
            ]);
        }
    }

    public function getGenreUrl($genre)
    {
        $params = request()->input();
        if (request()->filled('genre')) {
            $genres = $this->sanitizeUrlString(request()->get('genre'));
            $genre_query = request()->get('genre') . ',' . $genre;
            if (in_array($genre, $genres)) {
                $genre_query = implode(',', array_filter($genres, function($value, $key) use ($genre) {
                    return $value != $genre;
                }, ARRAY_FILTER_USE_BOTH));
            }
            
            if (!empty($genre_query)) {
                return request()->fullUrlWithQuery([
                    'genre'  => $genre_query,
                ]);
            } else {
                return $this->getUrlWithOutGenre();
            }

            
        } else {
            return request()->fullUrlWithQuery([
                'genre' => $genre
            ]);
        }
    }

    public function getGenreContainerClass()
    {
        if (request()->has('genre')) {
            return '';
        } else {
            return 'hide';
        }
    }

    public function getGenreContainerDisplay()
    {
        if (request()->has('genre')) {
            return '';
        } else {
            return "display: none;";
        }
    }

    public function getUrlWithOutFilter($label)
    {
        switch ($label) {
            case 'condition':
                return $this->getUrlWithOutCondition();
                break;
            case 'sleeve condition':
                return $this->getUrlWithOutSleeveCondition();
                break;
            case 'price min':
                return $this->getUrlWithOutPriceMin();
                break;
            case 'price max':
                return $this->getUrlWithOutPriceMax();
                break;
            case 'genre':
                return $this->getUrlWithOutGenre();
                break;
            case 'format':
                return $this->getUrlWithOutFormat();
                break;
        }
    }

    public function getUrlWithOutCondition()
    {
        $i = 1;
        $return_url = request()->url() . '?';
        foreach (request()->except('condition') as $param_key => $param_value) {
            if ($i == 1) {
                $return_url .= $param_key . '=' . urlencode($param_value);
            } else {
                $return_url .= '&' . $param_key . '=' . urlencode($param_value);
            }
            $i++;
        }
        return $return_url;
    }

    public function getUrlWithOutSleeveCondition()
    {
        $i = 1;
        $return_url = request()->url() . '?';
        foreach (request()->except('sleeve_condition') as $param_key => $param_value) {
            if ($i == 1) {
                $return_url .= $param_key . '=' . urlencode($param_value);
            } else {
                $return_url .= '&' . $param_key . '=' . urlencode($param_value);
            }
            $i++;
        }
        return $return_url;
    }

    public function getMinPriceUrl()
    {
        return $this->getUrlWithOutPriceMaxAndPriceMin() . '&price_min='. urlencode('#price_min');
    }

    public function getUrlWithOutPriceMin()
    {
        $i = 1;
        $return_url = request()->url() . '?';
        foreach (request()->except('price_min') as $param_key => $param_value) {
            if ($i == 1) {
                $return_url .= $param_key . '=' . urlencode($param_value);
            } else {
                $return_url .= '&' . $param_key . '=' . urlencode($param_value);
            }
            $i++;
        }
        return $return_url;
    }

    public function getMaxPriceUrl()
    {
        return $this->getUrlWithOutPriceMaxAndPriceMin() . '&price_max='. urlencode('#price_max');
    }

    public function getUrlWithOutPriceMax()
    {
        $i = 1;
        $return_url = request()->url() . '?';
        foreach (request()->except('price_max') as $param_key => $param_value) {
            if ($i == 1) {
                $return_url .= $param_key . '=' . urlencode($param_value);
            } else {
                $return_url .= '&' . $param_key . '=' . urlencode($param_value);
            }
            $i++;
        }
        return $return_url;
    }

    public function getUrlWithOutPriceMaxAndPriceMin()
    {
        $i = 1;
        $return_url = request()->url() . '?';
        $params = request()->input();

        unset($params['price_min']);
        unset($params['price_max']);

        foreach ($params as $param_key => $param_value) {
            if ($i == 1) {
                $return_url .= $param_key . '=' . urlencode($param_value);
            } else {
                $return_url .= '&' . $param_key . '=' . urlencode($param_value);
            }
            $i++;
        }
        //dd(request()->input(), $params, $return_url);
        return $return_url;
    }

    public function getUrlWithOutGenre()
    {
        $i = 1;
        $return_url = request()->url() . '?';
        foreach (request()->except('genre') as $param_key => $param_value) {
            if ($i == 1) {
                $return_url .= $param_key . '=' . urlencode($param_value);
            } else {
                $return_url .= '&' . $param_key . '=' . urlencode($param_value);
            }
            $i++;
        }
        return $return_url;
    }

    public function getUrlWithOutFormat()
    {
        $i = 1;
        $return_url = request()->url() . '?';
        foreach (request()->except('format') as $param_key => $param_value) {
            if ($i == 1) {
                $return_url .= $param_key . '=' . urlencode($param_value);
            } else {
                $return_url .= '&' . $param_key . '=' . urlencode($param_value);
            }
            $i++;
        }
        return $return_url;
    }

    public function sanitizeUrlString($condition_url_string)
    {
        return explode(',', $condition_url_string);
    }

    public function getGenres()
    {
        $genres = [];
        $genres_in_query = [];

        if (request()->filled('genre')) {
            $genres_in_query = explode(',', request()->get('genre'));
        }
        
        foreach ($this->getRootCategories() as $category_index => $category) {
            if ($category->id != 1) {
                $genres[$category->id] = $category;
                $genres[$category->id]->label_class = '';
                $product_count = DB::table('categories as cat')
                                    ->select(DB::raw('COUNT(DISTINCT ' . DB::getTablePrefix() . 'pc.product_id) as count'))
                                    ->leftJoin('product_categories as pc', 'cat.id', '=', 'pc.category_id')
                                    ->where('id', $category->id)
                                    ->first();

                $genres[$category->id]->products_count = $product_count->count;

                if (in_array($category->id, $genres_in_query)) {
                    $genres[$category->id]->label_class = 'active';
                }
            }
        }
        //dd($genres_in_query, $genres);
        return $genres;
    }

    public function getFormats()
    {
        $formats = $this->getAllFormats();
        
        if (request()->filled('format')) {
            foreach ($formats as $format_index => $format) {
                if ($format->slug == request()->get('format')) {
                    $formats->{$format_index}->label_class = 'active';
                }
            }
        }
        //dd($formats);
        return $formats;
    }

    public function getAllFormats()
    {
        return json_decode(json_encode($this->getAllFormatsArray()));
    }

    public function getAllFormatsArray()
    {
        return [
            'vinyl' => [
                'slug' => 'vinyl',
                'name' => 'Vinyl',
                'label_class' => '',
                'aliases' => [
                    'lp' => [
                        'slug' => 'lp',
                        'name' => 'LP',
                    ],
                    '12"' => [
                        'slug' => '12"',
                        'name' => '12"',
                    ],
                    '7"' => [
                        'slug' => '7"',
                        'name' => '7"',
                    ],
                ],
                'childs' => [],
            ],
            'cd' => [
                'slug' => 'cd',
                'name' => 'Cd',
                'label_class' => '',
                'aliases' => [],
                'childs' => [],
            ],
            'cassette' => [
                'slug' => 'cassette',
                'name' => 'Cassette',
                'label_class' => '',
                'aliases' => [],
                'childs' => [],
            ],
            'other' => [
                'slug' => 'other',
                'name' => 'Other',
                'label_class' => '',
                'aliases' => [],
                'childs' => [],
            ],
        ];
    }

    public function getFormatUrl($format)
    {
        return request()->fullUrlWithQuery([
            'format'  => $format,
        ]);
    }

    public function getFormatContainerClass()
    {
        if (request()->has('format')) {
            return '';
        } else {
            return 'hide';
        }
    }

    public function getFormatContainerDisplay()
    {
        if (request()->has('format')) {
            return '';
        } else {
            return "display: none;";
        }
    }

    public function getRootCategories()
    {
        return app('Webkul\Category\Repositories\CategoryRepository')->getRootCategories();
    }
}