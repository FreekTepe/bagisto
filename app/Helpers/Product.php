<?php

    namespace App\Helpers;

    class Product
    {
        public function getMarketplaceProductsByProduct($product_id)
        {
            return app('App\Models\ExtendWebkul\MarketplaceProduct')->where('product_id', $product_id)->get();
        }

        public function getCompleteProductByProduct($product_id)
        {
            return app('Webkul\Product\Repositories\ProductRepository')->with(['variants', 'variants.inventories'])->findOrFail($product_id);
        }

        public function getMarketplaceProductByProduct($product_id)
        {
            return app('Webkul\Marketplace\Repositories\MpProductRepository')->with(['variants', 'variants.inventories'])->findOrFail($product_id);
        }
    }