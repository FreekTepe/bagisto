<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EitherStringOrNumeric implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_string($value)) {
            return true;
        }
        if (is_numeric($value)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be either a string or a number.';
    }
}
