@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.login-form.page-title') }}
@endsection

@section('content-wrapper')
    {!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}
    {!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}

    <section class="container">
        <div class="row">
            <div class="offset-md-3"></div>

            <div class="col-12 col-md-6">
                <div class="auth-content">
                    <div class="sign-up-text my-md-3 my-lg-5 text-center">
                        {{ __('shop::app.customer.login-text.no_account') }} -

                        <a href="{{ route('customer.register.index') }}">{{ __('shop::app.customer.login-text.title') }}</a>
                    </div>

                    {!! view_render_event('bagisto.shop.customers.login.before') !!}

                    <div class="newsletter newsletter--inner border border-dark px-4 py-5 mb-5" style="border-width: 2px !important;">
                        <form method="POST" action="{{ route('customer.session.create') }}" @submit.prevent="onSubmit">
                            {{ csrf_field() }}

                            <div class="login-form">
                                <div class="login-text font-weight-bold pb-4">{{ __('shop::app.customer.login-form.title') }}</div>

                                {!! view_render_event('bagisto.shop.customers.login_form_controls.before') !!}

                                <div class="control-group pb-3" :class="[errors.has('email') ? 'has-error' : '']">
                                    <input type="text" class="control w-100" name="email" placeholder="{{ __('shop::app.customer.login-form.email') }}" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.login-form.email') }}&quot;">
                                    {{-- <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span> --}}
                                </div>

                                <div class="control-group pb-4" :class="[errors.has('password') ? 'has-error' : '']">
                                    <input type="password" v-validate="'required|min:6'" class="control w-100" id="password" name="password" placeholder="{{ __('shop::app.customer.login-form.password') }}" data-vv-as="&quot;{{ __('admin::app.users.sessions.password') }}&quot;" value=""/>
                                    {{-- <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span> --}}
                                </div>

                                {!! view_render_event('bagisto.shop.customers.login_form_controls.after') !!}

                                <div class="forgot-password-link pb-4">
                                    <a href="{{ route('customer.forgot-password.create') }}">{{ __('shop::app.customer.login-form.forgot_pass') }}</a>

                                    <div class="mt-10">
                                        @if (Cookie::has('enable-resend'))
                                            @if (Cookie::get('enable-resend') == true)
                                                <a href="{{ route('customer.resend.verification-email', Cookie::get('email-for-resend')) }}">{{ __('shop::app.customer.login-form.resend-verification') }}</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>

                                <button type="submit">{{ __('shop::app.customer.login-form.button_title') }}</button>
                            </div>
                        </form>
                    </div>

                    {!! view_render_event('bagisto.shop.customers.login.after') !!}
                </div>
            </div>

            <div class="offset-md-3"></div>
        </div>
    </section>
@stop