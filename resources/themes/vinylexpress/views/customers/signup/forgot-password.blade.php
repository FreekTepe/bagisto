@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.forgot-password.page_title') }}
@stop

@section('content-wrapper')

    <section class="container">
        <div class="row">
            <div class="offset-md-3"></div>

            <div class="col-12 col-md-6">
                <div class="newsletter newsletter--inner border border-dark px-4 py-5 mb-5 mt-5" style="border-width: 2px !important;">

                    {!! view_render_event('bagisto.shop.customers.forget_password.before') !!}

                    <form method="post" action="{{ route('customer.forgot-password.store') }}" @submit.prevent="onSubmit">
                        {{ csrf_field() }}

                        <div class="login-form">

                            <div class="login-text font-weight-bold pb-4">{{ __('shop::app.customer.forgot-password.title') }}</div>

                            {!! view_render_event('bagisto.shop.customers.forget_password_form_controls.before') !!}

                            <div class="control-group pb-3" :class="[errors.has('email') ? 'has-error' : '']">
                                <input type="email" class="control w-100" name="email" placeholder="{{ __('shop::app.customer.forgot-password.email') }}" v-validate="'required|email'">
                                {{-- <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span> --}}
                            </div>

                            {!! view_render_event('bagisto.shop.customers.forget_password_form_controls.before') !!}

                            <div class="button-group pb-3">
                                <button type="submit">{{ __('shop::app.customer.forgot-password.submit') }}</button>
                            </div>

                            <div class="control-group" style="margin-bottom: 0px;">
                                <a href="{{ route('customer.session.index') }}">
                                    <i class="icon primary-back-icon"></i>
                                    {{ __('shop::app.customer.reset-password.back-link-title') }}
                                </a>
                            </div>
                        </div>
                    </form>

                    {!! view_render_event('bagisto.shop.customers.forget_password.after') !!}

                </div>
            </div>

            <div class="offset-md-3"></div>
        </div>
    </section>

@endsection