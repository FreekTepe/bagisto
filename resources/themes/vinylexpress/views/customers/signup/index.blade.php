@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.signup-form.page-title') }}
@endsection

@section('content-wrapper')

    <section class="container">
        <div class="row">
            <div class="offset-md-3"></div>

            <div class="col-12 col-md-6">
                <div class="auth-content">
                    <div class="sign-up-text my-md-3 my-lg-5 text-center">
                        {{ __('shop::app.customer.signup-text.account_exists') }} - <a href="{{ route('customer.session.index') }}">{{ __('shop::app.customer.signup-text.title') }}</a>
                    </div>

                    {!! view_render_event('bagisto.shop.customers.signup.before') !!}

                    <div class="newsletter newsletter--inner border border-dark px-4 py-5 mb-5" style="border-width: 2px !important;">
                        <form method="post" action="{{ route('customer.register.create') }}" @submit.prevent="onSubmit">

                            {{ csrf_field() }}

                            <div class="login-form">
                                <div class="login-text font-weight-bold pb-4">{{ __('shop::app.customer.signup-form.title') }}</div>

                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.before') !!}

                                <div class="control-group pb-3" :class="[errors.has('first_name') ? 'has-error' : '']">
                                    <input type="text" class="control w-100" name="first_name" placeholder="{{ __('shop::app.customer.signup-form.firstname') }}" v-validate="'required'" value="{{ old('first_name') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.firstname') }}&quot;">
                                    {{-- <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span> --}}
                                </div>

                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.firstname.after') !!}

                                <div class="control-group pb-3" :class="[errors.has('last_name') ? 'has-error' : '']">
                                    <input type="text" class="control w-100" name="last_name" placeholder="{{ __('shop::app.customer.signup-form.lastname') }}" v-validate="'required'" value="{{ old('last_name') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.lastname') }}&quot;">
                                    {{-- <span class="control-error" v-if="errors.has('last_name')">@{{ errors.first('last_name') }}</span> --}}
                                </div>

                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.lastname.after') !!}

                                <div class="control-group pb-3" :class="[errors.has('email') ? 'has-error' : '']">
                                    <input type="email" class="control w-100" name="email" placeholder="{{ __('shop::app.customer.signup-form.email') }}" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                                    {{-- <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span> --}}
                                </div>

                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.email.after') !!}

                                <div class="control-group pb-3" :class="[errors.has('password') ? 'has-error' : '']">
                                    <input type="password" class="control w-100" name="password" placeholder="{{ __('shop::app.customer.signup-form.password') }}" v-validate="'required|min:6'" ref="password" value="{{ old('password') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.password') }}&quot;">
                                    {{-- <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span> --}}
                                </div>

                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.password.after') !!}

                                <div class="control-group pb-3" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                                    <input type="password" class="control w-100" name="password_confirmation" placeholder="{{ __('shop::app.customer.signup-form.confirm_pass') }}"  v-validate="'required|min:6|confirmed:password'" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.confirm_pass') }}&quot;">
                                    {{-- <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password_confirmation') }}</span> --}}
                                </div>

                                {{-- <div class="signup-confirm" :class="[errors.has('agreement') ? 'has-error' : '']">
                                    <span class="checkbox">
                                        <input type="checkbox" id="checkbox2" name="agreement" v-validate="'required'">
                                        <label class="checkbox-view" for="checkbox2"></label>
                                        <span>{{ __('shop::app.customer.signup-form.agree') }}
                                            <a href="">{{ __('shop::app.customer.signup-form.terms') }}</a> & <a href="">{{ __('shop::app.customer.signup-form.conditions') }}</a> {{ __('shop::app.customer.signup-form.using') }}.
                                        </span>
                                    </span>
                                    <span class="control-error" v-if="errors.has('agreement')">@{{ errors.first('agreement') }}</span>
                                </div> --}}

                                {!! view_render_event('bagisto.shop.customers.signup_form_controls.after') !!}

                                {{-- <div class="control-group" :class="[errors.has('agreement') ? 'has-error' : '']">

                                    <input type="checkbox" id="checkbox2" name="agreement" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.agreement') }}&quot;">
                                    <span>{{ __('shop::app.customer.signup-form.agree') }}
                                        <a href="">{{ __('shop::app.customer.signup-form.terms') }}</a> & <a href="">{{ __('shop::app.customer.signup-form.conditions') }}</a> {{ __('shop::app.customer.signup-form.using') }}.
                                    </span>
                                    <span class="control-error" v-if="errors.has('agreement')">@{{ errors.first('agreement') }}</span>
                                </div> --}}

                                <div class="control-group pb-4">
                                    <button type="submit">{{ __('shop::app.customer.signup-form.button_title') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    {!! view_render_event('bagisto.shop.customers.signup.after') !!}
                </div>
            </div>

            <div class="offset-md-3"></div>
        </div>
    </section>

@endsection