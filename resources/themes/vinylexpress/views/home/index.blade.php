@extends('shop::layouts.master')

@section('content-wrapper')

    @php
        $limit = 12;
        $products = app('App\Repositories\VinylExpressProductRepository')->getVinylExpressProducts($limit);

        $products_count = count($products);

        $total_products = app('App\Repositories\VinylExpressProductRepository')->getTotalProducts();

        $product_images = [
            'album-cover.jpg',
            'second-img.jpg',
            'sour-soul.jpg',
            'bbc.jpg',
            'black-star.jpg',
            'instant-image.jpg',
            'seeds.jpg',
            'whole-love.jpg',
            'looking.jpg',
        ];
    @endphp

    @include("shop::home.slider")
    @include("shop::home.usp")
    @include("shop::home.newproducts")
    @include("shop::home.goodies")
    @include("shop::home.genres")
    @include("shop::home.latestarticle")
    @include("shop::home.newsletter")

@endsection