@php

@endphp

<section class="slider" data-component="slider">
    <div class="slider__repeat">
        <div class="slider__holder" style="background-image: url({{ asset('themes/vinylexpress/assets/src/img/slider.jpg') }});">
            <div class="slider__caption">
                <div class="slider__title">Your one stop shop for all Vinyl-related collectables</div>
                <div class="slider__count">{{ $total_products }}</div>
                <a class="button slider__button" href="#">Records</a>
            </div>
        </div>
    </div>
    <div class="slider__repeat">
        <div class="slider__holder" style="background-image: url({{ asset('themes/vinylexpress/assets/src/img/slider.jpg') }});">
            <div class="slider__caption">
                <div class="slider__title">Your one stop shop for all Vinyl-related collectables</div>
                <div class="slider__count">{{ $total_products }}</div>
                <a class="button slider__button" href="#">Records</a>
            </div>
        </div>
    </div>
</section>