@php
    $iterator = 1;
@endphp

<section class="new-records container">
    <div class="heading">
        <h2>Goodies</h2>
    </div>

    <div class="new-records__holder">
        <div class="row">
            @if ($products_count)
                @foreach ($products as $product_index => $product)
                    @if ($iterator < 5)
                        <div class="col-lg-3 col-md-4 col-sm-6 col-6 new-records__mg">
                            <div class="card-products card-products--goodies-list">
                                <a href="#" class="card-products__list">
                                    <div class="card-products__image">
                                        <img src="{{ asset('themes/vinylexpress/assets/img/' . $product_images[rand(0, 8)]) }}" alt="" />
                                    </div>
                                    <div class="card-products__content">
                                        <div class="card-products__title">{{ $product->name }}</div>
                                        <div class="card-products__category">- ACDC</div>
                                        <div class="card-products__price">From € {{ str_replace('.', ',', number_format($product->special_price, 2, ',', '')) }}</div>
                                    </div>
                                </a>
                                <a class="card-products__bag" href="#">Add to my bag</a>
                            </div>
                        </div>
                    @endif

                    @php
                        $iterator++;
                    @endphp
                @endforeach
            @endif
        </div>

        <div class="new-records__load">
            <a href="#" class="button button--arrow">See All Goodies</a>
        </div>

    </div>
</section>