@php 
$product_link = '';
if(isset($has_from_price) || isset($is_catalog)) {
    $product_link = '/' . $product->url_key;
    $product_url_key = $product->url_key;
} else {
    $product_link = '/marketplace/seller/' . $product->seller->url . '/product/'. $product->product->url_key;
    $product_url_key = $product->product->url_key;
}
@endphp

<div class="product-card d-flex flex-row justify-content-center mx-auto">
    <a href="{{ $product_link }}" class="card-info">
        <div class="info-area d-flex justify-content-start align-items-center">
            <div class="area-year col-auto pl-0 mr-auto">{{ $product->product->getReleaseYear() }}</div>
            
            @if (isset($is_search) || isset($is_catalog))
                <div class="area-amount-of-records col-auto">{{ $product->product->marketplaceProducts()->count() }}</div>
            @endif
        </div>

        <div class="info-image">
            <div class="image-holder info-image-{{ $product_url_key }}"></div>
        </div>
        
        <div class="info-content">
            <div class="content-title">{{ $product->product->name }}</div>
            <div class="content-category">- {{ $product->product->getArtistName() }}</div>
        </div>

        <div class="info-price-add-to-cart d-flex justify-content-start align-items-center">
            @if (isset($has_from_price) || isset($is_catalog))
                @include('shop::products.card.from-price')
            @else
                @include('shop::products.card.price')

                <form action="{{ route('cart.add', $product->product->product_id) }}" method="POST" class="col-6 info-add-to-cart-form">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->product->product_id }}">
                    <input type="hidden" name="quantity" value="1">
                    <input type="hidden" name="seller_info[product_id]" value="{{ $product->id }}">
                    <input type="hidden" name="seller_info[seller_id]" value="{{ $product->seller->id }}">
                    <input type="hidden" name="seller_info[is_owner]" value="0">
                    
                    <button class="add-to-cart-button" {{ $product->product->isSaleable() ? '' : '' }}>{{ __('shop::app.products.add-to-cart') }}</button>
                </form>
            @endif
        </div>
    </a>
</div>