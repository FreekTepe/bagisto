@guest('customer')
    @php
        $shipping_price = $product->getShippingPrice();
    @endphp
@endguest

@auth('customer')
    @php
        $shipping_price = $product->getShippingPrice(auth()->guard('customer')->user());
    @endphp
@endauth


<div class="col-6 info-price">
    {{ core()->currency($product->price) }} @if ($shipping_price) + {{ core()->currency($shipping_price) }} @endif
</div>