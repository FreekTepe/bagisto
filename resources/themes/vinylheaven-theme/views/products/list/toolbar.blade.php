@inject ('toolbarHelper', 'Webkul\Product\Helpers\Toolbar')

<div class="search-result__top">
    <div class="search-result__name">
        <div class="search-result__results">
            @if (!$results)
                0 Results for:
            @else
                {{ $results->count() }} Results For:
            @endif
        </div>

        @if (isset($_GET['term']))
            {{ $_GET['term']}}
        @endif
    </div>

    <div class="search-result__sorting">
        <div class="search-result__sort">
            <span class="search-result__text">Sorted by</span>

            <select class="search-result__dropdown">
                <option>Relevance</option>
                <option>Price</option>
                <option>Date</option>
            </select>
        </div>

        <div class="search-result__view">
            <span class="search-result__text">View</span>
            @if ($toolbarHelper->isModeActive('grid') || (!$toolbarHelper->isModeActive('grid') && !$toolbarHelper->isModeActive('list')))
                <a href="#" class="search-result__icon is-active">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/grid-icon.png') }}" alt=""/>
                </a>
            @else
                <a href="{{ $toolbarHelper->getModeUrl('grid') }}" class="search-result__icon">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/grid-icon.png') }}" alt=""/>
                </a>
            @endif

            @if ($toolbarHelper->isModeActive('list'))
                <a href="#" class="search-result__icon is-active">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/list-Icon.png') }}" alt=""/>
                </a>
            @else
                <a href="{{ $toolbarHelper->getModeUrl('list') }}" class="search-result__icon">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/list-Icon.png') }}" alt=""/>
                </a>
            @endif
        </div>
    </div>
</div>