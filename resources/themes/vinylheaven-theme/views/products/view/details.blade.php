@if($product_view_config->is_seller_product)
    @guest('customer')
        @php
            $shipping_price = $product->getShippingPrice();
        @endphp
    @endguest

    @auth('customer')
        @php
            $shipping_price = $product->getShippingPrice(auth()->guard('customer')->user());
        @endphp
    @endauth
@endif

<div class="row">
    <div class="col-12">
        <div class="details-title">{{ $product->product->name }}</div>
    </div>

    <div class="col-12">
        <div class="details-artist">
            <span class="artist">{{ $product->product->getArtistName() }}</span>
        </div>
    </div>
</div>

<div class="row mb-3">
    <div class="col-auto details-statistic first">
        45 rpm
    </div>

    <div class="col-auto details-statistic">
        231 sales
    </div>

    <div class="col-auto details-statistic">
        rating
    </div>

    @if($product_view_config->is_seller_product)
        <div class="col-auto details-statistic last">
            Ships from {{ core()->country_name($seller->country) }}
        </div>
    @endif
</div>

<div class="row align-items-center mb-5">
    <div class="col-auto details-price-shipping">
        <div class="row aling-items-center">
            <div class="col details-price">
                @if($product_view_config->is_seller_product)
                    {{ core()->currency(core()->convertPrice($product->price, core()->getBaseCurrencyCode(), 'EUR')) }}
                @else
                    From: {{ $product->product->getMarketplaceProductsFromPrice() }}
                @endif
            </div>
        </div>

        @if($product_view_config->is_seller_product && $shipping_price)
            <div class="row align-items-center">
                <div class="col details-shipping-price">
                    + {{ core()->currency(core()->convertPrice($shipping_price, core()->getBaseCurrencyCode(), 'EUR')) }} shipping
                </div>
            </div>
        @endif
    </div>

    @if($product_view_config->is_seller_product)
        <div class="col-auto ml-auto details-add-to-cart">
            <form action="{{ route('cart.add', $product->product_id) }}" method="POST" class="form-inline">
                @csrf
                <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                <input type="hidden" name="quantity" value="1">
                <input type="hidden" name="seller_info[product_id]" value="{{ $product->id }}">
                <input type="hidden" name="seller_info[seller_id]" value="{{ $seller->id }}">
                <input type="hidden" name="seller_info[is_owner]" value="0">
                
                <button class="button button-bag" {{ $product->product->isSaleable() ? '' : '' }}>{{ __('shop::app.products.add-to-cart') }}</button>
            </form>
        </div>
    @endif

    @auth('customer')
        <div class="col-auto details-add-to-wishlist">
            <a href="#" class="button button-hearth">
                <img src="{{ asset('themes/vinylheaven-theme/assets/img/heart-icon.png') }}" alt="">
            </a>
        </div>
    @endauth
</div>

@if($product_view_config->is_seller_product)
    <div class="row mb-3 details-conditions">
        <div class="col-6">
            <div class="row mb-1">
                <div class="col-12 text-header">Media condition</div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="details-condition condition-grading-circle">
                        <span class="circle-text {{ $product->product->getConditionGradingBackgroundClass($product->product->getConditionGrading($product->condition)) }}">{{ $product->product->getConditionGrading($product->condition) }}</span> 
                        {{ $product->product->getConditionText($product->condition) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="row mb-1">
                <div class="col-12 text-header">Sleeve condition</div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="details-condition condition-grading-circle list-view">
                        <span class="circle-text  {{ $product->product->getConditionGradingBackgroundClass($product->product->getSleeveConditionGrading($product->sleeve_condition)) }}">{{ $product->product->getSleeveConditionGrading($product->sleeve_condition) }}</span> 
                        {{ $product->product->getSleeveConditionText($product->sleeve_condition) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-12">
            <div class="row mb-1">
                <div class="col-12 text-header">Seller notes</div>
            </div>

            <div class="row">
                <div class="col-12 details-seller-notes">{{ $product->description }}</div>
            </div>
        </div>
    </div>
@endif

<div class="row mb-3">
    <div class="col-12">
        <div class="row mb-1">
            <div class="col-12 text-header">Product summary</div>
        </div>

        <div class="row details-formats">
            <div class="col-auto"><strong>Formats:</strong></div>

            <div class="col">
                <div class="row">
                    @foreach($product->product->formatsFormated() as $format_index => $format)
                        @if ($format_index == 0)
                            <div class="col-auto details-format first">
                                {{ $format }}
                            </div>
                        @else
                            <div class="col-auto details-format">
                                {{ $format }}
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row details-labels">
            <div class="col-auto"><strong>Label(s):</strong></div>

            <div class="col">
                <div class="row">
                    @foreach ($product->product->labels()->get() as $label_index => $label)
                        @if($label_index == 0)
                            <div class="col-auto details-label first">
                                {{ $label->name }} 
                            </div>
                        @else
                            <div class="col-auto details-label">
                                {{ $label->name }} 
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-auto"><strong>Barcode/EAN:</strong></div>

            <div class="col">
                <div class="row">
                    @if (!empty($product->product->identifiers))
                        @foreach ($product->product->identifiers as $identifier)
                            @if(strtoupper($identifier['type']) == 'BARCODE')
                                <div class="col-auto details-barcode">
                                    {{ $identifier['type'] }} {{ $identifier['value'] }} @if(!empty($identifier['description'])) {{ $identifier['description'] }} @endif 
                                </div>

                                @php 
                                    break; 
                                @endphp
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>