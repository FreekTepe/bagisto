<section class="container product-seller mb-4">
    <div class="row">
        <div class="col-12 col-md-6 offset-md-6">
            <div class="d-flex justify-content-center align-items-center">
                <div class="col-4 text-center price-list-view">
                    {{ core()->currency($mp_product->price) }}
                </div>

                <div class="col">
                    <form action="{{ route('cart.add', $product->product_id) }}" method="POST" class="col-8">
                        @csrf
                        <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                        <input type="hidden" name="quantity" value="1">
                        <input type="hidden" name="seller_info[product_id]" value="{{ $mp_product->id }}">
                        <input type="hidden" name="seller_info[seller_id]" value="{{ $mp_product->marketplace_seller_id }}">
                        <input type="hidden" name="seller_info[is_owner]" value="0">

                        <button type="submit" class="button button-bag" {{ $product->isSaleable() ? '' : '' }}>{{ __('shop::app.products.add-to-cart') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>