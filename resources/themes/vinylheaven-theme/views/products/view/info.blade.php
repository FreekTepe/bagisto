<div class="container-fluid container-lg container-xl">
    <div class="row">    
        <div class="col-12 col-md-6">
            <div class="info-heading">Product Information</div>

            <ul class="info-list">
                @if (isset($product->product->companies))
                    <li class="list-item">
                        <div class="item-left">Companies, etc</div>

                        <div class="item-right">
                            @foreach ($product->product->companies as $company_index => $company)
                                <p>{{ $company['entity_type_name'] }} — {{ $company['name'] }}</p>
                            @endforeach
                        </div>
                    </li>
                @endif

                @if (isset($product->product->identifiers))
                    <li class="list-item">
                        <div class="item-left">Barcode and other identifiers</div>

                        <div class="item-right">
                            @foreach($product->product->identifiers as $identifier_index => $identifier)
                                <p>
                                    {{ $identifier['type'] }}
                                    @if ($identifier['description'] != null)
                                        ({{ $identifier['description']}})
                                    @endif
                                    : {{ $identifier['value'] }}
                                </p>
                            @endforeach
                        </div>
                    </li>
                @endif
                
                <li class="list-item">
                    <div class="item-left">Credits</div>

                    <div class="item-right">
                        @if (isset($product->product->artists))
                            @foreach($product->product->artists as $artist_index => $artist)
                                @if(isset($artist->name))
                                    <p>
                                        <strong>{{ $artist->name }}</strong>
                                    </p>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </li>
            </ul>
        </div>
        
        <div class="col-12 col-md-6">
            <div class="info-heading">Tracklist</div>

            <ul class="info-list">
                @foreach ($product->product->tracklist as $item_index => $item)
                    <li class="list-item">
                        <div class="item-col-1">{{ $item['position'] }}</div>
                        <div class="item-col-2">{{ $item['title'] }}</div>
                        <div class="item-col-3">{{ $item['duration'] }}</div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

<div class="info-show-more" id="info-show-more">
    <div class="d-flex justify-content-center align-items-end show-more" id="info-show-more-toggler">
        <i class="fa fa-chevron-down more-icon"></i>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#info-show-more-toggler').click(function(){
                if(!$('.product-info').hasClass('open')) {
                    $('.product-info').addClass('open');
                } else {
                    $('.product-info').removeClass('open');
                }
            });
        });
    </script>
@endpush