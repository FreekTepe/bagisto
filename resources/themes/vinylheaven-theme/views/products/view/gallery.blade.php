<div class="product-gallery gallery-images">
    <div class="product-format"></div>

    <div class="gallery-images-holder" id="gallery-images-holder">
        @if (count($product->product->images))
            @foreach ($product->product->images as $image_index => $image)
                @if ($image_index < 4)
                    <figure>
                        <img src="{{ $image->url }}" alt="">

                        <figcaption>Images are from an external source and may differ from reality</figcaption>
                    </figure>
                @endif
            @endforeach
        @else
            <figure>
                <img src="{{ asset('themes/vinylexpress/assets/images/Default-Product-Image.png') }}" alt="">

                <figcaption>Images are from an external source and may differ from reality</figcaption>
            </figure>
        @endif
    </div>

    <div class="gallery-images-navigator" id="gallery-images-navigator">
        @if (count($product->product->images))
            @foreach ($product->product->images as $image_index => $image)
                @if ($image_index < 4)
                    <figure>
                        <img src="{{ $image->url }}" alt="">
                    </figure>
                @endif
            @endforeach
        @else
            <figure>
                <img src="{{ asset('themes/vinylexpress/assets/images/Default-Product-Image.png') }}" alt="">
            </figure>
        @endif
    </div>
</div>



@push('scripts')
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/vendor/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/gallery.js') }}"></script>
@endpush