<div class="row product-view-toolbar">
    <div class="col-6 col-md-4">
        <div class="toolbar-result d-flex flex-column justify-content-start">
            <div class="result-count">{{ $products_count }} Results For:</div>

            <div class="result-term">{{ isset($_GET['term']) ? $_GET['term'] : '' }}</div>
        </div>
    </div>

    <div class="col-6 col-md-3">
        <div class="toolbar-limit d-flex justify-content-end align-center">
            <div class="limit-title">Results</div>

            <div class="limit-select">
                <select id="vinylheaven-catalog-results-limiter" class="select-dropdown">
					@foreach ($toolbarHelper->getAvailableLimits() as $limit_key => $limit_value)
						@if ($toolbarHelper->isLimitCurrent($limit_value))
							<option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}" selected>{{ $limit_value }}</option>
						@else
							<option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}">{{ $limit_value }}</option>
						@endif
					@endforeach
				</select>
            </div>
        </div>
    </div>

    <div class="col-6 col-md-3">
        <div class="toolbar-sort d-flex justify-content-end align-center">
            <div class="sort-title">Results</div>

            <div class="sort-select">
                <select id="vinylheaven-catalog-sorter" class="select-dropdown">
					@foreach ($toolbarHelper->getAvailableOrders() as $order_key => $order_value)
						@if ($toolbarHelper->isOrderCurrent($order_key))
							<option value="{{ $toolbarHelper->getOrderUrl($order_key) }}" selected>{{ $order_value }}</option>
						@else
							<option value="{{ $toolbarHelper->getOrderUrl($order_key) }}">{{ $order_value }}</option>
						@endif
					@endforeach
				</select>
            </div>
        </div>
    </div>

    <div class="col-6 col-md-2">
        <div class="toolbar-view d-flex justify-content-end align-center">
            <div class="view-title">View</div>

            <div class="view-switcher d-flex justify-content-center align-items-center">
            @if ($toolbarHelper->isModeActive('grid') || (!$toolbarHelper->isModeActive('grid') && !$toolbarHelper->isModeActive('list')))
					<a href="#" class="switcher-icon is-active">
						<img src="{{ asset('themes/vinylexpress/assets/src/img/grid-icon.png') }}" alt=""/>
					</a>
				@else
					<a href="{{ $toolbarHelper->getModeUrl('grid') }}" class="switcher-icon">
						<img src="{{ asset('themes/vinylexpress/assets/src/img/grid-icon.png') }}" alt=""/>
					</a>
				@endif

				@if ($toolbarHelper->isModeActive('list'))
					<a href="#" class="switcher-icon is-active">
						<img src="{{ asset('themes/vinylexpress/assets/src/img/list-Icon.png') }}" alt=""/>
					</a>
				@else
					<a href="{{ $toolbarHelper->getModeUrl('list') }}" class="switcher-icon">
						<img src="{{ asset('themes/vinylexpress/assets/src/img/list-Icon.png') }}" alt=""/>
					</a>
				@endif
            </div>
        </div>
    </div>
</div>