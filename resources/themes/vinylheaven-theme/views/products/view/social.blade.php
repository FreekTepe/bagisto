<section class="container product-social">
    <div class="row justify-content-end">
        <div class="col-auto">
            <a class="twitter social-icon-container" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </div>

        <div class="col-auto">
            <a class="facebook social-icon-container" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        </div>

        <div class="col-auto">
            <a class="gplus social-icon-container" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        </div>

        <div class="col-auto">
            <a class="linkedin social-icon-container" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
        </div>

        <div class="col-auto">
            <a class="telegram social-icon-container" href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a>
        </div>
    </div>
</section>