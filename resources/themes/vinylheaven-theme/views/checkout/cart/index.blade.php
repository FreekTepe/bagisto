@extends('shop::layouts.master-new')

@inject('searchHelper', 'App\Helpers\Search')
@inject('header_helper', 'App\Helpers\Header')

@section('page_title')
    {{ __('shop::app.checkout.cart.title') }}
@stop

@php
$no_add_to_bag_button = true;
@endphp

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-cart py-4">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-9 cart-sellers-container">
                <!-- SELLER CART ITEMS HERE -->
                @if($cart)
                    <div class="row">
                        @foreach ($header_helper->getCartItemsBySeller() as $seller)
                            <div class="col-12 col-md-6">
                                <div class="seller-container border-0">
                                    <div class="seller-info">
                                        <div class="info-header">
                                            <div class="d-flex justify-content-start align-items-center">
                                                <div class="d-inline-block">{{ $seller['info']->shop_title }}</div>

                                                <div class="d-inline-block ml-auto">{{ core()->currency($seller['totals']['items_total']) }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="seller-items">
                                        @foreach ($seller['items'] as $item_index => $item)
                                            @php
                                                view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]);
                                                $item_name = $item->product->name;
                                                view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]);

                                                view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.before', ['item' => $item]);
                                                $item_quantity = $item->quantity;
                                                view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.after', ['item' => $item]);

                                                view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]);
                                                $item_price = core()->currency($item->base_price);
                                                view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]);

                                                $i = $item_index + 1;
                                            @endphp

                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="list-item">
                                                        <div class="d-flex justify-content-start align-items-center">
                                                            <div class="item-image-container">
                                                                <div class="d-flex justify-content-center align-items-center item-image">
                                                                    <img src="{{ $item->product->getPrimaryImage() }}" alt="{{ $item_name }}" />
                                                                </div>
                                                            </div>

                                                            <div class="item-info-container">
                                                                <div class="d-flex justify-content-start align-items-center info-quantity-name">
                                                                    <div class="d-lg-inline-block item-quantity text-center mr-1">
                                                                        {{ $item_quantity }} X
                                                                    </div>

                                                                    <div class="d-lg-inline-block item-name">
                                                                        <strong>{{ $item_name }}</strong>
                                                                    </div>
                                                                </div>

                                                                <div class="d-flex justify-content-start align-items-center info-price-remove">
                                                                    <div class="d-lg-inline-block item-name ml-auto">
                                                                        <a class="d-flex justify-content-center align-items-center item-remove" href="{{ route('shop.checkout.cart.remove', $item->id) }}" alt="{{ __('shop::app.checkout.cart.remove-link') }}" onclick="removeLink('{{ __('shop::app.checkout.cart.cart-remove-action') }}')">
                                                                            <i class="fa fa-trash remove-icon"></i>
                                                                        </a>
                                                                    </div>

                                                                    <div class="d-lg-inline-block ml-2">
                                                                        {{ $item_price }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="row">
                        <div class="col-12">
                            <p>{{ __('shop::app.checkout.cart.empty') }}</p>
                        </div>
                    </div>
                @endif
            </div>
            
            <div class="col-12 col-md-6 col-lg-3">
                <!-- TOTALS HERE -->
                @if($cart)
                    <div class="row">
                        <div class="col-12 text-center">
                            <strong>Cart totals</strong>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6 text-left">
                            Subtotal
                        </div>

                        <div class="col-6 text-right">
                            {{ core()->currency($cart->base_sub_total) }}
                        </div>
                    </div>

                    @if ($cart->selected_shipping_rate)
                        <div class="row">
                            <div class="col-6 text-left">
                                {{ __('shop::app.checkout.total.delivery-charges') }}
                            </div>

                            <div class="col-6 text-right">
                                {{ core()->currency($cart->selected_shipping_rate->base_price) }}
                            </div>
                        </div>
                    @endif

                    @if ($cart->base_tax_total)
                        @foreach (Webkul\Tax\Helpers\Tax::getTaxRatesWithAmount($cart, true) as $taxRate => $baseTaxAmount )
                            <div class="row">
                                <div class="col-6 text-left">
                                    {{ __('shop::app.checkout.total.tax') }} {{ $taxRate }} %
                                </div>

                                <div class="col-6 text-right">
                                    {{ core()->currency($baseTaxAmount) }}
                                </div>
                            </div>
                        @endforeach
                    @endif

                    @if ($cart->base_discount_amount && $cart->base_discount_amount > 0)
                        <div class="row">
                            <div class="col-6 text-left">
                                {{ __('shop::app.checkout.total.disc-amount') }}
                            </div>

                            <div class="col-6 text-right">
                                -{{ core()->currency($cart->base_discount_amount) }}
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-6 text-left">
                            <strong>{{ __('shop::app.checkout.total.grand-total') }}</strong>
                        </div>

                        <div class="col-6 text-right">
                            <strong>{{ core()->currency($cart->base_grand_total) }}</strong>
                        </div>
                    </div>

                    <div class="row justify-content-center align-items-center mt-4 shopping-mini-cart-buttons">
                        <div class="col-12 col-lg-auto">
                            <a class="button button-bag" href="{{ route('shop.checkout.onepage.index') }}">{{ __('shop::app.minicart.checkout') }}</a>
                        </div>

                        <div class="col-12 col-lg-auto mt-2">
                            <a class="pt-4 pb-4" href="{{ route('shop.checkout.cart.index') }}">{{ __('shop::app.minicart.view-cart') }}</a>
                        </div>
                    </div>
                @else
                    <div class="row justify-content-center align-items-center mt-4 shopping-mini-cart-buttons">
                        <div class="col-12 col-lg-auto">
                            <a href="{{ route('shop.home.index') }}" class="button button-arrow">{{ __('shop::app.checkout.cart.continue-shopping') }}</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('dep-content-wrapper')
<section class="container cart-container">
    @if ($cart)
    <div class="row">
        <div class="col-12">
            <div class="heading subtitle">
                <h2 class="title">{{ __('shop::app.checkout.cart.title') }}</h2>
            </div>            
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form action="{{ route('shop.checkout.cart.update') }}" method="POST" @submit.prevent="onSubmit">
                @csrf
                <table class="table table-borderless table-sm w-100">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="w-50" colspan="2">Name</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Price</th>
                            <th class="text-right">Totals</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($cart->items as $item_index => $item)
                            <tr>
                                <td scope="row">
                                    <a href="{{ route('shop.checkout.cart.remove', $item->id) }}" alt="{{ __('shop::app.checkout.cart.remove-link') }}" onclick="removeLink('{{ __('shop::app.checkout.cart.cart-remove-action') }}')">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>

                                {!! view_render_event('bagisto.shop.checkout.name.before', ['item' => $item]) !!}
                                <td colspan="2" scope="row">{{ $item->product->name }}</td>
                                {!! view_render_event('bagisto.shop.checkout.name.after', ['item' => $item]) !!}
                                
                                {!! view_render_event('bagisto.shop.checkout.quantity.before', ['item' => $item]) !!}
                                <td class="text-center">{{ $item->quantity }}</td>
                                {!! view_render_event('bagisto.shop.checkout.quantity.after', ['item' => $item]) !!}

                                {!! view_render_event('bagisto.shop.checkout.price.before', ['item' => $item]) !!}
                                <td class="text-center">{{ core()->currency($item->base_price) }}</td>
                                {!! view_render_event('bagisto.shop.checkout.price.after', ['item' => $item]) !!}

                                @if (count($cart->items) == ($item_index + 1))
                                    <td class="text-right">{{ core()->currency($cart->base_sub_total) }}</td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach

                        @if ($cart->selected_shipping_rate)
                            <tr>
                                <td colspan="4" scope="row"></td>
                                <td class="text-right">{{ __('shop::app.checkout.total.delivery-charges') }}</td>
                                <td class="text-right">{{ core()->currency($cart->selected_shipping_rate->base_price) }}</td>
                            </tr>
                        @endif

                        @if ($cart->base_tax_total)
                            @foreach (Webkul\Tax\Helpers\Tax::getTaxRatesWithAmount($cart, true) as $taxRate => $baseTaxAmount )
                                <tr>
                                    <td colspan="4" scope="row"></td>
                                    <td class="text-right">{{ __('shop::app.checkout.total.tax') }} {{ $taxRate }} %</td>
                                    <td class="text-right">{{ core()->currency($baseTaxAmount) }}</td>
                                </tr>
                            @endforeach
                        @endif

                        @if ($cart->base_discount_amount && $cart->base_discount_amount > 0)
                            <tr>
                                <td colspan="4" scope="row"></td>
                                <td class="text-right">{{ __('shop::app.checkout.total.disc-amount') }}</td>
                                <td class="text-right">-{{ core()->currency($cart->base_discount_amount) }}</td>
                            </tr>
                        @endif

                        <tr>
                            <td colspan="4" scope="row"></td>
                            <td class="text-right">{{ __('shop::app.checkout.total.grand-total') }}</td>
                            <td class="text-right">{{ core()->currency($cart->base_grand_total) }}</td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-12">
            <div class="heading subtitle">
                <h2 class="title">{{ __('shop::app.checkout.cart.title') }}</h2>
            </div>            
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <p>{{ __('shop::app.checkout.cart.empty') }}</p>
        </div>
    </div>

    <div class="row justify-content-end">
        <div class="col-auto">
            <a href="{{ route('shop.home.index') }}" class="button button-arrow">{{ __('shop::app.checkout.cart.continue-shopping') }}</a>
        </div>
    </div>
    @endif    
</section>
@endsection



    