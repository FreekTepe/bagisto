@extends('shop::layouts.master-new')

@inject('headerHelper', 'App\Helpers\Header')

@section('page_title')
    {{ __('shop::app.checkout.onepage.title') }}
@stop

<?php
    $cart = $headerHelper->getCart();
    $billingAddress = !empty($headerHelper->getCart()->billingAddress) ? $headerHelper->getCart()->billingAddress : $headerHelper->getCleanAddressObject();
    $shippingAddress = !empty($headerHelper->getCart()->shippingAddress) ? $headerHelper->getCart()->shippingAddress : $headerHelper->getCleanAddressObject();
?>

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl onepage-checkout py-5">
        <form action="{{ route('shop.checkout.save-order') }}" method="POST" id="checkout-order-form" class="needs-validation" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" id="checkout-order-for-seller" name="checkout_order_for_seller" value="" />

            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 position-relative">
                    <div class="tab-switcher">
                        <div class="switcher-panes">
                            <div id="billing-address" class="switcher-pane show">
                                <div class="billing-address">
                                    <div class="row justify-content-center align-items-center mb-4">
                                        <div class="col-auto">
                                            <strong>Billing Address</strong>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_first_name" name="billing[first_name]" placeholder="First Name"  required>
                                                    <div class="invalid-feedback">Please Provide Your First Name</div>    
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_last_name" name="billing[last_name]" placeholder="Last Name" required>
                                                    <div class="invalid-feedback">Please Provide Your Last Name</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_company_name" name="billing[company_name]" placeholder="Company Name">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="email" class="form-control" id="billing_email" name="billing[email]" placeholder="Email address" required>
                                                    <div class="invalid-feedback">Please Provide A Valid Email Address</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_address1" name="billing[address1][]" placeholder="Address" value="{{ $billingAddress->address1 }}" required>
                                                    <div class="invalid-feedback">Please Provide Your Address</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_city" name="billing[city]" placeholder="City" value="{{ $billingAddress->city }}" required>
                                                    <div class="invalid-feedback">Please Provide Your City</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <select class="form-control" id="billling_country" name="billing[country]" required>
                                                        <option class="placeholder" value="" selected disabled>Select Country</option>
                                                        @foreach(core()->countries() as $country)
                                                            <option value="{{ $country->code }}">{{ $country->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Please Provide Your Country</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_state" name="billing[state]" placeholder="State" value="{{ $billingAddress->state }}" required>
                                                    <div class="invalid-feedback">Please Provide Your State</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_postcode" name="billing[postcode]" placeholder="Postal Code" value="{{ $billingAddress->postcode }}" required>
                                                    <div class="invalid-feedback">Please Provide Your Postal Code</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="billing_phone" name="billing[phone]" placeholder="Phone Number" value="{{ $billingAddress->phone }}" required>
                                                    <div class="invalid-feedback">Please Provide Your Phone Number</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input checkbox-use-for-shipping" id="billing_use_for_shipping" name="billing_use_for_shipping" checked>
                                                        <label for="billing_use_for_shipping" class="form-check-label">Ship To This Address</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                
                                </div>
                            </div>

                            <div id="shipping-address" class="switcher-pane">
                                <div class="shipping-address">
                                    <div class="row justify-content-center align-items-center mb-4">
                                        <div class="col-auto">
                                            <strong>Shipping Address</strong>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="shipping_first_name" name="shipping[first_name]" placeholder="First Name" value="{{ $shippingAddress->first_name }}">
                                                    <div class="invalid-feedback">Please Provide Your First Name</div>    
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="shipping_last_name" name="shipping[last_name]" placeholder="Last Name" value="{{ $shippingAddress->last_name }}">
                                                    <div class="invalid-feedback">Please Provide Your Last Name</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="shipping_address1" name="shipping[address1][]" placeholder="Address" value="{{ $shippingAddress->address1 }}">
                                                    <div class="invalid-feedback">Please Provide Your Address</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="shipping_city" name="shipping[city]" placeholder="City" value="{{ $shippingAddress->city }}">
                                                    <div class="invalid-feedback">Please Provide Your City</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <select class="form-control" id="shipping_country" name="shipping[country]">
                                                        @foreach(core()->countries() as $country)
                                                            <option value="{{ $country->code }}">{{ $country->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Please Provide Your Country</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="shipping_state" name="shipping[state]" placeholder="State" value="{{ $shippingAddress->state }}">
                                                    <div class="invalid-feedback">Please Provide Your State</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="shipping_postcode" name="shipping[postcode]" placeholder="Postal Code" value="{{ $shippingAddress->postcode }}">
                                                    <div class="invalid-feedback">Please Provide Your Postal Code</div>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="text" class="form-control" id="shipping_phone" name="shipping[phone]" placeholder="Phone Number" value="{{ $shippingAddress->phone }}">
                                                    <div class="invalid-feedback">Please Provide Your Phone Number</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>            
                                </div>
                            </div>
                        </div>
                    
                        <div class="switcher-tabs">
                            <div class="d-flex justify-content-center align-items-center">
                                <div class="d-inline-block w-50 switcher-tab tab-billing active" data-target="#billing-address">
                                    Billing address
                                </div>
                                
                                <div class="d-inline-block w-50 switcher-tab tab-shipping" data-target="#shipping-address">
                                    Shipping address
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-8">
                    <div class="row">
                        <div class="col-12 cart-sellers-container">
                            @foreach ($cart->sellerCarts() as $sellerCart)
                                @php
                                    $sellerCart->collectTotals();
                                @endphp

                                <div class="seller-container mb-5">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div class="seller-info">
                                                <div class="info-header row justify-content-start align-items-center">
                                                    <div class="col-auto mr-auto info-name">{{ $sellerCart->seller->shop_title }}</div>
                                                </div>
                                            </div>

                                            <div class="seller-cart-items">
                                                @foreach($sellerCart->items()->get() as $item)
                                                    @php
                                                        view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]);
                                                        view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.before', ['item' => $item]);
                                                        view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]);
                                                    @endphp

                                                    <div class="cart-item d-flex justify-content-start align-items-center">
                                                        <div class="item-image col-auto">
                                                            <img src="{{ $item->product->getPrimaryImage() }}" alt="{{ $item->name }}" />
                                                        </div>

                                                        <div class="item-guantity col-auto">
                                                            {{ $item->quantity }} X
                                                        </div>

                                                        <div class="col item-info p-0">
                                                            <div class="row align-items-center">
                                                                <div class="col-12 info-name">
                                                                    {{ $item->product->name }}
                                                                </div>
                                                            </div>

                                                            <div class="row align-items-center">
                                                                <div class="col-12 info-remove-price">
                                                                    <div class="row align-items-start">
                                                                        <a class="col-auto info-remove" href="{{ route('shop.checkout.cart.remove', $item->id) }}" alt="{{ __('shop::app.checkout.cart.remove-link') }}" onclick="removeLink('{{ __('shop::app.checkout.cart.cart-remove-action') }}')">
                                                                            <i class="fa fa-trash remove-icon"></i>
                                                                        </a>

                                                                        <div class="col-auto ml-auto info-price">
                                                                            {{ $item->quantity }} X {{ core()->currency($item->base_price) }} = {{ core()->currency($item->base_total) }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @php
                                                        view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]);
                                                        view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.after', ['item' => $item]);
                                                        view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]);
                                                    @endphp
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6">
                                            <div id="seller-checkout-accordion-container">
                                                <div id="seller-checkout-accordion-{{ $sellerCart->seller->id }}" class="accordion seller-checkout">
                                                    <div  id="checkout-shipping-card-{{ $sellerCart->seller->id }}" class="card">
                                                        <div class="card-header" id="header-shipping-options-{{ $sellerCart->seller->id }}">
                                                            <div class="d-flex justify-content-start align-items-center text-left header-text" type="button" data-toggle="collapse" data-target="#checkout-shipping-options-{{ $sellerCart->seller->id }}" aria-expanded="false" aria-controls="checkout-shipping-options-{{ $sellerCart->seller->id }}">
                                                                <span>Shipping options</span>
                                                                <span class="ml-1 mr-auto header-selected-options">(<span id="shipping-selected-options-{{ $sellerCart->seller->id }}">0</span>)</span>
                                                                <i class="fa fa-chevron-down"></i>
                                                            </div>
                                                        </div>

                                                        <div id="checkout-shipping-options-{{ $sellerCart->seller->id }}" class="collapse" aria-labbelledby="header-shipping-options-{{ $sellerCart->seller->id }}" data-parent="#seller-checkout-accordion-{{ $sellerCart->seller->id }}">
                                                            <div class="card-body checkout-shipping-container">
                                                                @foreach($sellerCart->seller->getShippingMethods() as $method_index => $method)
                                                                    @php
                                                                        $shipping_rate = $method->calculate();
                                                                    @endphp

                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="custom-control custom-radio">
                                                                                <input type="radio" data-seller-id="{{ $sellerCart->seller->id }}" class="custom-control-input shipping-method" id="{{ $method->getCode() }}-{{ $sellerCart->seller->id }}" name="shipping_method" value="{{ $method->getCode() }}" aria-describedby="{{ $method->getCode() }}-description-{{ $sellerCart->seller->id }}" required>
                                                                                <label for="{{ $method->getCode() }}-{{ $sellerCart->seller->id }}" class="custom-control-label">{{ $method->getTitle() }} ({{ core()->currency($shipping_rate->price) }})</label>
                                                                                <div class="invalid-feedback">Choose Shipping Method</div>
                                                                                <small id="{{ $method->getCode() }}-description-{{ $sellerCart->seller->id }}" class="form-text text-muted">{{ $method->getDescription() }}</small>
                                                                            </div>    
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="checkout-payment-card-{{ $sellerCart->seller->id }}" class="card">
                                                        <div class="card-header" id="header-payment-options-{{ $sellerCart->seller->id }}">
                                                            <div class="d-flex justify-content-start align-items-center text-left header-text" type="button" data-toggle="collapse" data-target="#checkout-payment-options-{{ $sellerCart->seller->id }}" aria-expanded="false" aria-controls="checkout-payment-options-{{ $sellerCart->seller->id }}">
                                                                <span>Payment options</span>
                                                                <span class="ml-1 mr-auto header-selected-options">(<span id="payment-selected-options-{{ $sellerCart->seller->id }}">0</span>)</span>
                                                                <i class="fa fa-chevron-down"></i>
                                                            </div>
                                                        </div>

                                                        <div id="checkout-payment-options-{{ $sellerCart->seller->id }}" class="collapse" aria-labbelledby="header-payment-options-{{ $sellerCart->seller->id }}" data-parent="#seller-checkout-accordion-{{ $sellerCart->seller->id }}">
                                                            <div class="card-body checkout-payment-container">
                                                                @if ($sellerCart->seller->hasPaymentMethods())
                                                                    @foreach($sellerCart->seller->getPaymentMethods() as $method)
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" data-seller-id="{{ $sellerCart->seller->id }}" class="custom-control-input payment-method" id="{{ $method->slug }}-{{ $sellerCart->seller->id }}" name="payment_method" value="{{ $method->slug }}" aria-describedby="{{ $method->slug }}-description-{{ $sellerCart->seller->id }}" required>
                                                                                    <label for="{{ $method->slug }}-{{ $sellerCart->seller->id }}" class="custom-control-label">{{ $method->title }}</label>
                                                                                    <div class="invalid-feedback">Choose Payment Method</div>
                                                                                    <small id="{{ $method->slug }}-description-{{ $sellerCart->seller->id }}" class="form-text text-muted">{{ $method->description }}</small>
                                                                                </div>    
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="checkout-totals mt-4">
                                                        <div class="row">
                                                            <div class="col-12 text-center">
                                                                <strong>Cart totals</strong>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-6 text-left">
                                                                Subtotal
                                                            </div>

                                                            <div class="col-6 text-right">
                                                                {{ core()->currency($sellerCart->base_sub_total) }}
                                                            </div>
                                                        </div>

                                                        @if ($sellerCart->selected_shipping_rate)
                                                            <div class="row">
                                                                <div class="col-6 text-left">
                                                                    {{ __('shop::app.checkout.total.delivery-charges') }}
                                                                </div>

                                                                <div class="col-6 text-right">
                                                                    {{ core()->currency($sellerCart->selected_shipping_rate->base_price) }}
                                                                </div>
                                                            </div>
                                                        @endif

                                                        @if ($sellerCart->base_tax_total)
                                                            @foreach (Webkul\Tax\Helpers\Tax::getTaxRatesWithAmount($sellerCart, true) as $taxRate => $baseTaxAmount)
                                                                <div class="row">
                                                                    <div class="col-6 text-left">
                                                                        {{ __('shop::app.checkout.total.tax') }} {{ $taxRate }} %
                                                                    </div>

                                                                    <div class="col-6 text-right">
                                                                        {{ core()->currency($baseTaxAmount) }}
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif

                                                        @if ($sellerCart->base_discount_amount && $sellerCart->base_discount_amount > 0)
                                                            <div class="row">
                                                                <div class="col-6 text-left">
                                                                    {{ __('shop::app.checkout.total.disc-amount') }}
                                                                </div>

                                                                <div class="col-6 text-right">
                                                                    -{{ core()->currency($sellerCart->base_discount_amount) }}
                                                                </div>
                                                            </div>
                                                        @endif

                                                        <div class="row">
                                                            <div class="col-6 text-left">
                                                                <strong>{{ __('shop::app.checkout.total.grand-total') }}</strong>
                                                            </div>

                                                            <div class="col-6 text-right">
                                                                <strong>{{ core()->currency($sellerCart->base_grand_total) }}</strong>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="checkout-buttons">
                                                        <div class="row justify-content-center align-items-center mt-4 shopping-mini-cart-buttons">
                                                            <div class="col-12 col-lg-auto">
                                                                <button type="submit" data-seller-id="{{ $sellerCart->seller->id }}" class="button button-bag button-submit-order">{{ __('shop::app.minicart.checkout') }}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/checkout.js') }}"></script>
@endpush