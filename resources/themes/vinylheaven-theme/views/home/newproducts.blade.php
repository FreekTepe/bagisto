@inject ('marketplaceProduct', 'App\Models\ExtendWebkul\MarketplaceProduct')

@php
    $latestsProducts = $marketplaceProduct->getNewestProducts(12);
@endphp

<section class="container-fluid container-lg mb-5 new-records">
    <div class="heading">
        <div class="heading-text medium">New Records</div>
    </div>

    <div class="row justify-content-center align-items-center">
        @if ($latestsProducts->count())
            @foreach ($latestsProducts as $product)
                {!! view_render_event('bagisto.shop.sellers.products.offers.before', ['product' => $product->product]) !!}

                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                    @include('shop::products.card')
                </div>

                {!! view_render_event('bagisto.shop.sellers.products.offers.after', ['product' => $product]) !!}
            @endforeach
        @endif
    </div>

    <div class="row justify-content-end">
        <div class="col col-md-auto">
            <a href="{{ route('shop.search.index') }}?term=" class="button button-arrow">See All Records</a>
        </div>
    </div>
</section>

@section('info-image-background-css')
    <style>
        @foreach ($latestsProducts as $product)
            @if (isset($product->product->url_key) && !empty($product->product->url_key))
                .product-card .card-info .info-image .image-holder.info-image-{{ $product->product->url_key }} {
                    background-image: url('{{ $product->product->getPrimaryImage() }}');
                }
            @endif
        @endforeach
    </style>
@endsection