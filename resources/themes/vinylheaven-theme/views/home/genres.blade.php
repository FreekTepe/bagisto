@inject('header_helper', 'App\Helpers\Header')

@php
    $genres = $header_helper->getGenres();
@endphp

<section class="genres container mb-5">
    <div class="heading">
        <div class="heading-text medium">Genres</div>
    </div>

    <div class="genres-image-holder justify-content-center">
        @foreach ($genres as $genre_index => $genre)
            @if (count($genre->marketplaceProducts) > 0)
                <div class="{{ $header_helper->getGenreConfig($genre_index) }}">
                    <img src="{{ asset($header_helper->getGenreImage($genre->name)) }}" alt="">
                </div>
            @endif
        @endforeach
    </div>

    <ul class="genres-list justify-content-center">
        @foreach ($genres as $genre_index => $genre)
            @if (count($genre->marketplaceProducts) > 0)
                <li id="{{ str_replace(' genres-image', '', $header_helper->getGenreConfig($genre_index)) }}">
                    <a href="#">{{ $genre->name }}<sup>{{ count($genre->marketplaceProducts) }}</sup></a>
                </li>
            @endif
        @endforeach
    </ul>
</section>

@push('scripts')
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/genres-popover.js') }}"></script>
@endpush