<section class="slider mb-5" data-component="slider">
    <div class="slider__repeat">
        <div class="slider__holder" style="background-image: url({{ asset('themes/vinylheaven-theme/assets/slider/1.jpg') }});">
            <div class="slider__caption">
                <div class="slider__title">Your one stop shop for all Vinyl-related collectables</div>
                <div class="slider__count">{{ $total_products }}</div>
                <a class="button slider__button" href="#">Records</a>
            </div>
        </div>
    </div>

    <div class="slider__repeat">
        <div class="slider__holder" style="background-image: url({{ asset('themes/vinylheaven-theme/assets/slider/2.jpg') }});">
            <div class="slider__caption">
                <div class="slider__title">Your one stop shop for all Vinyl-related collectables</div>
                <div class="slider__count">{{ $total_products }}</div>
                <a class="button slider__button" href="#">Records</a>
            </div>
        </div>
    </div>

    <div class="slider__repeat">
        <div class="slider__holder" style="background-image: url({{ asset('themes/vinylheaven-theme/assets/slider/3.jpg') }});">
            <div class="slider__caption">
                <div class="slider__title">Your one stop shop for all Vinyl-related collectables</div>
                <div class="slider__count">{{ $total_products }}</div>
                <a class="button slider__button" href="#">Records</a>
            </div>
        </div>
    </div>

    <div class="slider__repeat">
        <div class="slider__holder" style="background-image: url({{ asset('themes/vinylheaven-theme/assets/slider/4.jpg') }});">
            <div class="slider__caption">
                <div class="slider__title">Your one stop shop for all Vinyl-related collectables</div>
                <div class="slider__count">{{ $total_products }}</div>
                <a class="button slider__button" href="#">Records</a>
            </div>
        </div>
    </div>
</section>

@push('scripts')
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/vendor/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/slider.js') }}"></script>
@endpush