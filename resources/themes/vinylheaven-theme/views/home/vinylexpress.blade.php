@php
    $limit = 50;
    $products = app('Webkul\Product\Repositories\ProductRepository')->getVinylExpressProducts($limit);
    // dd($products);
    $products_count = count($products);

    $product_images = [
        'album-cover.jpg',
        'second-img.jpg',
        'sour-soul.jpg',
        'bbc.jpg',
        'black-star.jpg',
        'instant-image.jpg',
        'seeds.jpg',
        'whole-love.jpg',
        'looking.jpg',
    ];
@endphp

<section class="search-result container">
    <div class="search-result__top">
        <div class="search-result__name">
            <div class="search-result__results">{{ $products_count }} Results For:</div>
            Rock
        </div>

        <div class="search-result__sorting">
            <div class="search-result__sort">
                <span class="search-result__text">Sorted by</span>
                <select class="search-result__dropdown">
                    <option>Relevance</option>
                    <option>Relevance1</option>
                    <option>Relevance2</option>
                </select>
            </div>

            <div class="search-result__view">
                <span class="search-result__text">View</span>
                <a href="#" class="search-result__icon is-active"><img src="{{ asset('themes/searchelastic/assets/img/grid-icon.png') }}" alt=""/></a>
                <a href="#" class="search-result__icon"><img src="{{ asset('themes/searchelastic/assets/img/list-Icon.png') }}" alt=""/></a>
            </div>
        </div>
    </div>

    <div class="search-result__bottom">
        <div class="row">
            <div class="col-md-4 col-lg-3 col-sm-12 col-12">
                <span class="search-result__show-filter js-filter">Show Filters <i class="fa fa-chevron-down"
						                                                                   aria-hidden="true"></i></span>
                <div class="search-result__filter-area js-filter-area">

                    <h4 class="search-result__title">Filter by</h4>
                    <ul class="search-result__filter">
                        <li><a href="#"><span>Lp</span> <i class="fa fa-times" aria-hidden="true"></i></a></li>
                        <li><a href="#"><span>Cd</span> <i class="fa fa-times" aria-hidden="true"></i></a></li>
                        <li><a href="#"><span>13"</span> <i class="fa fa-times" aria-hidden="true"></i></a></li>
                        <li><a href="#"><span>Electronic</span> <i class="fa fa-times"
                                                                    aria-hidden="true"></i></a></li>
                        <li><a href="#"><span>Jazz</span> <i class="fa fa-times" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <a href="#" class="search-result__clear">Clear All</a>
                    <div class="search-result__price-filter">
                        <h5 class="search-result__head js-fold-click">Price</h5>
                        <div class="js-fold">
                            <input type="text" placeholder="Minimum Price"/>
                            <input type="text" placeholder="Maximum Price"/>
                            <input type="submit" value="Update"/>
                        </div>
                    </div>
                    <div class="search-result__format-filter">
                        <h5 class="search-result__head js-fold-click">Format Size</h5>
                        <div class="js-fold">
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check1">
                                <label class="css-label" for="check1">12"-(1583)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check2">
                                <label class="css-label" for="check2">CD-(339)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check3">
                                <label class="css-label" for="check3">7"-(284)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check4">
                                <label class="css-label" for="check4">DVD-(12)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check5">
                                <label class="css-label" for="check5">10"-(4)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check6">
                                <label class="css-label" for="check6">CDr-(2)</label>
                            </div>
                        </div>
                    </div>
                    <div class="search-result__condition-filter">
                        <h5 class="search-result__head js-fold-click">Condition</h5>
                        <div class="js-fold">
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check11">
                                <label class="css-label" for="check11"><span
                                    class="search-result__rate blue">SS</span> Still
                                    Sealed(9,321)</label>
                                <a href="#" class="search-result__info"><i class="fa fa-info-circle"
                                                                            aria-hidden="true"></i>
                                    <div class="search-result__tooltip">The grade of the media based on the
                                        Goldmine
                                        grading standards.
                                    </div>
                                </a>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check21">
                                <label class="css-label" for="check21"><span
                                    class="search-result__rate green">M</span> CD (339)</label>
                                <a href="#" class="search-result__info"><i class="fa fa-info-circle"
                                                                            aria-hidden="true"></i>
                                    <div class="search-result__tooltip">The grade of the media based on the
                                        Goldmine
                                        grading standards.
                                    </div>
                                </a>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check31">
                                <label class="css-label" for="check31"><span
                                    class="search-result__rate light-green">Ex</span> 7"
                                    (284)</label>
                                <a href="#" class="search-result__info"><i class="fa fa-info-circle"
                                                                            aria-hidden="true"></i>
                                    <div class="search-result__tooltip">The grade of the media based on the
                                        Goldmine
                                        grading standards.
                                    </div>
                                </a>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check41">
                                <label class="css-label" for="check41"><span
                                    class="search-result__rate highlight-green">Vg</span> DVD
                                    (12)</label>
                                <a href="#" class="search-result__info"><i class="fa fa-info-circle"
                                                                            aria-hidden="true"></i>
                                    <div class="search-result__tooltip">The grade of the media based on the
                                        Goldmine
                                        grading standards.
                                    </div>
                                </a>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check51">
                                <label class="css-label" for="check51"><span
                                    class="search-result__rate yellow">G</span> 10" (4)</label>
                                <a href="#" class="search-result__info"><i class="fa fa-info-circle"
                                                                            aria-hidden="true"></i>
                                    <div class="search-result__tooltip">The grade of the media based on the
                                        Goldmine
                                        grading standards.
                                    </div>
                                </a>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check61">
                                <label class="css-label" for="check61"><span
                                    class="search-result__rate orange">F</span> CDr (2)</label>
                                <a href="#" class="search-result__info"><i class="fa fa-info-circle"
                                                                            aria-hidden="true"></i>
                                    <div class="search-result__tooltip">The grade of the media based on the
                                        Goldmine
                                        grading standards.
                                    </div>
                                </a>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check71">
                                <label class="css-label" for="check71"><span
                                    class="search-result__rate red">P</span> CDr (2)</label>
                                <a href="#" class="search-result__info"><i class="fa fa-info-circle"
                                                                            aria-hidden="true"></i>
                                    <div class="search-result__tooltip">The grade of the media based on the
                                        Goldmine
                                        grading standards.
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="search-result__format-filter">
                        <h5 class="search-result__head js-fold-click">Genre</h5>
                        <div class="js-fold">
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check80">
                                <label class="css-label" for="check80">Blues (148.67)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check81">
                                <label class="css-label" for="check81">Classical (148.67)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check82">
                                <label class="css-label" for="check82">Electronic (148.67)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check83">
                                <label class="css-label" for="check83">Folk, World & Country (148.67)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check84">
                                <label class="css-label" for="check84">Funk/Soul (148.67)</label>
                            </div>
                            <div class="js-more" style="display:none;">
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check85">
                                    <label class="css-label" for="check85">HipHop (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check86">
                                    <label class="css-label" for="check86">Jazz (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check88">
                                    <label class="css-label" for="check88">Latin (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check89">
                                    <label class="css-label" for="check89">Pop (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check911">
                                    <label class="css-label" for="check911">Reggae (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check922">
                                    <label class="css-label" for="check922">Rock (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check923">
                                    <label class="css-label" for="check923">Stage & Screen (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check924">
                                    <label class="css-label" for="check924">Non-Music (148.67)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check925">
                                    <label class="css-label" for="check925">Brass & Military (674)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check926">
                                    <label class="css-label" for="check926">Children's (74)</label>
                                </div>
                            </div>
                            <a href="#" class="search-result__button js-more-click"><span class="search-result__show">Show More</span><span class="search-result__less">Show Less</span></a>
                        </div>
                    </div>

                    <div class="search-result__format-filter">
                        <h5 class="search-result__head js-fold-click">Label</h5>
                        <div class="js-fold">
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check90">
                                <label class="css-label" for="check90">Colombia (4,321)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check91">
                                <label class="css-label" for="check91">CBS (3,701)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check92">
                                <label class="css-label" for="check92">Capital Records (521)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check93">
                                <label class="css-label" for="check93">Epic (1,598)</label>
                            </div>
                            <div class="search-result__check">
                                <input class="css-checkbox" type="checkbox" id="check94">
                                <label class="css-label" for="check94">Polydor (2,458)</label>
                            </div>
                            <div class="js-more" style="display:none;">
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check95">
                                    <label class="css-label" for="check95">Colombia (4,321)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check96">
                                    <label class="css-label" for="check96">CBS (3,701)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check97">
                                    <label class="css-label" for="check97">Capital Records (521)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check98">
                                    <label class="css-label" for="check98">Epic (1,598)</label>
                                </div>
                                <div class="search-result__check">
                                    <input class="css-checkbox" type="checkbox" id="check99">
                                    <label class="css-label" for="check99">Polydor (2,458)</label>
                                </div>
                            </div>
                            <a href="#" class="search-result__button js-more-click"><span class="search-result__show">Show More</span><span class="search-result__less">Show Less</span></a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-8 col-lg-9 col-sm-12 col-12">
                <div class="search-result__grid-view">
                    <div class="row">
                        @if ($products_count)
                            @foreach ($products as $product_index => $product)
                                <div class="col-lg-4 col-md-6 col-sm-6 col-6 search-result__grid-list">
                                    <div class="card-products">
                                        <a href="#" class="card-products__list">
                                            <div class="card-products__area">
                                                <div class="card-products__year">2001-10"</div>
                                                <div class="card-products__records">5</div>
                                            </div>
                                            <div class="card-products__image">
                                                <img src="{{ asset('themes/searchelastic/assets/img/' . $product_images[rand(0, 8)]) }}" alt=""/>
                                            </div>
                                            <div class="card-products__content">
                                                <div class="card-products__title">{{ $product->name }}</div>
                                                <div class="card-products__category">- ACDC</div>
                                                <div class="card-products__price">From € {{ str_replace('.', ',', number_format($product->special_price, 2, ',', '')) }}</div>
                                            </div>
                                        </a>
                                        <a class="card-products__bag" href="#">Add to my bag</a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>