@extends('shop::layouts.master-new')

@php
    $total_products = app('App\Repositories\VinylExpressProductRepository')->getTotalProducts();
@endphp

@section('content-wrapper')

    @include("shop::home.slider")
    @include("shop::home.newproducts")
    @include("shop::home.genres")

@endsection