<section class="usps usps__mb-hide">
    <div class="container">
        <ul class="usps__listing">
            <li>
                <span class="usps__list"><img src="{{ asset('themes/vinylexpress/assets/src/img/payment-icon.png') }}" alt="" /> Secure Online Payment</span>
            </li>

            <li>
                <span class="usps__list"><img src="{{ asset('themes/vinylexpress/assets/src/img/free-shipping.png') }}" alt="" /> Free Shipping Over €20.-</span>
            </li>
            
            <li>
                <span class="usps__list"><img src="{{ asset('themes/vinylexpress/assets/src/img/free-return-icon.png') }}" alt="" /> Free Return</span>
            </li>

            <li>
                <span class="usps__list"><img src="{{ asset('themes/vinylexpress/assets/src/img/customer-icon.png') }}" alt="" /> Customer Service</span>
            </li>
            
            <li>
                <span class="usps__list"><img src="{{ asset('themes/vinylexpress/assets/src/img/tp-icon.png') }}" alt="" /></span>
            </li>
        </ul>
    </div>
    
</section>