@extends('shop::layouts.master-new')

@section('page_title')
    {{ __('shop::app.customer.signup-form.page-title') }}
@endsection

@php
    $form_has_errors = false;
    $email_error = '';
    $email_invalid_class = '';

    if (session()->has('errors')) {
        $form_errors = session()->get('errors');
        
        if ($form_errors->hasBag('default')) {
            $form_error_bag = $form_errors->getBag('default');
            if ($form_error_bag->has('email')) {
                $form_has_errors = true;
                $email_invalid_class = 'is-invalid';
                $email_error = $form_error_bag->get('email')[0];
            }
        }
    }
@endphp

@section('content-wrapper')
<section class="container-fluid container-lg container-xl container-register">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <div class="row justify-content-center align-items-center">
                <div class="col-auto">
                    <div class="mt-3 mb-4 my-md-3 mb-lg-5">
                        {{ __('shop::app.customer.signup-text.account_exists') }} - <a href="{{ route('customer.session.index') }}">{{ __('shop::app.customer.signup-text.title') }}</a>
                    </div>
                </div>
            </div>

            {!! view_render_event('bagisto.shop.customers.signup.before') !!}

            <div class="row">
                <div class="col-12">
                    <form class="needs-validation sign-up-form" method="post" action="{{ route('customer.register.create') }}" novalidate>
                        {{ csrf_field() }}
                        
                        <div class="form-row">
                            <div class="col-12">
                                <strong>{{ __('shop::app.customer.signup-form.title') }}</strong>
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.before') !!}

                        <div class="form-row">
                            <div class="col-12">
                                <label></label>
                                <input type="text" id="first_name" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="{{ __('shop::app.customer.signup-form.firstname') }}" required>
                                <div class="invalid-feedback">
                                    Please Provide First Name
                                </div>
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.firstname.after') !!}

                        <div class="form-row">
                            <div class="col-12">
                                <label></label>
                                <input type="text" id="last_name" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="{{ __('shop::app.customer.signup-form.lastname') }}" required>
                                <div class="invalid-feedback">
                                    Please Provide Last Name
                                </div>
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.lastname.after') !!}

                        <div class="form-row">
                            <div class="col-12">
                                <label></label>
                                <input type="email" id="email" class="form-control {{ $email_invalid_class }}" name="email" value="{{ old('email') }}" placeholder="{{ __('shop::app.customer.signup-form.email') }}" required>
                                @if (!$form_has_errors)
                                    <div class="invalid-feedback">
                                        Please Provide An Valid Email Address
                                    </div>
                                @else
                                    @if (!empty($email_error))
                                        <div class="invalid-feedback">
                                            {{ $email_error }}
                                        </div>
                                    @else
                                        <div class="invalid-feedback">
                                            Please Provide An Valid Email Address
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.email.after') !!}

                        <div class="form-row">
                            <div class="col-12">
                                <label></label>
                                <input type="password" id="password" class="form-control" name="password" placeholder="{{ __('shop::app.customer.signup-form.password') }}" required>
                                <div class="invalid-feedback">
                                    Please Provide A Password
                                </div>
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.password.after') !!}

                        <div class="form-row">
                            <div class="col-12">
                                <label></label>
                                <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" placeholder="{{ __('shop::app.customer.signup-form.confirm_pass') }}" required>
                                <div class="invalid-feedback">
                                    Please Provide The Correct Password Confirmation
                                </div>
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.customers.signup_form_controls.after') !!}

                        <div class="form-row">
                            <div class="col-12">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" required>
                                    <label class="form-check-label">{{ __('shop::app.customer.signup-form.agree') }} <a href="">{{ __('shop::app.customer.signup-form.terms') }}</a> & <a href="">{{ __('shop::app.customer.signup-form.conditions') }}</a> {{ __('shop::app.customer.signup-form.using') }}.</label>
                                    <div class="invalid-feedback">
                                        You Must Agree Before Submitting
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-12 mt-3">
                                <button type="submit" class="button">{{ __('shop::app.customer.signup-form.button_title') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            {!! view_render_event('bagisto.shop.customers.signup.after') !!}
        </div>
    </div>
</section>
@endsection

@push('scripts')
    <script type="text/javascript">
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                var submit_form = false;
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (form.checkValidity() === false) {
                    
                    } else {
                        if ($('#seller-shop-url').length) {
                            if ($('#seller-shop-url').prop('required')) {
                                if (!submit_form) {
                                    $.post("{{ route('marketplace.seller.url') }}", {url:$('#seller-shop-url').val(), _token:$('#seller-shop-url').data('csrf')})
                                    .then(function(response) {
                                        if (response.available) {
                                            submit_form = true;
                                            $(form).submit();
                                        } else {
                                            if (!$('#seller-shop-url').hasClass('is-invalid')) {
                                                $('#seller-shop-url').addClass('is-invalid');
                                            }
                                        }
                                    }).catch(function(error) {
                                        console.log('ERROR');
                                        console.log(error);
                                    });
                                } else {
                                    $(form).submit();
                                }
                            }
                        }
                    }

                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
        })();
    </script>
@endpush