@extends('shop::layouts.master-new')

@section('page_title')
    {{ __('shop::app.customer.forgot-password.page_title') }}
@stop

@php
    if (session()->has('errors')) {
        session()->flash('error', session()->get('errors')->getBag('default')->first('email'));
    }
@endphp

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-login">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="row justify-content-center align-items-center">
                    <div class="col-auto">
                        <div class="mt-3 mb-4 my-md-3 mb-lg-5">
                            {{ __('velocity::app.customer.forget-password.forgot-password')}} - <a href="{{ route('customer.session.index') }}">{{  __('velocity::app.customer.signup-form.login') }}</a>
                        </div>
                    </div>
                </div>

                {!! view_render_event('bagisto.shop.customers.forget_password.before') !!}

                <div class="row">
                    <div class="col-12">
                        <form class="needs-validation sign-in-form" method="post" action="{{ route('customer.forgot-password.store') }}" novalidate>
                            {{ csrf_field() }}

                            <div class="form-row">
                                <div class="col-12">
                                    <strong>{{ __('shop::app.customer.forgot-password.title') }}</strong>
                                </div>
                            </div>

                            {!! view_render_event('bagisto.shop.customers.forget_password_form_controls.before') !!}

                            <div class="form-row">
                                <div class="col-12">
                                    <label></label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ __('shop::app.customer.forgot-password.email') }}" required>
                                    <div class="invalid-feedback">
                                        Please Provide A Valid Email Address
                                    </div>
                                </div>
                            </div>

                            {!! view_render_event('bagisto.shop.customers.forget_password_form_controls.after') !!}

                            <div class="form-row justify-content-start align-items-center">
                                <div class="col-auto mt-3 mr-auto">
                                    <a href="{{ route('customer.session.index') }}" class="d-flex justify-content-center align-items-center">
                                        <i class="fa fa-chevron-left mr-3"></i>
                                        Back to login
                                    </a>
                                </div>

                                <div class="col-auto mt-3">
                                    <button type="submit" class="button">{{ __('shop::app.customer.forgot-password.submit') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                {!! view_render_event('bagisto.shop.customers.forget_password.after') !!}
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript">
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                var submit_form = false;
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }

                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
        })();
    </script>
@endpush