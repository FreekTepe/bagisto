<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>@yield('page_title') | VinylHeaven</title>

        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="robots" content="noindex, nofollow"/><!-- change into index, follow -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="content-language" content="{{ app()->getLocale() }}">
        
        {{-- @if ($token = \JWTAuth::fromUser(\Auth::guard('customer')->user()))
            <meta name="jwt" content="{{ $token }}">
        @endif --}}
        
        @meta_tags

        <meta name="description" content="Website description"/>
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        @if ($favicon = core()->getCurrentChannel()->favicon_url)
            <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
        @elseif (file_exists(public_path('themes/vinylheaven-theme/assets/img/favicon.ico')))
            <link rel="icon" sizes="16x16" href="{{ asset('themes/vinylheaven-theme/assets/img/favicon.ico') }}" />
        @else
            <link rel="icon" sizes="16x16" href="{{ bagisto_asset('images/favicon.ico') }}" />
        @endif

        @yield('head')

        @section('seo')
            @if (! request()->is('/'))
                <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
            @endif
        @show

        @stack('css')

        @yield('info-image-background-css')
        
        @yield('seller-profile-banner-logo-css')

        <link href="{{ asset('themes/vinylheaven-theme/assets/css/main.dev.css') }}" media="all" rel="stylesheet" type="text/css" />

        {!! view_render_event('bagisto.shop.layout.head') !!}
    </head>

    <body @if (core()->getCurrentLocale()->direction == 'rtl') class="rtl" @endif>
        {!! view_render_event('bagisto.shop.layout.body.before') !!}

        {!! view_render_event('bagisto.shop.layout.header.before') !!}

        @include('shop::layouts.header.index')

        {!! view_render_event('bagisto.shop.layout.header.after') !!}

        <main>
            {!! view_render_event('bagisto.shop.layout.content.before') !!}

                @yield('content-wrapper')

            {!! view_render_event('bagisto.shop.layout.content.after') !!}
        </main>

        @include('shop::layouts.footer.index');
        
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/js/vinylheaven.js') }}"></script>

        @stack('scripts')

        {!! view_render_event('bagisto.shop.layout.body.after') !!}

        <div class="modal-overlay"></div>

        <div class="alert-container" id="alert-container">
            @if (session()->has('error') || session()->has('success') || session()->has('errors'))
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <div class="container-fluid container-lg">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-12">{{ session()->get('error') }}</div>
                            </div>
                        </div>
                        
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <div class="container-fluid container-lg">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-12">{{ session()->get('success') }}</div>
                            </div>
                        </div>
                        
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if (session()->has('errors'))
                    <?php
                        $session_error_messages = [];
                        foreach (session()->get('errors')->getBags() as $errorBag) {
                            foreach ($errorBag->getMessages() as $messages) {
                                foreach ($messages as $message) {
                                    $session_error_messages[] = $message;
                                }
                            }
                        } 
                    ?>

                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <div class="container-fluid container-lg">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-12">
                                    <p>There are some errors:</p>

                                    <ul>
                                    @foreach ($session_error_messages as $message)
                                        <li>{{ $message }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            @endif
        </div>
    </body>
</html>