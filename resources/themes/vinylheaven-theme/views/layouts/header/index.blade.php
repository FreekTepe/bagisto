@inject('headerHelper', 'App\Helpers\Header')

@php
    $cart = $headerHelper->getCart();
    //dd('from header index blade', 'done my shit here', $cart, $cart->hasSellerCarts(), $cart->sellerCarts());
    
@endphp

<header class="header">
    <div class="container-fluid container-lg">
        <nav class="navbar navbar-expand-lg justify-content-start align-items-center">
            <button class="navbar-toggler pl-3 mr-2" type="button" data-toggle="collapse" data-target="#navbar-mobile" aria-controls="navbar-mobile" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="navbar-brand logo-container d-none d-lg-inline-block mr-2 mr-xl-auto" href="/">
                <img src="{{ asset('themes/vinylheaven-theme/assets/img/logo-vinyl-heaven3.png') }}" class="logo" alt="">
            </a>

            <a class="navbar-brand logo-container d-inline-block d-lg-none mr-auto ml-2" href="/">
                <img src="{{ asset('themes/vinylheaven-theme/assets/img/logo.png') }}" class="logo" alt="">
            </a>

            <div class="navbar-text has-collapse cursor-pointer text-font-weight-600 d-none d-lg-inline-flex justify-content-center align-items-center mx-2 mx-xl-auto" id="navbar-text-genres" data-toggle="collapse" data-target="#navbar-genres" aria-controls="navbar-genres" aria-expanded="false" aria-label="Toggle Genres">
                <span class="navbar-menu-text mr-1">Genres</span> <i class="fa fa-chevron-down ml-1"></i>
            </div>

            <div class="navbar-text navbar-search d-none d-xl-inline-block mx-auto">
                <form method="GET" action="/search" id="search-form" class="form search-form">
                    <div class="form-control-group">
                        <div class="input-group">
                            <input type="text" id="search-input" class="form-control search-input border-right-width-0" name="term" placeholder="Type and search...." autocomplete="off" data-url="{{ route('shop.search.autocomplete') }}" data-csrf="{{ csrf_token() }}" />
                            
                            <div class="input-group-append">
                                <button class="btn search-button border-left-width-0 border-radius-0" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="navbar-text cursor-pointer d-inline-block d-xl-none mx-2 ml-lg-auto mr-lg-2" data-toggle="collapse" data-target="#navbar-mobile-search" aria-controls="navbar-mobile-search" aria-expanded="false" aria-label="Toggle Mobile Search">
                <i class="fa fa-search"></i>
            </div>

            @guest('customer')
                <div class="navbar-text cursor-pointer d-lg-none mx-2" data-toggle="collapse" data-target="#navbar-mobile-login-register" aria-controls="navbar-mobile-login-register" aria-expanded="false" aria-label="Toggle Mobile Login Register">
                    <img src="{{ asset('themes/vinylheaven-theme/assets/img/user-icon.png') }}" alt="">
                </div>

                <div class="navbar-text d-none d-lg-inline-flex justify-content-center align-items-center mx-2">
                    <a href="{{ route('customer.session.index') }}" class="d-inline-block">Login</a><span class="d-inline-block mx-2">|</span><a href="{{ route('customer.register.index') }}" class="d-inline-block">Register</a>
                </div>
            @endguest

            @auth('customer')
                <div class="navbar-text mx-2">
                    <a href="{{ route('customer.profile.index') }}">
                        <img src="{{ asset('themes/vinylheaven-theme/assets/img/user-icon.png') }}" alt="">
                    </a>
                </div>
            @endauth

            @auth('customer')
                <div class="navbar-text cursor-pointer mx-2" data-toggle="collapse" data-target="#navbar-customer-wishlist" aria-controls="navbar-customer-wishlist" aria-expanded="false" aria-label="Toggle Customer WishList">
                    <img src="{{ asset('themes/vinylheaven-theme/assets/img/heart-icon.png') }}" alt="">
                </div>
            @endauth

            @if (core()->getCurrentChannel()->currencies->count() > 1)
                <div class="navbar-text text-font-weight-500 navbar-currency-switcher mx-2" id="navbar-currency">
                    <a href="#" class="d-inline-flex justify-content-center align-items-center" role="button" id="navbar-currency-switcher" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="currency-current mr-1">{{ core()->getCurrentCurrencyCode() }}</span> <i class="fa fa-chevron-down ml-1"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-currency-switcher">
                        @foreach (core()->getCurrentChannel()->currencies as $currency)
                            <a href="{{ $headerHelper->getCurrencyUrl($currency->code) }}" class="dropdown-item">{{ $currency->code }}</a>
                        @endforeach
                    </div>
                </div>
            @endif

            @if ($cart)
                <div class="navbar-text text-font-weight-500 cursor-pointer d-inline-flex justify-content-center align-items-center ml-2" id="navbar-text-minicart" data-toggle="collapse" data-target="#navbar-minicart" aria-controls="navbar-minicartt" aria-expanded="false" aria-label="Toggle MiniCart">
                    <img src="{{ asset('themes/vinylheaven-theme/assets/img/cart-icon.png') }}" alt=""><span class="ml-1 mx-md-1">({{ $cart->itemsQty() }})</span><span class="d-none d-md-inline">{{ core()->currency($cart->base_sub_total) }}</span>
                </div>
            @endif
        </nav>
    </div>

    <div class="navbar-mobile navbar-collapse collapse d-lg-none px-3" id="navbar-mobile">
        <ul class="navbar-nav justify-content-center flex-row flex-wrap align-items-center">
            @foreach ($headerHelper->getGenres() as $genre)
                @if (count($genre->marketplaceProducts) > 0)
                    <li class="nav-item list-item-genres d-inline-flex px-3 py-2">
                        <a href="/{{ $genre->slug }}" class="nav-item-link d-inline-block">{{ $genre->name }}<sup>{{ $genre->products_count }}</sup></a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>

    <div class="navbar-collapse has-transition navbar-minicart collapse border-0 pt-xl-4" data-trigger="#navbar-text-minicart" id="navbar-minicart">
        @include('shop::layouts.header.navbar.mini-cart')
    </div>

    <div class="navbar-collapse has-transition collapse border-0 pt-4" data-trigger="#navbar-text-genres" id="navbar-genres">
        <div class="container-fluid container-lg">
            <ul class="justify-content-center flex-row flex-wrap align-items-center">
                @foreach ($headerHelper->getGenres() as $genre)
                    @if (count($genre->marketplaceProducts) > 0)
                        <li class="d-inline-flex px-3 py-2">
                            <a href="/{{ $genre->slug }}" class="d-inline-block">{{ $genre->name }}<sup>{{ count($genre->marketplaceProducts) }}</sup></a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>

    <div class="navbar-collapse collapse border-0" id="navbar-mobile-login-register">
        <div class="d-flex justify-content-center align-items-center">
            <a href="{{ route('customer.session.index') }}" class="col col-lg-auto text-center px-lg-1">Login</a><span class="col-auto text-center px-lg-1">|</span><a href="{{ route('customer.register.index') }}" class="col col-lg-auto text-center px-lg-1">Register</a>
        </div>
    </div>

    <div class="navbar-collapse navbar-mobile-search collapse border-0" id="navbar-mobile-search">
        <div class="container-fluid container-lg pt-3">
            <form method="GET" action="/search" id="mobile-search-form" class="form search-form">
                <div class="form-control-group">
                    <div class="input-group">
                        <input type="text" id="mobile-search-input" class="form-control search-input border-right-width-0" name="term" placeholder="Type and search...." autocomplete="off" data-url="{{ route('shop.search.autocomplete') }}" data-csrf="{{ csrf_token() }}" />
                        
                        <div class="input-group-append">
                            <button class="btn search-button border-left-width-0 border-radius-0" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="navbar-collapse search-autocomplete collapse border-0 pt-2 pt-xl-4" id="search-autocomplete">
        <div class="container-fluid container-lg px-3">
            <div class="row mx-n3">
                <div class="col-12 col-lg-6 px-3">
                    <div class="autocomplete-header text-center mx-n3 mb-2">
                        Releases
                    </div>

                    <div id="autocomplete-releases" class="row"></div>
                </div>

                <div class="col-12 col-lg-6 px-3">
                    <div class="autocomplete-header text-center mx-n3 mb-2">
                        Artists
                    </div>

                    <div id="autocomplete-artists" class="row"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="navbar-collapse collapse customer-wishlist border-0" id="navbar-customer-wishlist"></div>
</header>

@push('scripts')
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/header/navbar.js') }}"></script>
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/search.js') }}"></script>
@endpush