<!DOCTYPE html>
<html>
    <head>
        <title>@yield('page_title') | VinylHeaven</title>

        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <meta name="description" content="Website description"/>
        <meta name="robots" content="noindex, nofollow"/><!-- change into index, follow -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            type="text/css">

        <link href="{{ asset('themes/vinylheaven-theme/assets/css/main.dev.css') }}" media="all" rel="stylesheet" type="text/css" />

        @if ($favicon = core()->getCurrentChannel()->favicon_url)
            <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
        @elseif (file_exists(public_path('themes/vinylheaven-theme/assets/img/favicon.ico')))
            <link rel="icon" sizes="16x16" href="{{ asset('themes/vinylheaven-theme/assets/img/favicon.ico') }}" />
        @else
            <link rel="icon" sizes="16x16" href="{{ bagisto_asset('images/favicon.ico') }}" />
        @endif

        @stack('css')

        {!! view_render_event('bagisto.shop.layout.head') !!}
    </head>

    <body>
        {!! view_render_event('bagisto.shop.layout.body.before') !!}

        <div class="wrapper">
            <header class="header container js-header-sticky">

                <a class="header__menu-toggle" href="#nav"><span></span></a>

                <a href="/" class="header__logo"><img src="{{ asset('themes/vinylheaven-theme/assets/img/logo.png') }}" alt=""></a>
                @php

                $categories = [];

                foreach (app('Webkul\Category\Repositories\CategoryRepository')->getVisibleCategoryTree(core()->getCurrentChannel()->root_category_id) as $category) {
                    if ($category->slug)
                        array_push($categories, $category);
                }
                //dd(core()->getCurrentChannel()->root_category_id);
                // dd($categories);
                @endphp

                <nav id="nav" class="header__menu">
                    <ul>
                        <li>
                            <a href="#">Genres</a>
                            <ul class="container container--small">
                                <li><a href="#">Blues<sup>148.674</sup></a></li>
                                <li><a href="#">Classical<sup>148.674</sup></a></li>
                                <li><a href="#">Electronic<sup>148.674</sup></a></li>
                                <li><a href="#">Folk, World, & Country<sup>148.674</sup></a></li>
                                <li><a href="#">Funk/Soul<sup>148.674</sup></a></li>
                                <li><a href="#">HipHop<sup>148.674</sup></a></li>
                                <li><a href="#">Jazz<sup>148.674</sup></a></li>
                                <li><a href="#">Latin<sup>148.674</sup></a></li>
                                <li><a href="#">Pop<sup>148.674</sup></a></li>
                                <li><a href="#">Reggae<sup>148.674</sup></a></li>
                                <li><a href="#">Rock<sup>148.674</sup></a></li>
                                <li><a href="#">Stage & Screen<sup>148.674</sup></a></li>
                                <li><a href="#">Non-Music<sup>148.674</sup></a></li>
                                <li><a href="#">Brass & Military<sup>674</sup></a></li>
                                <li><a href="#">Children's<sup>74</sup></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>

                <form class="header__search" action="/search" method="GET">
                    <input type="text" name="term" placeholder="Type and search for..."/>
                    <input type="submit"/>
                </form>

                <a href="/customer/login" class="header__user"><img src="{{ asset('themes/vinylheaven-theme/assets/img/user-icon.png') }}" alt=""></a>

                <a href="#" class="header__heart"><img src="{{ asset('themes/vinylheaven-theme/assets/img/heart-icon.png') }}" alt=""></a>

                <div class="header__lang">
                    <a href="#" class="header__language js-lang">EUR</a>
                    <ul class="header__lang-dropdown js-lang-dropdown">
                        <li><a href="#">USD</a></li>
                        <li><a href="#">GBP</a></li>
                    </ul>
                </div>

                <a href="#" class="header__cart"><img src="{{ asset('themes/vinylheaven-theme/assets/img/cart-icon.png') }}" alt=""> €0,00</a>

            </header>

            <main>
                {!! view_render_event('bagisto.shop.layout.content.before') !!}

                    @yield('content-wrapper')

                {!! view_render_event('bagisto.shop.layout.content.after') !!}
            </main>

            <footer class="footer container">
                <div class="footer__top">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                            <div class="footer__title">Vinyl Express, your source for great records since 1973.</div>

                            <div class="footer__text">2.376.485 records listed in our catalogue.</div>
                        </div>

                        <div class="col-lg-3 col-md-4 hide-mobile">
                            <ul class="footer__menu">
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="footer__bottom">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                            <p>Shipping discount in bulk orders.</p>

                            <p>Secure payment with the following:</p>

                            <ul class="footer__payments">
                                <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo1.png') }}" alt=""></li>
                                <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo2.png') }}" alt=""></li>
                                <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo3.png') }}" alt=""></li>
                                <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo4.png') }}" alt=""></li>
                                <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo5.png') }}" alt=""></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-4">
                            <div class="footer__title footer--title-arrow">Connect with us</div>

                            <ul class="footer__social">
                                <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/facebook.png') }}" alt=""></a></li>
                                <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/twitter.png') }}" alt=""></a></li>
                                <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/insta.png') }}" alt=""></a></li>
                                <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/pinterest.png') }}" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="footer__copyright">
                    <div class="row">
                        <div class="col-lg-9 col-md-8 footer__copyright-order1">
                            <p><a href="#" class="footer__logo"><img src="{{ asset('themes/vinylheaven-theme/assets/img/footer-logo.png') }}" alt=""></a> VINYL EXPRESS, Rockhouse Records © 2020</p>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <p><a href="#">Terms and Conditions</a></p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <script data-main="/themes/vinylheaven-theme/assets/dist/js/app" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/vendor/require.js') }}"></script>

        <script type="text/javascript">
            window.flashMessages = [];

            @if ($success = session('success'))
                window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
            @elseif ($warning = session('warning'))
                window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
            @elseif ($error = session('error'))
                window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }
                ];
            @elseif ($info = session('info'))
                window.flashMessages = [{'type': 'alert-info', 'message': "{{ $info }}" }
                ];
            @endif

            window.serverErrors = [];
            @if(isset($errors))
                @if (count($errors))
                    window.serverErrors = @json($errors->getMessages());
                @endif
            @endif
        </script>

        <script type="text/javascript" src="{{ bagisto_asset('js/shop.js') }}"></script>

        <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>



        @stack('scripts')

        {!! view_render_event('bagisto.shop.layout.body.after') !!}

        <div class="modal-overlay"></div>
    </body>
</html>