<footer class="footer">
    <div class="container">
        <div class="footer__top">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="footer__title">Vinyl Express, your source for great records since 1973.</div>

                    <div class="footer__text">2.376.485 records listed in our catalogue.</div>
                </div>

                <div class="col-lg-3 col-md-4 hide-mobile">
                    <ul class="footer__menu">
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer__bottom">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <p>Shipping discount in bulk orders.</p>

                    <p>Secure payment with the following:</p>

                    <ul class="footer__payments">
                        <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo1.png') }}" alt=""></li>
                        <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo2.png') }}" alt=""></li>
                        <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo3.png') }}" alt=""></li>
                        <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo4.png') }}" alt=""></li>
                        <li><img src="{{ asset('themes/vinylheaven-theme/assets/img/pay-logo5.png') }}" alt=""></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="footer__title footer--title-arrow">Connect with us</div>

                    <ul class="footer__social">
                        <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/facebook.png') }}" alt=""></a></li>
                        <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/twitter.png') }}" alt=""></a></li>
                        <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/insta.png') }}" alt=""></a></li>
                        <li><a href="#" target="_blank"><img src="{{ asset('themes/vinylheaven-theme/assets/img/pinterest.png') }}" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer__copyright">
            <div class="row">
                <div class="col-lg-9 col-md-8 footer__copyright-order1">
                    <p><a href="#" class="footer__logo"><img src="{{ asset('themes/vinylheaven-theme/assets/img/footer-logo.png') }}" alt=""></a> VINYL EXPRESS, Rockhouse Records © 2020</p>
                </div>

                <div class="col-lg-3 col-md-4">
                    <p><a href="#">Terms and Conditions</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>