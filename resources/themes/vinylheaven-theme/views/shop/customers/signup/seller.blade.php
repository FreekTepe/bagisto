@php
    if (old('want_to_be_seller')) {
        $checked_yes = 'checked';
        $checked_no = '';
    } else {
        $checked_yes = '';
        $checked_no = 'checked';
    }
@endphp

<div class="form-row mt-3 mb-1">
    <div class="col-12">
        <div>
            <label>
                <strong>{{ __('marketplace::app.shop.sellers.account.signup.want-to-be-seller') }}</strong>
            </label>
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-12">
        <div class="custom-control custom-radio">
            <input type="radio" id="want-to-be-seller-yes" class="custom-control-input" name="want_to_be_seller" value="1" required {{ $checked_yes }}>
            <label for="want-to-be-seller-yes" class="custom-control-label">{{ __('marketplace::app.shop.sellers.account.signup.yes') }}</label>
        </div>

        <div class="custom-control custom-radio">
            <input type="radio" id="want-to-be-seller-no" class="custom-control-input" name="want_to_be_seller" value="0" required {{ $checked_no }}>
            <label for="want-to-be-seller-no" class="custom-control-label">{{ __('marketplace::app.shop.sellers.account.signup.no') }}</label>
            <div class="invalid-feedback">
                Please Make A Choise
            </div>
        </div>
    </div>
</div>

<div class="form-row mb-3 collapse" id="seller-shop-url-container">
    <div class="col-12">
        <label></label>
        <input type="text" id="seller-shop-url" class="form-control" name="url" value="{{ old('url') }}" data-csrf="{{csrf_token()}}" placeholder="{{ __('marketplace::app.shop.sellers.account.signup.shop_url') }}">
        <div class="invalid-feedback">
            Please Provide Your Shop Url
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            var collapse_options = {
                toggle:false
            };

            $('#seller-shop-url-container').collapse(collapse_options);

            $('#seller-shop-url-container').on('show.bs.collapse', function(){
                $('#seller-shop-url').prop('required', true);
            });

            $('#seller-shop-url-container').on('hide.bs.collapse', function(){
                $('#seller-shop-url').prop('required', false);
            });

            $('#want-to-be-seller-yes').click(function(event){
                $('#seller-shop-url-container').collapse('show');
            });

            $('#want-to-be-seller-no').click(function(event){
                $('#seller-shop-url-container').collapse('hide');
            });
        });
    </script>
@endpush