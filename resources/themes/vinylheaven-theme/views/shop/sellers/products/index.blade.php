@extends('shop::layouts.master-new')

@inject('productRepository', 'Webkul\Marketplace\Repositories\ProductRepository')
@inject('marketplace_products', 'App\Models\ExtendWebkul\MarketplaceProduct')
@inject('products_full', 'App\Models\ExtendWebkul\Product')
@inject ('catalogHelper', 'VinylHeaven\Catalog\Helpers\Catalog')
@inject ('toolbarHelper', 'Webkul\Product\Helpers\Toolbar')
@inject ('searchHelper', 'App\Helpers\Search')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.products.title', ['shop_title' => $seller->shop_title]) }} - {{ $seller->name }}
@endsection

@php
    $products = $marketplace_products->findAllBySeller($seller);
    $products_count = $marketplace_products->countAllBySeller($seller);
@endphp

@section('content-wrapper')
<section class="container mt-5">
    <div class="row">
        <div class="col-12 col-md-4 col-lg-3">
            @if ($logo = $seller->logo_url)
                <img src="{{ $logo }}" />
            @else
                <img src="{{ bagisto_asset('images/default-logo.svg') }}" />
            @endif
        </div>

        <div class="col-12 col-md-8 col-lg-9">
            <div class="row">
                <div class="col-12">
                    <h2>{{ $seller->shop_title }}</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    @if ($seller->country)
                        <a target="_blank" href="https://www.google.com/maps/place/{{ $seller->city . ', '. $seller->state . ', ' . core()->country_name($seller->country) }}" class="shop-address">{{ $seller->city . ', '. $seller->state . ' (' . core()->country_name($seller->country) . ')' }}</a>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    @if ($seller->facebook)
                        <a href="https://www.facebook.com/{{$seller->facebook}}" target="_blank">
                            <i class="icon social-icon mp-facebook-icon"></i>
                        </a>
                    @endif

                    @if ($seller->twitter)
                        <a href="https://www.twitter.com/{{$seller->twitter}}" target="_blank">
                            <i class="icon social-icon mp-twitter-icon"></i>
                        </a>
                    @endif

                    @if ($seller->instagram)
                        <a href="https://www.instagram.com/{{$seller->instagram}}" target="_blank"><i class="icon social-icon mp-instagram-icon"></i></a>
                    @endif

                    @if ($seller->pinterest)
                        <a href="https://www.pinterest.com/{{$seller->pinterest}}" target="_blank"><i class="icon social-icon mp-pinterest-icon"></i></a>
                    @endif

                    @if ($seller->skype)
                        <a href="https://www.skype.com/{{$seller->skype}}" target="_blank">
                            <i class="icon social-icon mp-skype-icon"></i>
                        </a>
                    @endif

                    @if ($seller->linked_in)
                        <a href="https://www.linkedin.com/{{$seller->linked_in}}" target="_blank">
                            <i class="icon social-icon mp-linked-in-icon"></i>
                        </a>
                    @endif

                    @if ($seller->youtube)
                        <a href="https://www.youtube.com/{{$seller->youtube}}" target="_blank">
                            <i class="icon social-icon mp-youtube-icon"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

<section class="search-result container">
    @include('shop::products.view.toolbar')

    <div class="search-result-container">
        <div class="row">
            @if (!$products)
                <div class="col-12">
                    <h1>This seller has no products.</h1>
                </div>
            @else
                <div class="col-12">
					<div class="result-grid {{ $toolbarHelper->getGridViewDisplay() }}">
						<div class="row">
							@foreach ($products as $product)
                                @if (isset($product->url_key) && !empty($product->url_key))
                                    @php
                                        $product = $products_full->where('id', $product->product_id)->first();
                                        $marketplace_product = $marketplace_products->where([['product_id', $product->product_id], ['marketplace_seller_id', $seller->id]])->first();
                                    @endphp

                                    <div class="col-6 col-sm-6 col-md-4 col-lg-3">
                                        @include('shop::products.view.grid.item')
                                    </div>
								@endif
							@endforeach
						</div>
					</div>
				
					<div class="result-list {{ $toolbarHelper->getListViewDisplay() }}">
						@foreach ($products as $product)
							@if (isset($product->url_key) && !empty($product->url_key))
                                @php
                                    $product = $products_full->where('id', $product->product_id)->first();
                                    $marketplace_product = $marketplace_products->where([['product_id', $product->product_id], ['marketplace_seller_id', $seller->id]])->first();
                                @endphp
                                
                                @include('shop::products.view.list.item')
							@endif
						@endforeach
                    </div>
                    
                    <div class="col-12">
                        @include('shop::search.pagination')
                    </div>
				</div>
            @endif
        </div>
    </div>
</section>
@endsection