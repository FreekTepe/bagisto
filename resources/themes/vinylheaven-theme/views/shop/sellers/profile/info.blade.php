@inject('reviewRepository', 'Webkul\Marketplace\Repositories\ReviewRepository')

<div class="col-auto">
    <div class="row align-items-center">
        <div class="col-auto">
            {{ $seller->shop_title }}
        </div>

        @if ($seller->country)
            <div class="col-auto">
                <a target="_blank" href="https://www.google.com/maps/place/{{ $seller->city . ', '. $seller->state . ', ' . core()->country_name($seller->country) }}" class="shop-address">{{ $seller->city . ', '. $seller->state . ' (' . core()->country_name($seller->country) . ')' }}</a>
            </div>            
        @endif

        <div class="col-auto">
            @if ($seller->facebook)
                <a href="https://www.facebook.com/{{$seller->facebook}}" target="_blank">
                    <i class="icon social-icon mp-facebook-icon"></i>
                </a>
            @endif

            @if ($seller->twitter)
                <a href="https://www.twitter.com/{{$seller->twitter}}" target="_blank">
                    <i class="icon social-icon mp-twitter-icon"></i>
                </a>
            @endif

            @if ($seller->instagram)
                <a href="https://www.instagram.com/{{$seller->instagram}}" target="_blank"><i class="icon social-icon mp-instagram-icon"></i></a>
            @endif

            @if ($seller->pinterest)
                <a href="https://www.pinterest.com/{{$seller->pinterest}}" target="_blank"><i class="icon social-icon mp-pinterest-icon"></i></a>
            @endif

            @if ($seller->skype)
                <a href="https://www.skype.com/{{$seller->skype}}" target="_blank">
                    <i class="icon social-icon mp-skype-icon"></i>
                </a>
            @endif

            @if ($seller->linked_in)
                <a href="https://www.linkedin.com/{{$seller->linked_in}}" target="_blank">
                    <i class="icon social-icon mp-linked-in-icon"></i>
                </a>
            @endif

            @if ($seller->youtube)
                <a href="https://www.youtube.com/{{$seller->youtube}}" target="_blank">
                    <i class="icon social-icon mp-youtube-icon"></i>
                </a>
            @endif
        </div>
        

        <div class="col-auto"></div>
    </div>

    <div class="row align-items-center">
        <div class="col-auto">
            Average Rating: {{ $reviewRepository->getAverageRating($seller) }}
        </div>

        <div class="col-auto">
            @for ($i = 1; $i <= $reviewRepository->getAverageRating($seller); $i++)
                <span class="icon star-icon"></span>
            @endfor
        </div>

        <div class="col-auto">
            <a href="{{ route('marketplace.reviews.index', $seller->url) }}">
                {{
                    __('marketplace::app.shop.sellers.profile.total-rating', [
                            'total_rating' => $reviewRepository->getTotalRating($seller),
                            'total_reviews' => $reviewRepository->getTotalReviews($seller),
                        ])
                }}
            </a>
        </div>

        <div class="col-auto"></div>
    </div>
</div>