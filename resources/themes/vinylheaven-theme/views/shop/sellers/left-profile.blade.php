@inject('review_repository', 'Webkul\Marketplace\Repositories\ReviewRepository')
@inject('product_repository', 'Webkul\Marketplace\Repositories\ProductRepository')

<div class="row">
    <div class="col-12 col-md-4 col-lg-3">
        @if ($logo = $seller->logo_url)
            <img src="{{ $logo }}" />
        @else
            <img src="{{ bagisto_asset('images/default-logo.svg') }}" />
        @endif
    </div>

    <div class="col-12 col-md-8 col-lg-9">
        <div class="row">
            <div class="col-12">
                <h4>{{ $seller->shop_title }}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                @if ($seller->country)
                    <a target="_blank" href="https://www.google.com/maps/place/{{ $seller->city . ', '. $seller->state . ', ' . core()->country_name($seller->country) }}" class="shop-address">{{ $seller->city . ', '. $seller->state . ' (' . core()->country_name($seller->country) . ')' }}</a>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                @if ($seller->facebook)
                    <a href="https://www.facebook.com/{{$seller->facebook}}" target="_blank">
                        <i class="icon social-icon mp-facebook-icon"></i>
                    </a>
                @endif

                @if ($seller->twitter)
                    <a href="https://www.twitter.com/{{$seller->twitter}}" target="_blank">
                        <i class="icon social-icon mp-twitter-icon"></i>
                    </a>
                @endif

                @if ($seller->instagram)
                    <a href="https://www.instagram.com/{{$seller->instagram}}" target="_blank"><i class="icon social-icon mp-instagram-icon"></i></a>
                @endif

                @if ($seller->pinterest)
                    <a href="https://www.pinterest.com/{{$seller->pinterest}}" target="_blank"><i class="icon social-icon mp-pinterest-icon"></i></a>
                @endif

                @if ($seller->skype)
                    <a href="https://www.skype.com/{{$seller->skype}}" target="_blank">
                        <i class="icon social-icon mp-skype-icon"></i>
                    </a>
                @endif

                @if ($seller->linked_in)
                    <a href="https://www.linkedin.com/{{$seller->linked_in}}" target="_blank">
                        <i class="icon social-icon mp-linked-in-icon"></i>
                    </a>
                @endif

                @if ($seller->youtube)
                    <a href="https://www.youtube.com/{{$seller->youtube}}" target="_blank">
                        <i class="icon social-icon mp-youtube-icon"></i>
                    </a>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col">
                Average Rating: {{ $review_repository->getAverageRating($seller) }}
            </div>

            <div class="col">
                @for ($i = 1; $i <= $review_repository->getAverageRating($seller); $i++)
                    <span class="icon star-icon"></span>
                @endfor
            </div>

            <div class="col">
                <a href="{{ route('marketplace.reviews.index', $seller->url) }}">
                    {{
                        __('marketplace::app.shop.sellers.profile.total-rating', [
                                'total_rating' => $review_repository->getTotalRating($seller),
                                'total_reviews' => $review_repository->getTotalReviews($seller),
                            ])
                    }}
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <a href="{{ route('marketplace.products.index', $seller->url) }}">
                    {{ __('marketplace::app.shop.sellers.profile.count-products', [
                            'count' => $product_repository->getTotalProducts($seller)
                        ])
                    }}
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <a href="#" @click="showModal('contactForm')">{{ __('marketplace::app.shop.sellers.profile.contact-seller') }}</a>
            </div>
        </div>
    </div>
</div>