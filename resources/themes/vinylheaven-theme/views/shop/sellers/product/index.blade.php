@extends('shop::layouts.master-new')

@php
    $show_other_products = false;
    $other_products = $product->product->marketplace_product();

    if ($product_view_config->is_seller_product) {
        $other_products->where([['marketplace_seller_id', '!=', $seller->id]]);

        if ($product->product->marketplace_product()->where([['marketplace_seller_id', '!=', $seller->id]])->get()->count()) {
            $show_other_products = true;
        }
    } else {
        if ($product->product->marketplace_product()->count()) {
            $show_other_products = true;
        }
    }
    
    //dd($other_products->get()->count(), $product->product->marketplace_product()->get());
    
    //$product_full = $product_helper->getCompleteProductByProduct($product->product_id);
    //$mp_product = $marketplace_product->where([['product_id', '=', $product->product_id], ['marketplace_seller_id', '=', $seller->id]])->first();
    
    //dd($product->product->marketplace_product()->where([['marketplace_seller_id', '!=', $seller->id]])->get());
@endphp

@section('page_title')
    {{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($product->meta_description) != "" ? $product->meta_description : str_limit(strip_tags($product->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $product->meta_keywords }}"/>
@stop

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-product seller-product">
        <div class="row">
            <div class="col-12 col-lg-6">
                @include('shop::products.view.gallery')
            </div>

            <div class="col-12 col-lg-6 product-details">
                @include('shop::products.view.details')
            </div>
        </div>

        @if ($show_other_products)
            <div class="row my-3">
                <div class="col-12 product-other-sellers {{ $product_view_config->classes->product_other_sellers }}" data-toggle="collapse" data-target="#product-other-sellers" aria-controls="product-other-sellers" aria-expanded="{{ $product_view_config->aria_expanded }}" aria-label="Toggle Product Other Sellers">
                    {{ $product_view_config->other_products_count_text }} for sale from {{ $product->product->getMarketplaceProductsFromPrice() }}
                </div>

                <div class="col-12 product-other-sellers-collapse collapse {{ $product_view_config->classes->product_other_sellers_collapse }} py-4" id="product-other-sellers">
                    @foreach ($other_products->get() as $i => $other_product)
                        <a href="{{ route('marketplace.sellers.product.index', ['url' => $other_product->seller->url, 'slug' => $product->product->url_key]) }}">
                            <div class="row justify-content-start align-items-center mb-2">
                                <div class="col-auto">
                                    @if($i == 0 || strlen($i) == 1)
                                        {{ '0' . ($i + 1) }}
                                    @else
                                        {{ ($i + 1) }}
                                    @endif
                                </div>

                                <div class="col">{{ $other_product->product->name }}</div>

                                <div class="col-12 d-lg-none"></div>

                                <div class="col-6 col-lg-2">
                                    <div class="condition-grading-circle small-text mb-0">
                                        <span class="circle-text  {{ $other_product->product->getConditionGradingBackgroundClass($other_product->product->getConditionGrading($other_product->condition)) }}">{{ $other_product->product->getConditionGrading($other_product->condition) }}</span> 
                                        {{ $other_product->product->getConditionText($other_product->condition) }}
                                    </div>
                                </div>

                                <div class="col-6 col-lg-2">
                                    <div class="condition-grading-circle small-text mb-0">
                                        <span class="circle-text  {{ $other_product->product->getConditionGradingBackgroundClass($other_product->product->getSleeveConditionGrading($other_product->sleeve_condition)) }}">{{ $other_product->product->getSleeveConditionGrading($other_product->sleeve_condition) }}</span> 
                                        {{ $other_product->product->getSleeveConditionText($other_product->sleeve_condition) }}
                                    </div>
                                </div>

                                <div class="col-6 col-lg-1">{{ core()->currency(core()->convertPrice($other_product->price, core()->getBaseCurrencyCode(), 'EUR')) }}</div>

                                <div class="col-6 col-lg-auto">
                                    <form action="{{ route('cart.add', $other_product->product_id) }}" method="POST" class="form-inline">
                                        @csrf
                                        <input type="hidden" name="product_id" value="{{ $other_product->product_id }}">
                                        <input type="hidden" name="quantity" value="1">
                                        <input type="hidden" name="seller_info[product_id]" value="{{ $other_product->id }}">
                                        <input type="hidden" name="seller_info[seller_id]" value="{{ $other_product->marketplace_seller_id }}">
                                        <input type="hidden" name="seller_info[is_owner]" value="0">
                                        
                                        <button class="button button-bag" {{ $other_product->product->isSaleable() ? '' : '' }}></button>
                                    </form>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        @endif
    </section>

    <section class="product-info">
        @include('shop::products.view.info')
    </section>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#product-other-sellers').on('show.bs.collapse', function(){
                if (!$('.product-other-sellers').hasClass('collapse-open')) {
                    $('.product-other-sellers').addClass('collapse-open');
                }
            });

            $('#product-other-sellers').on('hide.bs.collapse', function(){
                if ($('.product-other-sellers').hasClass('collapse-open')) {
                    $('.product-other-sellers').removeClass('collapse-open');
                }
            });
        });
    </script>
@endpush