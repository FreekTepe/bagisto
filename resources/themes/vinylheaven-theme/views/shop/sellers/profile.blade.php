@extends('shop::layouts.master-new')

@inject('productRepository', 'Webkul\Marketplace\Repositories\ProductRepository')
@inject('gridHelper', 'VinylHeaven\Tools\Helpers\Grid')

@section('page_title')
    {{ $seller->shop_title }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($seller->meta_description) != "" ? $seller->meta_description : str_limit(strip_tags($seller->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $seller->meta_keywords }}"/>
@stop

@php
    $seller_products_where = [
        'marketplace_seller_id' => $seller->id
    ];
    $seller_products_skip = 0;
    $seller_products_take = 33;
    $seller_products = $productRepository->where($seller_products_where)->get();
    $products = $seller_products->skip($seller_products_skip)->take($seller_products_take);

    $toolbar_config = [
        'has_result_counter' => false,
        'has_seller_info' => true
    ];
@endphp

@section('content-wrapper')
    <section class="container-fluid container-seller-profile">
        <div class="seller-header">
            <div class="container-fluid container-lg container-xl">
                <div class="row">
                    <div class="col-12">
                        <div class="header-container">
                            <div class="header-logo mb-3 mr-3">
                                <img src="{{ $seller->getLogoUrl() }}" class="logo" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid container-lg container-xl">
            @include('shop::tools.list-grid-view-toolbar', ['toolbar_config' => $toolbar_config])
            
            <div class="row">
                @if ($products->count())
                    @foreach ($products as $product_index => $product)
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                            @include('shop::products.view.grid.item')
                        </div>
                    @endforeach
                @endif

                <div class="col-12">
                    @include('shop::search.pagination', ['products_count' => $seller_products->count()])
                </div>
            </div>
        </div>
    </section>
@endsection

@section('seller-profile-banner-logo-css')
    <style>
        .container-seller-profile .seller-header {
            background-image: url('{{ $seller->getBannerUrl() }}');
        }
    </style>
@endsection

@section('info-image-background-css')
    <style>
        @foreach ($products as $product)
            @if (isset($product->product->url_key) && !empty($product->product->url_key))
                .product-card .card-info .info-image .image-holder.info-image-{{ $product->product->url_key }} {
                    background-image: url('{{ $product->product->getPrimaryImage() }}');
                }
            @endif
        @endforeach
    </style>
@endsection