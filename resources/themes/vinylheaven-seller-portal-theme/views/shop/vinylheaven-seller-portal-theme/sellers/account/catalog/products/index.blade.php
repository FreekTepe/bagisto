@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.catalog.products.title') }}
@endsection

<!-- Destroy all products modal -->
<div class="modal fade" id="deleteAllProductsModal" tabindex="-1" role="dialog" aria-labelledby="deleteAllProductsModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Be carefull!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                You are about to delete all products from your store.
                <br>
                Are you sure you want to procceed?
                <hr>
                <input type="checkbox" id="deletecheck" onchange="document.getElementById('deleteButton').disabled = !this.checked;" >
                <label for="deletecheck">Yes i want to delete all products</label>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form method="POST" action="{{ route('mpproducts.delete.all' ) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button id="deleteButton" type="submit" disabled class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

@section('content')



        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-box2 icon-gradient bg-tempting-azure"></i>
                    </div>
                    <div>Products
                        <div class="page-title-subheading">Here you can manage your product catalog.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block">
                        <a href="{{ route('marketplace.account.products.search') }}" style="color:white;" class="btn-shadow btn btn-info">
                            {{ __('marketplace::app.shop.sellers.account.catalog.products.create') }}
                        </a>

                        <a target="_bank" href="{{ route('mpproducts.download') }}" style="color:white;" class="btn-shadow btn btn-warning">
                            Download
                        </a>

                        <button data-toggle="modal" data-target="#deleteAllProductsModal" class="btn-icon btn-icon-only btn btn-danger btn-shadow" style="color: white;" value="Delete user"><i class="lnr-trash btn-icon-wrapper"></i></button>



                    </div>
                </div>
            </div>
        </div>


        {!! view_render_event('marketplace.sellers.account.catalog.products.list.before') !!}

        {{-- <div class="account-items-list"> --}}

            {{-- ProductDataGrid renders table --}}
            {!! app('App\DataGrids\ProductDataGrid')->render() !!}

        {{-- </div> --}}

        {!! view_render_event('marketplace.sellers.account.catalog.products.list.after') !!}





@endsection