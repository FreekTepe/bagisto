@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.sales.orders.view-title', ['order_id' => $sellerOrder->order_id]) }}
@endsection

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-note icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('marketplace::app.shop.sellers.account.sales.orders.view-title', ['order_id' => $sellerOrder->order_id]) }}
                <div class="page-title-subheading">Manage the status of your order</div>
            </div>
        </div>
        <div class="page-title-actions">

            <div class="d-inline-block">
                @if (core()->getConfigData('marketplace.settings.general.can_cancel_order') && $sellerOrder->canCancel())
                    <a  href="{{ route('marketplace.account.orders.cancel', $sellerOrder->order_id) }}" v-alert:message="'{{ __('admin::app.sales.orders.cancel-confirm-msg') }}'" style="color:white;" class="btn-shadow btn btn-info">
                        {{ __('admin::app.sales.orders.cancel-btn-title') }}
                    </a>
                @endif
            </div>

            <div class="d-inline-block">
                @if (core()->getConfigData('marketplace.settings.general.can_create_invoice') && $sellerOrder->canInvoice())
                    <a  href="{{ route('marketplace.account.invoices.create', $sellerOrder->order_id) }}" style="color:white;" class="btn-shadow btn btn-info">
                        {{ __('admin::app.sales.orders.invoice-btn-title') }}
                    </a>
                @endif
            </div>

            <div class="d-inline-block">
                @if (core()->getConfigData('marketplace.settings.general.can_create_shipment') && $sellerOrder->canShip())
                    <a  href="{{ route('marketplace.account.shipments.create', $sellerOrder->order_id) }}" style="color:white;" class="btn-shadow btn btn-info">
                        {{ __('admin::app.sales.orders.shipment-btn-title') }}
                    </a>
                @endif
            </div>

        </div>
    </div>
</div>

{!! view_render_event('marketplace.sellers.account.sales.orders.view.before', ['sellerOrder' => $sellerOrder]) !!}

<ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">

    {{-- Order Information --}}
    <li class="nav-item">
        <a role="tab" id="tab-0" data-toggle="tab" href="#tab-content-0" aria-selected="true" class="nav-link active">
            <span>{{ __('marketplace::app.shop.sellers.account.sales.orders.info') }}</span>
        </a>
    </li>

    {{-- Invoices --}}
    @if ($sellerOrder->invoices->count())
        <li class="nav-item">
            <a role="tab" id="tab-1" data-toggle="tab" href="#tab-content-1" aria-selected="false" class="nav-link">
                <span>{{ __('marketplace::app.shop.sellers.account.sales.orders.invoices') }}</span>
            </a>
        </li>
    @endif

    {{-- Refunds --}}
    @if ($sellerOrder->refunds->count())
        <li class="nav-item">
            <a role="tab" id="tab-2" data-toggle="tab" href="#tab-content-2" aria-selected="false" class="nav-link">
                <span>{{ __('marketplace::app.shop.sellers.account.sales.orders.refunds') }}</span>
            </a>
        </li>
    @endif

    {{-- Shipments --}}
    @if ($sellerOrder->shipments->count())
        <li class="nav-item">
            <a role="tab" id="tab-2" data-toggle="tab" href="#tab-content-3" aria-selected="false" class="nav-link">
                <span>{{ __('marketplace::app.shop.sellers.account.sales.orders.shipments') }}</span>
            </a>
        </li>
    @endif

</ul>


{{-- Tab content sections --}}
<div class="tab-content">

    {{-- Order Information Tab --}}
    <div id="tab-content-0" role="tabpanel" class="tab-pane tabs-animation fade active show">
        <div class="main-card mb-2 card">
            <div class="card-body">
                <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.sales.orders.info') }}</h5>

                <div class="row">
                    <div class="col-6">
                        <ul class="list-group">
                            <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.placed-on') }}:</b> <span class="float-right ml-3">{{ core()->formatDate($sellerOrder->created_at, 'd M Y') }}</span></li>
                            <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.status') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->status_label }}</span></li>
                            <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.customer-name') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_full_name }}</span></li>
                            <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.email') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_email }}</span></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <div class="main-card mb-2 card">
            <div class="card-body">
                <h5 class="card-title">{{ __('shop::app.customer.account.order.view.products-ordered') }}</h5>


                <div class="table table-responsive">
                    <table class="table mt-3">
                    <thead>
                        <tr>
                            <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.item-status') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.discount') }}</th>
                            <th>{{ __('marketplace::app.shop.sellers.account.sales.orders.admin-commission') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.tax-percent') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                            <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach ($sellerOrder->items as $sellerOrderItem)
                            <tr>
                                <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">
                                    {{ $sellerOrderItem->item->type == 'configurable' ? $sellerOrderItem->item->child->sku : $sellerOrderItem->item->sku }}
                                </td>
                                <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">
                                    {{ $sellerOrderItem->item->name }}

                                    @if (isset($sellerOrderItem->additional['attributes']))
                                        <div class="item-options">

                                            @foreach ($sellerOrderItem->additional['attributes'] as $attribute)
                                                <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                            @endforeach

                                        </div>
                                    @endif
                                </td>
                                <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">{{ core()->formatPrice($sellerOrderItem->item->price, $sellerOrder->order->order_currency_code) }}</td>
                                <td data-value="{{ __('shop::app.customer.account.order.view.item-status') }}">
                                    <span class="qty-row">
                                        {{ __('shop::app.customer.account.order.view.item-ordered', ['qty_ordered' => $sellerOrderItem->item->qty_ordered]) }}
                                    </span>

                                    <span class="qty-row">
                                        {{ $sellerOrderItem->item->qty_invoiced ? __('shop::app.customer.account.order.view.item-invoice', ['qty_invoiced' => $sellerOrderItem->item->qty_invoiced]) : '' }}
                                    </span>

                                    <span class="qty-row">
                                        {{ $sellerOrderItem->item->qty_refunded ? __('admin::app.sales.orders.item-refunded', ['qty_refunded' => $sellerOrderItem->item->qty_refunded]) : '' }}
                                    </span>

                                    <span class="qty-row">
                                        {{ $sellerOrderItem->item->qty_shipped ? __('shop::app.customer.account.order.view.item-shipped', ['qty_shipped' => $sellerOrderItem->item->qty_shipped]) : '' }}
                                    </span>

                                    <span class="qty-row">
                                        {{ $sellerOrderItem->item->qty_canceled ? __('shop::app.customer.account.order.view.item-canceled', ['qty_canceled' => $sellerOrderItem->item->qty_canceled]) : '' }}
                                    </span>
                                </td>
                                <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">{{ core()->formatPrice($sellerOrderItem->item->total, $sellerOrder->order->order_currency_code) }}</td>

                                <td data-value="{{ __('shop::app.customer.account.order.view.discount') }}">{{ core()->formatPrice($sellerOrderItem->item->discount_amount, $sellerOrder->order->order_currency_code) }}</td>


                                <td data-value="{{ __('marketplace::app.shop.sellers.account.sales.orders.admin-commission') }}">{{ core()->formatPrice($sellerOrderItem->commission, $sellerOrder->order->order_currency_code) }}</td>
                                <td data-value="{{ __('shop::app.customer.account.order.view.tax-percent') }}">{{ number_format($sellerOrderItem->item->tax_percent, 2) }}%</td>
                                <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">{{ core()->formatPrice($sellerOrderItem->item->tax_amount, $sellerOrder->order->order_currency_code) }}</td>
                                <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">{{ core()->formatPrice($sellerOrderItem->item->total + $sellerOrderItem->item->tax_amount - $sellerOrder->discount_amount, $sellerOrder->order->order_currency_code) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="row">

                <div class="ml-auto mr-4">
                    <ul class="list-group">
                        <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.subtotal') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->sub_total, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->shipping_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.discount') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->discount_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.tax') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->tax_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.grand-total') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->grand_total, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.total-paid') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->grand_total_invoiced, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.total-due') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->base_total_due, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.total-seller-amount') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->seller_total, $sellerOrder->order->order_currency_code) }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.total-admin-commission') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->commission, $sellerOrder->order->order_currency_code) }}</span></li>

                    </ul>
                </div>

            </div>

            </div>
        </div>


        {{-- Messaging box --}}
        <div class="main-card mb-2 card">
            <div class="card-body">
                <div class="card-header">
                    <i class="header-icon lnr-users icon-gradient bg-tempting-azure"> </i>
                    <span class="mt-1">Timeline</span>
                    {{-- <div class="btn-actions-pane-right actions-icon-btn">
                        <div role="group" class="btn-group-sm nav btn-group">
                            <a data-toggle="tab" href="#tab-eg3-0" class="btn-shadow btn btn-info active">Invoiced</a>
                            <a data-toggle="tab" href="#tab-eg3-1" class="btn-shadow btn btn-info active">Payed</a>
                            <a data-toggle="tab" href="#tab-eg3-2" class="btn-shadow  btn btn-info">Shipped</a>
                        </div>
                    </div> --}}
                </div>
                {{-- <h5 class="card-title">Timeline</h5> --}}
                
                    <div class="table-responsive">
                        <div class="chat-wrapper">

                            @foreach ($sellerOrder->order->messages()->orderBy('created_at')->get() as $message)

                                @if ($message->sender == "seller")
                                    {{-- Seller chatbox --}}
                                    <div class="chat-box-wrapper">
                                        <div>
                                            <div class="avatar-icon-wrapper mr-1">
                                                <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg">
                                                </div>
                                                <div class="avatar-icon avatar-icon-lg rounded">
                                                    <img src="https://demo.dashboardpack.com/architectui-html-pro/assets/images/avatars/1.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            {!! $message->render() !!}
                                        </div>
                                    </div>
                                @elseif($message->sender == "customer")
                                    {{-- Customer chatbox --}}



                                    <div class="float-right">
                                        <div class="chat-box-wrapper chat-box-wrapper-right">
                                            <div>
                                                {!! $message->render() !!}
                                            </div>
                                            <div>
                                                <div class="avatar-icon-wrapper ml-1">
                                                    <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg">
                                                    </div>
                                                    <div class="avatar-icon avatar-icon-lg rounded">
                                                        <img src="https://demo.dashboardpack.com/architectui-html-pro/assets/images/avatars/2.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="clearfix"></div>

                            @endforeach         
                        </div>
                        <hr>
                        <form action="{{route('messages.store')}}" method="post">
                            @csrf
                            <input type="hidden" name="seller_id" value="{{$sellerOrder->seller->id}}">
                            <input type="hidden" name="customer_id" value="{{$sellerOrder->order->customer->id}}">
                            <input type="hidden" name="order_id" value="{{$sellerOrder->order->id}}">

                            <input type="hidden" name="sender" value="seller">
                            <input type="hidden" name="status_change" value="0">
                            <div class="app-inner-layout__bottom-pane d-block text-center">
                                <div class="mb-0 position-relative row form-group mt-2">
                                    <div class="col-sm-10">
                                        <input type="text" name="message" placeholder="Type your message.." class="form-control-lg form-control">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-shadow btn-info btn-block btn-round form-control-lg form-control">Send</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>

    {{-- Invoices --}}
    <div id="tab-content-1" role="tabpanel" class="tab-pane tabs-animation fade">
        <div class="main-card mb-2 card" >
            <div class="card-body">
                <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.sales.orders.invoices') }}</h5>

                @foreach ($sellerOrder->invoices as $sellerInvoice)


                        <div class="row p-2">
                            <div class="col-10">
                                <h6 >{{ __('shop::app.customer.account.order.view.individual-invoice', ['invoice_id' => $sellerInvoice->invoice_id]) }}</h6>
                            </div>
                            <div class="col-2">
                                <a href="{{ route('marketplace.account.invoices.print', $sellerInvoice->marketplace_order_id ) }}" class="pull-right btn-icon btn-icon-only btn btn-info btn-sm btn-shadow" style="color:white;">
                                    <i class="lnr lnr-printer btn-icon-wrapper"></i>
                                </a>
                            </div>
                        </div>

                        <div class="table table-responsive">
                            <table class="table mt-3">
                                <thead>
                                <tr>
                                    <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.discount') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach ($sellerInvoice->items as $sellerInvoiceItem)
                                    <?php $baseInvoiceItem = $sellerInvoiceItem->item; ?>
                                    <tr>
                                        <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $baseInvoiceItem->name }}</td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->price, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}">{{ $baseInvoiceItem->qty }}</td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->total, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->tax_amount, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.discount') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->discount_amount, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->total + $baseInvoiceItem->tax_amount - $sellerOrder->discount_amount, $sellerOrder->order->order_currency_code) }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="ml-auto mr-4">
                                <ul class="list-group">
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.subtotal') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->sub_total, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->shipping_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.tax') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->tax_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.discount') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->discount_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.grand-total') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->grand_total, $sellerOrder->order->order_currency_code) }}</span></li>
                                </ul>
                            </div>
                        </div>

                @endforeach
            </div>
        </div>


    </div>

    {{-- Refunds --}}
    <div id="tab-content-2" role="tabpanel" class="tab-pane tabs-animation fade">
        <div class="main-card mb-2 card">
            <div class="card-body">
                <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.sales.orders.refunds') }}</h5>
                    adsdasads
            </div>
        </div>
    </div>

    {{-- Shipments --}}
    <div id="tab-content-3" role="tabpanel" class="tab-pane tabs-animation fade">
        <div class="main-card mb-2 card">
            <div class="card-body">
                <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.sales.orders.shipments') }}</h5>
                @foreach ($sellerOrder->shipments as $sellerShipment)

                    <span class="mb-2">{{ __('shop::app.customer.account.order.view.individual-shipment', ['shipment_id' => $sellerShipment->shipment_id]) }}</span>

                    <div class="row">
                        <div class="col-6">
                            <ul class="list-group">
                                @if ($sellerShipment->shipment->inventory_source)
                                    <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.inventory-source') }}:</b> <span class="float-right ml-3">{{ $sellerShipment->shipment->inventory_source->name }}</span></li>
                                @endif
                                <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.carrier-title') }}:</b> <span class="float-right ml-3">{{ $sellerShipment->shipment->carrier_title }}</span></li>
                                <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.tracking-number') }}:</b> <span class="float-right ml-3">{{ $sellerShipment->shipment->track_number }}</span></li>
                                <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.email') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_email }}</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="table table-responsive">
                        <table class="table mt-3">
                            <thead>
                            <tr>
                                <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($sellerShipment->items as $sellerShipmentItem)

                                <tr>
                                    <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">{{ $sellerShipmentItem->item->sku }}</td>
                                    <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $sellerShipmentItem->item->name }}</td>
                                    <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}">{{ $sellerShipmentItem->item->qty }}</td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>


{{--    --}}

                @endforeach
            </div>
        </div>
    </div>

</div>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                    <div class="col-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-address') }}</div>
                                            <hr>
                                            <div class="widget-subheading">{{ $sellerOrder->order->billing_address->name }}</div>
                                            <div class="widget-subheading">{{ $sellerOrder->order->billing_address->address1 }}</div>
                                            <div class="widget-subheading">{{ $sellerOrder->order->billing_address->city }}</div>
                                            <div class="widget-subheading">{{ $sellerOrder->order->billing_address->state }}</div>
                                            <div class="widget-subheading">{{ core()->country_name($sellerOrder->order->billing_address->country) }} {{ $sellerOrder->order->billing_address->postcode }}</div>
                                            <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $sellerOrder->order->billing_address->phone }}</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.billing-address') }}</div>
                                            <hr>
                                            <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->name }}</div>
                                            <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->address1 }}</div>
                                            <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->city }}</div>
                                            <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->state }}</div>
                                            <div class="widget-subheading">{{ core()->country_name($sellerOrder->order->shipping_address->country) }} {{ $sellerOrder->order->shipping_address->postcode }}</div>
                                            <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $sellerOrder->order->shipping_address->phone }}</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-method') }}</div>
                                            <hr>
                                            <div class="widget-subheading">{{ $sellerOrder->order->shipping_title }}</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.payment-method') }}</div>
                                            <hr>
                                            <div class="widget-subheading">{{ core()->getConfigData('sales.paymentmethods.' . $sellerOrder->order->payment->method . '.title') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>


        {!! view_render_event('marketplace.sellers.account.sales.orders.view.after', ['sellerOrder' => $sellerOrder]) !!}


@endsection