@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.dashboard.title') }}
@endsection

@section('content')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-graph2 icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('marketplace::app.shop.sellers.account.dashboard.title') }}
                <div class="page-title-subheading">An overview of your shop</div>
            </div>
        </div>
      
    </div>
</div>   



{!! view_render_event('marketplace.sellers.account.dashboard.before') !!}


@php

    $seller = App\Models\ExtendWebkul\Seller::where('customer_id', auth()->guard('customer')->user()->id)->first();

    // dd($seller->totalOrderItems);
    // dd($seller->avgItemsPerOrder);

@endphp
    <div class="row">
            <div class="col-md-4">
                <div class="card mb-2 widget-chart widget-chart2 text-left">
                    <div class="widget-chart-content">
                        <div class="widget-chart-flex">
                            <div class="widget-title">
                                {{ __('admin::app.dashboard.total-orders') }}
                            </div>
                        </div>
                        <div class="widget-chart-flex">
                            <div class="widget-numbers text-primary">
                                <span>{{ $statistics['total_orders']['current'] }}</span>
                            </div>
    
                            @if ($statistics['total_orders']['progress'] < 0)
                                <div class="widget-description ml-auto text-danger">
                                    <i class="fa fa-angle-down "></i>
                                    <span class="pl-1">
                                        <span>
                                            {{ -number_format($statistics['total_orders']['progress']) }} %
                                        </span>
                                    </span>
                                </div>
                            @else
                                <div class="widget-description ml-auto text-success">
                                    <i class="fa fa-angle-up "></i>
                                    <span class="pl-1">
                                        <span>{{ -number_format($statistics['total_orders']['progress']) }} %</span>
                                    </span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        <div class="col-md-4">
            <div class="card mb-2 widget-chart widget-chart2 text-left">
                <div class="widget-chart-content">
                    <div class="widget-chart-flex">
                        <div class="widget-title">
                            Total ordered items
                        </div>
                    </div>
                    <div class="widget-chart-flex">
                        <div class="widget-numbers text-primary">
                            <span>{{ $seller->totalOrderItems }}</span>
                        </div>

                        {{-- @if ($statistics['total_orders']['progress'] < 0) --}}
                            {{-- <div class="widget-description ml-auto text-danger">
                                <i class="fa fa-angle-down "></i>
                                <span class="pl-1">
                                    <span>
                                        {{ -number_format($statistics['total_orders']['progress']) }} %
                                    </span>
                                </span>
                            </div>
                        @else
                            <div class="widget-description ml-auto text-success">
                                <i class="fa fa-angle-up "></i>
                                <span class="pl-1">
                                    <span>{{ -number_format($statistics['total_orders']['progress']) }} %</span>
                                </span>
                            </div>
                        @endif --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mb-2 widget-chart widget-chart2 text-left">
                <div class="widget-chart-content">
                    <div class="widget-chart-flex">
                        <div class="widget-title">
                            Average items per order
                        </div>
                    </div>
                    <div class="widget-chart-flex">
                        <div class="widget-numbers text-primary">
                            <span>{{ $seller->avgItemsPerOrder }}</span>
                        </div>

                        {{-- @if ($statistics['total_orders']['progress'] < 0)
                            <div class="widget-description ml-auto text-danger">
                                <i class="fa fa-angle-down "></i>
                                <span class="pl-1">
                                    <span>
                                        {{ -number_format($statistics['total_orders']['progress']) }} %
                                    </span>
                                </span>
                            </div>
                        @else
                            <div class="widget-description ml-auto text-success">
                                <i class="fa fa-angle-up "></i>
                                <span class="pl-1">
                                    <span>{{ -number_format($statistics['total_orders']['progress']) }} %</span>
                                </span>
                            </div>
                        @endif --}}
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6 mt-3">
            <div class="card mb-2 widget-chart widget-chart2 text-left">
                <div class="widget-chart-content">
                    <div class="widget-chart-flex">
                        <div class="widget-title">
                            {{ __('admin::app.dashboard.total-sale') }}
                        </div>
                    </div>
                    <div class="widget-chart-flex">
                        <div class="widget-numbers text-primary">
                            <span>{{ core()->formatBasePrice($statistics['total_sales']['current']) }}</span>
                        </div>

                        @if ($statistics['total_orders']['progress'] < 0)
                            <div class="widget-description ml-auto text-danger">
                                <i class="fa fa-angle-down "></i>
                                <span class="pl-1">
                                    <span>
                                        {{ -number_format($statistics['total_orders']['progress']) }} %
                                    </span>
                                </span>
                            </div>
                        @else
                            <div class="widget-description ml-auto text-success">
                                <i class="fa fa-angle-up "></i>
                                <span class="pl-1">
                                    <span>{{ -number_format($statistics['total_orders']['progress']) }} %</span>
                                </span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 mt-3">
            <div class="card mb-2 widget-chart widget-chart2 text-left">
                <div class="widget-chart-content">
                    <div class="widget-chart-flex">
                        <div class="widget-title">
                            {{ __('admin::app.dashboard.average-sale') }}
                        </div>
                    </div>
                    <div class="widget-chart-flex">
                        <div class="widget-numbers text-primary">
                            <span>{{ core()->formatBasePrice($statistics['avg_sales']['current']) }}</span>
                        </div>

                        @if ($statistics['total_orders']['progress'] < 0)
                            <div class="widget-description ml-auto text-danger">
                                <i class="fa fa-angle-down "></i>
                                <span class="pl-1">
                                    <span>
                                        {{ -number_format($statistics['total_orders']['progress']) }} %
                                    </span>
                                </span>
                            </div>
                        @else
                            <div class="widget-description ml-auto text-success">
                                <i class="fa fa-angle-up "></i>
                                <span class="pl-1">
                                    <span>{{ -number_format($statistics['total_orders']['progress']) }} %</span>
                                </span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>





    </div>

<div class="row">
    <div class="col-md-12 mt-3">

        {{-- NTS: load actual data --}}
        <sales-chart :sellerid="{{ $seller->id }}"></sales-chart>

    </div>
</div>


{!! view_render_event('marketplace.sellers.account.dashboard.after') !!}

@endsection

@push('scripts')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

    <script type="text/x-template" id="date-filter-template">
        <div>
            <div class="control-group date">
                <date @onChange="applyFilter('start', $event)"><input type="text" class="control" id="start_date" value="{{ $startDate->format('Y-m-d') }}" placeholder="{{ __('admin::app.dashboard.from') }}" v-model="start"/></date>
            </div>

            <div class="control-group date">
                <date @onChange="applyFilter('end', $event)"><input type="text" class="control" id="end_date" value="{{ $endDate->format('Y-m-d') }}" placeholder="{{ __('admin::app.dashboard.to') }}" v-model="end"/></date>
            </div>
        </div>
    </script>

    <script>
        Vue.component('date-filter', {

            template: '#date-filter-template',

            data: () => ({
                start: "{{ $startDate->format('Y-m-d') }}",
                end: "{{ $endDate->format('Y-m-d') }}",
            }),

            methods: {
                applyFilter(field, date) {
                    this[field] = date;

                    window.location.href = "?start=" + this.start + '&end=' + this.end;
                }
            }
        });

        $(document).ready(function () {

            var ctx = document.getElementById("myChart").getContext('2d');

            var data = @json($statistics['sale_graph']);

            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: data['label'],
                    datasets: [{
                        data: data['total'],
                        backgroundColor: 'rgba(34, 201, 93, 1)',
                        borderColor: 'rgba(34, 201, 93, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            maxBarThickness: 20,
                            gridLines : {
                                display : true,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: true,
                                fontColor: 'rgba(162, 162, 162, 1)'
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                drawBorder: false,
                            },
                            ticks: {
                                padding: 20,
                                beginAtZero: true,
                                fontColor: 'rgba(162, 162, 162, 1)'
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        displayColors: false,
                        callbacks: {
                            label: function(tooltipItem, dataTemp) {
                                return data['formated_total'][tooltipItem.index];
                            }
                        }
                    }
                }
            });
        });
    </script>

@endpush