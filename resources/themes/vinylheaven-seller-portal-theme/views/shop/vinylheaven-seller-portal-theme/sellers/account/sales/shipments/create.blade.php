@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.sales.shipments.create-title') }}
@endsection

@section('content')
    <form method="POST" action="{{ route('marketplace.account.shipments.store', $sellerOrder->order_id) }}" @submit.prevent="onSubmit">
        @csrf

    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-plus icon-gradient bg-tempting-azure"></i>
                </div>
                <div>{{ __('marketplace::app.shop.sellers.account.sales.shipments.create-title') }}
                    <div class="page-title-subheading">An overview of orders made at your shop</div>
                </div>
            </div>

            <div class="page-title-actions">
                <div class="d-inline-block">
                    <button type="submit" style="color:white;" class="btn-shadow btn btn-info">
                        {{ __('marketplace::app.shop.sellers.account.sales.shipments.create') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

    {!! view_render_event('marketplace.sellers.account.sales.shipments.create.before', ['sellerOrder' => $sellerOrder]) !!}


    <div class="main-card mb-2 card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <ul class="list-group">
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.shipments.order-id') }}:</b> <span class="float-right ml-3"><a href="{{ route('marketplace.account.orders.view', $sellerOrder->order_id) }}">#{{ $sellerOrder->order_id }}</a></span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.placed-on') }}:</b> <span class="float-right ml-3">{{ core()->formatDate($sellerOrder->created_at, 'd M Y') }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.status') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->status_label }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.customer-name') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_full_name }}</span></li>

                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.customer-name') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_full_name }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.email') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_email }}</span></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-card mb-2 card">
        <div class="card-body">
            <div class="control-group form-row mb-3" :class="[errors.has('price') ? 'has-error' : '']">
                <div class="col-md-6">
                    <label for="shipment[carrier_title]" class="required">{{ __('marketplace::app.shop.sellers.account.sales.shipments.carrier-title') }}</label>
                    <input type="text" class="form-control" :class="errors.has('shipment[carrier_title]') ? 'is-invalid'  : ''" v-validate="'required'" id="shipment[carrier_title]" name="shipment[carrier_title]" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.sales.shipments.carrier-title') }}&quot;"/>
                </div>

                <div class="col-md-6">
                    <label for="shipment[track_number]" class="required">{{ __('marketplace::app.shop.sellers.account.sales.shipments.tracking-number') }}</label>
                    <input type="text" class="form-control" :class="errors.has('shipment[track_number]') ? 'is-invalid'  : ''" v-validate="'required'" id="shipment[track_number]" name="shipment[track_number]" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.sales.shipments.tracking-number') }}&quot;"/>
                </div>
            </div>
        </div>
    </div>

    <div class="main-card mb-2 card">
        <div class="card-body">
            <h5 class="card-title">{{ __('shop::app.customer.account.order.view.products-ordered') }}</h5>

            <order-item-list></order-item-list>

        </div>
    </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-address') }}</div>
                                                    <hr>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->billing_address->name }}</div>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->billing_address->address1 }}</div>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->billing_address->city }}</div>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->billing_address->state }}</div>
                                                    <div class="widget-subheading">{{ core()->country_name($sellerOrder->order->billing_address->country) }} {{ $sellerOrder->order->billing_address->postcode }}</div>
                                                    <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $sellerOrder->order->billing_address->phone }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">{{ __('shop::app.customer.account.order.view.billing-address') }}</div>
                                                    <hr>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->name }}</div>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->address1 }}</div>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->city }}</div>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->state }}</div>
                                                    <div class="widget-subheading">{{ core()->country_name($sellerOrder->order->shipping_address->country) }} {{ $sellerOrder->order->shipping_address->postcode }}</div>
                                                    <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $sellerOrder->order->shipping_address->phone }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-method') }}</div>
                                                    <hr>
                                                    <div class="widget-subheading">{{ $sellerOrder->order->shipping_title }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="widget-content p-0">
                                            <div class="widget-content-wrapper">
                                                <div class="widget-content-left">
                                                    <div class="widget-heading">{{ __('shop::app.customer.account.order.view.payment-method') }}</div>
                                                    <hr>
                                                    <div class="widget-subheading">{{ core()->getConfigData('sales.paymentmethods.' . $sellerOrder->order->payment->method . '.title') }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


            {!! view_render_event('marketplace.sellers.account.sales.shipments.create.after', ['sellerOrder' => $sellerOrder]) !!}

        </form>

@endsection

@push('scripts')

    <script type="text/x-template" id="order-item-list-template">

        <div>



            <div class="position-relative form-group" :class="[errors.has('shipment[source]') ? 'has-error' : '']">
                <label for="shipment[source]" class="required">{{ __('marketplace::app.shop.sellers.account.sales.shipments.source') }}</label>

                <select v-validate="'required'" class="form-control" name="shipment[source]" id="shipment[source]" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.sales.shipments.source') }}&quot;" v-model="source">
                    <option value="">{{ __('marketplace::app.shop.sellers.account.sales.shipments.select-source') }}</option>

                    @foreach (core()->getCurrentChannel()->inventory_sources as $inventorySource)
                        <option value="{{ $inventorySource->id }}">{{ $inventorySource->name }}</option>
                    @endforeach

                </select>

                <span  class="mt-1" style="color:red;" v-if="errors.has('shipment[source]')">
                    @{{ errors.first('shipment[source]') }}
                </span>
            </div>

            <div class="table table-responsive">

                <table class="table">
                    <thead>
                        <tr>
                            <th>{{ __('marketplace::app.shop.sellers.account.sales.shipments.product-name') }}</th>
                            <th>{{ __('marketplace::app.shop.sellers.account.sales.shipments.qty-ordered') }}</th>
                            <th>{{ __('marketplace::app.shop.sellers.account.sales.shipments.qty-to-ship') }}</th>
                            <th>{{ __('marketplace::app.shop.sellers.account.sales.shipments.available-sources') }}</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach ($sellerOrder->items as $sellerOrderItem)
                            @if ($sellerOrderItem->item->qty_to_ship > 0 && $sellerOrderItem->product)
                                <tr>
                                    <td>
                                        {{ $sellerOrderItem->item->name }}

                                        @if (isset($sellerOrderItem->additional['attributes']))
                                            <div class="item-options">

                                                @foreach ($sellerOrderItem->additional['attributes'] as $attribute)
                                                    <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                                @endforeach

                                            </div>
                                        @endif
                                    </td>
                                    <td>{{ $sellerOrderItem->item->qty_ordered }}</td>
                                    <td>{{ $sellerOrderItem->item->qty_to_ship }}</td>
                                    <td>
                                        <div class="table table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>{{ __('marketplace::app.shop.sellers.account.sales.shipments.source') }}</th>
                                                        <th>{{ __('marketplace::app.shop.sellers.account.sales.shipments.qty-available') }}</th>
                                                        <th>{{ __('marketplace::app.shop.sellers.account.sales.shipments.qty-to-ship') }}</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach (core()->getCurrentChannel()->inventory_sources as $inventorySource)
                                                        <tr>
                                                            <td>
                                                                {{ $inventorySource->name }}
                                                            </td>

                                                            <td>
                                                                <?php
                                                                    $sourceQty = 0;

                                                                    $product = $sellerOrderItem->item->type == 'configurable' ? $sellerOrderItem->child->product->product : $sellerOrderItem->product->product;

                                                                    foreach ($product->inventories as $inventory) {
                                                                        if ($inventory->inventory_source_id == $inventorySource->id && $inventory->vendor_id == $sellerOrder->marketplace_seller_id) {
                                                                            $sourceQty = $inventory->qty;
                                                                            break;
                                                                        }
                                                                    }
                                                                ?>

                                                                {{ $sourceQty }}
                                                            </td>

                                                            <td>
                                                                <?php $inputName = "shipment[items][$sellerOrderItem->order_item_id][$inventorySource->id]"; ?>

                                                                <div class="control-group" :class="[errors.has('{{ $inputName }}') ? 'has-error' : '']">

                                                                    <input type="text" v-validate="'required|numeric|min_value:0|max_value:{{$sourceQty}}'" class="form-control" id="{{ $inputName }}" name="{{ $inputName }}" value="0" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.sales.shipments.qty-to-ship') }}&quot;" :disabled="source != '{{ $inventorySource->id }}'"/>

                                                                    <span class="control-error" v-if="errors.has('{{ $inputName }}')">
                                                                        @verbatim
                                                                            {{ errors.first('<?php echo $inputName; ?>') }}
                                                                        @endverbatim
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </script>

    <script>
        Vue.component('order-item-list', {

            template: '#order-item-list-template',

            inject: ['$validator'],

            data: () => ({
                source: ""
            })
        });
    </script>

@endpush