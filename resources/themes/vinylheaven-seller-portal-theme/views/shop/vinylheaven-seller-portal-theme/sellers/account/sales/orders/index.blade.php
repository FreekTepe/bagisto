@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.sales.orders.title') }}
@endsection

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-rocket icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('marketplace::app.shop.sellers.account.sales.orders.title') }}
                <div class="page-title-subheading">An overview of orders made at your shop</div>
            </div>
        </div>

    </div>
</div>

    <div class="account-layout">

        {!! view_render_event('marketplace.sellers.account.sales.orders.list.before') !!}

        <div class="account-items-list">

            {!! app('Webkul\Marketplace\DataGrids\Shop\OrderDataGrid')->render() !!}

        </div>

        {!! view_render_event('marketplace.sellers.account.sales.orders.list.after') !!}

    </div>

@endsection