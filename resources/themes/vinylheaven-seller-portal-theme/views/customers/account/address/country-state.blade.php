<country-state></country-state>

@push('scripts')

    <script type="text/x-template" id="country-state-template">
        <div>
            <div class="position-relative row form-group" :class="[errors.has('country') ? 'has-error' : '']">
                <label for="country" class="col-sm-2 col-form-label required">
                    {{ __('shop::app.customer.account.address.create.country') }}
                </label>
                <div class="col-sm-10">
                    <select type="text" v-validate="'required'" :class="errors.has('country') ? 'is-invalid'  : ''"  name="country" id="country" v-model="country"  data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.country') }}&quot;" class="form-control">
                        <option value=""></option>
                        @foreach (core()->countries() as $country)
                            <option {{ $country->code === $defaultCountry ? 'selected' : '' }}  value="{{ $country->code }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group" :class="[errors.has('state') ? 'has-error' : '']">
                <label for="state" class="col-sm-2 col-form-label required">
                    {{ __('shop::app.customer.account.address.create.state') }}

                </label>
                <div class="col-sm-10">
                    <input type="text" v-validate="'required'" class="form-control" :class="errors.has('state') ? 'is-invalid'  : ''" id="state" name="state" v-model="state" v-if="!haveStates()" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.state') }}&quot;"/>

                    <select v-validate="'required'"   id="state" name="state" v-model="state" v-if="haveStates()" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.state') }}&quot;" >
                        <option value="">{{ __('shop::app.customer.account.address.create.select-state') }}</option>
                    </select>
                </div>
            </div>

        </div>
    </script>

    <script>
        Vue.component('country-state', {

            template: '#country-state-template',

            inject: ['$validator'],

            data() {
                return {
                    country: "{{ $countryCode ?? $defaultCountry }}",

                    state: "{{ $stateCode }}",

                    countryStates: @json(core()->groupedStatesByCountries())
                }
            },

            methods: {
                haveStates() {
                    if (this.countryStates[this.country] && this.countryStates[this.country].length)
                        return true;

                    return false;
                },
            }
        });
    </script>
@endpush