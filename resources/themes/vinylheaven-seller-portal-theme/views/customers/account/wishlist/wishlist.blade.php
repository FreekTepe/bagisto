@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.wishlist.page-title') }}
@endsection

@section('content-wrapper')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-like icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('shop::app.customer.account.wishlist.title') }}
                <div class="page-title-subheading">Products you have added to your wishlist.</div>
            </div>
        </div>
    </div>
</div>   


@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

@inject ('reviewHelper', 'Webkul\Product\Helpers\Review')


<div class="container-fluid mt-4">

        {!! view_render_event('bagisto.shop.customers.account.wishlist.list.before', ['wishlist' => $items]) !!}


        @if ($items->count())
        <div class="row">
            @foreach ($items as $item)
                <div class="col-md-3">
                    <div class="card mb-3">
                        @php
                            $image = $item->product->getTypeInstance()->getBaseImage($item);
                        @endphp
                        <img class="card-img-top" src="{{ $image['small_image_url'] }}">
                        <div class="card-body">

                            {{-- Product name --}}
                            <h4 class="card-title">{{ $item->product->name }}</h4>

                            <p class="card-text">
                                {{-- attributes --}}
                                @if (isset($item->additional['attributes']))
                                    <div class="item-options">
                                        @foreach ($item->additional['attributes'] as $attribute)
                                            <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                        @endforeach
                                    </div>
                                @endif
                                {{-- Stars --}}
                                <span class="stars" style="display: inline">
                                    @for ($i = 1; $i <= $reviewHelper->getAverageRating($item->product); $i++)
                                        <span class="icon star-icon"></span>
                                    @endfor
                                </span>
                            </p>
                        </div>

                        <div class="d-block text-right card-footer">
                            <a href="{{ route('customer.wishlist.move', $item->id) }}" class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow">
                                {{ __('shop::app.customer.account.wishlist.move-to-cart') }}
                            </a> 
                            <a href="{{ route('customer.wishlist.remove', $item->id) }}" class="btn-icon btn-icon-only btn btn-danger btn-sm btn-shadow">
                                <i class="lnr-trash btn-icon-wrapper"></i>
                            </a>
                        </div>

                    </div>
                </div>
            @endforeach

        </div>
            {{ $items->links('vendor.pagination.architect')  }}

        @else
            <div class="empty">
                {{ __('customer::app.wishlist.empty') }}
            </div>
        @endif

        {!! view_render_event('bagisto.shop.customers.account.wishlist.list.after', ['wishlist' => $items]) !!}

</div>

@endsection


{{-- 
    <div class="account-content">
        @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

        @inject ('reviewHelper', 'Webkul\Product\Helpers\Review')

        <div class="account-layout">

            <div class="account-head mb-15">
                <span class="account-heading">{{ __('shop::app.customer.account.wishlist.title') }}</span>

                @if (count($items))
                    <div class="account-action">
                        <a href="{{ route('customer.wishlist.removeall') }}">{{ __('shop::app.customer.account.wishlist.deleteall') }}</a>
                    </div>
                @endif

                <div class="horizontal-rule"></div>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.wishlist.list.before', ['wishlist' => $items]) !!}

            <div class="account-items-list">

                @if ($items->count())
                    @foreach ($items as $item)
                        <div class="account-item-card mt-15 mb-15">
                            <div class="media-info">
                                @php
                                    $image = $item->product->getTypeInstance()->getBaseImage($item);
                                @endphp

                                <img class="media" src="{{ $image['small_image_url'] }}" />

                                <div class="info">
                                    <div class="product-name">
                                        {{ $item->product->name }}

                                        @if (isset($item->additional['attributes']))
                                            <div class="item-options">

                                                @foreach ($item->additional['attributes'] as $attribute)
                                                    <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                                @endforeach

                                            </div>
                                        @endif
                                    </div>

                                    <span class="stars" style="display: inline">
                                        @for ($i = 1; $i <= $reviewHelper->getAverageRating($item->product); $i++)
                                            <span class="icon star-icon"></span>
                                        @endfor
                                    </span>
                                </div>
                            </div>

                            <div class="operations">
                                <a class="mb-50" href="{{ route('customer.wishlist.remove', $item->id) }}">
                                    <span class="icon trash-icon"></span>
                                </a>

                                <a href="{{ route('customer.wishlist.move', $item->id) }}" class="btn btn-primary btn-md">
                                    {{ __('shop::app.customer.account.wishlist.move-to-cart') }}
                                </a>
                            </div>
                        </div>

                        <div class="horizontal-rule mb-10 mt-10"></div>
                    @endforeach

                    <div class="bottom-toolbar">
                        {{ $items->links()  }}
                    </div>
                @else
                    <div class="empty">
                        {{ __('customer::app.wishlist.empty') }}
                    </div>
                @endif
            </div>

            {!! view_render_event('bagisto.shop.customers.account.wishlist.list.after', ['wishlist' => $items]) !!}

        </div>
    </div>
@endsection --}}