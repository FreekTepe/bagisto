@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.downloadable_products.title') }}
@endsection

@section('content-wrapper')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-cloud-download icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('shop::app.customer.account.downloadable_products.title') }}
                <div class="page-title-subheading">Your downloadable products.</div>
            </div>
        </div>
    </div>
</div>   


{!! view_render_event('bagisto.shop.customers.account.downloadable_products.list.before') !!}


        {!! app('Webkul\Shop\DataGrids\DownloadableProductDataGrid')->render() !!}
        

{!! view_render_event('bagisto.shop.customers.account.downloadable_products.list.after') !!}

@endsection