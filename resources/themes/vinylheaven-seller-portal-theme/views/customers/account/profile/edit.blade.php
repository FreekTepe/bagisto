@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.profile.edit-profile.page-title') }}
@endsection

@section('content-wrapper')


{!! view_render_event('bagisto.shop.customers.account.profile.edit.before', ['customer' => $customer]) !!}

    <form method="post" action="{{ route('customer.profile.edit') }}" @submit.prevent="onSubmit">
    @csrf

{!! view_render_event('bagisto.shop.customers.account.profile.edit_form_controls.before', ['customer' => $customer]) !!}


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-note icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('shop::app.customer.account.profile.edit-profile.title') }}
                <div class="page-title-subheading">Edit your profile.</div>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block">
                <button  type="submit" style="color:white;" class="btn-shadow btn btn-info">
                    {{ __('marketplace::app.shop.sellers.account.profile.save-btn-title') }}
                </button>
            </div>
        </div>    
    </div>
</div>   


 {{-- FORM --}}

 <div class="col-md-12">
     <div class="main-card mb-2 card">

        {!! view_render_event('bagisto.shop.customers.account.profile.edit_form_controls.before', ['customer' => $customer]) !!}

         <div class="card-body">
             <h5 class="card-title">{{ __('shop::app.customer.account.profile.edit-profile.title') }}</h5>
            
            {{-- first_name --}}
            <div class="position-relative row form-group" :class="[errors.has('first_name') ? 'has-error' : '']">
                <label for="first_name" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.profile.fname') }}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" :class="errors.has('first_name') ? 'is-invalid'  : ''" name="first_name" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.fname') }}&quot;" value="{{ old('first_name') ?? $customer->first_name }}" >
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.profile.edit.first_name.after') !!}

            {{-- last_name --}}
            <div class="position-relative row form-group" :class="[errors.has('last_name') ? 'has-error' : '']">
                <label for="last_name" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.profile.fname') }}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" :class="errors.has('last_name') ? 'is-invalid'  : ''" name="last_name" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.fname') }}&quot;" value="{{ old('last_name') ?? $customer->last_name }}" >
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.profile.edit.last_name.after') !!}

            {{-- gender --}}
            <div class="position-relative row form-group" :class="[errors.has('gender') ? 'has-error' : '']">
                <label for="country" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.profile.gender') }}</label> 
                <div class="col-sm-10">
                    <select name="gender" class="form-control"  :class="errors.has('gender') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.gender') }}&quot;">
                        <option value=""  @if ($customer->gender == "") selected @endif></option>
                        <option value="Other"  @if ($customer->gender == "Other") selected @endif>{{ __('shop::app.customer.account.profile.other') }}</option>
                        <option value="Male"  @if ($customer->gender == "Male") selected @endif>{{ __('shop::app.customer.account.profile.male') }}</option>
                        <option value="Female" @if ($customer->gender == "Female") selected @endif>{{ __('shop::app.customer.account.profile.female') }}</option>
                    </select>
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.profile.edit.gender.after') !!}


            {{-- NTS: !! the input date format isnt properly saved to database --}}
            {{-- date_of_birth --}}
            <div class="position-relative row form-group" :class="[errors.has('date_of_birth') ? 'has-error' : '']">
                <label for="date_of_birth" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.profile.dob') }}</label> 
                <div class="col-sm-10 input-group">
                    <input type="text" name="date_of_birth" class="form-control" :class="errors.has('date_of_birth') ? 'is-invalid'  : ''" v-validate="'required'"  data-toggle="datepicker-button" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.dob') }}&quot;" />
                    <div class="input-group-append">
                        <button class="btn btn-info btn-shadow datepicker-trigger-btn" type="button">Open Datepicker</button>
                    </div>        
                </div>
            </div>

            {{-- OLD, this format works --}}
            {{--
                <div class="control-group"  :class="[errors.has('date_of_birth') ? 'has-error' : '']">
                    <label for="date_of_birth">{{ __('shop::app.customer.account.profile.dob') }}</label>
                    <input type="date" class="control" name="date_of_birth" value="{{ old('date_of_birth') ?? $customer->date_of_birth }}" v-validate="" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.dob') }}&quot;">
                    <span class="control-error" v-if="errors.has('date_of_birth')">@{{ errors.first('date_of_birth') }}</span>
                </div> 
            --}}
            {{-- -- --}}

            {!! view_render_event('bagisto.shop.customers.account.profile.edit.date_of_birth.after') !!}

            {{-- email --}}
            <div class="position-relative row form-group" :class="[errors.has('email') ? 'has-error' : '']">
                <label for="email" class="col-sm-2 col-form-label">{{ __('shop::app.customer.account.profile.email') }}</label>
                <div class="col-sm-10">
                    <input name="email" id="email" type="email" class="form-control" :class="errors.has('email') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.email') }}&quot;">
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.profile.edit.email.after') !!}

            {{-- oldpassword --}}
            <div class="position-relative row form-group" :class="[errors.has('oldpassword') ? 'has-error' : '']">
                <label for="oldpassword" class="col-sm-2 col-form-label">{{ __('shop::app.customer.account.profile.opassword') }}</label>
                <div class="col-sm-10">
                    <input name="oldpassword" id="oldpassword" type="password" class="form-control"  :class="errors.has('oldpassword') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.opassword') }}&quot;">
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.profile.edit.oldpassword.after') !!}

            {{-- password --}}
            <div class="position-relative row form-group" :class="[errors.has('password') ? 'has-error' : '']">
                <label for="password" class="col-sm-2 col-form-label">{{ __('shop::app.customer.account.profile.password') }}</label>
                <div class="col-sm-10">
                    <input name="password" id="password" type="password" class="form-control"  :class="errors.has('password') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.password') }}&quot;">
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.profile.edit.password.after') !!}

            {{-- password_confirmation --}}
            <div class="position-relative row form-group" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                <label for="password_confirmation" class="col-sm-2 col-form-label">{{ __('shop::app.customer.account.profile.cpassword') }}</label>
                <div class="col-sm-10">
                    <input name="password_confirmation" id="password_confirmation" type="password" class="form-control"  :class="errors.has('password_confirmation') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.cpassword') }}&quot;">
                </div>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.profile.edit_form_controls.after', ['customer' => $customer]) !!}


        </div>
    </div> 
 </div>
</form>

{!! view_render_event('bagisto.shop.customers.account.profile.edit.after', ['customer' => $customer]) !!}


{{-- END --}}
@endsection
