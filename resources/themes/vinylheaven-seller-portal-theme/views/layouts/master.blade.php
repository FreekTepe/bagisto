<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <title>@yield('page_title')</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-language" content="{{ app()->getLocale() }}">


    @if (\Auth::guard('customer')->user() !== NULL)
        @php
            $jwt_user = \Auth::guard('customer')->user();
            $token = \JWTAuth::fromUser($jwt_user);
        @endphp
        <meta name="jwt" content="{{ $token }}">
    @endif

    @meta_tags

    @if (!in_array(Route::getCurrentRoute()->getName(), config('exclude-bliss-css')))
        {{-- BLISS CSS --}}
{{--         <link rel="stylesheet" href="{{ bagisto_asset('css/shop.css') }}">--}}
{{--        <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}">--}}
    @endif

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

    @if ($favicon = core()->getCurrentChannel()->favicon_url)
        <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
    @else
        <link rel="icon" sizes="16x16" href="{{ bagisto_asset('images/favicon.ico') }}" />
    @endif

    @yield('head')

    @section('seo')
        @if (! request()->is('/'))
            <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
        @endif
    @show

    @stack('css')



    <link rel="stylesheet" href="{{ asset('themes/vinylheaven-seller-portal-theme/assets/css/base.min.css') }}">


    <style scoped>
        .alert-wrapper {
            width: 300px;
            top: 10px;
            right: 10px;
            position: fixed;
            z-index: 100;
            text-align: left;
        }

        .alert {
          width: 300px;
          padding: 15px;
          border-radius: 3px;
          display: inline-block;
          box-shadow: 0 4px 15.36px 0.64px rgba(0, 0, 0, 0.1),
            0 2px 6px 0 rgba(0, 0, 0, 0.12);
          position: relative;
          -webkit-animation: jelly 0.5s ease-in-out;
          animation: jelly 0.5s ease-in-out;
          transform-origin: center top;
          z-index: 500;
          margin-bottom: 10px;
        }

        .alert.alert-error {
          background: #fc6868;
        }
        .alert.alert-info {
          background: #204d74;
        }
        .alert.alert-success {
          background: #4caf50;
        }
        .alert.alert-warning {
          background: #ffc107;
        }
        .alert .icon {
          position: absolute;
          right: 10px;
          top: 10px;
          cursor: pointer;
        }
        .alert p {
          color: #fff;
          margin: 0;
          padding: 0;
          font-size: 15px;
        }

        </style>

    {!! view_render_event('bagisto.shop.layout.head') !!}

</head>


<body @if (core()->getCurrentLocale()->direction == 'rtl') class="rtl" @endif style="scroll-behavior: smooth;">

    {!! view_render_event('bagisto.shop.layout.body.before') !!}

    <div id="app" class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
        <flash-wrapper ref='flashes'></flash-wrapper>

        {{-- <div class="main-container-wrapper"> --}}

            {!! view_render_event('bagisto.shop.layout.header.before') !!}

             {{-- top navigation bar --}}
            @include('shop::layouts.header.index')

            {!! view_render_event('bagisto.shop.layout.header.after') !!}

            {{-- @yield('slider') --}}

            <div class="app-main">


                {{-- Render sidebar (left) --}}
                @include('shop::customers.account.partials.sidemenu')

                {{-- Yield page content --}}

               <div class="container-fluid" style="padding-left: 0; padding-right: 0;">
                <div class="app-main__outer">
                    {{-- Main content --}}
                    {!! view_render_event('bagisto.shop.layout.content.before') !!}
                    <div class="app-main__inner" >

                        @yield('content-wrapper')
                    </div>
                    {!! view_render_event('bagisto.shop.layout.content.after') !!}

                    {{-- Footer --}}
                    {!! view_render_event('bagisto.shop.layout.footer.before') !!}


                    {{-- RENDER THE Architect FOOTER --}}
                    @include('shop::layouts.footer.footer')

                    {!! view_render_event('bagisto.shop.layout.footer.after') !!}

                    @if (core()->getConfigData('general.content.footer.footer_toggle'))
                    <div class="footer">
                        <p style="text-align: center;">
                            @if (core()->getConfigData('general.content.footer.footer_content'))
                                {{ core()->getConfigData('general.content.footer.footer_content') }}
                            @else
                                {!! trans('admin::app.footer.copy-right') !!}
                            @endif
                        </p>
                    </div>
                    @endif

                </div>
            </div>



            </div>


    </div>

    <script type="text/javascript">
        window.flashMessages = [];

        @if ($success = session('success'))
            window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
        @elseif ($warning = session('warning'))
            window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
        @elseif ($error = session('error'))
            window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }
            ];
        @elseif ($info = session('info'))
            window.flashMessages = [{'type': 'alert-info', 'message': "{{ $info }}" }
            ];
        @endif

        window.serverErrors = [];
        @if(isset($errors))
            @if (count($errors))
                window.serverErrors = @json($errors->getMessages());
            @endif
        @endif
    </script>

    <script type="text/javascript" src="{{ bagisto_asset('js/shop.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>

    @stack('scripts')


    <!-- ARCHITECT PLAIN JS SCRIPTS INCLUDES-->
    <!--CORE-->
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> --}}
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/metismenu"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/app.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/demo.js') }}"></script>

    <!--CHARTS-->

    <!--Apex Charts-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/charts/apex-charts.js') }}"></script>

    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/charts/apex-charts.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/charts/apex-series.js') }}"></script>

    <!--Sparklines-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/charts/charts-sparklines.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/charts/charts-sparklines.js') }}"></script>

    <!--Chart.js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/charts/chartsjs-utils.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/charts/chartjs.js') }}"></script>

    <!--FORMS-->

    <!--Clipboard-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/clipboard.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/clipboard.js') }}"></script>

    <!--Datepickers-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/datepicker.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/daterangepicker.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/moment.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/datepicker.js') }}"></script>

    <!--Multiselect-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/bootstrap-multiselect.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/input-select.js') }}"></script>

    <!--Form Validation-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/form-validation.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/form-validation.js') }}"></script>

    <!--Form Wizard-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/form-wizard.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/form-wizard.js') }}"></script>

    <!--Input Mask-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/input-mask.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/input-mask.js') }}"></script>

    <!--RangeSlider-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/wnumb.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/range-slider.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/range-slider.js') }}"></script>

    <!--Textarea Autosize-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/textarea-autosize.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/form-components/textarea-autosize.js') }}"></script>

    <!--Toggle Switch -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/toggle-switch.js') }}"></script>


    <!--COMPONENTS-->

    <!--BlockUI -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/blockui.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/blockui.js') }}"></script>

    <!--Calendar -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/calendar.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/calendar.js') }}"></script>

    <!--Slick Carousel -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/carousel-slider.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/carousel-slider.js') }}"></script>

    <!--Circle Progress -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/circle-progress.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/circle-progress.js') }}"></script>

    <!--CountUp -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/count-up.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/count-up.js') }}"></script>

    <!--Cropper -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/cropper.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/jquery-cropper.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/image-crop.js') }}"></script>

    <!--Maps -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/gmaps.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/jvectormap.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/maps-word-map.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/maps.js') }}"></script>

    <!--Guided Tours -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/guided-tours.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/guided-tours.js') }}"></script>

    <!--Ladda Loading Buttons -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/ladda-loading.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/spin.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/ladda-loading.js') }}"></script>

    <!--Rating -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/rating.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/rating.js') }}"></script>

    <!--Perfect Scrollbar -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/scrollbar.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/scrollbar.js') }}"></script>

    <!--Toastr-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/toastr.js') }}"></script>

    <!--SweetAlert2-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/sweet-alerts.js') }}"></script>

    <!--Tree View -->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/treeview.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/treeview.js') }}"></script>


    <!--TABLES -->
    <!--DataTables-->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js" crossorigin="anonymous"></script>

    <!--Bootstrap Tables-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/tables.js') }}"></script>

    <!--Tables Init-->
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/scripts-init/tables.js') }}"></script>

    {!! view_render_event('bagisto.shop.layout.body.after') !!}

    <div class="modal-overlay"></div>

</body>

</html>