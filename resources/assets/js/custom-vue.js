/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import Element from "element-ui";
import locale from "element-ui/lib/locale/lang/en";
import VueSweetalert2 from "vue-sweetalert2";
import VeeValidate from "vee-validate";

Vue.use(VeeValidate);
Vue.use(VueSweetalert2);

// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Element, { locale });

import "sweetalert2/dist/sweetalert2.min.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

//////////////////////////////////
///// CUSTOM VUE COMPONTENTS /////
//////////////////////////////////
import ReleaseSearch from "./custom/vue/ReleaseImport/ReleaseSearch/index";
import ReleaseByDiscogsId from "./custom/vue/ReleaseImport/ReleaseByDiscogsId/index";
import CustomCsvImport from "./custom/vue/ReleaseImport/CustomCsvImport/index";
import DiscogsCsvImport from "./custom/vue/ReleaseImport/DiscogsCsvImport/index";

import PendingCustomCsvProduct from "./custom/vue/ReleaseImport/CustomCsvImport/PendingProducts/PendingProduct";

import ArtistPicker from "./custom/vue/ArtistPicker/ArtistPicker";

import SalesChart from "./custom/vue/Charts/Seller/SalesChart";

import ArchitectImagePicker from "./custom/vue/Images/Picker/ImageWrapper";
import FormatRules from "./custom/vue/FormatRules/FormatRules";

import CreateShippingPolicy from "./custom/vue/ShippingPolicies/create.vue";
import UpdateShippingPolicy from "./custom/vue/ShippingPolicies/update.vue";

import DestinationsPicker from "./custom/vue/ShippingPolicies/components/DestinationsPicker.vue";

import PreSelectCountries from "./custom/vue/ShippingPolicies/preselectcounties.vue"


import ReleaseImage from "./custom/vue/ReleaseImport/components/ReleaseImage.vue";


Vue.component("release-search", ReleaseSearch);
Vue.component("release-by-id", ReleaseByDiscogsId);
Vue.component("custom-csv-import", CustomCsvImport);
Vue.component("discogs-csv-import", DiscogsCsvImport);
Vue.component("artist-picker", ArtistPicker);
Vue.component("pending-custom-csv-product", PendingCustomCsvProduct);
Vue.component("discogs-csv-import", DiscogsCsvImport);
Vue.component("sales-chart", SalesChart);
Vue.component("custom-image-picker", ArchitectImagePicker);
Vue.component("format-rules", FormatRules);
Vue.component("create-shipping-policy", CreateShippingPolicy);
Vue.component("update-shipping-policy", UpdateShippingPolicy);
Vue.component("destination-picker", DestinationsPicker);
Vue.component("pre-select-countries", PreSelectCountries);
Vue.component("release-image", ReleaseImage);




/////////////////////////////////////
///// ARCHITECT VUE COMPONTENTS /////
/////////////////////////////////////
