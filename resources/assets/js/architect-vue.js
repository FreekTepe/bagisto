/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Accordions from "./architect-ui/vue/Components/Accordions";
import Croppa from "vue-croppa";

Vue.use(Croppa, { componentName: "image-cropper" });
Vue.component("accordions", Accordions);
