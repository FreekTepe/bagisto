@if ($product->tracklist)
    <table class="table">
        <thead>
        <tr>
            <th style="width:10%;">Position</th>
            <th style="width:60%;">Title</th>
            <th style="width:20%;">Duration</th>
            <th style="width:5%;"></th>
        </tr>
        </thead>
        <tbody>
            @forelse (collect($product->tracklist) as $track)
            <tr>
                <td>{{ isset($track['position']) ? $track['position'] : "" }}</td>
                <td>{{ isset($track['title']) ? $track['title'] : "" }}</td>
                <td>{{ isset($track['duration']) ? $track['duration'] : "" }}</td>
            </tr>
            @empty
                <tr><td colspan="6">No tracks in relational tracklist..</td></tr>
            @endforelse
        </tbody>
    </table>
@else
    <p><b>No tracklist.</b></p>
@endif

{{-- <!-- vue tracklist editor -->
<input type="hidden" id="tracklist" name="tracklist" value="{{ old($attribute->code) ?: $product[$attribute->code] }}" />

<tracklist-editor 
    v-on:setinputvalue="(value) => { document.getElementById('tracklist').value = value; }"
    @if (isset($product[$attribute->code]))
        :tracklist-id="{{ $product[$attribute->code] }}"
    @else
        :tracklist-id="null"
    @endif
    :product-id="{{ $product->id }}"
    :attribute-id="{{ $attribute->id }}"
 >
</tracklist-editor> --}}