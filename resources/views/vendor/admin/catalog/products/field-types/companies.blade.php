<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Cat no</th>
        <th scope="col">Entity type</th>
        <th scope="col">Entity type name</th>
        <th scope="col">Resource Url</th>
    </tr>
    </thead>
    <tbody>
        @if(isset($product->companies))
            @forelse ($product->companies as $company)
            <tr>
                <th scope="row">{{ $company['id'] }}</th>
                <th scope="row">{{ $company['name'] }}</th>
                <td>{{ $company['catno'] ? $company['catno'] : "-" }}</td>
                <td>{{ $company['entity_type'] ? $company['entity_type'] : "-" }}</td>
                <td>{{ $company['entity_type_name'] ? $company['entity_type_name'] : "-" }}</td>
                <td>
                    @if ($company['resource_url'])
                        <a href="{{ $company['resource_url'] }}">{{ $company['resource_url'] }}</a>
                    @else
                        -
                    @endif
                </td>
            </tr>
            @empty
                <tr><td colspan="6">No companies.</td></tr>
            @endforelse
        @else
            <tr><td colspan="6">No companies.</td></tr>
        @endif
    </tbody>
</table>