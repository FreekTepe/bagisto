<table class="table">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Qty</th>
        <th scope="col">Text</th>
        <th scope="col">Descriptions</th>
    </tr>
    </thead>
    <tbody>
        @if(isset($product['formats']))

        @forelse ($product['formats'] as $format)
        <tr>
            <th>{{ $format['name'] ? $format['name'] : "-" }}</th>
            <th>{{ $format['qty'] ? $format['qty'] : "-" }}</th>
            <td>{{ $format['text'] ? $format['text'] : "-" }}</td>
            <td><?php if($format['descriptions']){
                if(is_array($format['descriptions'][0])){
                    foreach($format['descriptions'][0] as $format){
                        echo $format.", ";
                    }
                }else{
                    echo $format['descriptions'][0];
                }
            }else{
                echo "-";
            } ?></td>
        </tr>
        @empty
            <tr><td colspan="6">No formats.</td></tr>
        @endforelse
        @else
            <tr><td colspan="6">No formats.</td></tr>
        @endif
    </tbody>
</table>