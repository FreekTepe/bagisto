
<table class="table">
    <thead>
    <tr>
        <th scope="col">Description</th>
        <th scope="col">Type</th>
        <th scope="col">Value</th>
    </tr>
    </thead>
    <tbody>
        @if(isset($product->companies))
            @forelse ($product->identifiers as $identifier)
            <tr>
                <td>{{ $identifier['description'] ? $identifier['description'] : "-" }}</td>
                <td>{{ $identifier['type'] ? $identifier['type'] : "-" }}</td>
                <td>{{ $identifier['value'] ? $identifier['value'] : "-" }}</td>                
            </tr>
            @empty
                <tr><td colspan="6">No Identifiers.</td></tr>
            @endforelse
        @else
            <tr><td colspan="6">No Identifiers.</td></tr>
        @endif
    </tbody>
</table>