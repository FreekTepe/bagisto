@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.catalog.attributes.add-title') }}
@stop

@section('content')

<div class="tabs">
    <ul>
        <li class="active">
            <a href="#">Add format rules</a>
        </li>
    </ul>
</div>

<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>Format rules</h1>
        </div>
    </div>
</div>



{{-- Insert format rules table vue component --}}
{{-- ... --}}

<div class="page-content">
    <div class="row">
        <div class="col-12">
            <format-rules></format-rules>
        </div>
    </div>
</div>

@endsection