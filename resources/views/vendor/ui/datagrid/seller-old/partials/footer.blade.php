<tfoot>
    <tr>
        @foreach($results['columns'] as $key => $column)
            <th>
                {{ $column['label'] }}
            </th>
        @endforeach

        @if ($results['enableActions'])
            <th>
                {{ __('ui::app.datagrid.actions') }}
            </th>
        @endif
    </tr>
</tfoot>