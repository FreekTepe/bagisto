<thead>
    <tr>
        {{-- @if (count($results['records']) && $results['enableMassActions'])
            <th>
                <span class="checkbox">
                    <input type="checkbox" v-model="allSelected" v-on:change="selectAll">

                    <label class="checkbox-view" for="checkbox"></label>
                </span>
            </th>
        @endif --}}

        @foreach($results['columns'] as $key => $column)
            <th>
                {{ $column['label'] }}
            </th>
        @endforeach

        @if ($results['enableActions'])
            <th>
                {{ __('ui::app.datagrid.actions') }}
            </th>
        @endif
    </tr>
</thead>