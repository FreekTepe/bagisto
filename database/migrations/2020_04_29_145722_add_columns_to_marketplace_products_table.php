<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToMarketplaceProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketplace_products', function (Blueprint $table) {
            $table->text('comments')->nullable()->after('description');
            $table->text('private_comments')->nullable()->after('comments');
            $table->text('media_condition')->nullable()->after('private_comments');
            $table->text('sleeve_condition')->nullable()->after('media_condition');
            $table->text('location')->nullable()->after('sleeve_condition');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketplace_products', function (Blueprint $table) {
            $table->dropColumn(['tracklist_id', 'has_artists']);
        });
    }
}
