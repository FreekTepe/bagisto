<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToMarketplaceSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketplace_sellers', function (Blueprint $table) {
            $table->boolean('paypal_allowed')->default(false);
            $table->string('paypal_account')->nullable();

            $table->boolean('bank_allowed')->default(false);
            $table->string('bank_account_holder_name')->nullable();
            $table->string('bank_iban')->nullable();
            $table->string('bank_bic_swift')->nullable();

            $table->boolean('cash_allowed')->default(false);
            $table->boolean('pickup_allowed')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketplace_sellers', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'paypal_allowed', 
                    'paypal_account', 
                    'bank_allowed', 
                    'bank_account_holder_name', 
                    'bank_iban', 
                    'bank_bic_swift', 
                    'cash_allowed', 
                    'pickup_allowed'
                ]);
        });
    }
}
