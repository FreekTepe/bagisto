<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FetchedImageReleases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fetched_image_releases', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('release_id')->nullable();
            $table->string('file_name')->nullable();
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->string('image_type')->nullable();
            $table->string('url')->nullable();
            $table->string('downloaded')->nullable();
            $table->dateTime('last_updated')->nullable();

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fetched_image_releases');
    }
}
