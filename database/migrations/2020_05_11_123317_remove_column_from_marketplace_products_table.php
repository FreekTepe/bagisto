<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnFromMarketplaceProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketplace_products', function (Blueprint $table) {
            $table->dropColumn(['media_condition', 'comments', 'location']);
            $table->text('storage_location')->nullable()->after('sleeve_condition');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketplace_products', function (Blueprint $table) {
            $table->dropColumn(['storage_location']);
            $table->text('media_condition')->nullable()->after('private_comments');
            $table->text('comments')->nullable()->after('description');
            $table->text('location')->nullable()->after('sleeve_condition');
        });
    }
}
