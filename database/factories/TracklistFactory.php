<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

namespace  VinylHeaven\CustomAttributeType\Database;

use Faker\Generator as Faker;
use VinylHeaven\CustomAttributeType\Models\Tracklist;

$factory->define(Tracklist::class, function (Faker $faker) {
    return [
        'name' => NULL
    ];
});
