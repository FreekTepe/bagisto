<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Webkul\Category\Models\CategoryTranslation;
use Illuminate\Support\Str;

$factory->define(CategoryTranslation::class, function (Faker $faker) {
    return [
        'name'             => $faker->name(),
        'slug'             => Str::slug($faker->name(), '-'),
        'description'      => $faker->sentence(6, true),
        'meta_title'       => '',
        'meta_description' => '',
        'meta_keywords'    => '',
        'category_id'      => '0',
        'locale'           => 'en',
    ];
});
