<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

namespace  VinylHeaven\CustomAttributeType\Database;

use Faker\Generator as Faker;
use VinylHeaven\CustomAttributeType\Models\Track;

$factory->define(Track::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'duration' => $faker->numberBetween(2, 60)
    ];
});
