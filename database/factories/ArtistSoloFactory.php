<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

namespace  VinylHeaven\CustomAttributeType\Database;

use Faker\Generator as Faker;
use VinylHeaven\CustomAttributeType\Models\ArtistSolo;

$factory->define(ArtistSolo::class, function (Faker $faker) {
    return [
        "name" => $faker->firstNameMale,
        "realname" => $faker->firstNameMale,
        "profile" => $faker->paragraph(5, true),
        "data_quality" => $faker->paragraph(5, true)
    ];
});
