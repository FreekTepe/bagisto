<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use VinylHeaven\CustomAttributeType\Models\Label;

$factory->define(Label::class, function (Faker $faker) {
    return [
        "name" => $faker->firstNameMale,
        "contactinfo" => $faker->email,
        "profile" => $faker->paragraph(5, true),
        "data_quality" => $faker->paragraph(5, true)
    ];
});
