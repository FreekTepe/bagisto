<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Webkul\Category\Models\Category;
use Webkul\Category\Models\CategoryTranslation;
use Illuminate\Support\Str;

/**
 * Here we seed the (discogs)genres as categories
 * And the (discogs)styles as sub-categories
 */

class GenreSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        // dd($this->categories);
        DB::table('categories')->delete();

        // initialize manditory Root category
        DB::table('categories')->insert([
            [
                'id'         => '1',
                'position'   => '1',
                'image'      => NULL,
                'status'     => '1',
                '_lft'       => '1',
                '_rgt'       => '14',
                'parent_id'  => NULL,
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
        DB::table('category_translations')->insert([
            [
                'id'               => '1',
                'name'             => 'Root',
                'slug'             => 'root',
                'description'      => 'Root',
                'meta_title'       => '',
                'meta_description' => '',
                'meta_keywords'    => '',
                'category_id'      => '1',
                'locale'           => 'en',
            ]
        ]);

        // Seed all genres and sub-genres/styles from config.genres
        foreach (config('genres') as $genre => $styles) {
            $id = $this->genre($genre);
            foreach ($styles as $style) {
                $this->style($style, $id);
            }
        }
    }

    private function genre($name)
    {
        $id = factory(Category::class)->create(['parent_id' => NULL])->id;
        factory(CategoryTranslation::class)->create(['category_id' => $id, 'name' => $name, 'slug' => Str::slug($name, '-')]);
        return $id;
    }

    private function style($name, $parent_id)
    {
        $id = factory(Category::class)->create(['parent_id' => $parent_id])->id;
        factory(CategoryTranslation::class)->create(['category_id' => $id, 'name' => $name, 'slug' => Str::slug($name, '-')]);
        return $id;
    }
}
