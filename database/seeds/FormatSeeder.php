<?php

use Illuminate\Database\Seeder;
use VinylHeaven\Shipping\Models\Format;

class FormatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $formats = [
            'Vinyl',
            'Acetate',
            'Flexi-disc',
            'Lathe Cut',
            'Shellac',
            'Pathé Disc',
            'Edison Disc',
            'Cylinder',
            'CD',
            'CDr',
            'DVD',
            'DVDr',
            'HD DVD',
            'HD DVD-R',
            'Blu-ray',
            'Blu-ray-R',
            'SACD',
            '4-Track Cartridge',
            '8-Track Cartridge',
            'Cassette',
            'DC-International',
            'Elcaset',
            'PlayTape',
            'RCA Tape Cartridge',
            'DAT',
            'DCC',
            'Microcassette',
            'NT Cassette',
            'Pocket Rocker',
            'Revere Magnetic Stereo Tape Ca',
            'Tefifon',
            'Reel-To-Reel',
            'Sabamobil',
            'Betacam',
            'Betacam SP',
            'Betamax',
            'Cartrivision',
            'MiniDV',
            'U-matic',
            'VHS',
            'Video 2000',
            'Video8',
            'Film Reel',
            'Laserdisc',
            'SelectaVision',
            'VHD',
            'Wire Recording',
            'Minidisc',
            'MVD',
            'UMD',
            'Floppy Disk',
            'Memory Stick',
            'Hybrid',
            'All Media',
            'Box Set',
        ];

        foreach ($formats as $format) {
            Format::create(['name' => $format]);
        }
    }
}
