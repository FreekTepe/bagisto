<?php

namespace VinylHeaven\ImageImport\Providers;

use Illuminate\Support\ServiceProvider;
use VinylHeaven\ImageImport\Classes\ImageImport;

class ImageImportServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('imageimport', function () {
            return new ImageImport();
        });
    }

    public function boot()
    {
        //
    }
}
