
@extends('marketplace::shop.layouts.account')

@section('page_title')
Create Shipping Policy
@endsection

@section('content')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-box1 icon-gradient bg-tempting-azure"></i>
            </div>
            <div>Create Shipping Policy
                <div class="page-title-subheading">Add shipping prices based on weight/dimension and order region.</div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <create-shipping-policy />
    </div>
</div>

@endsection


