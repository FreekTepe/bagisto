<?php

namespace VinylHeaven\Shipping\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use VinylHeaven\Shipping\Http\Resources\RuleResource;
use VinylHeaven\Shipping\Models\Rule;
use VinylHeaven\Shipping\Repositories\FormatRulesRepository;

class RuleController extends Controller
{

    protected $_config;

    public function __construct()
    {
        $this->_config = request('_config');
    }

    public function index()
    {
        return view($this->_config['view']);
    }
}
