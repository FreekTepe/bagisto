<?php

namespace VinylHeaven\Shipping\Controllers\Api;

use Khsing\World\World;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Repositories\SellerRepository;
use VinylHeaven\Shipping\Models\Policy;
use App\Http\Resources\ResponseResource;
use VinylHeaven\Shipping\Repositories\ShippingPolicyRepository;

class ShippingPoliciesController extends Controller
{

    /**
     * SellerRepository object
     *
     * @var Object
     */
    protected $sellerRepository;

    protected $shippingPolicyRepository;

    public function __construct(SellerRepository $sellerRepository, ShippingPolicyRepository $shippingPolicyRepository)
    {
        $this->sellerRepository = $sellerRepository;
        $this->shippingPolicyRepository = $shippingPolicyRepository;
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $seller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);
            $policy = $this->shippingPolicyRepository->insert($request->all(), $seller);

            \DB::commit();
            return (new ResponseResource('success', 'Well done.', 'Policy is created, you may now add price ranges!', 3000, true, [
                'redirect' => route('shipping.policies.edit', $policy->id)
            ]))->response()->setStatusCode(200);
            
        } catch (\Throwable $th) {
            \DB::rollBack();
            return (new ResponseResource('warning', 'Oops..', 'Something went wrong while trying to save your policy.', null, true))->response()->setStatusCode(500);
        }
    }

    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $policy = Policy::findOrFail($id);
            $policy->name = $request->name;
            $policy->description = $request->description;
            $policy->countries()->sync($request->country_ids);
            $policy->save();
            \DB::commit();
            return (new ResponseResource('success', 'Well done.', 'Policy is succesfully updated!', 3000, true))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            \DB::rollBack();
            return (new ResponseResource('warning', 'Oops..', 'Something went wrong while trying to save your policy.', null, true))->response()->setStatusCode(500);
        }
    }
}
