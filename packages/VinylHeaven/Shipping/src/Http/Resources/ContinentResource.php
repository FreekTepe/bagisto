<?php

namespace VinylHeaven\Shipping\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use VinylHeaven\Shipping\Http\Resources\ContinentCountriesResource;

class ContinentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'selected' => ($this->selected) ? ContinentCountriesResource::collection($this->selected) : [],
            'name' => $this->name,
            'code' => $this->code,
            'countries' => ContinentCountriesResource::collection($this->countries()->get())
        ];
    }
}
