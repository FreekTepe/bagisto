<?php

namespace VinylHeaven\Shipping\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RangeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "policy_id" => $this->policy_id,
            "max_width" => $this->max_width,
            "max_length" => $this->max_length,
            "max_height" => $this->max_height,
            "weight_from" => $this->weight_from,
            "weight_to" => $this->weight_to,
            "base_weight" => $this->base_weight,
            "price" => $this->price
        ];
    }
}
