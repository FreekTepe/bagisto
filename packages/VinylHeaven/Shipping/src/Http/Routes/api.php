<?php

Route::namespace('\VinylHeaven\Shipping\Controllers\Api')->group(function () {
    Route::prefix('api/shipping')->group(function () {
        Route::get('rules/all', 'RuleController@all')->name('shipping.rules.all');
        Route::get('formats/all', 'RuleController@allFormats')->name('shipping.formats.all');
        Route::get('descriptions/all', 'RuleController@allDescriptions')->name('shipping.descriptions.all');
        Route::post('rules/store', 'RuleController@store')->name('shipping.rules.store');
        Route::put('rules/update/{id}', 'RuleController@update')->name('shipping.rules.update');
        Route::delete('rules/delete/{id}', 'RuleController@destroy')->name('shipping.rules.destroy');
    });

    Route::prefix('api/shipping/policies')->group(function () {
        Route::post('', 'ShippingPoliciesController@store')->name('api.shipping.policies.store');
        Route::put('{id}', 'ShippingPoliciesController@update')->name('api.shipping.policies.update');
    });

    Route::prefix('api/shipping/ranges')->group(function () {
        Route::get('{id}', 'ShippingRangesController@getByPolicyId')->name('api.shipping.ranges.get');
        Route::post('', 'ShippingRangesController@store')->name('api.shipping.ranges.store');
        Route::put('{id}', 'ShippingRangesController@update')->name('api.shipping.ranges.update');
        Route::delete('{id}', 'ShippingRangesController@delete')->name('api.shipping.ranges.delete');
    });

    Route::prefix('api/world')->group(function () {
        Route::get('continentCountryTree', 'WorldController@getContinentCountryTree')->name('world.continentCountryTree');
        Route::get('continentCountryTreeBySellerId/{id}', 'WorldController@getContinentCountryTreeBySellerId')->name('world.continentCountryTreeBySellerId');
        Route::get('continentCountryTreeByPolicyId/{id}', 'WorldController@getContinentCountryTreeByPolicyId')->name('world.continentCountryTreeByPolicyId');
        Route::post('linkCountriesToSeller/{seller_id}', 'WorldController@linkCountriesToSeller')->name('world.linkCountriesToSeller');
    });
});