<?php

namespace VinylHeaven\Shipping\Repositories;

use App\Models\ExtendWebkul\Seller;
use Webkul\Core\Eloquent\Repository;
use VinylHeaven\Shipping\Models\Policy;
use Illuminate\Database\Eloquent\Builder;
use VinylHeaven\Shipping\Models\Description;

class ShippingPolicyRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'VinylHeaven\Shipping\Models\Policy';
    }


    public function insert($data, $seller)
    {
        if(isset($data['country_ids'])){
            $country_ids = $data['country_ids'];
            unset($data['country_ids']);
        }
        $data['seller_id'] = $seller->id;
        $policy = Policy::create($data);
        $policy->countries()->sync($country_ids); 
        
        return $policy;
    }
}