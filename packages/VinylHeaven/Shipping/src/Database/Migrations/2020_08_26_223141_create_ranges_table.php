<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ranges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('max_width')->nullable();
            $table->bigInteger('max_length')->nullable();
            $table->bigInteger('max_height')->nullable();
            $table->bigInteger('weight_from');
            $table->bigInteger('weight_to');
            $table->string('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ranges');
    }
}
