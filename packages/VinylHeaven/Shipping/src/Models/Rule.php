<?php

namespace VinylHeaven\Shipping\Models;

use Illuminate\Database\Eloquent\Model;
use VinylHeaven\Shipping\Models\Format;
use VinylHeaven\Shipping\Models\Description;
use App\Models\ExtendWebkul\Product;

class Rule extends Model
{

    protected $fillable = ['format_id', 'width', 'height', 'length', 'weight'];


    public function format()
    {
        return $this->belongsTo(Format::class);
    }

    public function descriptions()
    {
        return $this->belongsToMany(Description::class);
    }


    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    // this is a recommended way to declare event handlers
    public static function boot()
    {
        parent::boot();
        // cascade relationship
        self::deleting(function ($rule) { // before delete() method call this
            $rule->descriptions()->detach();
        });
    }
}
