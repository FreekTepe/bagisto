<?php

namespace VinylHeaven\Shipping\Models;

use Khsing\World\Models\Country;
use App\Models\ExtendWebkul\Seller;
use VinylHeaven\Shipping\Models\Range;
use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{

    protected $fillable = ['seller_id', 'name', 'description'];
    
    public function countries()
    {
        return $this->belongsToMany(Country::class, 'country_policy', 'policy_id', 'country_id');
    }

    public function ranges()
    {
        return $this->hasMany(Range::class);
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }
}
