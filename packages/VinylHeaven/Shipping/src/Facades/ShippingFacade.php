<?php

namespace VinylHeaven\Shipping\Facades;

use Illuminate\Support\Facades\Facade;

class ShippingFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'shipping';
    }
}
