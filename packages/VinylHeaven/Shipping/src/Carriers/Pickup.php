<?php

    namespace VinylHeaven\Shipping\Carriers;

    use Config;
    use Webkul\Checkout\Models\CartShippingRate;
    use Webkul\Shipping\Carriers\AbstractShipping;
    use Webkul\Shipping\Facades\Shipping;
    use Webkul\Checkout\Facades\Cart;

    /**
     * Class Rate.
     *
     */
    class Pickup extends AbstractShipping
    {
        /**
         * Payment method code
         *
         * @var string
         */
        protected $code  = 'pickup';

        /**
         * Returns rate for flatrate
         *
         * @return array
         */
        public function calculate()
        {
            if (! $this->isAvailable()) {
                return false;
            }

            $shipping_rates = [];
            $cart = Cart::getCart();

            $object = new CartShippingRate;

            $object->carrier = 'pickup';
            $object->carrier_title = $this->getConfigData('title');
            $object->method = 'pickup';
            $object->method_title = $this->getConfigData('title');
            $object->method_description = $this->getConfigData('description');
            $object->price = 0.00;
            $object->base_price = 0.00;

            return $object;
        }
    }