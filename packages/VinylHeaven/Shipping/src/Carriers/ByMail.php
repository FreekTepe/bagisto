<?php

    namespace VinylHeaven\Shipping\Carriers;

    use Config;
    use Webkul\Checkout\Models\CartShippingRate;
    use Webkul\Shipping\Carriers\AbstractShipping;
    use Webkul\Shipping\Facades\Shipping;
    use Webkul\Checkout\Facades\Cart;

    /**
     * Class Rate.
     *
     */
    class ByMail extends AbstractShipping
    {
        /**
         * Payment method code
         *
         * @var string
         */
        protected $code  = 'bymail';

        /**
         * Returns rate for flatrate
         *
         * @return array
         */
        public function calculate()
        {
            if (! $this->isAvailable()) {
                return false;
            }

            $cart = Cart::getCart();
            $object = new CartShippingRate;
            $shipping_rates = [];

            $object->carrier = 'bymail';
            $object->carrier_title = $this->getConfigData('title');
            $object->method = 'bymail';
            $object->method_title = $this->getConfigData('title');
            $object->method_description = $this->getConfigData('description');
            $object->price = 0;
            $object->base_price = $this->getConfigData('default_rate');

            foreach ($cart->items as $item) {
                $product_id = $item->product->product_id;
                $seller_id = $item->additional['seller_info']['seller_id'];
                $mp_product = app('App\Models\ExtendWebkul\MarketplaceProduct')->where([['product_id', '=', $product_id], ['marketplace_seller_id', '=', $seller_id]])->first();
                $shipping_rates[] = $mp_product->getShippingPrice();
            }

            $object->price = core()->convertPrice(max($shipping_rates));

            return $object;
        }
    }