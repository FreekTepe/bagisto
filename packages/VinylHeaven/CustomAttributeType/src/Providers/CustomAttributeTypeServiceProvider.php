<?php

namespace VinylHeaven\CustomAttributeType\Providers;

use Illuminate\Support\ServiceProvider;

class CustomAttributeTypeServiceProvider extends ServiceProvider
{
    public function register()
    {
        // ..
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
