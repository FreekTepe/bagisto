<?php

namespace VinylHeaven\CustomAttributeType\Controllers;

use Webkul\Product\Http\Controllers\ProductController as BaseProductController;
use Webkul\Product\Http\Requests\ProductForm;

class ProductController extends BaseProductController
{

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductForm $request, $id)
    {
        // Handle custom product update logics
        // ..
        dd('update rerouted');

        // Continue webkul flow
        parent::update($request, $id);
    }
}
