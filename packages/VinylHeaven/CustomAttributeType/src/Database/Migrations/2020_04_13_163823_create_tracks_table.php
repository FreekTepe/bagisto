<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tracklist_id')->nullable();
            $table->foreign('tracklist_id')->references('id')->on('tracklists')->onDelete('cascade');
            $table->string('track_id')->nullable();
            $table->string('title');
            $table->string('duration')->nullable();
            $table->string('position')->nullable();
            $table->integer('trackno');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
    }
}
