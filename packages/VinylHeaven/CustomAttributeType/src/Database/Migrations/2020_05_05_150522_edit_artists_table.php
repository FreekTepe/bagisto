<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('artists');
        Schema::create('artists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artist_id')->unique();
            $table->string('name');
            $table->string('real_name')->nullable();
            $table->text('profile')->nullable();
            $table->text('data_quality')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artists');
        Schema::create('artists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artistable_type');
            $table->integer('artistable_id');
            $table->timestamps();
        });
    }
}
