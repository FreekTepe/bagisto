<?php

namespace VinylHeaven\CustomAttributeType\Repositories;

use Webkul\Core\Eloquent\Repository;
use Illuminate\Support\Facades\DB;
use VinylHeaven\CustomAttributeType\Models\ArtistSolo;
use VinylHeaven\CustomAttributeType\Models\ArtistGroup;
use VinylHeaven\CustomAttributeType\Models\Label;

class LabelRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return 'VinylHeaven\CustomAttributeType\Models\Label';
    }


    public function createLabels($labels)
    {
        DB::beginTransaction();
        try {
            $collection = collect();
            foreach ($labels as $label) {
                // check if label already exists in db, in that case: skip create
                if (Label::where('label_id', $label['label_id'])->exists()) {
                    $collection->push(Label::where('label_id', $label['label_id'])->first());
                } else {
                    // label does not yet exist, create one
                    $collection->push($this->create($label));
                }
            }

            DB::commit();

            // array of created artists
            return $collection;
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }

    // turn array of discogs label ids into bagisto label->ids
    // directly filters the labels it couldn't match the label_id in bagisto DB.
    public function prepareLabelIds($label_ids)
    {
        // build array of label ids (mysql ids)
        $ids = array();
        // Check if artists
        foreach ($label_ids as $label_id) {
            if ($label = Label::where('label_id', $label_id)->first()) {
                array_push($ids, $label->id);
            }
        }
        return $ids;
    }
}
