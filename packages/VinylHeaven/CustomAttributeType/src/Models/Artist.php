<?php

namespace VinylHeaven\CustomAttributeType\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ExtendWebkul\Product;

class Artist extends Model
{
    protected $touches = [
        'products'
    ];

    protected $fillable = [
        'artist_id',
        'name',
        'real_name',
        'profile',
        'data_quality'
    ];

    /**
     * The roles that belong to the user.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
