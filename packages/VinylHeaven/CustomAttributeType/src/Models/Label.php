<?php

namespace VinylHeaven\CustomAttributeType\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ExtendWebkul\Product;

class Label extends Model
{

    protected $fillable = [
        "label_id", 
        "name", 
        "cat_no"
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
