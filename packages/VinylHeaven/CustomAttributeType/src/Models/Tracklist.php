<?php

namespace VinylHeaven\CustomAttributeType\Models;

use Illuminate\Database\Eloquent\Model;

class Tracklist extends Model
{
    protected $fillable = ['name'];

    public function tracks()
    {
        return $this->hasMany(Track::class)->orderBy('trackno');
    }

    public function refreshTracknoColumn()
    {
        $count = 1;
        foreach ($this->tracks as $track) {
            $track->trackno = $count;
            $track->save();
            $count++;
        }
    }
}
