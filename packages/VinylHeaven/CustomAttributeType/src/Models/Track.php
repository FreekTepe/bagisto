<?php

namespace VinylHeaven\CustomAttributeType\Models;

use Illuminate\Database\Eloquent\Model;
use VinylHeaven\CustomAttributeType\Models\Tracklist;

class Track extends Model
{

    protected $fillable = ['tracklist_id', 'trackno', 'title', 'track_id', 'duration', 'position'];

    public function tracklist()
    {
        return $this->belongsTo(Tracklist::class);
    }
}
