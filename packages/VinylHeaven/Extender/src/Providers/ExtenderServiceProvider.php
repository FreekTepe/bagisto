<?php

namespace VinylHeaven\Extender\Providers;

use Illuminate\Support\ServiceProvider;
use VinylHeaven\Extender\Classes\Extender;

class ExtenderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('extender', function () {
            return new Extender();
        });
    }

    public function boot()
    {
        //
    }
}
