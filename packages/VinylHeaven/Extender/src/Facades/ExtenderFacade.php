<?php

namespace VinylHeaven\Extender\Facades;

use Illuminate\Support\Facades\Facade;

class ExtenderFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'extender';
    }
}
