<?php

namespace VinylHeaven\Checkout\Facades;

use Illuminate\Support\Facades\Facade;

class Checkout extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'checkout';
    }
}