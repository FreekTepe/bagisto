<?php
    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['middleware' => ['web', 'locale', 'theme', 'currency']], function () {
        Route::get('/checkout/onepage/reset/carts', 'VinylHeaven\Checkout\Http\Controllers\OnepageController@resetCarts')->name('shop.checkout.onepage.reset.carts');
        Route::get('/checkout/process/bank/payment/{seller_id}', 'VinylHeaven\Checkout\Http\Controllers\OnepageController@processBankPayment')->name('shop.checkout.process.bank.payment');
        Route::get('/checkout/process/cash/payment/{seller_id}', 'VinylHeaven\Checkout\Http\Controllers\OnepageController@processCashPayment')->name('shop.checkout.process.cash.payment');

        Route::post('/checkout/process/paypal/payment', 'VinylHeaven\Checkout\Http\Controllers\OnepageController@processPaypalPayment')->name('shop.checkout.process.paypal.payment');
    });