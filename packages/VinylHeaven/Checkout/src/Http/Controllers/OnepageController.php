<?php
    namespace VinylHeaven\Checkout\Http\Controllers;

    use Illuminate\Support\Facades\Event;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Arr;
    
    use Webkul\Shop\Http\Controllers\CartController as OnepageControllerBaseController;
    use Webkul\Shipping\Facades\Shipping;
    use Webkul\Checkout\Facades\Cart as BaseCart;
    use Webkul\Checkout\Models\CartPayment;

    use VinylHeaven\Checkout\Facades\VinylHeavenCart as Cart;
    use VinylHeaven\Payment\Methods\Paypal;
    use VinylHeaven\Payment\Methods\Bank;
    use VinylHeaven\Payment\Methods\Cash;

    class OnepageController extends OnepageControllerBaseController
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\View\View
        */
        public function index()
        {
            $cart = Cart::getCart();

            if (! auth()->guard('customer')->check()
                && ! core()->getConfigData('catalog.products.guest-checkout.allow-guest-checkout')) {
                return redirect()->route('customer.session.index');
            }

            if (Cart::hasError()) {
                return redirect()->route('shop.checkout.cart.index');
            }

            if (! auth()->guard('customer')->check() && $cart->hasDownloadableItems()) {
                return redirect()->route('customer.session.index');
            }

            if (! auth()->guard('customer')->check() && ! $cart->hasGuestCheckoutItems()) {
                return redirect()->route('customer.session.index');
            }

            $cart->collectTotals();

            return view($this->_config['view'], compact('cart'));
        }

        public function resetCarts()
        {
            if (!$this->resetAllCarts()) {
                return redirect()->to('/checkout/onepage/reset/carts/failed');
            }
            return redirect()->to('/checkout/onepage/reset/carts/successfull');
        }
        
        public function saveOrder()
        {
            if (Cart::hasError()) {
                session()->flash('error', 'Cart has error(s)!');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            if (!request()->filled('payment_method')) {
                session()->flash('error', 'No payment method provided!');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            $data = request()->all();
            $cart = Cart::getCart();

            $data['billing']['address1'] = implode(PHP_EOL, array_filter($data['billing']['address1']));
            $data['shipping']['address1'] = implode(PHP_EOL, array_filter($data['shipping']['address1']));

            $order_seller_id = $data['checkout_order_for_seller'];
            $payment = ['method' => $data['payment_method']];
            $shipping_method = $data['shipping_method'];

            if (!BaseCart::saveCustomerAddress($data)) {
                session()->flash('error', 'There was an error while saving the customer address');
                return redirect()->to(route('shop.checkout.onepage.index'));
            } else {
                Cart::saveSellerCartCustomerAddress($data, $order_seller_id);
            }

            if (!Shipping::collectRates()) {
                session()->flash('error', 'There was an error while collecting the shipping rates');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            if (!BaseCart::saveShippingMethod($shipping_method)) {
                session()->flash('error', 'There was an error while saving the shipping method');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            if (!BaseCart::savePaymentMethod($payment)) {
                session()->flash('error', 'There was an error while saving the payment method');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            
            $sellerCart = $cart->getSellerCart($order_seller_id);
            $sellerCart->collectTotals();
            $paymentMethodOptions = $sellerCart->seller->getPaymentMethodOptions($payment['method']);
            $paymentMethod = $sellerCart->seller->getPaymentMethod($payment['method']);
            $paymentMethod = new $paymentMethod['class']($paymentMethodOptions);

            switch($payment['method']) {
                case 'paypal':
                    $paymentMethod->setAmount($sellerCart->grand_total);
                    $paymentMethod->setItemName('VinylHeaven-Order-' . $sellerCart->id . '-' . $sellerCart->seller->id);
                    $paymentMethod->setCurrencyCode($sellerCart->cart_currency_code);
                    break;
                case "bank":
                    $paymentMethod->setSellerId($sellerCart->seller->id);
                    break;
                case "cash":
                    $paymentMethod->setSellerId($sellerCart->seller->id);
                    break;
            }
            $paymentMethod->request();
        }

        public function processPaypalPayment()
        {
            $seller_id = null;
            $seller_cart_id = null;

            if (request()->filled('item_name1')) {
                $item_name_array = explode('-', request()->get('item_name1'));
                if (isset($item_name_array[2])) {
                    $seller_cart_id = $item_name_array[2];
                }

                if (isset($item_name_array[3])) {
                    $seller_id = $item_name_array[3];
                }
            }

            if (empty($seller_cart_id) || empty($seller_id)) {
                session()->flash('error', 'Unknown payment transaction');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            $orderRepository = app('Webkul\Sales\Repositories\OrderRepository');
            $cartPayment = app('Webkul\Checkout\Models\CartPayment');
            $cart = Cart::getCart();
            $sellerCart = $cart->getSellerCart($seller_id);
            $sellerCart->collectTotals();
            
            if ($sellerCartPayment = $cartPayment->where('cart_id', $sellerCart->id)->first()) {
                $sellerCart->payment->delete();
            }
            
            $sellerCartPayment = new CartPayment;
            $sellerCartPayment->method = $cart->payment->method;
            $sellerCartPayment->method_title = $cart->payment->method_title;
            $sellerCartPayment->cart_id = $sellerCart->id;
            $sellerCartPayment->save();

            $order_data = Cart::prepareSellerCartDataForOrder($seller_id);
            $order = $orderRepository->create($order_data);

            if (!$order) {
                session()->flash('error', 'There has been an error while processing your order!');
            }

            if ($order && (request()->get('payment_status') == 'Completed')) {
                $order->status = 'processing';
                $order->save();
            }

            foreach ($sellerCart->items()->get() as $item) {
                Cart::removeItem($item->id);
            }

            if (!Cart::deActivateCart($sellerCart)) {
                session()->flash('error', 'There has been an error while deactivating the cart!');
            }

            if (!Cart::getCart()->sellerCarts()->count()) {
                if (!Cart::deActivateCart(Cart::getCart())) {
                    session()->flash('error', 'There has been an error while deactivating the cart!');
                }

                session()->flash('success', 'All of your Orders have been placed. Please check your email for further instructions');

                return redirect()->to(route('shop.checkout.cart.index'));
            }

            session()->flash('success', 'Your Order has been placed. Please check your email for further instructions');

            return redirect()->to(route('shop.checkout.onepage.index'));
        }

        public function processBankPayment($seller_id)
        {
            if (empty($seller_id)) {
                session()->flash('error', 'Unknown payment transaction');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            $orderRepository = app('Webkul\Sales\Repositories\OrderRepository');
            $cartPayment = app('Webkul\Checkout\Models\CartPayment');
            $cart = Cart::getCart();
            $sellerCart = $cart->getSellerCart($seller_id);
            $sellerCart->collectTotals();

            if ($sellerCartPayment = $cartPayment->where('cart_id', $sellerCart->id)->first()) {
                $sellerCart->payment->delete();
            }

            $sellerCartPayment = new CartPayment;
            $sellerCartPayment->method = $cart->payment->method;
            $sellerCartPayment->method_title = $cart->payment->method_title;
            $sellerCartPayment->cart_id = $sellerCart->id;
            $sellerCartPayment->save();

            $order_data = Cart::prepareSellerCartDataForOrder($seller_id);
            $order = $orderRepository->create($order_data);

            if (!$order) {
                session()->flash('error', 'There has been an error while processing your order!');
            }

            foreach ($sellerCart->items()->get() as $item) {
                Cart::removeItem($item->id);
            }

            if (!Cart::deActivateCart($sellerCart)) {
                session()->flash('error', 'There has been an error while deactivating the cart!');
            }

            if (!Cart::getCart()->sellerCarts()->count()) {
                if (!Cart::deActivateCart(Cart::getCart())) {
                    session()->flash('error', 'There has been an error while deactivating the cart!');
                }

                session()->flash('success', 'All of your Orders have been placed. Please check your email for further instructions');

                return redirect()->to(route('shop.checkout.cart.index'));
            }

            session()->flash('success', 'Your Order has been placed. Please check your email for further instructions');

            return redirect()->to(route('shop.checkout.onepage.index'));
        }

        public function processCashPayment($seller_id)
        {
            if (empty($seller_id)) {
                session()->flash('error', 'Unknown payment transaction');
                return redirect()->to(route('shop.checkout.onepage.index'));
            }

            $orderRepository = app('Webkul\Sales\Repositories\OrderRepository');
            $cartPayment = app('Webkul\Checkout\Models\CartPayment');
            $cart = Cart::getCart();
            $sellerCart = $cart->getSellerCart($seller_id);
            $sellerCart->collectTotals();

            if ($sellerCartPayment = $cartPayment->where('cart_id', $sellerCart->id)->first()) {
                $sellerCart->payment->delete();
            }

            $sellerCartPayment = new CartPayment;
            $sellerCartPayment->method = $cart->payment->method;
            $sellerCartPayment->method_title = $cart->payment->method_title;
            $sellerCartPayment->cart_id = $sellerCart->id;
            $sellerCartPayment->save();

            $order_data = Cart::prepareSellerCartDataForOrder($seller_id);
            $order = $orderRepository->create($order_data);

            if (!$order) {
                session()->flash('error', 'There has been an error while processing your order!');
            }

            foreach ($sellerCart->items()->get() as $item) {
                Cart::removeItem($item->id);
            }

            if (!Cart::deActivateCart($sellerCart)) {
                session()->flash('error', 'There has been an error while deactivating the cart!');
            }

            if (!Cart::getCart()->sellerCarts()->count()) {
                if (!Cart::deActivateCart(Cart::getCart())) {
                    session()->flash('error', 'There has been an error while deactivating the cart!');
                }

                session()->flash('success', 'All of your Orders have been placed. Please check your email for further instructions');

                return redirect()->to(route('shop.checkout.cart.index'));
            }

            session()->flash('success', 'Your Order has been placed. Please check your email for further instructions');

            return redirect()->to(route('shop.checkout.onepage.index'));
        }

        private function resetAllCarts()
        {
            $carts_are_reset = false;
            $cart = Cart::getCart();

            if (!empty($cart)) {
                foreach ($cart->sellerCarts() as $sellerCart) {
                    Cart::deActivateCart($sellerCart);
                }

                Cart::deActivateCart($cart);

                Session::flush();
                Session::regenerate();

                return true;
            }
            return false;
        }
    }