<?php

namespace VinylHeaven\Checkout\Http\Controllers;

use Illuminate\Support\Facades\Event;

use Webkul\Shop\Http\Controllers\CartController as CartControllerBaseController;
use Webkul\Checkout\Contracts\Cart as CartModel;
use Webkul\Product\Repositories\ProductRepository;

use Webkul\Checkout\Facades\Cart;

class CartController extends CartControllerBaseController
{
    /**
     * Function for guests user to add the product in the cart.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add($id)
    {
        try {
            $result = $this->addProductToCart($id, request()->all());

            if ($this->onWarningAddingToCart($result)) {
                session()->flash('warning', $result['warning']);

                return redirect()->back();
            }

            if ($result instanceof CartModel) {
                session()->flash('success', trans('shop::app.checkout.cart.item.success'));

                if ($customer = auth()->guard('customer')->user()) {
                    app('Webkul\Customer\Repositories\WishlistRepository')->deleteWhere(['product_id' => $id, 'customer_id' => $customer->id]);
                }

                if (request()->get('is_buy_now')) {
                    Event::dispatch('shop.item.buy-now', $id);

                    return redirect()->route('shop.checkout.onepage.index');
                }
            }
        } catch(\Exception $e) {
            session()->flash('error', trans($e->getMessage()));

            $product = $this->productRepository->find($id);

            return redirect()->route('shop.productOrCategory.index', $product->url_key);
        }

        return redirect()->back();
    }

    /**
     * Add Items in a cart with some cart and item details.
     *
     * @param  int  $productId
     * @param  array  $data
     * @return \Webkul\Checkout\Contracts\Cart|\Exception|array
     */
    private function addProductToCart($productId, $data)
    {
        Event::dispatch('checkout.cart.add.before', $productId);
        
        $cart = Cart::getCart();

        if (! $cart && ! $cart = Cart::create($data)) {
            return ['warning' => __('shop::app.checkout.cart.item.error-add')];
        }

        $productRepository = app('Webkul\Product\Repositories\ProductRepository');
        $product = $productRepository->findOneByField('id', $productId);
        
        $product_simple_type_instance = $product->getTypeInstance();
        $cartProducts = $this->prepareForCart($data, $product, $product_simple_type_instance);
        
        if (is_string($cartProducts)) {
            Cart::collectTotals();

            if (count($cart->all_items) <= 0) {
                session()->forget('cart');
            }

            throw new \Exception($cartProducts);
        } else {
            $parentCartItem = null;

            foreach ($cartProducts as $cartProduct) {
                if (empty($cartProduct['price'])) {
                    $cartProduct['price'] = 0.00;
                }

                if (empty($cartProduct['base_price'])) {
                    $cartProduct['base_price'] = 0.00;
                }

                $cartItem = $this->getItemByProduct($cartProduct, true);

                if (isset($cartProduct['parent_id'])) {
                    $cartProduct['parent_id'] = $parentCartItem->id;
                }

                $cartItemRepository = app('Webkul\Checkout\Repositories\CartItemRepository');
                if (! $cartItem) {
                    $cartItem = $cartItemRepository->create(array_merge($cartProduct, ['cart_id' => $cart->id]));
                } else {
                    if (isset($cartProduct['parent_id']) && $cartItem->parent_id != $parentCartItem->id) {
                        $cartItem = $cartItemRepository->create(array_merge($cartProduct, [
                            'cart_id' => $cart->id
                        ]));
                    } else {
                        $cartItem = $cartItemRepository->update($cartProduct, $cartItem->id);
                    }
                }

                if (! $parentCartItem) {
                    $parentCartItem = $cartItem;
                }
            }
        }
        
        Event::dispatch('checkout.cart.add.after', $cart);
        
        Cart::collectTotals();

        return Cart::getCart();
    }

    /**
     * Add product. Returns error message if can't prepare product.
     *
     * @param  array  $data
     * @return array
     */
    public function prepareForCart($data, $product, $type_instance)
    {
        $data['quantity'] = $data['quantity'] ?? 1;
   
        $data = $this->getQtyRequest($data);
        
        if (!$type_instance->haveSufficientQuantity($data['quantity'])) {
            return trans('shop::app.checkout.cart.quantity.inventory_warning');
        }

        $price = $type_instance->getFinalPrice();
        
        $products = [
            [
                'product_id'        => $product->id,
                'sku'               => $product->sku,
                'quantity'          => $data['quantity'],
                'name'              => $product->name,
                'price'             => $convertedPrice = core()->convertPrice($price),
                'base_price'        => $price,
                'total'             => $convertedPrice * $data['quantity'],
                'base_total'        => $price * $data['quantity'],
                'weight'            => $product->weight ?? 0,
                'total_weight'      => ($product->weight ?? 0) * $data['quantity'],
                'base_total_weight' => ($product->weight ?? 0) * $data['quantity'],
                'type'              => $product->type,
                'additional'        => $type_instance->getAdditionalOptions($data),
            ]
        ];

        return $products;
    }

    /**
     * Get request quantity
     *
     * @param  array  $data
     * @return array
     */
    public function getQtyRequest($data)
    {
        if ($item = $this->getItemByProduct(['additional' => $data], true)) {
            $data['quantity'] += $item->quantity;
        }

        return $data;
    }

    /**
     * Get cart item by product
     *
     * @param  array  $data
     * @return \Webkul\Checkout\Contracts\CartItem|void
     */
    public function getItemByProduct($data, $check = false)
    {
        $items = Cart::getCart()->all_items;

        foreach ($items as $item) {
            if ($item->product->getTypeInstance()->compareOptions($item->additional, $data['additional'])) {
                if (isset($data['additional']['parent_id'])) {
                    if ($item->parent->product->getTypeInstance()->compareOptions($item->parent->additional, request()->all())) {
                        return $item;
                    }
                } else {
                    if (!$check) {
                        return $item;
                    } else {
                        if ($item->additional['seller_info']['seller_id'] == $data['additional']['seller_info']['seller_id']) {
                            return $item;
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns true, if result of adding product to cart is an array and contains a key "warning"
     *
     * @param  array  $result
     * @return boolean
     */
    private function onWarningAddingToCart($result): bool
    {
        return is_array($result) && isset($result['warning']);
    }
}