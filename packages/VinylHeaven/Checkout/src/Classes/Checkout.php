<?php

    namespace VinylHeaven\Checkout\Classes;

    use VinylHeaven\Checkout\Facades\VinylHeavenCart as Cart;

    class Checkout
    {
        private $main_cart;

        private $seller_carts;

        private $seller_carts_items = [];

        public function __construct()
        {
            
            $this->setMainCart(Cart::getMainCart());
            
            $this->setSellerCarts(Cart::getSellerCarts($this->getMainCart()->id));
            
            if ($this->bagistoCartHasItems()) {
                $this->putSellerCartsItems();

                if (!$this->hasSellerCarts()) {
                    $this->createSellerCarts();
                } else {
                    $this->putSellerCarts();
                }
            }
        }

        public function getMainCart()
        {
            return $this->main_cart;
        }

        public function setMainCart($main_cart)
        {
            $this->main_cart = $main_cart;
        }

        public function hasSellerCarts()
        {
            return count($this->seller_carts) ? true : false;
        }

        public function getSellerCarts()
        {
            return $this->seller_carts;
        }

        public function setSellerCarts($seller_carts)
        {
            $this->seller_carts = $seller_carts;
        }

        public function putSellerCarts()
        {
            foreach ($this->getSellerCartsItems() as $seller_id => $seller_cart_items) {
                if (isset($this->seller_carts[$seller_id])) {
                    $this->seller_carts[$seller_id]->items = $seller_cart_items;
                    $this->seller_carts[$seller_id]->collectTotals();
                }
            }
        }

        public function createSellerCarts()
        {
            $seller_carts = [];

            foreach ($this->getSellerCartsItems() as $seller_id => $seller_cart_items) {
                $seller_cart_data = [
                    'customer_email' => null,
                    'customer_first_name' => null,
                    'customer_last_name' => null,
                    'shipping_method' => null,
                    'coupon_code' => null,
                    'is_gift' => 0,
                    'items_count' => 0,
                    'items_qty' => 0,
                    'grand_total' => 0.0,
                    'base_grand_total' => 0.0,
                    'sub_total' => 0.0,
                    'base_sub_total' => 0.0,
                    'tax_total' => 0.0,
                    'base_tax_total' => 0.0,
                    'discount_amount' => 0.0,
                    'base_discount_amount' => 0.0,
                    'checkout_method' => null,
                    'conversion_time' => null,
                    'customer_id' => null,
                    'applied_cart_rule_ids' => null,
                    'global_currency_code' => 'USD',
                    'base_currency_code' => core()->getBaseCurrencyCode(),
                    'channel_currency_code' => core()->getChannelBaseCurrencyCode(),
                    'cart_currency_code' => core()->getCurrentCurrencyCode(),
                    'is_guest' => 1,
                    'is_active' => 1,
                    'channel_id' => 1,
                    'is_vinylheaven_cart' => 1,
                    'vinylheaven_cart_parent_id' => $this->getMainCart()->id,
                    'seller_id' => $seller_id,
                ];
                $seller_cart = Cart::createCart($seller_cart_data);
                $seller_cart->items = $seller_cart_items;
                $seller_carts[$seller_id] = $seller_cart;
            }

            $this->setSellerCarts($seller_carts);
        }

        public function hasSellerCartsItems()
        {
            return count($this->seller_carts_items) ? true : false;
        }

        public function getSellerCartsItems()
        {
            return $this->seller_carts_items;
        }

        public function setSellerCartsItems($seller_carts_items)
        {
            $this->seller_carts_items = $seller_carts_items;
        }

        public function putSellerCartsItems()
        {
            $seller_carts_items = [];

            foreach ($this->getBagistoCart()->all_items as $item) {
                if (!isset($seller_carts_items[$item->additional['seller_info']['seller_id']])) {
                    $seller_carts_items[$item->additional['seller_info']['seller_id']] = [];
                }

                $seller_carts_items[$item->additional['seller_info']['seller_id']][] = $item;
            }

            foreach ($seller_carts_items as $items_index => $items) {
                $seller_carts_items[$items_index] = collect($items);
            }

            $this->setSellerCartsItems($seller_carts_items);
        }

        public function bagistoCartHasItems()
        {
            return ($this->getBagistoCart()->items()->count()) ? true : false;
        }

        public function getBagistoCart()
        {
            return Cart::getBagistoCart();
        }

        public function sellers()
        {
            return $this->getBagistoCart()->sellers();
        }
    }