<?php

namespace VinylHeaven\Checkout\Providers;

use Illuminate\Support\ServiceProvider;

use VinylHeaven\Checkout\Classes\VinylHeavenCart;
use VinylHeaven\Checkout\Classes\Checkout;

class CheckoutServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('vinylheavencart', function () {
            return new VinylHeavenCart();
        });

        $this->app->bind('checkout', function () {
            return new Checkout();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/web.php');
        $this->loadViewsFrom(__DIR__  . '/../Resources/views', 'checkout');
    }
}
