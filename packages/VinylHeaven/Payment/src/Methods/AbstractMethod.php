<?php
    namespace VinylHeaven\Payment\Methods;

    abstract class AbstractMethod
    {
        abstract public function request();
    }