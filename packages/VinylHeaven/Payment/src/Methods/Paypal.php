<?php
    namespace VinylHeaven\Payment\Methods;

    use Illuminate\Support\Facades\Config;
    use VinylHeaven\Payment\Methods\AbstractMethod;

    class Paypal extends AbstractMethod
    {
        private $sandbox = true;

        private $method_config;

        private $attributes;

        private $account;

        private $cmd = '_xclick';
        
        private $currency_code;
        
        private $amount;
        
        private $return_url;

        private $item_name;

        private $base_paypal_sandbox_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

        private $base_paypal_url = 'https://www.paypal.com/cgi-bin/webscr';

        public function __construct($attributes = [])
        {
            $this->setConfig(Config::get('paymentmethods')['paypal']);
            $this->setAttributes($attributes);

            if ($this->hasAttribute('account')) {
                $this->setAccount($this->getAttribute('account'));
            }
        }

        public function hasConfig()
        {
            return empty($this->method_config) ? false : true;
        }

        public function setConfig($method_config)
        {
            $this->method_config = $method_config;
        }

        public function getConfig()
        {
            return $this->method_config;
        }

        public function hasAttributes()
        {
            return empty($this->attributes) ? false : true;
        }

        public function setAttributes($attributes)
        {
            $this->attributes = $attributes;
        }

        public function getAttributes()
        {
            return $this->attributes;
        }

        public function attributes()
        {
            return $this->attributes;
        }

        public function hasAttribute($attribute)
        {
            return isset($this->attributes[$attribute]) ? empty($this->attributes[$attribute]) ? false : true : false;
        }

        public function getAttribute($attribute)
        {
            return isset($this->attributes[$attribute]) ? $this->attributes[$attribute] : false;
        }

        public function hasAccount()
        {
            return empty($this->account) ? false : true;
        }

        public function setAccount($account)
        {
            $this->account = $account;
        }

        public function getAccount()
        {
            return $this->account;
        }

        public function account()
        {
            return $this->account;
        }

        public function business()
        {
            return $this->account;
        }

        public function cmd()
        {
            return $this->cmd;
        }
        
        public function currencyCode() 
        {
            return $this->currency_code;
        }

        public function setCurrencyCode($currency_code)
        {
            $this->currency_code = $currency_code;
        }

        public function getCurrencyCode()
        {
            return $this->currency_code;
        }
        
        public function amount()
        {
            return $this->amount;
        }
        
        public function setAmount($amount)
        {
            $this->amount = $amount;
        }

        public function getAmount()
        {
            return $this->amount;
        }

        public function returnUrl()
        {
            return route($this->getConfig()['return_route']);
        }

        public function setItemName($item_name)
        {
            $this->item_name = $item_name;
        }

        public function getItemName()
        {
            return $this->item_name;
        }

        public function itemName()
        {
            return $this->item_name;
        }

        public function createPaypalUrl()
        {
            $base_url = $this->base_paypal_url;
            
            if ($this->sandbox) {
                $base_url = $this->base_paypal_sandbox_url;
            }
            
            return $base_url . '?cmd=' . $this->cmd() . '&business=' . $this->business() . '&currency_code=' . $this->currencyCode() . '&amount=' . $this->amount() . '&return=' . $this->returnUrl() . '&item_name=' . $this->itemName() . '&rm=2';
        }

        public function request()
        {
            //dd($this->createPaypalUrl());
            redirect()->to($this->createPaypalUrl())->send();
            // header("location: " . $this->createPaypalUrl());
        }
    }