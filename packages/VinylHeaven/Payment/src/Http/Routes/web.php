<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web', 'theme', 'locale', 'currency', 'customer', 'set.theme.sellerdashboard']], function () {
    Route::namespace('\VinylHeaven\Payment\Controllers')->group(function () {
        Route::prefix('marketplace/payment')->group(function () {
            Route::get('/options', 'SellerPaymentController@index')->defaults('_config', [
                'view' => 'payment::index'
            ])->name('shipping.policies');
        });
    });
});