<?php

namespace VinylHeaven\ReleaseImport\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use VinylHeaven\ReleaseImport\Mail\DiscogsCsvImportCompletedMail;
use App\Models\ExtendWebkul\Product;
use \App\Repositories\SellerRepository;
use VinylHeaven\ReleaseImport\Repositories\ReleaseImportRepository;
use Webkul\Marketplace\Repositories\ProductRepository as SellerProductRepository;
use VinylHeaven\ReleaseImport\Models\DiscogsCsv;
use VinylHeaven\ReleaseImport\CsvImports\Resolvers\DiscogsCSVRow;
use VinylHeaven\ReleaseImport\CsvImports\Resolvers\MongoReleaseMatch;


class DiscogsCsvImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file;
    protected $seller;
    protected $user;
    protected $delimiter;

    protected $successes = 0; // successfull upload count

    protected $sellerRepository;
    protected $sellerProductRepository;
    protected $releaseImportRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file, $seller, $user, $delimiter = ",")
    {
        $this->file = $file;
        $this->seller = $seller;
        $this->user = $user;
        $this->delimiter = $delimiter;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // initialize repositories
        $this->sellerRepository = resolve(SellerRepository::class);
        $this->sellerProductRepository = resolve(SellerProductRepository::class);
        $this->releaseImportRepository = resolve(ReleaseImportRepository::class);

        // Normally jobs should run seperately of any auth sessions, but bagisto repository methods rely on auth() guard. So we need to authenticate the user object.
        \Auth::guard('customer')->login($this->user);

        $csv_row_collection = DiscogsCsv::where([
            ['import_status', 100],
            ['seller_id', $this->seller->id],
            ['media_id', $this->file->id]
        ])->get();

        // NTS: Maybe CHUNK?
        foreach ($csv_row_collection as $csv_row_model) {
            if($csv_row_model->status !== "For Sale"){
                continue;
            }
            try {
                // throws Exception if $row doesnt meet class validation rules
                $row = new DiscogsCSVRow($csv_row_model->toArray()); // validate row and make processable
                
                // check if product already exists
                if ($product = Product::where('sku', $row->getReleaseId())->first()) {

                    // check if seller is already selling this product
                    if ($this->sellerRepository->sellerIsSellingProduct($this->seller, $product->sku)) {
                        $this->succeed($csv_row_model);
                        continue;
                    }

                    try {
                        // Assign existing product
                        $this->sellerProductRepository->createAssign(array_merge($row->getSellerData(), ['product_id' => $product->id, 'is_owner' => 0, "inventories" => [1 => 1]]));
                        $this->succeed($csv_row_model);
                        continue;
                    } catch (\Throwable $th) {
                        $this->fail($csv_row_model, $th);
                        continue;
                    }
                }

                // Do mongo fetch, and create new product
                try {
                    // does discogs api fetch, throws Exception if api fails, throws Exception if api repsonse data doesnt meet validation rules
                    $release = new MongoReleaseMatch($row->getReleaseId(), $this->seller);

                    $this->releaseImportRepository->import($release->getReleaseData(), array_merge($row->getSellerData(), ['is_owner' => 0, "inventories" => [1 => 1]]));
                    $this->succeed($csv_row_model);
                } catch (\Throwable $th) {

                    $this->fail($csv_row_model, $th);
                    continue;
                }
            } catch (\Throwable $th) {

                // add message to array of errors to be send out in results email
                $this->fail($csv_row_model, $th);
                continue;
            }
        }

        // send confirmation email
        \Mail::to($this->seller->customer->email)->send(new DiscogsCsvImportCompletedMail($this->seller, $this->file));
    }

    private function succeed($csv_row_model)
    {
        $csv_row_model->import_status = 200;
        $csv_row_model->save();
    }

    private function fail($csv_row_model, $th)
    {
        $csv_row_model->import_status = 300;
        $csv_row_model->exception = $th;
        $csv_row_model->save();
    }
}
