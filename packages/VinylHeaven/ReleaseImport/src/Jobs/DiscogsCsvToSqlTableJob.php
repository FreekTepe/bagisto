<?php

namespace VinylHeaven\ReleaseImport\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use VinylHeaven\ReleaseImport\CsvImports\DiscogsCsvImportModel;

class DiscogsCsvToSqlTableJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $seller;
    protected $uploaded_file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($seller, $uploaded_file)
    {
        $this->seller = $seller;
        $this->uploaded_file = $uploaded_file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // fix for Excel import memory limit overload
        // NTS: Cleaner fix would be to use \Excel import chunking: https://docs.laravel-excel.com/3.1/imports/chunk-reading.html
        ini_set('memory_limit', '-1');

        // add csv file data to mysql table
        $discogsCsvImportModel = new DiscogsCsvImportModel($this->seller->id, $this->uploaded_file->id);
        \Excel::import($discogsCsvImportModel, $this->uploaded_file->getPath());
    }
}
