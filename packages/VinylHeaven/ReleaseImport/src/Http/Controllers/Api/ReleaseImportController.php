<?php

namespace VinylHeaven\ReleaseImport\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\ExtendWebkul\Product;
use \App\Repositories\SellerRepository;
use App\Http\Resources\ResponseResource;
use VinylHeaven\ReleaseImport\Requests\Release;
use VinylHeaven\ReleaseImport\Jobs\CustomCsvImportJob;
use VinylHeaven\ReleaseImport\Jobs\DiscogsCsvImportJob;
use VinylHeaven\ReleaseImport\Jobs\DiscogsCsvToSqlTableJob;
use VinylHeaven\ReleaseImport\Models\PendingCustomCsvImportMatches;
use VinylHeaven\ReleaseImport\Repositories\ReleaseImportRepository;
use Webkul\Marketplace\Repositories\ProductRepository as SellerProductRepository;


class ReleaseImportController extends Controller
{
    /**
     * ProductRepository object (Marketplace)
     *
     * @var \Webkul\Marketplace\Repositories\ProductRepository
     */
    protected $sellerProduct;

    /**
     * SellerRepository object
     *
     * @var \App\Repositories\SellerRepository
     */
    protected $sellerRepository;

    /**
     * ReleaseImportRepository object
     *
     * @var \VinylHeaven\ReleaseImport\Repositories\ReleaseImportRepository
     */
    protected $releaseImportRepository;


    public function __construct(
        SellerProductRepository $sellerProductRepository,
        SellerRepository $sellerRepository,
        ReleaseImportRepository $releaseImportRepository
    ) {

        // JWT AUTH
        auth()->setDefaultDriver('customer');
        $this->middleware('auth.jwt');
        app('debugbar')->disable(); // fix: axios debugbar bug

        $this->sellerProductRepository = $sellerProductRepository;
        $this->sellerRepository = $sellerRepository;
        $this->releaseImportRepository = $releaseImportRepository;
    }


    public function import(Release $request)
    {

        $data = $request->validated();

        $seller = $this->sellerRepository->getSellerByUserId(auth()->guard('customer')->user()->id);

        $sellerData = $data['seller'];

        // NTS: temp quick way to add qty to $sellerData (for the marketplace_product.qty)
        if (isset($sellerData['inventories'])) {
            $sellerData['qty'] = $sellerData['inventories'][1];
        }

        unset($data['seller']);


        // IF product already in products table
        if ($product = Product::where('sku', $data['sku'])->first()) {

            //            if ($this->sellerRepository->sellerIsSellingProduct($seller, $product->sku)) {
            //                // Product was already linked to seller
            //                return (new ResponseResource('success', 'Well done.', 'This product was already linked to your store', 3000, true))->response()->setStatusCode(200);
            //            }

            try {
                $this->sellerProductRepository->createAssign(array_merge($sellerData, ['product_id' => $product->id, 'is_owner' => 0]));
                // Product was already in database, and is now linked to the seller
                return (new ResponseResource('success', 'Well done.', 'We linked the product to your store', 3000, true))->response()->setStatusCode(200);
            } catch (\Throwable $th) {
                // error
                return (new ResponseResource('warning', 'Oops..', 'Something went wrong while trying to add the product to your store.', null, true))->response()->setStatusCode(500);
            }
        }

        // IF product not yet in products table
        \DB::beginTransaction();
        try {
            $this->releaseImportRepository->import($data, $sellerData);
            \DB::commit();
            // imported the product and linked it to seller
            return (new ResponseResource('success', 'Well done.', 'We imported the product and linked it to your store!', 3000, true))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            \DB::rollBack();
            // error
            return (new ResponseResource('warning', 'Oops..', 'Something went wrong while trying to add the product to your store.', null, true))->response()->setStatusCode(500);
        }
    }

    public function discogs_csv_import(Request $request)
    {
        // Validate if file is valid csv.
        $validator = \Validator::make($request->all(), [
            'file' => 'required|mimes:csv,txt'
        ]);
        if ($validator->fails()) {
            // file input is not present in request
            return (new ResponseResource('warning', 'Oops...', 'The file is either invalid, to big or missing.', null, true))->response()->setStatusCode(422);
        }
        $file = $request->file('file');

        // Check for invalid file extension
        if ((!in_array($file->getClientOriginalExtension(), ['csv']))) {
            return (new ResponseResource('warning', 'Oops...', 'Your file has the wrong extension. We only allow .csv files.', null, true))->response()->setStatusCode(400);
        };

        // extract the delimiter (either , or ;), if extraction fails, then throw error ('because file contents are unreadable')
        // in this case it can be only ,
        try {
            $delimiter = \CsvAnalyser::extractDelimiter($file);
            if ($delimiter != ",") {
                return (new ResponseResource('warning', 'Oops...', 'Sorry your file does not seem to be a discogs csv. Use a recent discogs csv export and dont make any changes to the csv file.', true))->response()->setStatusCode(422);
            }
        } catch (\Throwable $th) {
            return (new ResponseResource('warning', 'Oops...', 'The contents of the csv file are corrupted.', null, true))->response()->setStatusCode(400);
        }

        // Validate if required header fields are present.
        $csv_header = \CsvAnalyser::extractDiscogsCsvHeader($file, $delimiter);
        $validator = \Validator::make($csv_header, [
            'release_id' => 'required',
            'price' => 'required',
            'comments' => 'required',
            'media_condition' => 'required',
            'sleeve_condition' => 'required'
        ]);

        if ($validator->fails()) {
            return (new ResponseResource('warning', 'Oops...', 'Sorry your file does not seem to be a discogs csv. Use a recent discogs csv export and dont make any changes to the csv file.', null, true))->response()->setStatusCode(422);
        }

        // extract seller data
        $user = auth()->guard('customer')->user();
        $seller = $this->sellerRepository->getSellerByUserId($user->id);

        // check if file already exists
        // hash check or something?
        if (\CsvAnalyser::hasDuplicate($file, $seller, 'dicogscsv')) {
            // seller already uploaded this file before
            return (new ResponseResource('warning', 'Oops...', 'You have already imported this file.', null, true))->response()->setStatusCode(422);
        }

        // Execute, store file, load csv data in mysql and dispatch worker
        try {
            // add csv file to storage
            $uploaded_file = $seller->addMedia($file)->toMediaCollection('dicogscsv');

            // Chain both jobs so they always run in sequence (DiscogsCsvImportJob can only run if DiscogsCsvToSqlTableJob finished successfully)
            DiscogsCsvToSqlTableJob::withChain([
                new DiscogsCsvImportJob($uploaded_file, $seller, $user, $delimiter)
            ])->dispatch($seller, $uploaded_file);

            return (new ResponseResource('success', 'Your CSV is being processed.', 'You will receive an email once it is done!', 2500, false))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            return (new ResponseResource('warning', 'Oops...', 'There was a problem storing your file.', null, true))->response()->setStatusCode(422);
        }
    }

    public function custom_csv_import(Request $request)
    {

        $request->validate([
            'file' => 'required',
            'csv_map' => 'required'
        ]);

        $user = auth()->guard('customer')->user();
        $csv_map = json_decode($request->csv_map);

        $seller = $this->sellerRepository->getSellerByUserId($user->id);
        $file = $request->file('file');
        $valid_extension = ['csv', 'xls'];

        try {
            $delimiter = \CsvAnalyser::extractDelimiter($file);
        } catch (\Throwable $th) {
            return (new ResponseResource('warning', 'Oops...', 'Your CSV file seems to be corrupted.', null, true))->response()->setStatusCode(400);
        }

        // update the header row of csv file according to the csv_map
        if ((in_array($file->getClientOriginalExtension(), $valid_extension))) {

            $uploaded_file = $seller->addMedia($file)->toMediaCollection('customcsv');
            CustomCsvImportJob::dispatch($uploaded_file, $seller, $user, $csv_map, $delimiter);

            return (new ResponseResource('success', 'Your CSV is being processed.', 'You will receive an email once it is done!', 2500, false))->response()->setStatusCode(200);
        } else {
            return (new ResponseResource('warning', 'Oops...', 'Your file has the wrong extension.', null, true))->response()->setStatusCode(400);
        }
    }

    // import a product from from the pending_custom_csv_import_matches table
    public function delete_pending_custom_csv_product($pending_id)
    {
        try {
            PendingCustomCsvImportMatches::find($pending_id)->delete();;
            \DB::commit();
            return (new ResponseResource('success', 'Blabla.', 'Blabla bla bla bla.', 2500, false))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            \DB::rollBack();
            return (new ResponseResource('warning', 'Oops...', 'Blabla.', null, true))->response()->setStatusCode(500);
        }
    }
}
