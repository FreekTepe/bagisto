<?php

namespace VinylHeaven\ReleaseImport\Controllers\Api;

use Illuminate\Routing\Controller;
use VinylHeaven\ReleaseImport\Http\Requests\MatchReleases;
use App\Repositories\CategoryRepository;
use \App\Repositories\SellerRepository;
use Webkul\Theme\Facades\Themes;
use Illuminate\Validation\ValidationException;
use App\Mail\ValidationExceptionOccured;
use App\Http\Resources\ResponseResource;
use VinylHeaven\ReleaseImport\CsvImports\Resolvers\ElasticReleaseMatches;
use VinylHeaven\ReleaseImport\CsvImports\Resolvers\MongoReleaseMatch;


class ReleaseMatchController extends Controller
{

    protected $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        SellerRepository $sellerRepository
    ) {
        Themes::set('default'); // NTS!: temp fix for VIN-52

        // JWT AUTH
        auth()->setDefaultDriver('customer');
        $this->middleware('auth.jwt');

        $this->categoryRepository = $categoryRepository;
        $this->sellerRepository = $sellerRepository;
    }

    public function find($id)
    {
        $seller = $this->sellerRepository->getSellerByUserId(auth()->user()->id);

        try {
            // Match a release in mongo db by id
            $match = new MongoReleaseMatch($id, $seller);
            return response()->json(['data' => $match->getReleaseData()], 200);
        } catch (\Throwable $th) {
            return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
        }
    }

    // Try to make a match with the discogs mongodb data based on sellers search input.
    public function search(MatchReleases $request)
    {

        $data = $request->validated();
        $seller = $this->sellerRepository->getSellerByUserId(auth()->user()->id);
        try {
            $matches = new ElasticReleaseMatches($data, $seller);
            return response()->json(['data' => $matches->getReleases(), 'suggestions' => $matches->getSuggestions()], 200);
        } catch (\Throwable $th) {
            dd($th);
            return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
        }
    }

    private function status_code($th)
    {
        if (method_exists($th, 'getStatusCode')) {
            return $th->getStatusCode();
        } else if (isset($th->status)) {
            return $th->status;
        }
        return 500;
    }
}
