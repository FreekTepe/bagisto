<?php

namespace VinylHeaven\ReleaseImport\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\StringRequiredAndEmptyStringAllowed;

class Release extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            // Seller values
            'seller' => 'array|required',
            'seller.price' => 'required',
            'seller.is_owner' => 'required',
            'seller.condition' => 'required|string',
            'seller.description' => 'string',
            'seller.private_comments' => 'string',
            'seller.sleeve_condition' => 'string',
            'seller.storage_location' => 'string',
            "seller.shipping_price" => "numeric|nullable",

            // Product values
            'channel'             => 'string|required',
            'locale'              => 'string|required',
            'type'                => 'required|string',
            'seller_id'           => 'required',
            'attribute_family_id' => 'required',
            'attribute_family_id' => 'exists:Webkul\Attribute\Models\AttributeFamily,id',
            'price'               => 'required',
            'url_key'             => 'required',
            'status'              => 'required',
            'visible_individually' => 'required',
            'guest_checkout'      => 'required',
            'weight'              => 'required',

            'sku' => 'string|required',
            'master_id' => 'string|nullable',
            'is_main_release' => 'boolean|nullable',
            'name' => 'string|required',
            'description' => new StringRequiredAndEmptyStringAllowed,
            'short_description' => new StringRequiredAndEmptyStringAllowed,
            'release_date' => 'string|nullable',
            'country' => 'string|nullable',

            'artists' => 'array',
            "artists.*.artist_id" => "string|required",
            "artists.*.name" => "string|required",
            "artists.*.extra_artists" => 'array|nullable',
            "artists.*.real_name" => "string|nullable",
            "artists.*.profile" => "string|nullable",
            "artists.*.data_quality" => "string|nullable",

            'labels' => 'array',
            'labels.*.label_id' => 'string|required',
            'labels.*.name' => 'string|required',
            'labels.*.cat_no' => 'string',

            'tracklist' => 'array|nullable',
            'formats' => 'array|nullable',
            'identifiers' => 'array|nullable',
            'companies' => 'array|nullable',

            'categories' => 'array|nullable',
            'categories.*' => 'string|nullable',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        // .. hook for flashing errors
        // dd($validator->errors());
    }
}
