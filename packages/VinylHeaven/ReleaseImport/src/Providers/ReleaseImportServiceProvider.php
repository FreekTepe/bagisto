<?php

namespace VinylHeaven\ReleaseImport\Providers;

use Illuminate\Support\ServiceProvider;
use VinylHeaven\ReleaseImport\Classes\CsvAnalyser;

class ReleaseImportServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('csvanalyser', function () {
            return new CsvAnalyser();
        });
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'release-import');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
