@extends('shop::layouts.master')

@section('page_title')
CSV import
@endsection

@section('content-wrapper')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-cloud-upload icon-gradient bg-tempting-azure"></i>
            </div>
            <div>CSV import
                <div class="page-title-subheading">Here you can import products to your store using CSV files.</div>
            </div>
        </div>
    </div>
</div>   

<ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
    <li class="nav-item">
        <a role="tab" class="nav-link" href="{{route('discogs-csv-import.index')}}">
            <span>Discogs CSV</span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link active show" id="tab-1" data-toggle="tab" href="#tab-content-1" aria-selected="true">
            <span>Custom CSV</span>
        </a>
    </li>
</ul>


{{-- Upload --}}
<div class="card mb-3 mt-3">
    <div class="card-body">
        <custom-csv-import></custom-csv-import>
    </div>
</div>


{{-- Results --}}

{{-- CustomCsvDataGrid renders table --}}
{!! app('App\DataGrids\CustomCsvFilesDataGrid')->render() !!}



@endsection