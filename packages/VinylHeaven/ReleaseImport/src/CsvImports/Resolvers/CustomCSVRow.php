<?php

namespace VinylHeaven\ReleaseImport\CsvImports\Resolvers;

use \Illuminate\Validation\ValidationException;
use App\Rules\EitherStringOrNumeric;

class CustomCSVRow
{
    // validated row will be stored here
    protected $row;

    // Dynamische validatie uitdenken aan de hand van ervaringen met elasticsearch
    protected $rules = array();

    public function __construct($row, $csv_map)
    {
        // set rules
        $this->rules = [
            'release_title' => 'string',
            'artist_name' => 'string',
            'label' => 'string',
            'cat_no' => new EitherStringOrNumeric,
            'barcode' => new EitherStringOrNumeric,
            'released' => 'string'
        ];

        // edit the row keys according to $csv_map
        $row = $this->apply_csv_map($row, $csv_map);

        $validator = \Validator::make($row, $this->rules);
        if (!$validator->fails()) {

            // implement custom validation classes in rules!
            // $this->row = $validator->validated();
            $this->row = $row;
        } else {
            // Throw Validation Exception
            throw ValidationException::withMessages(array_merge(['origin' => 'CSV file'], $validator->errors()->getMessages()));
        }
    }

    public function getElasticMatchData()
    {
        return array_filter([
            'release_title' => $this->insert('release_title'),
            'artist_name' => $this->insert('artist_name'),
            'label' => $this->insert('label'),
            'cat_no' => $this->insert('cat_no'),
            'barcode' => $this->insert('barcode'),
            'released' => $this->insert('released'),
        ]);
    }

    public function getSellerData()
    {
        return array_filter([
            'price' => $this->insert('price'),
            'comments' => $this->insert('comments'),
            'media_condition' => $this->insert('media_condition'),
            'sleeve_condition' => $this->insert('sleeve_condition'),
            'private_comments' => $this->insert('private_comments'),
            'storage_location' => $this->insert('storage_location'),
            'shipping_price' => $this->insert('shipping_price'),
        ]);
    }

    private function insert($value)
    {
        return (isset($this->row[$value])) ? (string)$this->row[$value] : null;
    }

    private function apply_csv_map($row, $csv_map)
    {
        $csv_map = collect($csv_map);
        $mapped_row = array();
        foreach ($row as $key => $value) {
            if ($csv_map_item = $csv_map->where('csv_header', $key)->first()) {
                if ($csv_map_item['bagisto_field']) {
                    $mapped_row[$csv_map_item['bagisto_field']] = $value;
                }
            }
        }
        return $mapped_row;
    }
}
