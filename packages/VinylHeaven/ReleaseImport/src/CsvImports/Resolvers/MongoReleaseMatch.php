<?php

namespace VinylHeaven\ReleaseImport\CsvImports\Resolvers;

use \Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;
use Webkul\Attribute\Models\AttributeFamily;
use App\Rules\StringRequiredAndEmptyStringAllowed;
use App\Repositories\VinylExpressProductRepository as ProductRepository;

class MongoReleaseMatch
{
    // release array will be stored here
    protected $release;

    public function __construct($release_id, $seller)
    {
        $this->seller = $seller;

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->get(env('DISCOGS_API') . "/releases/" . $release_id);
        $status = $result->getStatusCode();

        if (!$status == 200) {
            // throw a exception!, mongo fetch failed
            // NTS: specify error api origin?
            abort($status);
        }

        $array = json_decode($result->getBody()->getContents(), true)['data'];
        $array = $this->insertDefaultValues($array);

        $validator = \Validator::make($array,  [

            'channel'             => 'string|required',
            'locale'              => 'string|required',
            'type'                => 'required|string',
            'seller_id'           => 'required',
            'attribute_family_id' => 'required',
            'attribute_family_id' => 'exists:Webkul\Attribute\Models\AttributeFamily,id',
            'price'               => 'required',
            'url_key'             => 'required',
            'status'              => 'required',
            'visible_individually' => 'required',
            'guest_checkout'      => 'required',
            'weight'              => 'required',

            'sku' => 'string|required',
            'master_id' => 'string|nullable',
            'is_main_release' => 'boolean|nullable',
            'name' => 'string|required',
            'description' => new StringRequiredAndEmptyStringAllowed,
            'short_description' => new StringRequiredAndEmptyStringAllowed,
            'release_date' => 'string|nullable',
            'country' => 'string|nullable',

            'artists' => 'array',
            "artists.*.artist_id" => "string|required",
            "artists.*.name" => "string|required",
            "artists.*.extra_artists" => 'array|nullable',
            "artists.*.real_name" => "string|nullable",
            "artists.*.profile" => "string|nullable",
            "artists.*.data_quality" => "string|nullable",

            'labels' => 'array',
            'labels.*.label_id' => 'string|required',
            'labels.*.name' => 'string|required',
            'labels.*.cat_no' => 'string',

            'tracklist' => 'array|nullable',
            'formats' => 'array|nullable',
            'identifiers' => 'array|nullable',
            'companies' => 'array|nullable',

            'categories' => 'array|nullable',
            'categories.*' => 'string|nullable',

        ]);

        if (!$validator->fails()) {
            $this->release = $validator->validated();
        } else {
            // Throw Validation Exception
            throw ValidationException::withMessages(array_merge(['origin' => env('DISCOGS_API')], $validator->errors()->getMessages()));
        }
    }

    private function insertDefaultValues($array)
    {
        return array_merge($array, [
            'seller_id'           =>  $this->seller->id,
            'attribute_family_id' => AttributeFamily::where('code', 'release')->first()->id,
            'status'              => 1,
            'visible_individually' => 1,
            'guest_checkout'      => 1,
            'url_key'             => ProductRepository::generateSlug($array['name']),
            'channel'             => 'default',
            'locale'              => 'en',
            'type'                => 'simple',
            'price'               => 0,
            'weight'              => 0
        ]);
    }

    // return productdata 
    public function getReleaseData()
    {
        return $this->release;
    }
}
