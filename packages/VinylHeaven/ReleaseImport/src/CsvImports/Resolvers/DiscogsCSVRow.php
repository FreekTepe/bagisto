<?php

namespace VinylHeaven\ReleaseImport\CsvImports\Resolvers;

use \Illuminate\Validation\ValidationException;

class DiscogsCSVRow
{
    // validated row will be stored here
    protected $row;

    // Validate incoming row
    protected $rules = [
        "release_id" => "required",
        "price" => "required",
        // "comments" => "string",
        "media_condition" => "required",
        "sleeve_condition" => "required",
        "private_comments" => "string",
        "storage_location" => "string",
        "shipping_price" => "nummeric",
        "listing_id" => "numeric"
    ];

    public function __construct($row)
    {
        $validator = \Validator::make($row, $this->rules);
        if (!$validator->fails()) {
            $this->row = $validator->validated();
        } else {
            // Throw Validation Exception
            throw ValidationException::withMessages(array_merge(['origin' => 'CSV file'], $validator->errors()->getMessages()));
        }
    }

    public function getReleaseId()
    {
        return $this->row['release_id'];
    }

    public function getSellerData()
    {
        $user_data = $this->row;
        unset($user_data['release_id']);

        // rename some discogs column names to bagisto column names
        $user_data['condition'] = $user_data['media_condition'];
        unset($user_data['media_condition']);

        if(isset($user_data['comments'])){
            $user_data['description'] = $user_data['comments'];
            unset($user_data['comments']);
        }

        return $user_data;
    }

    public function all()
    {
        return $this->row;
    }
}
