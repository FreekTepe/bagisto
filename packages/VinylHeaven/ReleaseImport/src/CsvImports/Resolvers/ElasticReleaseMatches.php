<?php

namespace VinylHeaven\ReleaseImport\CsvImports\Resolvers;

use GuzzleHttp\Client;
use App\Rules\StringRequiredAndEmptyStringAllowed;

class ElasticReleaseMatches
{
    // release array will be stored here
    protected $releases;
    protected $suggestions;

    public function __construct($data, $seller)
    {
        $this->seller = $seller;
        $client = new Client(); //GuzzleHttp\Client

        $result = $client->request('POST', env('DISCOGS_API') . "/releases", [
            'json' => array_filter($data)
        ]);
        $status = $result->getStatusCode();

        if (!$status == 200) {
            abort($status);
        }

        // $array = $this->objToArray(json_decode($result->getBody()->getContents(), true));
        $array = json_decode($result->getBody()->getContents(), true);
        // dd($array);
        $validator = \Validator::make($array, [

            // releases
            'data.*.sku' => 'string|required',
            'data.*.name' => 'string|required',
            'data.*.release_date' => 'string|nullable',
            'data.*.country' => 'string|nullable',
            'data.*.description' => new StringRequiredAndEmptyStringAllowed,
            'data.*.short_description' => new StringRequiredAndEmptyStringAllowed,

            'data.*.categories' => 'array|nullable',
            'data.*.categories.*' => 'string|nullable',

            'data.*.formats' => 'array|nullable',
            'data.*.tracklist' => 'array|nullable',
            'data.*.artists' => 'array|nullable',
            'data.*.labels' => 'array|nullable',
            'data.*.identifiers.*' => 'array|nullable',
            'data.*.companies' => 'array|nullable',

        ]);

        $this->releases = $validator->valid();

        $this->suggestions = $this->releases['suggestions'];
    }

    // return productdata 
    public function getReleases()
    {
        return $this->releases;
    }

    public function getSuggestions()
    {
        return [
            "release_title" => $this->mapSuggestions('release_title'),
            "artist_name" => $this->mapSuggestions('artist_name'),
            "released" => $this->mapSuggestions('released'),
            "barcode" => $this->mapSuggestions('barcode'),
            "label" => $this->mapSuggestions('label'),
            "cat_no" => $this->mapSuggestions('cat_no')
        ];

    }

    private function mapSuggestions($key)
    {
        if(isset($this->suggestions[$key]) && count($this->suggestions[$key]) > 0){
            $suggestion_options = array();
            foreach($this->suggestions[$key] as $suggestion){
                foreach($suggestion['options'] as $option){
                    array_push($suggestion_options, $option['text']);
                }
            }
            return $suggestion_options;
        }
        return null;
    }
}
