<?php

namespace VinylHeaven\ReleaseImport\Repositories;

use Webkul\Core\Eloquent\Repository;
use App\Repositories\VinylExpressProductRepository as ProductRepository;
use \Webkul\Marketplace\Repositories\ProductRepository as SellerProductRepository;
use VinylHeaven\CustomAttributeType\Repositories\LabelRepository;
use VinylHeaven\CustomAttributeType\Repositories\ArtistRepository;
use App\Repositories\CategoryRepository;
use \App\Repositories\SellerRepository;
use VinylHeaven\Shipping\Repositories\FormatRulesRepository;


class ReleaseImportRepository extends Repository
{

    /**
     * ProductRepository object
     *
     * @var \Webkul\Product\Repositories\ProductRepository
     */
    protected $productRepository;

    /**
     * ProductRepository object (Marketplace)
     *
     * @var \Webkul\Marketplace\Repositories\ProductRepository
     */
    protected $sellerProduct;

    /**
     * LabelRepository object
     *
     * @var \VinylHeaven\CustomAttributeType\Repositories\LabelRepository
     */
    protected $labelRepository;

    /**
     * ArtistRepository object
     *
     * @var \VinylHeaven\CustomAttributeType\Repositories\ArtistRepository
     */
    protected $artistRepository;

    /**
     * FormatRulesRepository object
     *
     * @var VinylHeaven\Shipping\Repositories\FormatRulesRepository
     */
    protected $formatRulesRepository;


    public function __construct(
        ProductRepository $productRepository,
        SellerProductRepository $sellerProductRepository,
        LabelRepository $labelRepository,
        ArtistRepository $artistRepository,
        CategoryRepository $categoryRepository,
        SellerRepository $sellerRepository,
        FormatRulesRepository $formatRulesRepository
    ) {
        $this->productRepository = $productRepository;
        $this->sellerProductRepository = $sellerProductRepository;
        $this->labelRepository = $labelRepository;
        $this->artistRepository = $artistRepository;
        $this->categoryRepository = $categoryRepository;
        $this->sellerRepository = $sellerRepository;
        $this->formatRulesRepository = $formatRulesRepository;
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        // ...
    }

    public function import($data, $sellerData)
    {

        // try to extract dimensions based on format
        // TEMP browser testing //
        // $match_id = 

        // dd($match_id);
        // // dd($dimensions);

        if(!isset($sellerData['description']) || is_null($sellerData['description'])){
            $sellerData['description'] = "";
        }

        // destructure data
        if (isset($data['seller'])) {
            unset($data['seller']);
        };
        if (isset($data['artists'])) {
            $artistsData = $data['artists'];
            unset($data['artists']);
        }
        if (isset($data['labels'])) {
            $labelsData = $data['labels'];
            unset($data['labels']);
        }
        if (isset($data['tracklist'])) {
            $data['tracklist'] = json_encode($data['tracklist']);
        }
        if (isset($data['identifiers'])) {
            $data['identifiers'] = json_encode($data['identifiers']);
        }
        if (isset($data['companies'])) {
            $data['companies'] = json_encode($data['companies']);
        }
        if (isset($data['formats'])) {
            $formats = $data['formats'];
            $data['formats'] = json_encode($formats);
        }
        if (isset($data['categories'])) {
            $data['categories'] = $this->categoryRepository->getIdsByNames($data['categories']); // convert category names to ids
        }


        // Do database saves.
        // table: products
        $product = $this->productRepository->create($data);

        // table: (product_attribute_values)
        $this->productRepository->update($data, $product->id);


        // attach format rules
        $this->formatRulesRepository->attachFormatRulesToProduct($product, $formats);

        // table: artists
        if (isset($artistsData)) {
            $artists = $this->artistRepository->createArtists($artistsData);
            $this->productRepository->attachArtists($product->id, $artists->pluck('artist_id'));
        }

        // table: labels
        if (isset($labelsData)) {
            $labels = $this->labelRepository->createLabels($labelsData);
            $this->productRepository->attachLabels($product->id, $labels->pluck('label_id'));
        }

        // table: marketplace_products
        $this->sellerProductRepository->createAssign(array_merge($sellerData, ['product_id' => $product->id, 'is_owner' => 0]));
        
    }
}
