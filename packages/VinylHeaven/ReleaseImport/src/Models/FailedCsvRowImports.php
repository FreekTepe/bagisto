<?php

namespace VinylHeaven\ReleaseImport\Models;

use App\Models\ExtendWebkul\Seller;
use Illuminate\Database\Eloquent\Model;

class FailedCsvRowImports extends Model
{

    protected $fillable = ["seller_id", "csv_id", "collection_type", "csv_row", "exception"];

    protected $casts = [
        'csv_row' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(Seller::class, 'id', 'seller_id');
    }
}
