<?php

namespace VinylHeaven\ReleaseImport\Models;

use Illuminate\Database\Eloquent\Model;

class DiscogsCsv extends Model
{

    protected $import_status_map = [
        100 => "Awaiting import",
        200 => "Import successfull",
        300 => "Import failed"
    ];

    protected $fillable = [
        'import_status',
        'seller_id',
        'media_id',
        'listing_id',
        'exception',
        'artist',
        'title',
        'label',
        'catno',
        'format',
        'release_id',
        'status',
        'price',
        'listed',
        'comments',
        'media_condition',
        'sleeve_condition',
        'accept_offer',
        'external_id',
        'weight',
        'format_quantity',
        'flat_shipping',
        'location'
    ];

    // Return human readable track status
    public function getStatusNameAttribute()
    {
        return $this->import_status_map[$this->import_status];
    }
}
