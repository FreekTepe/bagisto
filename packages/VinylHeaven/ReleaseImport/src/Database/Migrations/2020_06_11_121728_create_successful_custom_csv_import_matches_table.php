<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuccessfulCustomCsvImportMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_custom_csv_import_matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('seller_id');
            $table->integer('csv_id');
            $table->string('collection_type');
            $table->longText('csv_row');
            $table->longText('matches'); // array of all the elastic csv matches
            $table->longText('seller_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('successful_custom_csv_import_matches');
    }
}
