<?php

namespace VinylHeaven\Catalog\Helpers;

class Catalog
{
    public function getTotalPaginationPages($products_count)
    {
        return ceil(($products_count / $this->getLimit()));
    }

    public function getLimit()
    {
        $params = request()->input();

        return isset($params['limit']) ? $params['limit'] : 33;
    }

    public function getCurrentPage()
    {
        $params = request()->input();

        return isset($params['page']) ? $params['page'] : 1;
    }

    public function getPageRange()
    {
        return [
            'start' => $this->getCurrentPage(),
            'max' => ($this->getCurrentPage() + 10)
        ];
    }

    public function getPaginationUrl($page_number)
    {
        return request()->fullUrlWithQuery([
            'page'  => $page_number,
        ]);
    }

    public function getPaginationNextPageUrl($products_count)
    {
        if ($this->getCurrentPage() == $products_count) {
            return '#';
        } else {
            return request()->fullUrlWithQuery([
                'page' => ($this->getCurrentPage() + 1)
            ]);
        }
    }

    public function getPaginationPreviousPageUrl()
    {
        if ($this->getCurrentPage() == 1) {
            return '#';
        } else {
            return request()->fullUrlWithQuery([
                'page' => ($this->getCurrentPage() - 1)
            ]);
        }
    }

    public function getReleaseYear($release_date)
    {
        $release_date_split = explode('-', $release_date);

        if (count($release_date_split) > 1) {
            foreach ($release_date_split as $item_index => $item) {
                if (strlen($item) == 4) {
                    return $item;
                }
            }
        } else {
            return isset($release_date_split[0]) ? $release_date_split[0] : $release_date;
        }
    }

    public function getTotalAmountOfProduct($product_id)
    {
        return app('App\Models\ExtendWebkul\MarketplaceProduct')->where('product_id', $product_id)->get()->count();
    }
}
