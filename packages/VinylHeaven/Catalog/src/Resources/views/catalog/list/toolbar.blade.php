@inject ('toolbarHelper', 'Webkul\Product\Helpers\Toolbar')

<div class="search-result__top">
    <div class="search-result__name">
        <div class="search-result__results"></div>
    </div>

    <div class="search-result__sorting">
        <div class="search-result__sort">
            <span class="search-result__text">Results</span>

            <select id="vinylheaven-catalog-results-limiter" class="search-result__dropdown">
                @foreach ($toolbarHelper->getAvailableLimits() as $limit_key => $limit_value)
                    @if ($toolbarHelper->isLimitCurrent($limit_value))
                        <option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}" selected>{{ $limit_value }}</option>
                    @else
                        <option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}">{{ $limit_value }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        
        <div class="search-result__sort">
            <span class="search-result__text">Sorted by</span>

            <select id="vinylheaven-catalog-sorter" class="search-result__dropdown">
                @foreach ($toolbarHelper->getAvailableOrders() as $order_key => $order_value)
                    @if ($toolbarHelper->isOrderCurrent($order_key))
                        <option value="{{ $toolbarHelper->getOrderUrl($order_key) }}" selected>{{ $order_value }}</option>
                    @else
                        <option value="{{ $toolbarHelper->getOrderUrl($order_key) }}">{{ $order_value }}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="search-result__view">
            <span class="search-result__text">View</span>
            @if ($toolbarHelper->isModeActive('grid') || (!$toolbarHelper->isModeActive('grid') && !$toolbarHelper->isModeActive('list')))
                <a href="#" class="search-result__icon is-active">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/grid-icon.png') }}" alt=""/>
                </a>
            @else
                <a href="{{ $toolbarHelper->getModeUrl('grid') }}" class="search-result__icon">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/grid-icon.png') }}" alt=""/>
                </a>
            @endif

            @if ($toolbarHelper->isModeActive('list'))
                <a href="#" class="search-result__icon is-active">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/list-Icon.png') }}" alt=""/>
                </a>
            @else
                <a href="{{ $toolbarHelper->getModeUrl('list') }}" class="search-result__icon">
                    <img src="{{ asset('themes/vinylexpress/assets/src/img/list-Icon.png') }}" alt=""/>
                </a>
            @endif
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        (function( $ ) {
            if ($('#vinylheaven-catalog-sorter').length) {
                $('#vinylheaven-catalog-sorter').on('change', function(){
                    window.location = $(this).children('option:selected').val();
                });
            }

            if ($('#vinylheaven-catalog-results-limiter').length) {
                $('#vinylheaven-catalog-results-limiter').on('change', function(){
                    window.location = $(this).children('option:selected').val();
                });
            }
        })(jQuery);
    </script>
@endpush