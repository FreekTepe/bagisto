@extends('shop::layouts.master-new')

@inject ('catalogHelper', 'VinylHeaven\Catalog\Helpers\Catalog')
@inject ('toolbarHelper', 'Webkul\Product\Helpers\Toolbar')
@inject ('product_full', 'App\Models\ExtendWebkul\Product')
@inject ('searchHelper', 'App\Helpers\Search')

@php
$is_catalog = true;
$has_from_price = true;
@endphp

@section('content-wrapper')
    <section class="search-result container">
        @include('shop::products.view.toolbar')


        <div class="search-result-container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
					@include('shop::search.filters')
				</div>

				<div class="col-12 col-md-8 col-lg-9">
					<div class="result-grid {{ $toolbarHelper->getGridViewDisplay() }}">
						<div class="row">
							@foreach ($products as $product)
								@if (isset($product->url_key) && !empty($product->url_key))
                                    @php
                                        $product = $product_full->where('id', $product->product_id)->first();
                                    @endphp

                                    <div class="col-lg-4 col-md-6 col-sm-6 col-6">
                                        @include('shop::search.grid.item')
                                    </div>
								@endif
							@endforeach
						</div>
					</div>
				
					<div class="result-list {{ $toolbarHelper->getListViewDisplay() }}">
						@foreach ($products as $product)
							@if (isset($product->url_key) && !empty($product->url_key))
                                @php
                                    $product = $product_full->where('id', $product->product_id)->first();
                                @endphp
                                
                                @include('shop::search.list.item')
							@endif
						@endforeach
                    </div>
                    
                    <div class="col-12">
                        @include('shop::search.pagination')
                    </div>
				</div>
            </div>
        </div>
    </section>
@endsection