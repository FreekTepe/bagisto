<?php

namespace Vinylheaven\Catalog\Http\Controllers;

use Illuminate\Http\Request;
use Webkul\Marketplace\Http\Controllers\Shop\Controller;
use Webkul\Category\Repositories\CategoryRepository;
use Webkul\Product\Repositories\ProductRepository;

class CatalogController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    public function __construct()
    {
        $this->_config = request('_config');
    }

    public function index()
    {
        $products_model = app('App\Models\ExtendWebkul\Product');

        return view($this->_config['view'])
            ->with('products', $products_model->getProductsForCatalog())
            ->with('products_count', $products_model->getTotalProductsAmountForCatalog());
    }

    /**
     * Show product or category view. If neither category nor product matches, abort with code 404.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View|\Exception
     */
    public function productOrCategory(Request $request)
    {
        $slugOrPath = trim($request->getPathInfo(), '/');

        if (preg_match('/^([a-z0-9-]+\/?)+$/', $slugOrPath)) {
            $categoryRepository = app('Webkul\Category\Repositories\CategoryRepository');
            if ($category = $categoryRepository->findByPath($slugOrPath)) {
                return view($this->_config['category_view'], compact('category'));
            }

            $productRepository = app('Webkul\Product\Repositories\ProductRepository');
            if ($product = $productRepository->findBySlug($slugOrPath)) {
                $product->product = $productRepository->where('id', $product->product_id)->first();
                $customer = auth()->guard('customer')->user();
                $product_view_config = json_decode(json_encode([
                    'is_seller_product' => false,
                    'aria_expanded' => true,
                    'has_from_price' => true,
                    'classes' => [
                        'product_other_sellers' => 'collapse-open',
                        'product_other_sellers_collapse' => 'show'
                    ],
                    'other_products_count_text' => $product->product->marketplace_product()->count() . ' of this release are'
                ]));

                return view('marketplace::shop.sellers.product.index', compact('product', 'customer', 'product_view_config'));
            }
        }

        abort(404);
    }

    public function sellerProduct($url, $slug)
    {
        $seller = app('Webkul\Marketplace\Repositories\SellerRepository')->findByUrlOrFail($url);
        $baseProduct = app('Webkul\Product\Repositories\ProductRepository')->findBySlugOrFail($slug);
        $product = app('Webkul\Marketplace\Repositories\ProductRepository')->where([['product_id', "=", $baseProduct->product_id], ['marketplace_seller_id', "=", $seller->id]])->first();
        $product_view_config = json_decode(json_encode([
            'is_seller_product' => true,
            'aria_expanded' => false,
            'has_from_price' => false,
            'classes' => [
                'product_other_sellers' => '',
                'product_other_sellers_collapse' => ''
            ],
            'other_products_count_text' => ($product->product->marketplace_product()->count() - 1) . ' more'
        ]));
        
        return view($this->_config['view'], compact('seller', 'product', 'product_view_config'));
    }
}
