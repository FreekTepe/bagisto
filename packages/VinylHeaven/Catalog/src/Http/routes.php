<?php

Route::group(['middleware' => ['web', 'locale', 'theme', 'currency']], function () {
    Route::namespace('\VinylHeaven\Catalog\Http\Controllers')->group(function () {
        Route::get('/catalog', 'CatalogController@index')->defaults('_config', [
            'view' => 'catalog::catalog.index'
        ])->name('catalog.catalog.index');
    });
});