<div class="row">
    <div class="col-12">
        <div class="heading subtitle">
            <h2 class="title">{{ __('shop::app.checkout.onepage.summary') }}</h2>
        </div>
    </div>
</div>

<div class="row">
    @if ($billingAddress = $cart->billing_address)
        <div class="col-12 col-md-6">
            <div class="row mb-2">
                <div class="col-12">
                    <strong>{{ __('shop::app.checkout.onepage.billing-address') }}</strong>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ $billingAddress->first_name }} {{ $billingAddress->last_name }}
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ $billingAddress->address1 }},<br/> {{ $billingAddress->state }}
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ $billingAddress->country }} {{ $billingAddress->postcode }}
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ __('shop::app.checkout.onepage.contact') }} : {{ $billingAddress->phone }}
                </div>
            </div>            
        </div>
    @endif

    <div class="col-12 col-md-6">
        @php
            $shippingAddress = $cart->shipping_address;
        @endphp

        @if ($cart->haveStockableItems() && !empty($shippingAddress->address1))
            <div class="row mb-2">
                <div class="col-12">
                    <b>{{ __('shop::app.checkout.onepage.shipping-address') }}</b>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ $shippingAddress->first_name }} {{ $shippingAddress->last_name }}
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ $shippingAddress->address1 }},<br/> {{ $shippingAddress->state }}
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ $shippingAddress->country }} {{ $shippingAddress->postcode }}
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    {{ __('shop::app.checkout.onepage.contact') }} : {{ $shippingAddress->phone }}
                </div>
            </div>
        @endif
    </div>
</div>

<div class="row mt-4 mb-2">
    <div class="col-12">
       <strong>Order Details</strong> 
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-start align-items-center">
            <i class="fa fa-truck mr-4"></i><span>{{ core()->currency($cart->selected_shipping_rate->base_price) }} {{ $cart->selected_shipping_rate->method_title }}</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-start align-items-center">
            <i class="fa fa-money mr-4"></i><span>{{ core()->getConfigData('sales.paymentmethods.' . $cart->payment->method . '.title') }}</span>
        </div>
    </div>
</div>

<div class="row mt-4 mb-2">
    <div class="col-12">
        <strong>Order Items</strong>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table table-borderless table-sm w-100">
            <thead>
                <tr>
                    <th class="w-50" colspan="2">Name</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Price</th>
                    <th class="text-right">Totals</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($cart->items as $item_index => $item)
                    <tr>
                        {!! view_render_event('bagisto.shop.checkout.name.before', ['item' => $item]) !!}
                        <td colspan="2" scope="row">{{ $item->product->name }}</td>
                        {!! view_render_event('bagisto.shop.checkout.name.after', ['item' => $item]) !!}
                        
                        {!! view_render_event('bagisto.shop.checkout.quantity.before', ['item' => $item]) !!}
                        <td class="text-center">{{ $item->quantity }}</td>
                        {!! view_render_event('bagisto.shop.checkout.quantity.after', ['item' => $item]) !!}

                        {!! view_render_event('bagisto.shop.checkout.price.before', ['item' => $item]) !!}
                        <td class="text-center">{{ core()->currency($item->base_price) }}</td>
                        {!! view_render_event('bagisto.shop.checkout.price.after', ['item' => $item]) !!}

                        @if (count($cart->items) == ($item_index + 1))
                            <td class="text-right">{{ core()->currency($cart->base_sub_total) }}</td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                @endforeach

                @if ($cart->selected_shipping_rate)
                    <tr>
                        <td colspan="3" scope="row"></td>
                        <td class="text-right">{{ __('shop::app.checkout.total.delivery-charges') }}</td>
                        <td class="text-right">{{ core()->currency($cart->selected_shipping_rate->base_price) }}</td>
                    </tr>
                @endif

                @if ($cart->base_tax_total)
                    @foreach (Webkul\Tax\Helpers\Tax::getTaxRatesWithAmount($cart, true) as $taxRate => $baseTaxAmount )
                        <tr>
                            <td colspan="3" scope="row"></td>
                            <td class="text-right">{{ __('shop::app.checkout.total.tax') }} {{ $taxRate }} %</td>
                            <td class="text-right">{{ core()->currency($baseTaxAmount) }}</td>
                        </tr>
                    @endforeach
                @endif

                @if ($cart->base_discount_amount && $cart->base_discount_amount > 0)
                    <tr>
                        <td colspan="3" scope="row"></td>
                        <td class="text-right">{{ __('shop::app.checkout.total.disc-amount') }}</td>
                        <td class="text-right">-{{ core()->currency($cart->base_discount_amount) }}</td>
                    </tr>
                @endif

                <tr>
                    <td colspan="3" scope="row"></td>
                    <td class="text-right">{{ __('shop::app.checkout.total.grand-total') }}</td>
                    <td class="text-right">{{ core()->currency($cart->base_grand_total) }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>