<div class="row">
    <div class="col-12">
        <div class="heading subtitle">
            <h2 class="title">{{ __('shop::app.checkout.onepage.payment-methods') }}</h2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        @foreach ($paymentMethods as $payment)

            {!! view_render_event('bagisto.shop.checkout.payment-method.before', ['payment' => $payment]) !!}

            <div class="row">
                <div class="col-12">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="{{ $payment['method'] }}" name="payment_method" value="{{ $payment['method'] }}" aria-describedby="{{ $payment['method'] . '-description' }}" required> 
                        <label for="{{ $payment['method'] }}" class="custom-control-label">{{ $payment['method_title'] }}</label>
                        <div class="invalid-feedback">Choose Payment Method</div>
                        <small id="{{ $payment['method'] . '-description' }}" class="form-text text-muted">{{ __($payment['description']) }}</small>
                    </div>    
                </div>
            </div>

            {!! view_render_event('bagisto.shop.checkout.payment-method.after', ['payment' => $payment]) !!}

        @endforeach
    </div>
</div>