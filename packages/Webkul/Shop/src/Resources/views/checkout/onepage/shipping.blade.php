<div class="row">
    <div class="col-12">
        <div class="heading subtitle">
            <h2 class="title">{{ __('shop::app.checkout.onepage.shipping-method') }}</h2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        @foreach ($shippingRateGroups as $rateGroup)

            {!! view_render_event('bagisto.shop.checkout.shipping-method.before', ['rateGroup' => $rateGroup]) !!}

            <div>
                <strong>{{ $rateGroup['carrier_title'] }}</strong>
            </div>
            
            <div class="row">
                <div class="col-12">
                    @foreach ($rateGroup['rates'] as $rate)
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="{{ $rate->method }}" name="shipping_method" value="{{ $rate->method }}" aria-describedby="{{ $rate->method . '-description' }}" required> 
                            <label for="{{ $rate->method }}" class="custom-control-label">{{ $rate->method_title }} - {{ core()->currency($rate->base_price) }}</label>
                            <div class="invalid-feedback">Choose Shipping Method</div>
                            <small id="{{ $rate->method . '-description' }}" class="form-text text-muted">{{ __($rate->method_description) }}</small>
                        </div>
                    @endforeach
                </div>
            </div>

            {!! view_render_event('bagisto.shop.checkout.shipping-method.after', ['rateGroup' => $rateGroup]) !!}

        @endforeach
    </div>
</div>

