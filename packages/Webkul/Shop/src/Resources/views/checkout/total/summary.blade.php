<div class="row">
    <div class="col-12">
        <strong>{{ __('shop::app.checkout.total.order-summary') }}</strong>
    </div>
</div>

<div class="row">
    <div class="col">
        {{ intval($cart->items_qty) }} {{ __('shop::app.checkout.total.sub-total') }} {{ __('shop::app.checkout.total.price') }}
    </div>

    <div class="col">
        {{ core()->currency($cart->base_sub_total) }}
    </div>
</div>

@if ($cart->selected_shipping_rate)
    <div class="row">
        <div class="col">
            {{ __('shop::app.checkout.total.delivery-charges') }}
        </div>

        <div class="col">
            {{ core()->currency($cart->selected_shipping_rate->base_price) }}
        </div>
    </div>
@endif

@if ($cart->base_tax_total)
    @foreach (Webkul\Tax\Helpers\Tax::getTaxRatesWithAmount($cart, true) as $taxRate => $baseTaxAmount )
        <div class="row">
            <div class="col">
                {{ __('shop::app.checkout.total.tax') }} {{ $taxRate }} %
            </div>

            <div class="col">
                {{ core()->currency($baseTaxAmount) }}
            </div>
        </div>
    @endforeach
@endif

@if ($cart->base_discount_amount && $cart->base_discount_amount > 0)
    <div class="row">
        <div class="col">
            {{ __('shop::app.checkout.total.disc-amount') }}
        </div>

        <div class="col">
            -{{ core()->currency($cart->base_discount_amount) }}
        </div>
    </div>
@endif

<div class="row">
    <div class="col">
        {{ __('shop::app.checkout.total.grand-total') }}
    </div>

    <div class="col">
        {{ core()->currency($cart->base_grand_total) }}
    </div>
</div>