<?php
 
namespace Webkul\MarketplacePerProductShipping\Providers;

use Illuminate\Support\ServiceProvider;
 
/**
 * Service provider
 *
 * @author    shaiv roy 
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class MarketplacePerProductShippingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');

        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'marketplaceperproductshipping');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'marketplaceperproductshipping');

        $this->app->register(EventServiceProvider::class);
    }
 
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {   
        $this->registerConfig();
    }

    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/carriers.php', 'carriers' 
          );

        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/system.php', 'core'   // add menu inside configuration
        );
    }
}
