<?php

namespace Webkul\MarketplacePerProductShipping\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen('marketplace.sellers.account.catalog.product.edit-assign.after', function($viewRenderEventManager) {
            $viewRenderEventManager->addTemplate('marketplaceperproductshipping::shop.catalog.products.edit-assign');
        });

        Event::listen('marketplace.sellers.account.catalog.products.assign.after', function($viewRenderEventManager) {
            $viewRenderEventManager->addTemplate('marketplaceperproductshipping::shop.catalog.products.assign');
        });

        Event::listen('marketplace.catalog.assign-product.create.after', function($product) {
            $product->shipping_price = request()->get('shipping_price');
            
            $product->save();
        });

        Event::listen('marketplace.catalog.assign-product.update.after', function($product) {
            $product->shipping_price = request()->get('shipping_price');

            $product->save();
        });
    }
}