<?php

return [
    [
        'key' => 'sales.carriers.mpperproductshipping',
        'name' => 'marketplaceperproductshipping::app.admin.system.mpperproductshipping',
        'sort' => 3,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'marketplaceperproductshipping::app.admin.system.title',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'marketplaceperproductshipping::app.admin.system.description',
                'type' => 'textarea',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'default_rate',
                'title' => 'marketplaceperproductshipping::app.admin.system.rate',
                'type' => 'text',
                'channel_based' => true,
                'locale_based' => false
            ],  [
                'name' => 'active',
                'title' => 'marketplaceperproductshipping::app.admin.system.status',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Active',
                        'value' => true
                    ], [
                        'title' => 'Inactive',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ]
];