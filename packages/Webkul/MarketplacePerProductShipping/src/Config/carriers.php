<?php

return [
    'mpperproductshipping' => [
        'code' => 'mpperproductshipping',
        'title' => 'Marketplace Per Product Shipping',
        'description' => 'This is a Marketplace Per Product Shipping rate',
        'active' => true,
        'default_rate' => '10',
        'class' => 'Webkul\MarketplacePerProductShipping\Carriers\MarketplacePerProductShipping',
    ]
];