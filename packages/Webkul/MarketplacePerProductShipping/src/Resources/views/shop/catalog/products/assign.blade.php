<accordian :title="'{{ __('marketplaceperproductshipping::app.shop.sellers.account.catalog.products.shipping') }}'" :active="true">
    <div slot="body">

        <div class="control-group">
            <label for="shipping_price">{{ __('marketplaceperproductshipping::app.shop.sellers.account.catalog.products.shipping-price') }}</label>
            
            <input type="text" class="control" id="shipping_price" name="shipping_price" value="{{ old('shipping_price')}}">
        </div>

    </div>
</accordian>