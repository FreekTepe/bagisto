<?php

return [
    'admin' => [
        'system' => [
            'mpperproductshipping'           => 'Marketplace Per Product Shipping',
            'shipping-methods'              => 'Shipping Method',
            'title'                         => 'Title',
            'description'                   => 'Description',
            'status'                        => 'Status',
            'rate'                          => 'Rate',
        ]
    ],
    
    'shop' => [
        'sellers' => [
            'account' => [
                'catalog'=> [
                    'products'=>[
                        'shipping-price'    => 'Shipping Price',
                        'shipping'          => 'Shipping',
                    ]
                ]
            ] 
        ]
    ]
];