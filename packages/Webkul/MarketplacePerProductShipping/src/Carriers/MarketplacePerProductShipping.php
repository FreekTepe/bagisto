<?php

namespace Webkul\MarketplacePerProductShipping\Carriers;

use Webkul\Shipping\Carriers\AbstractShipping;
use Webkul\Checkout\Models\CartShippingRate;
use Webkul\Checkout\Facades\Cart;

/**
 * Class Rate.
 *
 */
class MarketplacePerProductShipping extends AbstractShipping
{  
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'mpperproductshipping';

    public function calculate()
    {    
        $this->sellerRepository = app('Webkul\Marketplace\Repositories\SellerRepository');

        $this->marketplaceProductRepository = app('Webkul\Marketplace\Repositories\ProductRepository');

        if (! $this->isAvailable())
            return false;

        $cart = Cart::getCart();

        $totalShippingPrice = 0;

        $rates = [];

        foreach ($cart->items()->get() as $item) {
            if (isset($item->additional['seller_info']) && ! $item->additional['seller_info']['is_owner']) {
                $seller = $this->sellerRepository->find($item->additional['seller_info']['seller_id']);
            } else {
                $seller = $this->marketplaceProductRepository->getSellerByProductId($item->product_id);
            }
            
            $itemShippingPrice = $sellerId = 0;

            if ($seller && $seller->is_approved) {
                $sellerId = $seller->id;

                $sellerProduct = $this->marketplaceProductRepository->findOneWhere([
                        'product_id' => $item->product->id,
                        'marketplace_seller_id' => $seller->id,
                    ]);

                if ($productShippingPrice = $sellerProduct->shipping_price) {
                    $itemShippingPrice = $productShippingPrice * $item->quantity;
                } else if($productShippingPrice = $item->product->shipping_price) {
                    $itemShippingPrice = $productShippingPrice * $item->quantity;
                } else {
                    $itemShippingPrice = $this->getConfigData('default_rate') * $item->quantity;
                }
            } else {
                $sellerId = 0;

                if($productShippingPrice = $item->product->shipping_price) {
                    $itemShippingPrice = $productShippingPrice * $item->quantity;
                } else {
                    $itemShippingPrice = $this->getConfigData('default_rate') * $item->quantity;
                }
            }

            if (isset($rates[$sellerId])) {
                $rates[$sellerId]['amount'] = core()->convertPrice($rates[$sellerId]['amount'] + $itemShippingPrice);
                $rates[$sellerId]['base_amount'] = $rates[$sellerId]['base_amount'] + $itemShippingPrice;
            } else {
                $rates[$sellerId] = [
                    'amount' => core()->convertPrice($itemShippingPrice),
                    'base_amount' => $itemShippingPrice
                ];
            }
            
            $totalShippingPrice += $itemShippingPrice;
        }
        
        $object = new CartShippingRate;

        $object->carrier = 'mpperproductshipping';
        $object->carrier_title = $this->getConfigData('title');
        $object->method = 'mpperproductshipping_mpperproductshipping';
        $object->method_title = $this->getConfigData('title');
        $object->method_description = $this->getConfigData('description');
        $object->price = core()->convertPrice($totalShippingPrice);
        $object->base_price = $totalShippingPrice;

        $marketplaceShippingRates = session()->get('marketplace_shipping_rates');

        if (! is_array($marketplaceShippingRates))
            $marketplaceShippingRates = [];

        $marketplaceShippingRates['mpperproductshipping'] = [ 'mpperproductshipping' => $rates ];

        session()->put('marketplace_shipping_rates', $marketplaceShippingRates);

        return $object;
    }
}