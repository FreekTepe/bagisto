<?php

namespace Webkul\MarketplacePerProductShipping\Database\Seeders;

use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    public function run()
    {
        $attribute = app('Webkul\Attribute\Repositories\AttributeRepository')->create([
            "code" => "shipping_price",
            "type" => "price",
            "admin_name" => "Shipping Price",
            "is_required" => 0,
            "is_unique" => 0,
            "validation" => "",
            "value_per_locale" => 0,
            "value_per_channel" => 1,
            "is_filterable" => 0,
            "is_configurable" => 0,
            "is_visible_on_front" => 0
        ]);

        $attributeFamilies = app('Webkul\Attribute\Repositories\AttributeFamilyRepository')->all();

        foreach ($attributeFamilies as $attributeFamily) {
            $shippingGroup = $attributeFamily->attribute_groups()->where('name', 'Shipping')->first();

            $shippingGroup->custom_attributes()->save($attribute, [ 'position' => $shippingGroup->custom_attributes()->count() + 1 ]);
        }
    }
}