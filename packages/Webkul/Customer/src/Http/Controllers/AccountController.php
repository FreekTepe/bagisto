<?php

namespace Webkul\Customer\Http\Controllers;

use Webkul\Theme\Facades\Themes;

class AccountController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        Themes::set('vinylheaven-seller-portal-theme');

        $this->middleware('customer');

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }
}
