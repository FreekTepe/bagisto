<?php

return [
    /* exclude views from dashboard menu as they are not needed */

    // [
    //     'key' => 'marketplace.dataflow',
    //     'name' => 'bulkupload::app.admin.bulk-upload.bulk-upload-dataflow-profile',
    //     'route' => 'marketplace.bulk-upload.dataflow.index',
    //     'sort' => 9
    // ], [
    //     'key' => 'marketplace.bulkupload',
    //     'name' => 'bulkupload::app.admin.bulk-upload.bulk-upload',
    //     'route' => 'marketplace.bulk-upload.index',
    //     'sort' => 10
    // ]
];
