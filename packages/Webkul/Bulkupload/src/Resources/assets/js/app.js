window.Vue = require("vue");
window.axios = require("axios");

Vue.prototype.$http = axios

window.eventBus = new Vue();

Vue.component("profiler", require("./components/profiler"));

Vue.component("demo", require("./components/demo"));
