@extends('shop::layouts.master')

@section('page_title')
{{ __('bulkupload::app.admin.bulk-upload.bulk-upload') }}
@endsection

@section('content-wrapper')
<div class="account-content">
    @include('shop::customers.account.partials.sidemenu')
    <div class="account-layout">
        <div class="account-head mb-20">
            <span class="account-heading">
                Import New Products
            </span>
        </div>
            <!-- download sample -->
        <accordian :title="'{{ __('bulkupload::app.shop.bulk-upload.download-sample') }}'" :active="true">
            <div slot="body">
                <div class="import-product">
                    <form action="{{ route('shop.download-sample-files') }}" method="post">
                        <div class="account-table-content">
                            @csrf
                            <div class="control-group">
                                <select class="control" id="download-sample" name="download_sample">
                                    <option value="">Please Select</option>
                                    <option value="simple-csv">Sample Simple CSV File</option>
                                    <option value="configurable-csv">Sample Configurable CSV File</option>
                                    <option value="simple-xls">Sample Simple XLS File</option>
                                    <option value="configurable-xls">Sample Configurable XLS File</option>
                                </select>

                                <div class="mt-10">
                                    <button type="submit" class="btn btn-lg btn-primary">
                                        {{ __('bulkupload::app.admin.bulk-upload.download-sample') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </accordian>

            <!-- Import New products -->
        <accordian :title="'{{ __('bulkupload::app.shop.bulk-upload.import-products') }}'" :active="true">
            <div slot="body">
                <div class="import-new-products">
                    <form method="POST" action="{{ route('marketplace.bulk-upload.import-new-products-form-submit') }}" enctype="multipart/form-data">
                        @csrf
                        <?php $familyId = app('request')->input('family') ?>

                        <div class="page-content">
                            <div class="attribute-family">
                                <attributefamily></attributefamily>
                            </div>

                            <div class="control-group {{ $errors->first('file_path') ? 'has-error' :'' }}">
                                <label class="required">{{ __('bulkupload::app.admin.bulk-upload.file') }} </label>

                                <input type="file" class="control" name="file_path" id="file">

                                <span class="control-error">{{ $errors->first('file_path') }}</span>
                            </div>

                            <div class="control-group {{ $errors->first('image_path') ? 'has-error' :'' }}">
                                <label> {{ __('bulkupload::app.admin.bulk-upload.image') }} </label>

                                <input type="file" class="control" name="image_path" id="image">

                                <span class="control-error">{{ $errors->first('image_path') }}</span>
                            </div>
                        </div>

                        <div class="page-action">
                            <button type="submit" class="btn btn-lg btn-primary">
                                Upload
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </accordian>

            <!-- Run Profile -->
        <accordian :title="'{{ __('bulkupload::app.shop.bulk-upload.run-profile') }}'" :active="true">
            <div slot="body">
                <div class="app-profiler">
                    <profiler></profiler>
                </div>
            </div>
        </accordian>


    </div>
</div>
@endsection

@push('scripts')
    <script type="text/x-template" id="attribute-family-template">
        <div>
             <div class="control-group {{ $errors->first('attribute_family_id') ? 'has-error' :'' }}">
                <label for="attribute_family_id" class="required">{{ __('admin::app.catalog.products.familiy') }}</label>

                <select @change="onChange()" v-model="key" class="control" id="attribute_family_id" name="attribute_family_id" {{ $familyId ? 'disabled' : '' }}>
                    <option value="">Please Select</option>
                    @foreach ($families as $family)
                        <option value="{{ $family->id }}" {{ ($familyId == $family->id || old('attribute_family_id') == $family->id) ? 'selected' : '' }}>{{ $family->name }}</option>
                    @endforeach
                </select>

                @if ($familyId)
                    <input type="hidden" name="attribute_family_id" value="{{ $familyId }}"/>
                @endif

                <span class="control-error">{{ $errors->first('attribute_family_id') }}</span>
            </div>

            <div class="control-group {{ $errors->first('data_flow_profile') ? 'has-error' :'' }}">
                <label for="data-flow-profile" class="required">{{ __('bulkupload::app.admin.bulk-upload.data-flow-profile') }}</label>

                <select class="control" id="data-flow-profile" name="data_flow_profile">
                    <option value="">Please Select</option>
                    <option v-for="dataflowprofile,index in dataFlowProfiles" :value="dataflowprofile.id">@{{ dataflowprofile.profile_name }}</option>
                </select>

            <span class="control-error">{{ $errors->first('data_flow_profile') }}</span>

            </div>


        </div>
    </script>

    <script type="text/x-template" id="profiler-template">
        <div class="run-profile">
            <form id="run_profiler">
                @csrf
                <div class="control-group">
                    <label for="export-product-type">Select File</label>

                    <select class="control" id="data-flow-profile" @change="detectProfile" v-model="data_flow_profile" name="data_flow_profile">
                        <option>Please Select</option>
                        @if (isset($profiles))
                            @foreach ($profiles as $profile)
                                @foreach ($profile as $getProfileToExecute)
                                    <option value="{{ $getProfileToExecute->id }}">
                                        {{ $getProfileToExecute->profile_name }}
                                    </option>
                                @endforeach
                            @endforeach
                        @endif
                    </select>

                    <div class="page-action">
                        <button type="submit" :class="{ disabled: isDisabled }" :disabled="isDisabled" @click.prevent="runProfiler" class="btn btn-lg btn-primary mt-10">
                            Run
                        </button>
                    </div>
                </div>
            </form>

            <div class="uploading-records" v-if="this.product.totalCSVRecords">
                <uploadingrecords :percentCount="percent" :uploadedProducts="product.countOfImportedProduct" :errorProduct="product.error" :totalRecords="product.totalCSVRecords" :countOfError="product.countOfError" :remainData="product.remainDataInCSV"></uploadingrecords>
            </div>
        </div>
    </script>

    <script type="text/x-template" id="uploadingRecords-template">
        <ul>
            <li>
                <i class="icon check-accent"></i>
                <span>Started profile execution, please wait...</span>
            </li>

            <li v-if="this.countOfError > '0'">
                <i class="icon cross-accent"></i>
                <span>Number of errors while product uploading:  @{{this.countOfError}}</span>
            </li>

            <li v-if="this.countOfError > '0'">
                <i class="icon cross-accent"></i>
                <span>
                    Error while product uploading:
                   <label v-for= "error in this.errorProduct" style="display: inline-block; width: 100%; margin-left: 50px;">
                        <i class="icon icon-crossed"></i>
                        @{{ error}}
                    </label>
                </span>
            </li>

            <li>
                <i class="icon check-accent"></i>
                <span>'Warning: Please do not close the window during importing data'</span>

                <progress v-if="this.remainData > '0'" :value="percentCount" max="100">
                </progress>
                <progress v-else :value="100" max="100"></progress>

                <span style="vertical-align: 75%;" v-if="this.remainData > '0'"> @{{ this.percentCount}}%</span>
                <span style="vertical-align: 75%;" v-else>100%</span>
            </li>

            <li>
                <i class="icon check-accent"></i>
                <span> @{{this.uploadedProducts}}/@{{this.totalRecords}} Products Uploaded</span>
            </li>

            <li v-if="this.remainData == '0'">
                <i class="icon finish-icon"></i>
                <span>Finished Profile Execution </span>
            </li>
        </ul>
    </script>

    <script>
        Vue.component('attributefamily', {
            template: '#attribute-family-template',
            data: function() {
                return {
                    key: "",
                    dataFlowProfiles: [],
                }
            },

            mounted: function() {
            },

            methods:{
                onChange: function() {
                    this_this = this;

                    var uri = "{{ route('marketplace.bulk-upload-shop.get-data-profile') }}"

                    this_this.$http.post(uri, {
                        attribute_family_id: this_this.key
                    })
                    .then(response => {
                        this_this.dataFlowProfiles = response.data.dataFlowProfiles;
                    })

                    .catch(function(error) {
                        console.log(error);
                    });
                }
            }
        })

        Vue.component('profiler', {
            template:'#profiler-template',

            data: function() {
                return {
                    data_flow_profile: '',
                    percent: 0,
                    product: {
                        countOfImportedProduct : 0,
                        countOfStartedProfiles : 0,
                        fetchedRecords : 10,
                        numberOfTimeInitiateProfilerCalled: 0,
                        totalCSVRecords:'',
                        dataArray:[],
                        error: [],
                        countOfError: 0,
                        remainDataInCSV: 1
                    },
                }
            },

            mounted() {
            },

            computed: {
                isDisabled () {
                    if (this.data_flow_profile == '' || this.data_flow_profile == 'Please Select') {
                        return true;
                    } else {
                        return false;
                    }
                }
            },

            methods:{
                detectProfile: function() {
                },

                runProfiler: function(e) {
                    event.target.disabled = true;

                    const uri = "{{ route('marketplace.bulk-upload-shop.read-csv') }}"

                    this.$http.post(uri, {
                        data_flow_profile_id: this.data_flow_profile
                    })
                    .then((result) => {
                        totalRecords = result.data;

                        this.product.totalCSVRecords = this.product.remainDataInCSV = totalRecords;

                        if (totalRecords > this.product.countOfStartedProfiles) {
                            this.initiateProfiler(totalRecords);
                        } else {
                            window.flashMessages = [{
                                'type': 'alert-error',
                                'message': result.data.message
                            }];

                            this.$root.addFlashMessages()
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },

                initiateProfiler: function(totalRecords) {

                    const url = "{{ route('marketplace.bulk-upload-shop.run-profile') }}"

                    this.$http.post(url, {
                        data_flow_profile_id: this.data_flow_profile,
                        numberOfCSVRecord: totalRecords,
                        countOfStartedProfiles: this.product.countOfStartedProfiles,
                        totalNumberOfCSVRecord: this.product.totalCSVRecords,
                        productUploaded: this.product.countOfImportedProduct,
                        errorCount: this.product.countOfError
                    })
                    .then((result) => {
                        this.data = result.data;

                        if (this.data.error) {
                            if (typeof(this.data.error) == "object") {
                                for (const [key, value] of Object.entries(this.data.error)) {
                                    this.product.error.push(value);
                                }
                            } else {
                                this.product.error.push(this.data.error);
                            }

                            this.product.countOfError++;
                        }

                        this.product.countOfImportedProduct = this.data.productsUploaded;
                        this.product.remainDataInCSV = this.data.remainDataInCSV;
                        this.product.countOfStartedProfiles = this.data.countOfStartedProfiles;

                        this.calculateProgress(result.data);
                    })
                    .catch(function(error) {
                    });
                },

                calculateProgress(result) {
                    finish = this.product.countOfImportedProduct;
                    progressPercent = parseInt((this.product.countOfImportedProduct/
                    this.product.totalCSVRecords)*100);

                    this.percent = progressPercent;

                    if (result.remainDataInCSV > 0) {
                        this.initiateProfiler(result.remainDataInCSV);
                    } else {
                        this.finishProfiler(this.percent);
                    }
                },

                errorCount: function(count) {
                    return console.count(this.product.error);
                },

                finishProfiler(percent) {
                }
            },
        })

        Vue.component('uploadingrecords', {
            template:'#uploadingRecords-template',
            props: ['percentCount', 'uploadedProducts','errorProduct','totalRecords', 'countOfError', 'remainData'],
            data: function() {
                return {
                    percentage: this.percentCount,
                }
            },
        })
    </script>
@endpush