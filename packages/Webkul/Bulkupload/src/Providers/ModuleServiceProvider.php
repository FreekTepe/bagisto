<?php

namespace Webkul\Bulkupload\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Webkul\Bulkupload\Models\ImportNewProducts::class,
        \Webkul\Bulkupload\Models\ImportNewProductsByAdmin::class,
        \Webkul\Bulkupload\Models\DataFlowProfile::class,
        \Webkul\Bulkupload\Models\DataFlowProfileAdmin::class,
        \Webkul\Bulkupload\Models\MarketplaceProduct::class,
    ];
}