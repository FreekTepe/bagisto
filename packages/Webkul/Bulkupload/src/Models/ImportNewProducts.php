<?php

namespace Webkul\Bulkupload\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Bulkupload\Contracts\ImportNewProducts as ImportNewProductsContract;

class ImportNewProducts extends Model implements ImportNewProductsContract
{
    //
    protected $table = "marketplace_import_new_products";

    protected $guarded = array();

    // protected $fillable = [''];

}
