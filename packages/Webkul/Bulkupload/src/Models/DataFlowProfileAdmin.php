<?php

namespace Webkul\Bulkupload\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Bulkupload\Contracts\DataFlowProfileAdmin as DataFlowProfileAdminContract;
use Webkul\Attribute\Models\AttributeFamily;


class DataFlowProfileAdmin extends Model implements DataFlowProfileAdminContract
{
    protected $guarded = array();

    protected $table = "marketplace_bulkupload_admin_dataflowprofile";

    public function attribute_family()
    {
        return $this->hasOne(AttributeFamily::class);
    }
}
