<?php

namespace Webkul\Bulkupload\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Webkul\Bulkupload\Models\DataFlowProfileAdmin;
use Webkul\Bulkupload\Models\DataFlowProfile;
use Webkul\Admin\Imports\DataGridImport;
use Webkul\Product\Repositories\ProductFlatRepository;
use Illuminate\Database\Eloquent;
use Webkul\Product\Http\Requests\ProductForm;
use Webkul\Attribute\Repositories\AttributeRepository;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Product\Repositories\ProductImageRepository;
use Webkul\Product\Repositories\ProductAttributeValueRepository;
use Webkul\Product\Models\ProductAttributeValue;
use Webkul\Bulkupload\Repositories\BulkProductRepository;
use Webkul\User\Repositories\AdminRepository;
use Webkul\Attribute\Repositories\AttributeOptionRepository;
use Webkul\Product\Repositories\ProductInventoryRepository;
use Webkul\Bulkupload\Repositories\ImportNewProductsByAdminRepository as ImportNewProducts;
use Webkul\Attribute\Repositories\AttributeFamilyRepository as AttributeFamily;
use Webkul\Bulkupload\Repositories\ProductImageRepository as BulkUploadImages;
use Webkul\Bulkupload\Repositories\MarketplaceProductRepository;
use Webkul\Marketplace\Repositories\ProductRepository as SellerProduct;
use Illuminate\Support\Facades\Event;
use Webkul\Category\Repositories\CategoryRepository;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Bulkupload\Repositories\ImportNewProductsByAdminRepository;
use Webkul\Bulkupload\Repositories\DataFlowProfileRepository as AdminDataFlowProfileRepository;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Validators\Failure;

use Storage;
use Exception;
use File;
use DB;

class BulkUploadController extends Controller
{
    protected $attributeFamily;

    protected $product = [];

    protected $productRepository;

    protected $productImage;

    protected $_config;

    protected $attribute;

    protected $bulkUploadImages;

    protected $sellerRepository;

    protected $bulkProductRepository;

    protected $adminRepository;

    /**
     * ProductFlatRepository Repository Object
     *
     * @var object
     */
    protected $productFlatRepository;

    /**
     * ProductAttributeValueRepository Repository Object
     *
     * @var object
     */
    protected $productAttributeValueRepository;

    protected $productInventoryRepository;

      /**
     * AttributeOptionRepository Repository Object
     *
     * @var object
     */
    protected $attributeOptionRepository;

     /**
     * Marketplace ProductRepository object
     *
     * @var array
     */
    protected $sellerProduct;

    /**
     * CategoryRepository object
     *
     * @var array
     */
    protected $categoryRepository;

    /**
     * dataFlowProfileAdmin object
     *
     * @var array
     */
    protected $dataFlowProfileAdmin;

    /**
     * adminDataFlowProfileRepository object
     *
     * @var array
     */
    protected $adminDataFlowProfileRepository;

    protected $marketplaceProductRepository;

    protected $importNewProductsByAdminRepository;

    public function __construct(
        AttributeFamily $attributeFamily,
        DataFlowProfile $dataFlowProfile,
        DataFlowProfileAdmin $dataFlowProfileAdmin,
        ImportNewProducts $importNewProducts,
        ProductRepository $productRepository,
        ProductImageRepository $productImage,
        AttributeRepository $attribute,
        ProductFlatRepository $productFlatRepository,
        ProductAttributeValueRepository $productAttributeValueRepository,
        AttributeOptionRepository $attributeOptionRepository,
        BulkUploadImages $bulkUploadImages,
        SellerProduct $sellerProduct,
        CategoryRepository $categoryRepository,
        AdminDataFlowProfileRepository $adminDataFlowProfileRepository,
        SellerRepository $sellerRepository,
        MarketplaceProductRepository $marketplaceProductRepository,
        ImportNewProductsByAdminRepository $importNewProductsByAdminRepository,
        BulkProductRepository $bulkProductRepository,
        AdminRepository $adminRepository,
        ProductInventoryRepository $productInventoryRepository
        )
    {
        $this->attributeFamily = $attributeFamily;

        $this->adminRepository = $adminRepository;

        $this->productInventoryRepository = $productInventoryRepository;

        $this->dataFlowProfile = $dataFlowProfile;

        $this->dataFlowProfileAdmin = $dataFlowProfileAdmin;

        $this->importNewProducts = $importNewProducts;

        $this->bulkProductRepository = $bulkProductRepository;

        $this->productRepository = $productRepository;

        $this->productImage = $productImage;

        $this->attribute = $attribute;

        $this->productFlatRepository = $productFlatRepository;

        $this->productAttributeValueRepository = $productAttributeValueRepository;

        $this->importNewProductsByAdminRepository = $importNewProductsByAdminRepository;

        $this->attributeOptionRepository = $attributeOptionRepository;

        $this->bulkUploadImages = $bulkUploadImages;

        $this->sellerProduct = $sellerProduct;

        $this->categoryRepository = $categoryRepository;

        $this->sellerRepository = $sellerRepository;

        $this->adminDataFlowProfileRepository = $adminDataFlowProfileRepository;

        $this->marketplaceProductRepository = $marketplaceProductRepository;

        $this->_config = request('_config');
    }

    public function index()
    {
        $profiles = null;
        $families = $this->attributeFamily->all();
        $allProfiles = $this->importNewProductsByAdminRepository->get()->toArray();
        $adminRecords = $this->adminRepository->get()->toArray();
        $configurableFamily = null;

        if (! empty($allProfiles) ) {
            foreach ($allProfiles as $allProfile)
            {
                $profilers[] = $allProfile['data_flow_profile_id'];
            }

            foreach($profilers as $key => $profiler)
            {
                $profiles[] = $this->adminDataFlowProfileRepository->findByfield(['id' => $profilers[$key], 'run_status' => '0']);
            }
        }

        $sellerData = DB::table('marketplace_sellers')
            ->leftJoin('customers', 'marketplace_sellers.customer_id', '=', 'customers.id')
            ->select('marketplace_sellers.customer_id', DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as customer_name'))->get();

        if ($familyId = request()->get('attribute_family_id')) {
            $configurableFamily = $this->attributeFamily->find($familyId);
        }

        return view($this->_config['view'], compact('families', 'profiles', 'configurableFamily', 'sellerData', 'adminRecords'));
    }

    public function getDataProfilesToExecute()
    {
        $profiler = [];
        $families = $this->attributeFamily->all();
        $allProfiles = $this->importNewProductsByAdminRepository->all();

        foreach($allProfiles as $allProfile)
        {
            $profiler = $allProfile->data_flow_profile_id;
        }

        $configurableFamily = null;

        $sellerData = DB::table('marketplace_sellers')
            ->leftJoin('customers', 'marketplace_sellers.customer_id', '=', 'customers.id')
            ->select('marketplace_sellers.customer_id', DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as customer_name'))->get();

        if ($familyId = request()->get('attribute_family_id')) {
            $configurableFamily = $this->attributeFamily->find($familyId);
        }

        return view($this->_config['view'], compact('families', 'profiles', 'configurableFamily', 'sellerData'));
    }

    public function downloadFile()
    {
        if (request()->download_sample == 'simple-csv') {
            return response()->download(public_path('storage/downloads/sample-files/mpbulksimpleproductupload.csv'));
        } else if (request()->download_sample == 'configurable-csv') {
            return response()->download(public_path('storage/downloads/sample-files/mpbulkconfigurableproductupload.csv'));
        } else if (request()->download_sample == 'simple-xls') {
            return response()->download(public_path('storage/downloads/sample-files/mpbulksimpleproductupload.xls'));
        } else if (request()->download_sample == 'configurable-xls') {
            return response()->download(public_path('storage/downloads/sample-files/mpbulkconfigurableproductupload.xls'));
        } else {
            return redirect()->back();
        }
    }

    public function getAllDataFlowProfiles()
    {
        $loggedInSeller = $this->sellerRepository->findOneByField('id', request()->seller_id);

        $attribute_family_id = request()->attribute_family_id;

        if (request()->seller_id == '0') {
            $dataFlowProfiles = $this->adminDataFlowProfileRepository->findWhere(['seller_id' => 'admin'])->where('attribute_family_id', '=', request()->attribute_family_id);
        } else {
            $dataFlowProfiles = $this->adminDataFlowProfileRepository->findWhere(['seller_id' => $loggedInSeller->id])->where('attribute_family_id', '=', request()->attribute_family_id);
        }

        return ['dataFlowProfiles' => $dataFlowProfiles];
    }

    public function importNewProductsStore(Request $request)
    {
        $seller = $request->set_seller;
        $attribute_family_id = $request->attribute_family;
        $data_flow_profile_id = $request->data_flow_profile;

        $valid_extension = ['csv', 'xls'];
        $valid_image_extension = ['zip', 'rar'];

        $request->validate([
            'set_seller' => 'required',
            'attribute_family' => 'required',
            'file_path' => 'required',
            'data_flow_profile' => 'required',
        ]);

        $imageDir = 'imported-products/admin/images';
        $fileDir = 'imported-products/admin/files';

        $image = $request->file('image_path');
        $file = $request->file('file_path');

        if (! isset($image)) {
            $image = '';
        }

        $product['customer_id'] = $seller;
        $product['data_flow_profile_id'] = $data_flow_profile_id;
        $product['attribute_family_id'] = $attribute_family_id;

        if (( !empty($image) && in_array($image->getClientOriginalExtension(), $valid_image_extension)) && (in_array($file->getClientOriginalExtension(), $valid_extension))) {
            $uploadedImage = $image->storeAs($imageDir, uniqid().'.'.$image->getClientOriginalExtension());

            $product['image_path'] = $uploadedImage;

            $uploadedFile = $file->storeAs($fileDir, uniqid().'.'.$file->getClientOriginalExtension());

            $product['file_path'] = $uploadedFile;
        } else if (empty($image) && (in_array($file->getClientOriginalExtension(), $valid_extension))) {
            $product['image_path'] = '';

            $uploadedFile = $file->storeAs($fileDir, uniqid().'.'.$file->getClientOriginalExtension());

            $product['file_path'] = $uploadedFile;
        } else {
            session()->flash('error', trans('bulkupload::app.shop.message.file-format-error'));

            return redirect()->route('admin.marketplace.bulk-upload.index');
        }

        if ($data_flow_profile_id) {
            $data = $this->importNewProducts->findOneByField('data_flow_profile_id', $data_flow_profile_id);

            if ($data) {
                $this->adminDataFlowProfileRepository->update(["run_status" => "0"], $data_flow_profile_id);

                $this->importNewProducts->Update($product, $data->id);

                Session()->flash('success',trans('bulkupload::app.shop.profile.edit-success'));

                return redirect()->route('admin.marketplace.bulk-upload.index');
            } else {
                $importNewProductsStore = $this->importNewProducts->create($product);

                Session()->flash('success',trans('bulkupload::app.shop.profile.success'));

                return redirect()->route('admin.marketplace.bulk-upload.index');
            }
        } else {
            session()->flash('error', trans('bulkupload::app.shop.message.data-profile-not-selected'));

            return back();
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'profile_name' => 'required|unique:marketplace_bulkupload_dataflowprofile',
            'attribute_family' => 'required'
        ]);

        $dataFlowProfileAdmin = new DataFlowProfile();
        $dataFlowProfileAdmin->profile_name = $request->profile_name;
        $dataFlowProfileAdmin->attribute_family_id = $request->attribute_family;
        $dataFlowProfileAdmin->seller_id = "admin";
        $dataFlowProfileAdmin->save();

        Session()->flash('success',trans('bulkupload::app.shop.profile.success'));

        return redirect()->route('admin.marketplace.dataflow-profile.index');
    }

    public function runProfile(Request $request)
    {
        $data_flow_profile_id = request()->data_flow_profile_id;
        $numberOfCSVRecord = request()->numberOfCSVRecord;
        $countOfStartedProfiles = request()->countOfStartedProfiles;
        $totalNumberOfCSVRecord = request()->totalNumberOfCSVRecord;
        $productUploaded = request()->productUploaded;
        $errorCount = request()->errorCount;
        $product = array();
        $dataToBeReturn = array();
        $categoryID = array();
        $error = null;
        $productSample = null;
        $categoryError = null;
        $imageZipName = null;
        $ids = [];
        $urlKeyOfCSV = "";
        $CSVRecordErrors = [];
        $urlKeyValidation = null;
        $listOfUrlKey = [];
        // static $duplicate = [];

        if ($totalNumberOfCSVRecord < 1000) {
            $processCSVRecords = $totalNumberOfCSVRecord/($totalNumberOfCSVRecord/10);
        } else {
            $processCSVRecords = $totalNumberOfCSVRecord/($totalNumberOfCSVRecord/100);
        }

        $dataFlowProfileRecord = $this->importNewProducts->findOneByField
        ('data_flow_profile_id', $data_flow_profile_id);

        if ($dataFlowProfileRecord) {

            $csvData = (new DataGridImport)->toArray($dataFlowProfileRecord->file_path)[0];

            if (isset($dataFlowProfileRecord->image_path) && ($dataFlowProfileRecord->image_path != "") ) {
                $imageZip = new \ZipArchive();

                $extractedPath = storage_path('app/public/imported-products/extracted-images/admin/'.$dataFlowProfileRecord->id.'/');


                if ($imageZip->open(storage_path('app/public/'.$dataFlowProfileRecord->image_path))) {
                    for ($i = 0; $i < $imageZip->numFiles; $i++) {
                        $filename = $imageZip->getNameIndex($i);
                        $imageZipName = pathinfo($filename);
                    }
                    $imageZip->extractTo($extractedPath);
                    $imageZip->close();
                }
            }

            foreach ($csvData as $key => $value)
            {
                if ($numberOfCSVRecord >= 0) {
                    for ($i = $countOfStartedProfiles; $i < count($csvData); $i++)
                    {
                        $product['loopCount'] = $i;

                        if ($csvData[$i]['type'] == 'configurable') {
                            try {
                                $csvData = (new DataGridImport)->toArray($dataFlowProfileRecord->file_path)[0];

                                $categoryData = explode(',', $csvData[$i]['categories_slug']);

                                foreach ($categoryData as $key => $value)
                                {
                                    $categoryID[$key] = $this->categoryRepository->findBySlugOrFail($categoryData[$key])->id;
                                }

                                unset($data);

                                $productFlatData = $this->productFlatRepository->findWhere(['sku' => $csvData[$i]['sku'], 'url_key' => $csvData[$i]['url_key']])->first();

                                $productData = $this->productRepository->findWhere(['sku' => $csvData[$i]['sku']])->first();

                                $attributeFamilyData = $this->attributeFamily->findOneByfield(['name' => $csvData[$i]['attribute_family_name']]);

                                if (! isset($productFlatData) && empty($productFlatData)) {
                                    $data['type'] = $csvData[$i]['type'];
                                    $data['attribute_family_id'] = $attributeFamilyData->id;
                                    $data['sku'] = $csvData[$i]['sku'];
                                   $product = $this->productRepository->create($data);
                                } else {
                                    $product = $productData;
                                }

                                $marketplace = $this->importNewProducts->findOneByField('data_flow_profile_id', $data_flow_profile_id);

                                if (($marketplace->customer_id >= 1) && ! isset($productFlatData) && empty($productFlatData) ) {
                                    $sellerProduct = [
                                        'product_id' => $product->id,
                                        'is_owner' => 1,
                                        'marketplace_seller_id' => $marketplace->customer_id
                                    ];

                                    $this->marketplaceProductRepository->create($sellerProduct);
                                }

                                unset($data);

                                //necessary attributes for configurable product

                                $data['vendor_id'] = $marketplace->customer_id;
                                $data['categories'] = $categoryID;
                                $data['name'] = $csvData[$i]['name'];
                                $data['sku'] = (string)$csvData[$i]['sku'];
                                $data['description'] = $csvData[$i]['description'];
                                $data['dataFlowProfileRecordId'] = $dataFlowProfileRecord->id;
                                $data['url_key'] = $csvData[$i]['url_key'];
                                $data['channel'] = core()->getCurrentChannel()->code;
                                $data['locale'] = core()->getCurrentLocale()->code;
                                $data['new'] = (string)$csvData[$i]['new'];
                                $data['price'] = (string)$csvData[$i]['price'];
                                $data['meta_title'] = (string)$csvData[$i]['meta_title'];
                                $data['meta_keywords'] = (string)$csvData[$i]['meta_keyword'];
                                $data['meta_description'] = (string)$csvData[$i]['meta_description'];
                                $data['featured'] = (string)$csvData[$i]['featured'];
                                $data['visible_individually'] = (string)$csvData[$i]['visible_individually'];
                                $data['tax_category_id'] = (string)$csvData[$i]['tax_category_id'];
                                $data['status'] = (string)$csvData[$i]['status'];
                                $data['weight'] = (string)$csvData[$i]['weight'];
                                $data['attribute_family_id'] = $attributeFamilyData->id;
                                $data['short_description'] = (string)$csvData[$i]['short_description'];
                                $data['special_price'] = (string)$csvData[$i]['special_price'];
                                $data['special_price_from'] = (string)$csvData[$i]['special_price_from'];
                                $data['special_price_to'] = (string)$csvData[$i]['special_price_to'];
                                //Product Images
                                $individualProductimages = explode(',', $csvData[$i]['images']);

                                if (isset($imageZipName)) {
                                    $images = Storage::disk('local')->files('public/imported-products/extracted-images/admin/'.$dataFlowProfileRecord->id.'/'.$imageZipName['dirname'].'/');

                                    foreach ($images as $imageArraykey => $imagePath)
                                    {
                                        $imageName = explode('/', $imagePath);

                                        if (in_array(last($imageName), $individualProductimages)) {
                                            $data['images'][$imageArraykey] = $imagePath;
                                        }
                                    }
                                }

                                $returnRules = $this->validateCSV($data_flow_profile_id, $csvData, $dataFlowProfileRecord, $product->id);

                                $validationCheckForUpdateData = $this->productFlatRepository->findByField(['sku' => $csvData[$i]['sku'], 'url_key' => $csvData[$i]['url_key']]);

                                if (($validationCheckForUpdateData->count() < 1) || (! isset($validationCheckForUpdateData) && empty($validationCheckForUpdateData))) {
                                    $urlKeyUniqueness = "unique:product_flat,url_key";
                                    $dateFormat = 'date_format:"Y-m-d"';

                                    array_push($returnRules["url_key"], $urlKeyUniqueness);
                                    array_push($returnRules["special_price_to"], $dateFormat);
                                    array_push($returnRules["special_price_from"], $dateFormat);
                                }

                                $csvValidator = Validator::make($data, $returnRules);

                                if ($csvValidator->fails()) {
                                    $errors = $csvValidator->errors()->getMessages();

                                    $this->deleteProductIfNotValidated($product->id);

                                    foreach($errors as $key => $error)
                                    {
                                        if ($error[0] == "The url key has already been taken.") {
                                            $errorToBeReturn[] = "The url key " . $data['url_key'] . " has already been taken";
                                        } else {
                                            $errorToBeReturn[] = $error[0]. " for sku " . $data['sku'];
                                        }
                                    }

                                    $productUploadedWithError = $productUploaded + 1;
                                    $countOfStartedProfiles = $i + 1;

                                    if ($numberOfCSVRecord != 0) {
                                        $remainDataInCSV = $totalNumberOfCSVRecord - $productUploadedWithError;
                                    } else {
                                        $remainDataInCSV = 0;
                                    }

                                    $dataToBeReturn = array(
                                        'remainDataInCSV' => $remainDataInCSV,
                                        'productsUploaded' => $productUploaded,
                                        'countOfStartedProfiles' => $countOfStartedProfiles,
                                        'error' => $errorToBeReturn,
                                    );
                                    return response()->json($dataToBeReturn);
                                }

                                $productAttributeStore = $this->bulkProductRepository->productRepositoryUpdateForVariants($data, $product->id);

                                if (isset($imageZipName)) {
                                    $this->bulkUploadImages->bulkuploadImages($data, $product, $imageZipName);
                                }

                                if (! isset($productFlatData) && empty($productFlatData)) {
                                    $productFlatData = DB::table('product_flat')->select('id')->where('locale', app()->getLocale())->orderBy('id', 'desc')->first();
                                }

                                $product['productFlatId'] = $productFlatData->id;

                                $arr[] = $productFlatData->id;
                            } catch (\Exception $e) {
                                $categoryError = explode('[' ,$e->getMessage());
                                $categorySlugError = explode(']' ,$e->getMessage());

                                $error = $e;

                                $productUploadedWithError = $productUploaded + 1;
                                $remainDataInCSV = $totalNumberOfCSVRecord - $productUploadedWithError;
                                $countOfStartedProfiles = $i + 1;

                                if ($categoryError[0] == "No query results for model ") {
                                    $dataToBeReturn = array(
                                        'remainDataInCSV' => $remainDataInCSV,
                                        'productsUploaded' => $productUploaded,
                                        'countOfStartedProfiles' => $countOfStartedProfiles,
                                        'error' => "Invalid Category Slug: " . $categorySlugError[1],
                                    );
                                    $categoryError[0] = null;
                                } else if (isset($e->errorInfo)) {
                                    $dataToBeReturn = array(
                                        'remainDataInCSV' => $remainDataInCSV,
                                        'productsUploaded' => $productUploaded,
                                        'countOfStartedProfiles' => $countOfStartedProfiles,
                                        'error' => $e->errorInfo[2],
                                    );
                                } else {
                                    $dataToBeReturn = array(
                                        'remainDataInCSV' => $remainDataInCSV,
                                        'productsUploaded' => $productUploaded,
                                        'countOfStartedProfiles' => $countOfStartedProfiles,
                                        'error' => $e->getMessage(),
                                    );
                                }
                                return response()->json($dataToBeReturn);
                            }
                        } else if (isset($product['productFlatId'])) {
                            try {
                                $current = $product['loopCount'];
                                $num = 0;
                                $inventory = [];
                                $attributeOptionColor = null;
                                $attributeOptionSize = null;

                                $csvData = (new DataGridImport)->toArray($dataFlowProfileRecord->file_path)[0];

                                for ($i = $current; $i < count($csvData); $i++)
                                {
                                    $product['loopCount'] = $i;

                                    if ($csvData[$i]['type'] != 'configurable') {
                                        unset($data);

                                        $productFlatData = $this->productFlatRepository->findWhere(['sku' => $csvData[$i]['sku'], 'url_key' => null])->first();

                                        $productData = $this->productRepository->findWhere(['sku' => $csvData[$i]['sku']])->first();

                                        $attributeFamilyData = $this->attributeFamily->findOneByfield(['name' => $csvData[$i]['attribute_family_name']]);

                                        if (! isset($productFlatData) && empty($productFlatData)) {
                                            $data['parent_id'] = $product->id;
                                            $data['type'] = $csvData[$i]['type'];
                                            $data['attribute_family_id'] = $attributeFamilyData->id;
                                            $data['sku'] = $csvData[$i]['sku'];

                                            $configSimpleproduct = $this->productRepository->create($data);
                                        } else {
                                            $configSimpleproduct = $productData;
                                        }

                                        $marketplace = $this->importNewProducts->findOneByField('data_flow_profile_id', $data_flow_profile_id);

                                        if (($marketplace->customer_id >= 1) && ! isset($productFlatData) && empty($productFlatData) ) {
                                            $sellerProduct = [
                                                'product_id' => $configSimpleproduct->id,
                                                'is_owner' => 1,
                                                'marketplace_seller_id' => $marketplace->customer_id
                                            ];

                                            $this->marketplaceProductRepository->create($sellerProduct);
                                        }

                                        unset($data);
                                        $validateVariant = Validator::make($csvData[$i], [
                                            'sku' => ['required', 'unique:products,sku,' . $configSimpleproduct->id, new \Webkul\Core\Contracts\Validations\Slug],
                                            'name' => 'required',
                                            'super_attribute_price' => 'required|decimal',
                                            'super_attribute_weight' => 'required|decimal',
                                            'super_attribute_option' => 'required',
                                            'super_attributes' => 'required'
                                        ]);

                                        if ($validateVariant->fails()) {
                                            $errors = $validateVariant->errors()->getMessages();
                                            $this->deleteProductIfNotValidated($product->id);

                                            foreach($errors as $key => $error)
                                            {
                                                $errorToBeReturn[] = $error[0]. " for sku " .$csvData[$i]['sku'];
                                            }
                                            $productUploadedWithError = $productUploaded + 1;

                                            $countOfStartedProfiles = $i + 1;

                                            if ($numberOfCSVRecord != 0) {
                                                $remainDataInCSV = $totalNumberOfCSVRecord - $productUploadedWithError;
                                            } else {
                                                $remainDataInCSV = 0;
                                            }

                                            $dataToBeReturn = array(
                                                'remainDataInCSV' => $remainDataInCSV,
                                                'productsUploaded' => $productUploaded,
                                                'countOfStartedProfiles' => $countOfStartedProfiles,
                                                'error' => $errorToBeReturn,
                                            );
                                            return response()->json($dataToBeReturn);
                                        }
                                        $inventory_data = core()->getCurrentChannel()->inventory_sources;

                                        foreach($inventory_data as $key => $datas)
                                        {
                                            $inventoryId = $datas->id;
                                        }

                                        $inventoryData[] = (string)$csvData[$i]['super_attribute_qty'];

                                        foreach ($inventoryData as $key => $d)
                                        {
                                            $inventory[$inventoryId] = $d;
                                        }

                                        $productInventory = $this->productInventoryRepository->findOneByField(['vendor_id' => $marketplace->customer_id, 'product_id' => $configSimpleproduct->id]);

                                        if (! isset($productInventory) && empty($productInventory) || $productInventory->count() < 1) {
                                            $data['inventories'] =  $inventory;
                                        }

                                        $superAttributes = explode(',', $csvData[$i]['super_attributes']);
                                        $superAttributesOption = explode(',', $csvData[$i]['super_attribute_option']);

                                        $data['super_attributes'] = array_combine($superAttributes, $superAttributesOption);

                                        if (isset($data['super_attributes']) && $i == $current) {
                                            $super_attributes = [];

                                            foreach ($data['super_attributes'] as $attributeCode => $attributeOptions) {
                                                $attribute = $this->attribute->findOneByField('code', $attributeCode);

                                                $super_attributes[$attribute->id] = $attributeOptions;

                                                $users = $product->super_attributes()->where('id', $attribute->id)->exists();

                                                if (! $users) {
                                                    $product->super_attributes()->attach($attribute->id);
                                                }
                                            }
                                        }

                                        $data['vendor_id'] = $marketplace->customer_id;
                                        $data['channel'] = core()->getCurrentChannel()->code;
                                        $data['locale'] = core()->getCurrentLocale()->code;
                                        $data['dataFlowProfileRecordId'] = $dataFlowProfileRecord->id;
                                        $data['price'] = (string)$csvData[$i]['super_attribute_price'];
                                        $data['special_price'] = (string)$csvData[$i]['special_price'];
                                        $data['special_price_from'] = (string)$csvData[$i]['special_price_from'];
                                        $data['special_price_to'] = (string)$csvData[$i]['special_price_to'];
                                        // $data['new'] = (string)$csvData[$i]['new'];
                                        // $data['featured'] = (string)$csvData[$i]['featured'];
                                        // $data['visible_individually'] = (string)$csvData[$i]['visible_individually'];
                                        $data['tax_category_id'] = (string)$csvData[$i]['tax_category_id'];
                                        $data['cost'] = (string)$csvData[$i]['cost'];
                                        $data['width'] = (string)$csvData[$i]['width'];
                                        $data['height'] = (string)$csvData[$i]['height'];
                                        $data['depth'] = (string)$csvData[$i]['depth'];
                                        $data['status'] = (string)$csvData[$i]['status'];
                                        $data['attribute_family_id'] = $attributeFamilyData->id;
                                        $data['short_description'] = (string)$csvData[$i]['short_description'];
                                        $data['description'] = (string)$csvData[$i]['description'];
                                        $data['sku'] = (string)$csvData[$i]['sku'];
                                        $data['name'] = (string)$csvData[$i]['name'];
                                        $data['weight'] = (string)$csvData[$i]['super_attribute_weight'];
                                        $data['status'] = (string)$csvData[$i]['status'];
                                        // $data['url_key'] = (string)$csvData[$i]['url_key'];

                                        $attributeOptionColor = $this->attributeOptionRepository->findOneByField('admin_name', $data['super_attributes']['color']);
                                        $attributeOptionSize = $this->attributeOptionRepository->findOneByField('admin_name', $data['super_attributes']['size']);

                                        if ($attributeOptionColor == null || $attributeOptionSize == null) {
                                            $data['color'] = NULL;
                                            $data['size'] = NULL;
                                        } else {
                                            $data['color'] = $attributeOptionColor->id;
                                            $data['size'] = $attributeOptionSize->id;
                                        }

                                        // if ( $data['new'] == 1 || $data['featured'] == 1 || $data['visible_individually'] == 1 ) {
                                        //     if ($data['short_description'] == "" && $data['description'] == "" &&$data['url_key'] == "") {
                                        //         $data['new'] = 0;
                                        //         $data['featured'] = 0;
                                        //         $data['visible_individually'] = 0;
                                        //     }
                                        // }

                                        $configSimpleProductAttributeStore = $this->bulkProductRepository->productRepositoryUpdateForVariants($data, $configSimpleproduct->id);

                                        $configSimpleProductAttributeStore['parent_id'] = $product['productFlatId'];

                                        $this->createFlat($configSimpleProductAttributeStore);

                                    } else {
                                        $savedProduct = $productUploaded + 1;
                                        $remainDataInCSV = $totalNumberOfCSVRecord - $savedProduct;
                                        $productsUploaded = $savedProduct;

                                        $countOfStartedProfiles = $product['loopCount'];

                                        $dataToBeReturn = array(
                                            'remainDataInCSV' => $remainDataInCSV,
                                            'productsUploaded' => $productsUploaded,
                                            'countOfStartedProfiles' => $countOfStartedProfiles
                                        );

                                        return response()->json($dataToBeReturn);
                                    }
                                }

                                if ($errorCount == 0) {
                                    // $this->adminDataFlowProfileRepository->findOneByField('id', $data_flow_profile_id)->delete();

                                    $dataToBeReturn = [
                                        'remainDataInCSV' => 0,
                                        'productsUploaded' => $totalNumberOfCSVRecord,
                                        'countOfStartedProfiles' => count($csvData),
                                    ];

                                    return response()->json($dataToBeReturn);
                                } else {
                                    $dataToBeReturn = [
                                        'remainDataInCSV' => 0,
                                        'productsUploaded' => $totalNumberOfCSVRecord - $errorCount,
                                        'countOfStartedProfiles' => count($csvData),
                                    ];

                                    return response()->json($dataToBeReturn);
                                }

                                $product['productFlatId'] = null;
                            } catch (\Exception $e) {
                                $error = $e;
                                $countOfStartedProfiles = $i + 1;
                                $remainDataInCSV = $totalNumberOfCSVRecord - $productUploaded;

                                $dataToBeReturn = array(
                                    'remainDataInCSV' => $remainDataInCSV,
                                    'productsUploaded' => $productUploaded,
                                    'countOfStartedProfiles' => $countOfStartedProfiles,
                                    'error' => $error->errorInfo[2],
                                );

                                return response()->json($dataToBeReturn);
                            }
                        } else if ($csvData[$i]['type'] == "simple" && empty($csvData[$i]['super_attribute_option'] )) {
                            try  {
                                $uptoProcessCSVRecords = (int)$countOfStartedProfiles + 10;
                                $processRecords = (int)$countOfStartedProfiles + (int)$numberOfCSVRecord;
                                $inventory = [];

                                if ($numberOfCSVRecord > $processCSVRecords) {
                                    for ($i = $countOfStartedProfiles; $i < $uptoProcessCSVRecords; $i++)
                                    {
                                        $categoryData = explode(',', $csvData[$i]['categories_slug']);
                                        foreach ($categoryData as $key => $value)
                                        {
                                            $categoryID[$key] = $this->categoryRepository->findBySlugOrFail($categoryData[$key])->id;
                                        }

                                        $productFlatData = $this->productFlatRepository->findWhere(['sku' => $csvData[$i]['sku'], 'url_key' => $csvData[$i]['url_key']])->first();

                                        $productData = $this->productRepository->findWhere(['sku' => $csvData[$i]['sku']])->first();

                                        $attributeFamilyData = $this->attributeFamily->findOneByfield(['name' => $csvData[$i]['attribute_family_name']]);

                                        if (! isset($productFlatData) && empty($productFlatData)) {
                                            $data['type'] = $csvData[$i]['type'];
                                            $data['attribute_family_id'] = $attributeFamilyData->id;
                                            $data['sku'] = $csvData[$i]['sku'];

                                            $simpleproductData = $this->productRepository->create($data);
                                        } else {
                                            $simpleproductData = $productData;
                                        }

                                        $marketplace = $this->importNewProducts->findOneByField('data_flow_profile_id', $data_flow_profile_id);

                                        if (($marketplace->customer_id >= 1) && ! isset($productFlatData) && empty($productFlatData) ) {
                                            $sellerProduct = [
                                                'product_id' => $simpleproductData->id,
                                                'is_owner' => 1,
                                                'marketplace_seller_id' => $marketplace->customer_id
                                            ];

                                            $this->marketplaceProductRepository->create($sellerProduct);
                                        }

                                        unset($data);

                                        $inventory_data = core()->getCurrentChannel()->inventory_sources;

                                        foreach($inventory_data as $key => $datas)
                                        {
                                            $inventoryId = $datas->id;
                                        }

                                        $inventoryData[] = (string)$csvData[$i]['inventories'];

                                        foreach ($inventoryData as $key => $d)
                                        {
                                            $inventory[$inventoryId] = $d;
                                        }


                                        $productInventory = $this->productInventoryRepository->findOneByField(['vendor_id' => $marketplace->customer_id, 'product_id' => $simpleproductData->id]);

                                        if (! isset($productInventory) && empty($productInventory) || $productInventory->count() < 1) {
                                            $data['inventories'] =  $inventory;
                                        }

                                        $categoryData = explode(',', $csvData[$i]['categories_slug']);

                                        $data['vendor_id'] = $marketplace->customer_id;
                                        $data['categories'] = $categoryID;
                                        $data['channel'] = core()->getCurrentChannel()->code;
                                        $data['locale'] = core()->getCurrentLocale()->code;
                                        $data['description'] = $csvData[$i]['description'];
                                        $data['dataFlowProfileRecordId'] = $dataFlowProfileRecord->id;
                                        $data['url_key'] = $csvData[$i]['url_key'];
                                        $data['type'] = $csvData[$i]['type'];
                                        $data['name'] = (string)$csvData[$i]['name'];
                                        $data['sku'] = (string)$csvData[$i]['sku'];
                                        $data['price'] = (string)$csvData[$i]['price'];
                                        $data['weight'] = (string)$csvData[$i]['weight'];
                                        $data['new'] = (string)$csvData[$i]['new'];
                                        $data['meta_title'] = (string)$csvData[$i]['meta_title'];
                                        $data['meta_keywords'] = (string)$csvData[$i]['meta_keyword'];
                                        $data['meta_description'] = (string)$csvData[$i]['meta_description'];
                                        $data['featured'] = (string)$csvData[$i]['featured'];
                                        $data['tax_category_id'] = (string)$csvData[$i]['tax_category_id'];
                                        $data['status'] = (string)$csvData[$i]['status'];
                                        $data['cost'] = (string)$csvData[$i]['cost'];
                                        $data['width'] = (string)$csvData[$i]['width'];
                                        $data['height'] = (string)$csvData[$i]['height'];
                                        $data['depth'] = (string)$csvData[$i]['depth'];
                                        $data['attribute_family_id'] = $attributeFamilyData->id;
                                        $data['short_description'] = (string)$csvData[$i]['short_description'];
                                        $data['visible_individually'] = (string)$csvData[$i]['visible_individually'];
                                        $data['special_price'] = (string)$csvData[$i]['special_price'];
                                        $data['special_price_from'] = (string)$csvData[$i]['special_price_from'];
                                        $data['special_price_to'] = (string)$csvData[$i]['special_price_to'];
                                        $attributeOptionColor = $this->attributeOptionRepository->findOneByField('admin_name', (string)$csvData[$i]['color']);

                                        $attributeOptionSize = $this->attributeOptionRepository->findOneByField('admin_name', (string)$csvData[$i]['size']);

                                        if ($attributeOptionColor == null || $attributeOptionSize == null) {
                                            $data['color'] = null;
                                            $data['size'] = null;
                                        } else {
                                            $data['color'] = $attributeOptionColor->id;
                                            $data['size'] = $attributeOptionSize->id;
                                        }

                                        //Product Images
                                        $individualProductimages = explode(',', $csvData[$i]['images']);
                                        if (isset($imageZipName)) {
                                            $images = Storage::disk('local')->files('public/imported-products/extracted-images/admin/'.$dataFlowProfileRecord->id.'/'.$imageZipName['dirname'].'/');

                                            foreach ($images as $imageArraykey => $imagePath)
                                            {
                                                $imageName = explode('/', $imagePath);

                                                if (in_array(last($imageName), $individualProductimages)) {
                                                    $data['images'][$imageArraykey] = $imagePath;
                                                }
                                            }
                                        }

                                        //
                                        $returnRules = $this->validateCSV($data_flow_profile_id, $csvData, $dataFlowProfileRecord, $simpleproductData->id);

                                        $validationCheckForUpdateData = $this->productFlatRepository->findByField(['sku' => $csvData[$i]['sku'], 'url_key' => $csvData[$i]['url_key']]);

                                        if (($validationCheckForUpdateData->count() < 1) || (! isset($validationCheckForUpdateData) && empty($validationCheckForUpdateData))) {
                                            $urlKeyUniqueness = "unique:product_flat,url_key";
                                            $dateFormat = 'date_format:"Y-m-d"';

                                            array_push($returnRules["url_key"], $urlKeyUniqueness);
                                            array_push($returnRules["special_price_to"], $dateFormat);
                                            array_push($returnRules["special_price_from"], $dateFormat);

                                        }
                                        $csvValidator = Validator::make($data, $returnRules);

                                        if ($csvValidator->fails()) {
                                            $errors = $csvValidator->errors()->getMessages();
                                            $this->deleteProductIfNotValidated($simpleproductData->id);

                                            foreach($errors as $key => $error)
                                            {
                                                if ($error[0] == "The url key has already been taken.") {
                                                    $errorToBeReturn[] = "The url key " . $data['url_key'] . " has already been taken";
                                                } else {
                                                    $errorToBeReturn[] = $error[0]. " for sku " . $data['sku'];
                                                }
                                            }

                                            $countOfStartedProfiles =  $i + 1;

                                            $productsUploaded = $i - $errorCount;

                                            if ($numberOfCSVRecord != 0) {
                                                $remainDataInCSV = (int)$totalNumberOfCSVRecord - (int)$countOfStartedProfiles;
                                            } else {
                                                $remainDataInCSV = 0;
                                            }

                                            $dataToBeReturn = array(
                                                'remainDataInCSV' => $remainDataInCSV,
                                                'productsUploaded' => $productsUploaded,
                                                'countOfStartedProfiles' => $countOfStartedProfiles,
                                                'error' => $errorToBeReturn,
                                            );

                                            return response()->json($dataToBeReturn);
                                        }

                                        $configSimpleProductAttributeStore = $this->productRepository->update($data, $simpleproductData->id);

                                        if (isset($imageZipName)) {
                                            $this->bulkUploadImages->bulkuploadImages($data, $simpleproductData, $imageZipName);
                                        }
                                    }
                                } else if ($numberOfCSVRecord <= 10) {
                                    for ($i = $countOfStartedProfiles; $i < $processRecords; $i++)
                                    {
                                        $categoryData = explode(',', $csvData[$i]['categories_slug']);

                                        foreach ($categoryData as $key => $value)
                                        {
                                            $categoryID[$key] = $this->categoryRepository->findBySlugOrFail($categoryData[$key])->id;
                                        }

                                        $productFlatData = $this->productFlatRepository->findWhere(['sku' => $csvData[$i]['sku'], 'url_key' => $csvData[$i]['url_key']])->first();

                                        $productData = $this->productRepository->findWhere(['sku' => $csvData[$i]['sku']])->first();

                                        $attributeFamilyData = $this->attributeFamily->findOneByfield(['name' => $csvData[$i]['attribute_family_name']]);

                                        if (! isset($productFlatData) && empty($productFlatData)) {
                                            $data['type'] = $csvData[$i]['type'];
                                            $data['attribute_family_id'] = $attributeFamilyData->id;
                                            $data['sku'] = $csvData[$i]['sku'];

                                            $simpleproductData = $this->productRepository->create($data);
                                        } else {
                                            $simpleproductData = $productData;
                                        }

                                        unset($data);

                                        $marketplace = $this->importNewProducts->findOneByField('data_flow_profile_id', $data_flow_profile_id);

                                        if (($marketplace->customer_id >= 1) && ! isset($productFlatData) && empty($productFlatData) ) {
                                            $sellerProduct = [
                                                'product_id' => $simpleproductData->id,
                                                'is_owner' => 1,
                                                'marketplace_seller_id' => $marketplace->customer_id
                                            ];

                                            $this->marketplaceProductRepository->create($sellerProduct);
                                        }

                                        $inventory_data = core()->getCurrentChannel()->inventory_sources;

                                        foreach($inventory_data as $key => $datas)
                                        {
                                            $inventoryId = $datas->id;
                                        }

                                        $inventoryData[] = (string)$csvData[$i]['inventories'];

                                        foreach ($inventoryData as $key => $d)
                                        {
                                            $inventory[$inventoryId] = $d;
                                        }

                                        $data['inventories'] =  $inventory;

                                        $categoryData = explode(',', $csvData[$i]['categories_slug']);

                                        $data['categories'] = $categoryID;
                                        $data['vendor_id'] = $marketplace->customer_id;
                                        $data['channel'] = core()->getCurrentChannel()->code;
                                        $data['locale'] = core()->getCurrentLocale()->code;
                                        $data['description'] = $csvData[$i]['description'];
                                        $data['dataFlowProfileRecordId'] = $dataFlowProfileRecord->id;
                                        $data['url_key'] = $csvData[$i]['url_key'];
                                        $data['type'] = $csvData[$i]['type'];
                                        $data['name'] = (string)$csvData[$i]['name'];
                                        $data['sku'] = (string)$csvData[$i]['sku'];
                                        $data['price'] = (string)$csvData[$i]['price'];
                                        $data['weight'] = (string)$csvData[$i]['weight'];
                                        $data['new'] = (string)$csvData[$i]['new'];
                                        $data['meta_title'] = (string)$csvData[$i]['meta_title'];
                                        $data['meta_keywords'] = (string)$csvData[$i]['meta_keyword'];
                                        $data['meta_description'] = (string)$csvData[$i]['meta_description'];
                                        $data['featured'] = (string)$csvData[$i]['featured'];
                                        $data['tax_category_id'] = (string)$csvData[$i]['tax_category_id'];
                                        $data['status'] = (string)$csvData[$i]['status'];
                                        $data['cost'] = (string)$csvData[$i]['cost'];
                                        $data['color'] = (string)$csvData[$i]['color'];
                                        $data['size'] = (string)$csvData[$i]['size'];
                                        $data['width'] = (string)$csvData[$i]['width'];
                                        $data['height'] = (string)$csvData[$i]['height'];
                                        $data['depth'] = (string)$csvData[$i]['depth'];
                                        $data['attribute_family_id'] = $attributeFamilyData->id;
                                        $data['short_description'] = (string)$csvData[$i]['short_description'];
                                        $data['visible_individually'] = (string)$csvData[$i]['visible_individually'];
                                        $data['special_price'] = (string)$csvData[$i]['special_price'];
                                        $data['special_price_from'] = (string)$csvData[$i]['special_price_from'];
                                        $data['special_price_to'] = (string)$csvData[$i]['special_price_to'];
                                        $attributeOptionColor = $this->attributeOptionRepository->findOneByField('admin_name', (string)$csvData[$i]['color']);
                                        $attributeOptionSize = $this->attributeOptionRepository->findOneByField('admin_name', (string)$csvData[$i]['size']);

                                        if ($attributeOptionColor == null || $attributeOptionSize == null) {
                                            $data['color'] = null;
                                            $data['size'] = null;
                                        } else {
                                            $data['color'] = $attributeOptionColor->id;
                                            $data['size'] = $attributeOptionSize->id;
                                        }

                                        //Product Images
                                        $individualProductimages = explode(',', $csvData[$i]['images']);

                                        if (isset($imageZipName)) {
                                            $images = Storage::disk('local')->files('public/imported-products/extracted-images/admin/'.$dataFlowProfileRecord->id.'/'.$imageZipName['dirname'].'/');

                                            foreach ($images as $imageArraykey => $imagePath)
                                            {
                                                $imageName = explode('/', $imagePath);

                                                if (in_array(last($imageName), $individualProductimages)) {
                                                    $data['images'][$imageArraykey] = $imagePath;
                                                }
                                            }
                                        }

                                        $returnRules = $this->validateCSV($data_flow_profile_id, $csvData, $dataFlowProfileRecord, $simpleproductData->id);

                                        $validationCheckForUpdateData = $this->productFlatRepository->findByField(['sku' => $csvData[$i]['sku'], 'url_key' => $csvData[$i]['url_key']]);

                                        if (($validationCheckForUpdateData->count() < 1) || (! isset($validationCheckForUpdateData) && empty($validationCheckForUpdateData))) {
                                            $urlKeyUniqueness = "unique:product_flat,url_key";
                                            $dateFormat = 'date_format:"Y-m-d"';

                                            array_push($returnRules["url_key"], $urlKeyUniqueness);
                                            array_push($returnRules["special_price_to"], $dateFormat);
                                            array_push($returnRules["special_price_from"], $dateFormat);
                                        }

                                        $csvValidator = Validator::make($data, $returnRules);

                                        if ($csvValidator->fails()) {
                                            $errors = $csvValidator->errors()->getMessages();

                                            $this->deleteProductIfNotValidated($simpleproductData->id);

                                            foreach($errors as $key => $error)
                                            {
                                                if ($error[0] == "The url key has already been taken.") {
                                                    $errorToBeReturn[] = "The url key " . $data['url_key'] . " has already been taken";
                                                } else {
                                                    $errorToBeReturn[] = $error[0]. " for sku " . $data['sku'];
                                                }
                                            }

                                            $countOfStartedProfiles =  $i + 1;

                                            $productsUploaded = $i - $errorCount;

                                            if ($numberOfCSVRecord != 0) {
                                                $remainDataInCSV = (int)$totalNumberOfCSVRecord - (int)$countOfStartedProfiles;
                                            } else {
                                                $remainDataInCSV = 0;
                                            }

                                            $dataToBeReturn = array(
                                                'remainDataInCSV' => $remainDataInCSV,
                                                'productsUploaded' => $productsUploaded,
                                                'countOfStartedProfiles' => $countOfStartedProfiles,
                                                'error' => $errorToBeReturn,
                                            );

                                            return response()->json($dataToBeReturn);
                                        }

                                        $configSimpleProductAttributeStore = $this->productRepository->update($data, $simpleproductData->id);

                                        if (isset($imageZipName)) {
                                            $this->bulkUploadImages->bulkuploadImages($data, $simpleproductData, $imageZipName);
                                        }
                                    }
                                }

                                if ($numberOfCSVRecord > 10) {
                                    $remainDataInCSV = (int)$numberOfCSVRecord - (int)$processCSVRecords;
                                } else {
                                    $remainDataInCSV = 0;

                                    // $this->adminDataFlowProfileRepository->findOneByField('id', $data_flow_profile_id)->delete();

                                    if($errorCount > 0) {
                                        $uptoProcessCSVRecords = $totalNumberOfCSVRecord - $errorCount;
                                    } else {
                                        $uptoProcessCSVRecords = $processRecords;
                                    }
                                }

                                $countOfStartedProfiles = $i;

                                $dataToBeReturn = [
                                    'remainDataInCSV' => $remainDataInCSV,
                                    'productsUploaded' => $uptoProcessCSVRecords,
                                    'countOfStartedProfiles' => $countOfStartedProfiles,
                                ];

                                return response()->json($dataToBeReturn);
                            } catch (\Exception $e) {
                                $categoryError = explode('[' ,$e->getMessage());
                                $categorySlugError = explode(']' ,$e->getMessage());
                                $countOfStartedProfiles =  $i + 1;
                                $productsUploaded = $i - $errorCount;

                                if ($numberOfCSVRecord != 0) {
                                    $remainDataInCSV = (int)$totalNumberOfCSVRecord - (int)$countOfStartedProfiles;
                                } else {
                                    $remainDataInCSV = 0;
                                }

                                if ($categoryError[0] == "No query results for model ") {
                                    $dataToBeReturn = array(
                                        'remainDataInCSV' => $remainDataInCSV,
                                        'productsUploaded' => $productsUploaded,
                                        'countOfStartedProfiles' => $countOfStartedProfiles,
                                        'error' => "Invalid Category Slug: " . $categorySlugError[1],
                                    );
                                    $categoryError[0] = null;
                                } else if (isset($e->errorInfo)) {
                                    $dataToBeReturn = array(
                                        'remainDataInCSV' => $remainDataInCSV,
                                        'productsUploaded' => $productsUploaded,
                                        'countOfStartedProfiles' => $countOfStartedProfiles,
                                        'error' => $e->errorInfo[2],
                                    );
                                } else {
                                    $dataToBeReturn = array(
                                        'remainDataInCSV' => $remainDataInCSV,
                                        'productsUploaded' => $productsUploaded,
                                        'countOfStartedProfiles' => $countOfStartedProfiles,
                                        'error' => $e->getMessage(),
                                    );
                                }
                                return response()->json($dataToBeReturn);
                            }
                        }
                        else {
                            $product['loopCount'] = $i + 1;
                            $countOfStartedProfiles = $product['loopCount'];
                            $savedProduct = $productUploaded + 1;
                            $remainDataInCSV = $totalNumberOfCSVRecord - $savedProduct;
                            $countOfStartedProfiles = $i + 1;

                            $dataToBeReturn = array(
                                'remainDataInCSV' => $remainDataInCSV,
                                'productsUploaded' => $productUploaded,
                                'countOfStartedProfiles' => $countOfStartedProfiles
                            );

                            return response()->json($dataToBeReturn);
                        }
                    }

                    if ($errorCount == 0) {
                        $dataToBeReturn = [
                            'remainDataInCSV' => 0,
                            'productsUploaded' => $totalNumberOfCSVRecord,
                            'countOfStartedProfiles' => count($csvData),
                        ];

                        return response()->json($dataToBeReturn);
                    } else if ($totalNumberOfCSVRecord == $errorCount) {
                        $dataToBeReturn = [
                            'remainDataInCSV' => 0,
                            'productsUploaded' => 0,
                            'countOfStartedProfiles' => count($csvData),
                        ];

                        return response()->json($dataToBeReturn);
                    }

                } else {
                    return response()->json([
                        "success" => true,
                        "message" => "CSV Product Successfully Imported"
                    ]);
                }
            }
        }
    }

    public function validateCSV($dataFlowProfileId, $records, $dataFlowProfileRecord, $simpleproductDataId)
    {
        $messages = [];

        $profiler = $this->adminDataFlowProfileRepository->findOneByField('id', $dataFlowProfileId);

        if ($dataFlowProfileRecord) {
            // $records = (new DataGridImport)->toArray($dataFlowProfileRecord->file_path)[0];
            foreach($records as $data) {
                $validate = Validator::make($data, [
                    'sku' => ['required', 'unique:products,sku,' . $simpleproductDataId, new \Webkul\Core\Contracts\Validations\Slug],
                    'variants.*.name' => 'required',
                    'variants.*.sku' => 'required',
                    'variants.*.price' => 'required',
                    'variants.*.weight' => 'required',
                    'images.*' => 'mimes:jpeg,jpg,bmp,png',
                ]);

                if (isset($validate) && $validate->fails()) {
                    continue;
                } else {
                    $attributeFamily = $profiler->attribute_family;

                    $attributes = $attributeFamily->custom_attributes;

                    foreach ($attributes as $attribute) {
                        if ($attribute->code == 'sku') {
                            continue;
                        }

                        if ($data['type'] == 'configurable' && in_array($attribute->code, ['price', 'cost', 'special_price', 'special_price_from', 'special_price_to', 'width', 'height', 'depth', 'weight'])) {
                            continue;
                        }

                        $validations = [];

                        if ($attribute->is_required) {
                            array_push($validations, 'required');
                        } else {
                            array_push($validations, 'nullable');
                        }

                        if ($attribute->type == 'text' && $attribute->validation) {
                            if ($attribute->validation == 'decimal') {
                                array_push($validations, new \Webkul\Core\Contracts\Validations\Decimal);
                            } else {
                                array_push($validations, $attribute->validation);
                            }
                        }

                        if ($attribute->type == 'price') {
                            array_push($validations, new \Webkul\Core\Contracts\Validations\Decimal);
                        }

                        if ($attribute->is_unique) {
                            $this->id = $simpleproductDataId;

                            array_push($validations, function ($field, $value, $fail) use ($data, $attribute) {
                                $column = ProductAttributeValue::$attributeTypeFields[$attribute->type];
                                // if (! $this->productAttributeValueRepository->isValueUnique($this->id, $attribute->id, $column, $data[$attribute->code])) {
                                //     $fail('The :attribute has already been taken.');
                                // }
                            });
                        }
                        $this->rules[$attribute->code] = $validations;
                    }

                    // $validationCheckForUpdateData = $this->productFlatRepository->findWhere(['sku' => $data['sku'], 'url_key' => $data['url_key']]);

                    // if (! isset($validationCheckForUpdateData) || empty($validationCheckForUpdateData)) {
                    //     $urlKeyUniqueness = "unique:product_flat,url_key";
                    //     array_push($this->rules["url_key"], $urlKeyUniqueness);
                    // }
                }
            }
        }

        return $this->rules;
    }

    public function readCSVData(Request $request)
    {
        $countCSV = 0;
        $urlKey = "";
        $data_flow_profile_id = request()->data_flow_profile_id;

        $dataFlowProfileRecord = $this->importNewProducts->findOneByField('data_flow_profile_id', $data_flow_profile_id);

        $this->adminDataFlowProfileRepository->update(["run_status" => "1"], $data_flow_profile_id);

        if ($dataFlowProfileRecord) {
            $csvData = (new DataGridImport)->toArray($dataFlowProfileRecord->file_path)[0];

            for ($i = 0; $i < count($csvData); $i++)
            {
                if ($csvData[$i]['type'] == 'configurable') {
                    $countCSV += 1;
                } else if ($csvData[0]['type'] != 'configurable') {
                    $countCSV = count($csvData);
                }
            }

            return $countCSV;
        } else {
            return response()->json([
                "error" => true,
                "message" => "Record Not Found"
            ]);
        }
    }

    public function createFlat($product, $parentProduct = null)
    {
        static $familyAttributes = [];

        static $superAttributes = [];

        if (! array_key_exists($product->attribute_family->id, $familyAttributes))
            $familyAttributes[$product->attribute_family->id] = $product->attribute_family->custom_attributes;

        if ($parentProduct && ! array_key_exists($parentProduct->id, $superAttributes))
            $superAttributes[$parentProduct->id] = $parentProduct->super_attributes()->pluck('code')->toArray();

        foreach (core()->getAllChannels() as $channel) {
            foreach ($channel->locales as $locale) {
                $productFlat = $this->productFlatRepository->findOneWhere([
                    'product_id' => $product->id,
                    'channel' => $channel->code,
                    'locale' => $locale->code
                ]);

                if (! $productFlat) {
                    $productFlat = $this->productFlatRepository->create([
                        'product_id' => $product->id,
                        'channel' => $channel->code,
                        'locale' => $locale->code
                    ]);
                }
                foreach ($familyAttributes[$product->attribute_family->id] as $attribute) {
                    if ($parentProduct && ! in_array($attribute->code, array_merge($superAttributes[$parentProduct->id], ['sku', 'name', 'price', 'weight', 'status'])))
                        continue;

                    if (in_array($attribute->code, ['tax_category_id']))
                        continue;

                    if (! Schema::hasColumn('product_flat', $attribute->code))
                        continue;

                    if ($attribute->value_per_channel) {
                        if ($attribute->value_per_locale) {
                            $productAttributeValue = $product->attribute_values()->where('channel', $channel->code)->where('locale', $locale->code)->where('attribute_id', $attribute->id)->first();
                        } else {
                            $productAttributeValue = $product->attribute_values()->where('channel', $channel->code)->where('attribute_id', $attribute->id)->first();
                        }
                    } else {
                        if ($attribute->value_per_locale) {
                            $productAttributeValue = $product->attribute_values()->where('locale', $locale->code)->where('attribute_id', $attribute->id)->first();
                        } else {
                            $productAttributeValue = $product->attribute_values()->where('attribute_id', $attribute->id)->first();
                        }
                    }

                    if ($product->type == 'configurable' && $attribute->code == 'price') {
                        $productFlat->{$attribute->code} = app('Webkul\Product\Helpers\Price')->getVariantMinPrice($product);
                    } else {
                        $productFlat->{$attribute->code} = $productAttributeValue[ProductAttributeValue::$attributeTypeFields[$attribute->type]];
                    }

                    if ($attribute->type == 'select') {
                        $attributeOption = $this->attributeOptionRepository->find($product->{$attribute->code});

                        if ($attributeOption) {
                            if ($attributeOptionTranslation = $attributeOption->translate($locale->code)) {
                                $productFlat->{$attribute->code . '_label'} = $attributeOptionTranslation->label;
                            } else {
                                $productFlat->{$attribute->code . '_label'} = $attributeOption->admin_name;
                            }
                        }
                    } elseif ($attribute->type == 'multiselect') {
                        $attributeOptionIds = explode(',', $product->{$attribute->code});

                        if (count($attributeOptionIds)) {
                            $attributeOptions = $this->attributeOptionRepository->findWhereIn('id', $attributeOptionIds);

                            $optionLabels = [];

                            foreach ($attributeOptions as $attributeOption) {
                                if ($attributeOptionTranslation = $attributeOption->translate($locale->code)) {
                                    $optionLabels[] = $attributeOptionTranslation->label;
                                } else {
                                    $optionLabels[] = $attributeOption->admin_name;
                                }
                            }

                            $productFlat->{$attribute->code . '_label'} = implode(', ', $optionLabels);
                        }
                    }
                }

                $productFlat->created_at = $product->created_at;

                $productFlat->updated_at = $product->updated_at;

                if ($parentProduct) {
                    $parentProductFlat = $this->productFlatRepository->findOneWhere([

                            'product_id' => $parentProduct->id,
                            'channel' => $channel->code,
                            'locale' => $locale->code
                        ]);
                }
                $productFlat->parent_id = $product->parent_id;

                $productFlat->save();
            }
        }
    }

    public function edit($id)
    {
        $families = $this->attributeFamily->all();
        $profiles = $this->dataFlowProfile->findOrFail($id);
        $configurableFamily = null;

        if ($familyId = request()->get('family')) {
            $configurableFamily = $this->attributeFamily->find($familyId);
        }

        return view($this->_config['view'], compact('families', 'profiles', 'configurableFamily'));
    }

    public function update($id)
    {
        $product = $this->adminDataFlowProfileRepository->update(request()->except('_token'), $id);
        $families = $this->attributeFamily->all();
        $profiles = $this->dataFlowProfile->findOrFail($id);

        $configurableFamily = null;

        if ($familyId = request()->get('family')) {
            $configurableFamily = $this->attributeFamily->find($familyId);
        }

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Product']));

        return redirect()->route('admin.marketplace.dataflow-profile.index');
    }

    public function destroy($id)
    {
        $product = $this->adminDataFlowProfileRepository->findOrFail($id)->delete();

        Session()->flash('success',trans('bulkupload::app.shop.profile.profile-deleted'));

        return redirect()->route('admin.marketplace.dataflow-profile.index');
    }

    public function deleteProductIfNotValidated($id)
    {
        // $productDataToDelete = $this->productRepository->find($id);
        // $productFlatDataToDelete = $this->productFlatRepository->find($id);

        // if (isset($productDataToDelete) || !empty($productDataToDelete)) {
            $this->productRepository->findOrFail($id)->delete();
        // } else {
            // $this->productFlatRepository->findOrFail($id)->delete();
        // }

    }

    public function massDestroy()
    {
        $profileIds = explode(',', request()->input('indexes'));

        foreach ($profileIds as $profileId) {
            $profile = $this->adminDataFlowProfileRepository->find($profileId);

            if (isset($profile)) {
                $this->adminDataFlowProfileRepository->delete($profileId);
            }
        }

        session()->flash('success', trans('bulkupload::app.admin.bulk-upload.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }
}