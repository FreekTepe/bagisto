<?php

Route::group(['middleware' => ['web']], function () {

    Route::prefix('admin/marketplace')->group(function () {

        Route::group(['middleware' => ['admin']], function () {

            //Seller routes
            Route::get('sellers', 'Webkul\Marketplace\Http\Controllers\Admin\SellerController@index')->defaults('_config', [
                'view' => 'marketplace::admin.sellers.index'
            ])->name('admin.marketplace.sellers.index');

            Route::get('sellers/delete/{id}', 'Webkul\Marketplace\Http\Controllers\Admin\SellerController@destroy')
                ->name('admin.marketplace.sellers.delete');

            Route::post('sellers/massdelete', 'Webkul\Marketplace\Http\Controllers\Admin\SellerController@massDestroy')->defaults('_config', [
                'redirect' => 'admin.marketplace.sellers.index'
            ])->name('admin.marketplace.sellers.massdelete');

            Route::post('sellers/massupdate', 'Webkul\Marketplace\Http\Controllers\Admin\SellerController@massUpdate')->defaults('_config', [
                'redirect' => 'admin.marketplace.sellers.index'
            ])->name('admin.marketplace.sellers.massupdate');

            Route::get('sellers/{id}/orders', 'Webkul\Marketplace\Http\Controllers\Admin\OrderController@index')->defaults('_config', [
                'view' => 'marketplace::admin.orders.index'
            ])->name('admin.marketplace.sellers.orders.index');

            Route::get('orders', 'Webkul\Marketplace\Http\Controllers\Admin\OrderController@index')->defaults('_config', [
                'view' => 'marketplace::admin.orders.index'
            ])->name('admin.marketplace.orders.index');

            Route::post('orders', 'Webkul\Marketplace\Http\Controllers\Admin\OrderController@pay')->defaults('_config', [
                'redirect' => 'admin.marketplace.orders.index'
            ])->name('admin.marketplace.orders.pay');

            Route::get('transactions', 'Webkul\Marketplace\Http\Controllers\Admin\TransactionController@index')->defaults('_config', [
                'view' => 'marketplace::admin.transactions.index'
            ])->name('admin.marketplace.transactions.index');


            //Seller products routes
            Route::get('products', 'Webkul\Marketplace\Http\Controllers\Admin\ProductController@index')->defaults('_config', [
                'view' => 'marketplace::admin.products.index'
            ])->name('admin.marketplace.products.index');

            Route::get('products/delete/{id}', 'Webkul\Marketplace\Http\Controllers\Admin\ProductController@destroy')
                ->name('admin.marketplace.products.delete');

            Route::post('products/massdelete', 'Webkul\Marketplace\Http\Controllers\Admin\ProductController@massDestroy')->defaults('_config', [
                'redirect' => 'admin.marketplace.products.index'
            ])->name('admin.marketplace.products.massdelete');

            Route::post('products/massupdate', 'Webkul\Marketplace\Http\Controllers\Admin\ProductController@massUpdate')->defaults('_config', [
                'redirect' => 'admin.marketplace.products.index'
            ])->name('admin.marketplace.products.massupdate');


            //Seller review routes
            Route::get('reviews', 'Webkul\Marketplace\Http\Controllers\Admin\ReviewController@index')->defaults('_config', [
                'view' => 'marketplace::admin.reviews.index'
            ])->name('admin.marketplace.reviews.index');

            Route::post('reviews/massupdate', 'Webkul\Marketplace\Http\Controllers\Admin\ReviewController@massUpdate')->defaults('_config', [
                'redirect' => 'admin.marketplace.reviews.index'
            ])->name('admin.marketplace.reviews.massupdate');

            // Bulk Upload Products
            Route::get('bulkupload-upload-files', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@index')->defaults('_config', [
                'view' => 'bulkupload::admin.bulk-upload.upload-files.index'
            ])->name('admin.marketplace.bulk-upload.index');

            Route::get('bulk-upload-run-profile', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@index')->defaults('_config', [
                'view' => 'bulkupload::admin.bulk-upload.run-profile.index'
            ])->name('admin.marketplace.run-profile.index');

            Route::post('read-csv', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@readCSVData')
            ->name('marketplace.bulk-upload-admin.read-csv');

            Route::post('getprofiles', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@getAllDataFlowProfiles')
            ->name('marketplace.bulk-upload-admin.get-all-profile');


            // Download Sample Files
            Route::post('download','Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@downloadFile')->defaults('_config',[
                'view' => 'bulkupload::admin.bulk-upload.upload-files.index'
            ])->name('download-sample-files');

            // import new products
            Route::post('importnew', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@importNewProductsStore')->defaults('_config',['view' => 'bulkupload::admin.bulk-upload.upload-files.index' ])->name('import-new-products-form-submit');

            Route::get('dataflowprofile', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@index')->defaults('_config', [
                'view' => 'bulkupload::admin.bulk-upload.data-flow-profile.index'
            ])->name('admin.marketplace.dataflow-profile.index');

            Route::post('addprofile', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@store')->defaults('_config', [
                'view' => 'bulkupload::admin.bulk-upload.data-flow-profile.index'
            ])->name('bulkupload.bulk-upload.dataflow.add-profile');

            Route::post('runprofile', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@runProfile')->defaults('_config', [
                'view' => 'bulkupload::admin.bulk-upload.run-profile.progressbar'
            ])->name('marketplace.bulk-upload-admin.run-profile');

            // edit actions
            Route::get('/dataflowprofile/delete/{id}','Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@destroy')->name('marketplace.bulkupload.admin.profile.delete');

            Route::get('/dataflowprofile/edit/{id}', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@edit')->defaults('_config', [
                'view' => 'bulkupload::admin.bulk-upload.data-flow-profile.edit'
            ])->name('marketplace.bulkupload.admin.profile.edit');

            Route::post('/dataflowprofile/update/{id}', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@update')->defaults('_config', [
                'view' => 'bulkupload::admin.bulk-upload.data-flow-profile.index'
            ])->name('marketplace.admin.bulk-upload.dataflow.update-profile');

            //mass destroy

            Route::post('products/massdestroy', 'Webkul\Bulkupload\Http\Controllers\Admin\BulkUploadController@massDestroy')->defaults('_config', [
                'redirect' => 'admin.marketplace.dataflow-profile.index'
            ])->name('marketplace.bulkupload.admin.profile.massDelete');
        });
    });
});