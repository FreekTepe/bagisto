<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceImportNewProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_import_new_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id')->references('id')->on('customers');
            $table->integer('attribute_family_id')->unsigned();
            $table->foreign('attribute_family_id', 'mp_import_foreign_attribute_family_id')->references('id')->on('attribute_families')->onDelete('cascade');

            $table->string('file_path');
            $table->string('image_path');

            $table->integer('data_flow_profile_id')->unsigned();
            $table->foreign('data_flow_profile_id')->references('id')->on('marketplace_bulkupload_dataflowprofile')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_import_new_products');
    }
}
