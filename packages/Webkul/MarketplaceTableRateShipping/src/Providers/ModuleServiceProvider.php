<?php

namespace Webkul\MarketplaceTableRateShipping\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Webkul\MarketplaceTableRateShipping\Models\Superset::class,
        \Webkul\MarketplaceTableRateShipping\Models\SupersetRate::class,
        \Webkul\MarketplaceTableRateShipping\Models\ShippingRate::class
    ];
}