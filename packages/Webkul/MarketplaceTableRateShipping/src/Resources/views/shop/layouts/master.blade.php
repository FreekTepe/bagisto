@extends('marketplace::shop.layouts.account')

@push('css')
    <link rel="stylesheet" href="{{ bagisto_asset('css/marketplacetablerateshipping-admin.css') }}">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ bagisto_asset('js/marketplacetablerateshipping.js') }}"></script>
@endpush