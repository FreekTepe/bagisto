@extends('marketplace_tablerate_shipping::admin.layouts.content')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.supersets.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('marketplace_tablerate_shipping::app.admin.supersets.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.marketplace.tablerate.super_sets.create') }}" class="btn btn-lg btn-primary">

                    {{ __('marketplace_tablerate_shipping::app.admin.supersets.add-btn-title') }}

                </a>
            </div>
        </div>

        <div class="page-content">
            {!! app('Webkul\MarketplaceTableRateShipping\DataGrids\Admin\SupersetDataGrid')->render() !!}
        </div>
    </div>
@stop