<?php

namespace Webkul\MarketplaceTableRateShipping\DataGrids\Shop;

use DB;
use Webkul\Ui\DataGrid\DataGrid;
use Webkul\Marketplace\Repositories\SellerRepository;

/**
 * Seller Shipping Superset  Data Grid Class
 *
 *
 * @author Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SupersetRateDataGrid Extends DataGrid
{
    /**
     *
     * @var integer
     */
    public $index = 'id';

    protected $sellerRepository;

    /**
     * Create a new repository instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository $sellerRepository
     * @return void
     */
    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;

        $this->invoker = $this;
    }

    public function prepareQueryBuilder()
    {
        $seller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $queryBuilder = DB::table('marketplace_tablerate_superset_rates')
                ->leftJoin('marketplace_tablerate_supersets', 'marketplace_tablerate_superset_rates.marketplace_superset_id', '=', 'marketplace_tablerate_supersets.id')
                ->select(
                    'marketplace_tablerate_superset_rates.id',
                    'marketplace_tablerate_supersets.name',
                    'marketplace_tablerate_superset_rates.price_from',
                    'marketplace_tablerate_superset_rates.price_to',
                    'marketplace_tablerate_superset_rates.shipping_type',
                    'marketplace_tablerate_superset_rates.price'
                )
                ->where('marketplace_tablerate_superset_rates.marketplace_seller_id', $seller->id);

        $this->addFilter('id', 'marketplace_tablerate_superset_rates.id');
        $this->addFilter('created_at', 'marketplace_tablerate_superset_rates.created_at');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.id'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'price_from',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.price-from'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'price_to',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.price-to'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'shipping_type',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-type'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'price',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.price'),
            'type' => 'price',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'type' => 'Edit',
            'method' => 'GET',
            'route' => 'shop.marketplace.tablerate.super_set_rates.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'method' => 'POST',
            'route' => 'shop.marketplace.tablerate.super_set_rates.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Superset Rate']),
            'icon' => 'icon trash-icon'
        ]);

        $this->enableAction = true;
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.delete'),
            'action' => route('shop.marketplace.tablerate.super_set_rates.mass-delete'),
            'method' => 'DELETE'
        ]);

        $this->enableMassAction = true;
    }
}