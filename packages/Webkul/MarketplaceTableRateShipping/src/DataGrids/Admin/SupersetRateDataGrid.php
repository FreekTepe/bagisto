<?php

namespace Webkul\MarketplaceTableRateShipping\DataGrids\Admin;

use DB;
use Webkul\Ui\DataGrid\DataGrid;

/**
 * Superset Rate Data Grid Class
 *
 *
 * @author Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
Class SupersetRateDataGrid extends DataGrid
{
    /**
     *
     * @var integer
    */
    public $index = 'id';

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('marketplace_tablerate_superset_rates')
                ->leftJoin('marketplace_sellers', 'marketplace_tablerate_superset_rates.marketplace_seller_id', '=', 'marketplace_sellers.id')
                ->leftJoin('customers', 'marketplace_sellers.customer_id', '=', 'customers.id')
                ->leftJoin('marketplace_tablerate_supersets', 'marketplace_tablerate_superset_rates.marketplace_superset_id', '=', 'marketplace_tablerate_supersets.id')
                ->select(
                    'marketplace_tablerate_superset_rates.id',
                    'marketplace_tablerate_supersets.name as shipping_method',
                    'marketplace_tablerate_superset_rates.marketplace_seller_id',
                    'marketplace_tablerate_superset_rates.price_from',
                    'marketplace_tablerate_superset_rates.price_to',
                    'marketplace_tablerate_superset_rates.shipping_type',
                    'marketplace_tablerate_superset_rates.price',
                    DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as seller_name')
                );

        $this->addFilter('shipping_method', 'marketplace_tablerate_supersets.name');
        $this->addFilter('seller_name', DB::raw('CONCAT(customers.first_name, " ", customers.last_name)'));
        $this->addFilter('created_at', 'marketplace_tablerate_superset_rates.created_at');
        $this->addFilter('id', 'marketplace_tablerate_superset_rates.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.id'),
            'type' => 'number',
            'searchable' => false,
            'sortable' => true,
            'filterable' => true
        ]);

        // $this->addColumn([
        //     'index' => 'marketplace_seller_id',
        //     'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.seller-id'),
        //     'type' => 'number',
        //     'searchable' => true,
        //     'sortable' => true,
        //     'filterable' => true
        // ]);

        $this->addColumn([
            'index' => 'price_from',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.price-from'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'price_to',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.price-to'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'shipping_type',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-type'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'shipping_method',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'seller_name',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.seller-name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
            'wrapper' => function($data) {
                if ($data->marketplace_seller_id == 0) {
                    return 'Admin';
                } else {
                    return $data->seller_name;
                }
            }
        ]);

        $this->addColumn([
            'index' => 'price',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.price'),
            'type' => 'price',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

    }

    public function prepareActions()
    {
        $this->addAction([
            'type' => 'Edit',
            'method' => 'GET',
            'route' => 'admin.marketplace.tablerate.super_set_rates.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'method' => 'POST',
            'route' => 'admin.marketplace.tablerate.super_set_rates.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Superset Rate']),
            'icon' => 'icon trash-icon'
        ]);

        $this->enableAction = true;
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('marketplace_tablerate_shipping::app.admin.superset-rates.delete'),
            'action' => route('admin.marketplace.tablerate.super_set_rates.mass_delete'),
            'method' => 'DELETE'
        ]);

        $this->enableMassAction = true;
    }
}