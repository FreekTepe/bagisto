<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceTablerateShippingRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_tablerate_shipping_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->boolean('is_zip_range')->default(0);
            $table->string('zip_from')->nullable();
            $table->string('zip_to')->nullable();
            $table->string('zip_code')->nullable();
            $table->decimal('price', 12, 4)->default(0);
            $table->decimal('weight_from', 12, 4)->default(0);
            $table->decimal('weight_to', 12, 4)->default(0);
            $table->timestamps();

            $table->integer('marketplace_seller_id')->unsigned()->nullable();
            $table->foreign('marketplace_seller_id', 'mp_seller_tablerate_shipping_methods_seller_id_foreign')->references('id')->on('marketplace_sellers')->onDelete('cascade');

            $table->integer('marketplace_tablerate_superset_id')->unsigned()->nullable();
            $table->foreign('marketplace_tablerate_superset_id','mp_seller_tablerate_shipping_rates_superset_id_foreign')->references('id')->on('marketplace_tablerate_supersets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_tablerate_shipping_rates');
    }
}
