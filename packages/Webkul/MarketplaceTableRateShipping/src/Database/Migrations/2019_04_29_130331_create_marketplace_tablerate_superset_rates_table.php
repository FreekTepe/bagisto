<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceTablerateSupersetRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_tablerate_superset_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price_from', 12, 4)->unsigned();
            $table->decimal('price_to', 12, 4)->unsigned();
            $table->decimal('price', 12, 4)->default(0);
            $table->string('shipping_type');
            $table->timestamps();

            $table->integer('marketplace_seller_id')->unsigned()->nullable();
            $table->foreign('marketplace_seller_id', 'mp_seller_tablerate_supersets_seller_id_foreign')->references('id')->on('marketplace_sellers')->onDelete('cascade');

            $table->integer('marketplace_superset_id')->unsigned()->nullable();
            $table->foreign('marketplace_superset_id', 'mp_seller_tablerate_supersets_superset_id_foreign')->references('id')->on('marketplace_tablerate_supersets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_tablerate_superset_rates');
    }
}
