<?php

namespace Webkul\MarketplaceTableRateShipping\Repositories;

use Webkul\Core\Eloquent\Repository;

/**
 * Superset Rate Reposotory
 *
 * @author    Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SupersetRateRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\MarketplaceTableRateShipping\Contracts\SupersetRate';
    }
}