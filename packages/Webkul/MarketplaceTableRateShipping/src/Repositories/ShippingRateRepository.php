<?php

namespace Webkul\MarketplaceTableRateShipping\Repositories;

use Webkul\Core\Eloquent\Repository;

/**
 * ShippingRate Reposotory
 *
 * @author    Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ShippingRateRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\MarketplaceTableRateShipping\Contracts\ShippingRate';
    }
}