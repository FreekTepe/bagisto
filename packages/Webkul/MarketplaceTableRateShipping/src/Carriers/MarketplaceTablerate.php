<?php

namespace Webkul\MarketplaceTableRateShipping\Carriers;

use Webkul\Checkout\Models\CartShippingRate;
use Webkul\Shipping\Carriers\AbstractShipping;
use Webkul\Checkout\Facades\Cart;

/**
 * Marketplace Table Rate Shipping.
 *
 */
class MarketplaceTablerate extends AbstractShipping
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'mptablerate';

    /**
     * Returns rate for flatrate
     *
     * @return array
     */
    public function calculate()
    {
        $cart = Cart::getCart();
        $cartItems = $cart->items;
        $itemCount = count($cartItems);

        $shippingData = app('Webkul\MarketplaceTableRateShipping\Helpers\ShippingHelper');

        $shippingDetails = $shippingData->findAppropriateTableRateMethods();

        $allMethods = $shippingData->getMethodWiseShippingData($shippingDetails);

        $commonMethods = $shippingData->validateShippingRate($shippingDetails, $itemCount);

        $marketplaceShipping = session()->get('marketplace_shipping_rates');

        $shippingMethods = [];

        $rates = [];
        $mprates = [];

        if (! $this->isAvailable())
            return false;

        if ($allMethods != null && $shippingDetails != null) {
            foreach ($commonMethods as $code => $shippingRates) {
                $totalShippingCost = 0;
                $methodCode = $code;

                foreach ($shippingRates as $shippingRate) {
                    $methodName = $shippingRate['supersetName'];

                    if ($shippingRate['marketplace_seller_id'] == null) {
                        $sellerId = 0;
                    } else {
                        $sellerId = $shippingRate['marketplace_seller_id'];
                    }

                    $itemShippingCost =  $shippingRate['shipping_cost'] * $shippingRate['quantity'];

                    if (isset($rates[$methodName][$sellerId])) {
                        $rates[$methodName][$sellerId] = [
                            'amount' => core()->convertPrice($rates[$methodName][$sellerId]['amount'] + $itemShippingCost),
                            'base_amount' => $rates[$methodName][$sellerId]['base_amount'] + $itemShippingCost
                        ];
                    } else {
                        $rates[$methodName][$sellerId] = [
                            'amount' => core()->convertPrice($itemShippingCost),
                            'base_amount' => $itemShippingCost
                        ];
                    }

                    $totalShippingCost += $itemShippingCost;
                }

                $object = new CartShippingRate;
                $object->carrier = 'mptablerate';
                $object->carrier_title = $this->getConfigData('title');
                $object->method = 'mptablerate_'.''.$methodCode;
                $object->method_title = $methodName;
                $object->method_description = $this->getConfigData('title') .' - ' .$methodName;

                $object->price = core()->convertPrice($totalShippingCost);
                $object->base_price = $totalShippingCost;

                $marketplaceShippingRates = session()->get('marketplace_shipping_rates');

                if (! is_array($marketplaceShipping)) {

                    $marketplaceShippingRates['mptablerate'] = $rates;

                    session()->put('marketplace_shipping_rates', $marketplaceShippingRates);

                } else {

                    session()->put('marketplace_shipping_rates.mptablerate', $rates);
                }

                array_push($shippingMethods, $object);
            }

            //get the uncommon method for multi shipping
            foreach ($allMethods as $code => $randomMethod) {
                if (count($randomMethod) != $itemCount) {

                    $totalShippingCost = 0;
                    $methodCode = $code;

                    foreach ($randomMethod as $multishippingMethods) {
                        $methodName = $multishippingMethods['supersetName'];

                        if ($shippingRate['marketplace_seller_id'] == null) {
                            $sellerId = 0;
                        } else {
                            $sellerId = $shippingRate['marketplace_seller_id'];
                        }

                        $itemShippingCost =  $multishippingMethods['shipping_cost'] * $multishippingMethods['quantity'];

                        if (isset($mprates[$methodName][$sellerId])) {
                            $mprates[$methodName][$sellerId] = [
                                'amount' => core()->convertPrice($mprates[$methodName][$sellerId]['amount'] + $itemShippingCost),
                                'base_amount' => $mprates[$methodName][$sellerId]['base_amount'] + $itemShippingCost
                            ];
                        } else {
                            $mprates[$methodName][$sellerId] = [
                                'amount' => core()->convertPrice($itemShippingCost),
                                'base_amount' => $itemShippingCost
                            ];
                        }

                        $totalShippingCost += $itemShippingCost;
                    }

                    $marketplaceShippingRates = session()->get('marketplace_shipping_rates');

                    if (! is_array($marketplaceShippingRates)) {

                        $marketplaceShippingRates['mptablerate'] = $mprates;

                        session()->put('marketplace_shipping_rates', $marketplaceShippingRates);

                    } else {

                        $data = array_merge($marketplaceShippingRates['mptablerate'], $mprates);

                        session()->put('marketplace_shipping_rates.mptablerate', $data);
                    }
                }
            }

            return $shippingMethods;
        }
    }

    public function getServices()
    {
        return null;
    }
}
