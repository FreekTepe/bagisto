<?php

namespace Webkul\MarketplaceTableRateShipping\Helpers;

use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\ShippingRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;
use Webkul\Marketplace\Repositories\ProductRepository;
use Webkul\Product\Repositories\ProductFlatRepository;
use Webkul\Checkout\Facades\Cart;
use Webkul\Checkout\Repositories\CartAddressRepository;

class SellerRate
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * supersetRateRepository Object
     *
     * @var array
    */
    protected $supersetRate;

    /**
     * shippingRateRepository Object
     *
     * @var array
    */
    protected $shippingRate;

    /**
     * sellerRepository Object
     *
     * @var array
    */
    protected $sellerRepository;

    /**
     * supersetRepository Object
     *
     * @var array
    */
    protected $supersetRepository;

    /**
     * productRepository Object
     *
     * @var array
    */
    protected $productRepository;

    /**
     * cart Address Object
     *
     * @var array
    */
    protected $cartAddressRepository;

    /**
     * product flat Object
     *
     * @var array
    */
    protected $productFlatRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SupersetRateRepository $supersetRate
     * @return void
     */
    public function __construct(
        SupersetRateRepository $supersetRate,
        SellerRepository $sellerRepository,
        SupersetRepository $supersetRepository,
        ShippingRateRepository $shippingRate,
        ProductRepository $productRepository,
        CartAddressRepository $cartAddressRepository,
        ProductFlatRepository $productFlatRepository
    )

    {
        $this->_config = request('_config');

        $this->supersetRate = $supersetRate;

        $this->sellerRepository = $sellerRepository;

        $this->supersetRepository = $supersetRepository;

        $this->shippingRate = $shippingRate;

        $this->productRepository = $productRepository;

        $this->cartAddressRepository = $cartAddressRepository;

        $this->productFlatRepository = $productFlatRepository;
    }

    /**
     * Get the Selle Shipping Rate
     *
     * @param $cartItem
     * @return $sellerRate
     */
    public function getAvailableSellerRate($cartItem)
    {
        $sellerSupesetRate = $this->supersetRate->findWhere([
            'marketplace_seller_id' => $cartItem['marketplace_seller_id']
        ]);

        if (isset($sellerSupesetRate) && $sellerSupesetRate != null && count($sellerSupesetRate) > 0) {

            foreach ($sellerSupesetRate as $supersetRate) {
                if ($supersetRate->price_from <= $cartItem['price'] && $supersetRate->price_to >= $cartItem['price']) {
                    $superSet = $this->supersetRepository->findOneWhere(['id' => $supersetRate->marketplace_superset_id, 'status' => 1]);

                    if ($superSet != null) {
                        $sellerRate[$superSet->code] = $this->getDataInArrayFormate($cartItem, $supersetRate, $superSet);
                    }
                }
            }

            if (! isset($sellerRate)) {
                $sellerRate = $this->getSellerShippingRate($cartItem);
            }
        } else {
            $sellerRate = $this->getSellerShippingRate($cartItem);
        }

        return $sellerRate;
    }

    /**
     * Get Data in Array
     *
     * @param $methods, $cartItem, $superSet
     * @return $adminRate
     */
    public function getDataInArrayFormate($cartItem, $methods, $superSet)
    {
        $adminRate = [
            'price' => $cartItem['price'],
            'base_price' => $cartItem['base_price'],
            'weight' => $cartItem['weight'],
            'shipping_cost' => $methods->price,
            'marketplace_seller_id' => $cartItem['additional']['seller_info']['seller_id'],
            'supersetName' => $superSet->name,
            'methodCode' => $superSet->code,
            'quantity' => $cartItem['quantity']
        ];

        return $adminRate;
    }

    /**
     * Get Admin ShippingRate
     *
     * @param $cartItem
     * @return $adminRate
     */
    public function getSellerShippingRate($cartItem)
    {
        $sellerRate = null;

        $destinationAddress = $this->cartAddressRepository->findOneWhere(['cart_id' => $cartItem['cart_id']])->postcode;

        $sellerShippingRate = $this->shippingRate->findWhere([
            'marketplace_seller_id' => $cartItem['marketplace_seller_id']
        ]);

        if (isset($sellerShippingRate) && $sellerShippingRate != null && count($sellerShippingRate) >0) {

            foreach ($sellerShippingRate as $shippingRate) {

                $superSet = $this->supersetRepository->findOneWhere(['id' => $shippingRate->marketplace_tablerate_superset_id, 'status' => 1]);

                if ($superSet != null) {
                    if ($shippingRate->weight_from <= $cartItem['weight']
                       && $shippingRate->weight_to >= $cartItem['weight']) {

                        if ($shippingRate->zip_from <= $destinationAddress
                            && $shippingRate->zip_to >= $destinationAddress) {

                            if ($shippingRate->is_zip_range == 0) {

                                $sellerRate[$superSet->code] = $this->getDataInArrayFormate($cartItem, $shippingRate, $superSet);
                            } elseif ($shippingRate->is_zip_range == 1
                                && $shippingRate->zip_code == '*'
                                || $shippingRate->zip_code == $destinationAddress) {

                                $sellerRate[$superSet->code] = $this->getDataInArrayFormate($cartItem, $shippingRate, $superSet);
                            }
                        }
                    }
                }
            }
        }

        return $sellerRate;
    }
}