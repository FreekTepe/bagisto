<?php

return [
    [
        'key' => 'marketplace.tablerate',
        'name' => 'marketplace_tablerate_shipping::app.admin.layouts.tablerate',
        'route' => 'admin.marketplace.tablerate.super_sets.index',
        'sort' => 6
    ], [
        'key' => 'marketplace.tablerate.super_sets',
        'name' => 'marketplace_tablerate_shipping::app.admin.layouts.manage-superset',
        'route' => 'admin.marketplace.tablerate.super_sets.index',
        'sort' => 1
    ], [
        'key' => 'marketplace.tablerate.super_set_rates',
        'name' => 'marketplace_tablerate_shipping::app.admin.layouts.manage-superset-rates',
        'route' => 'admin.marketplace.tablerate.super_set_rates.index',
        'sort' => 2
    ], [
        'key' => 'marketplace.tablerate.rates',
        'name' => 'marketplace_tablerate_shipping::app.admin.layouts.manage-shipping-rates',
        'route' => 'admin.marketplace.tablerate.rates.index',
        'sort' => 3
    ],
];