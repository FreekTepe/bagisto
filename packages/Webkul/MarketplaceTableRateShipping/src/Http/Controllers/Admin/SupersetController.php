<?php

namespace Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin;

use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;

class SupersetController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * supersetRepository object
     *
     * @var array
    */
    protected $supersetRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SupersetRepository $supersetRepository
     * @return void
     */
    public function __construct(SupersetRepository $supersetRepository)
    {
        $this->_config = request('_config');

        $this->supersetRepository = $supersetRepository;
    }

    /**
     * Method to populate the superSet page.
     *
     * @return Mixed
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * create new  superset
     *
     * @return response
     */
    public function create()
    {
        return view($this->_config['view']);
    }

     /**
     * Method to store the shipping Method.
     *
     * @return Mixed
     */
    public function store()
    {
        $this->validate(request(),[
            'code' => 'required|unique:marketplace_tablerate_supersets',
            'name' => 'required',
        ]);

        $data = request()->all();

        if (isset($data['status'])) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        $superSet = $this->supersetRepository->create($data);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.supersets.create-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Method to edit the shipping Method.
     *
     * @return Mixed
     */
    public function edit($id)
    {
        $superset = $this->supersetRepository->findOrFail($id);

        return view($this->_config['view'], compact('superset'));
    }

    /**
     * Method to Update the shipping Method.
     *
     * @return Mixed
     */
    public function update($id)
    {
        $this->validate(request(),[
            'code' => 'required',
            'name' => 'required',
        ]);

        $data = request()->all();

        if (isset($data['status'])) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        $superSet = $this->supersetRepository->update($data,$id);

        session()->flash('success',trans('marketplace_tablerate_shipping::app.admin.supersets.update-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Method to Mass Update the Superset.
     *
     * @return Mixed
     */
    public function massupdate()
    {
        $data = request()->all();

        if (! isset($data['massaction-type']) || !$data['massaction-type'] == 'update') {
            return redirect()->back();
        }

        $sellerIds = explode(',', $data['indexes']);

        foreach ($sellerIds as $sellerId) {
            $this->supersetRepository->update([
                    'status' => $data['update-options']
                ], $sellerId);
        }

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.supersets.mass-update-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Delete Super set from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->supersetRepository->delete($id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.supersets.delete-success'));

        return response()->json(['message' => true], 200);
    }

    /**
     * Mass Delete The Super Shipping Set
     *
     * @return response
     */
    public function massDestroy()
    {
        $supersetIds = explode(',', request()->input('indexes'));

        foreach ($supersetIds as $supersetId) {

            $this->supersetRepository->delete($supersetId);
        }

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.supersets.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }
}