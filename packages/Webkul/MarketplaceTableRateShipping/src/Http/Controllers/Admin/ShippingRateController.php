<?php

namespace Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Webkul\Admin\Imports\DataGridImport;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Validators\Failure;
use Webkul\MarketplaceTableRateShipping\Repositories\ShippingRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Core\Repositories\CountryRepository as Country;

class ShippingRateController extends Controller
{
    /**
     * route configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * shippingRateRepository object
     *
     * @var array
    */
    protected $shippingRateRepository;

    /**
     * supersetRepository object
     *
     * @var array
    */
    protected $supersetRepository;

    /**
     * countryRepository object
     *
     * @var array
    */
    protected $country;

    /**
     * sellerRepository object
     *
     * @var array
    */
    protected $sellerRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\shippingRateRepository $ShippingRateRepository
     * @return void
     */
    public function __construct(
        ShippingRateRepository $shippingRateRepository,
        SupersetRepository $supersetRepository,
        SellerRepository $sellerRepository,
        Country $country
    )

    {
        $this->_config = request('_config');

        $this->shippingRateRepository = $shippingRateRepository;

        $this->supersetRepository = $supersetRepository;

        $this->sellerRepository = $sellerRepository;

        $this->country = $country;
    }

    /**
     * Method to populate shipping rate page.
     *
     * @return Mixed
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Method to create the shipping Rates.
     *
     * @return Mixed
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Edit Shipping Rate page .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $countries = $this->country->all();

        $shippingData = $this->shippingRateRepository->findOrFail($id);

        return view($this->_config['view'])->with('shippingData', $shippingData)->with('countries', $countries)->with('shippingMethods', $shippingMethods);
    }

    /**
     * update Shipping Rate
     *
     * @return response
     */
    public function update($id)
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $data = request()->all();

        $this->validate(request(), [
            'shipping_type' => 'string|required',
            'country' => 'string|required',
            'region' => 'string|nullable',
            'weight_from' => 'required|numeric|min:0.0001',
            'weight_to' => 'required|numeric|min:'.$data['weight_from'],
            'zip_from' => 'string|required',
            'zip_to' => 'string|required',
            'price' => 'numeric|required|min:0.0001',
            'is_zip_range' => 'sometimes',
        ]);

        $data = request()->all();

        foreach ($shippingMethods as $method) {
            if($method->id == $data['shipping_type']) {
                $data['marketplace_tablerate_superset_id'] = $method->id;
            }
        }

        $shippingRates = $this->shippingRateRepository->all();

        $this->shippingRateRepository->update($data, $id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.update-success', ['name' => 'Shipping Rate']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Delete shipping
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->shippingRateRepository->delete($id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.superset-rates.delete-success',
        ['name' => 'Shipping Rate']));

        return response()->json(['message' => true], 200);
    }

    /**
     * Mass Delete Shipping Method
     *
     * @return response
     */
    public function massDestroy()
    {
        $tableRateIds = explode(',', request()->input('indexes'));

        foreach ($tableRateIds as $tableRateId) {
            $this->shippingRateRepository->delete($tableRateId);
        }

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Method to Download The Sample File.
     *
     */
    public function sampleDownload()
    {
        $pathToFile=base_path(). "/packages/Webkul/MarketplaceTableRateShipping/src/Resources/views/sampledownload/sampleupload.csv";

        return response()->download($pathToFile);
    }

    /**
     * import function for the upload CSV
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        $sellers = $this->sellerRepository->all();

        foreach ($sellers as $seller) {
            $sellerName[] = $seller->id;
        }

        $shippingMethods = $this->supersetRepository->all();

        $valid_extension = ['csv'];
        $exist = 0;
        $file_extension[] = request()->file('file')->getClientOriginalExtension();

        if ($file_extension != $valid_extension) {
            session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.upload-error'));
        } else {
            try {
                $excelData = (new DataGridImport)->toArray(request()->file('file'));
                foreach ($excelData as $tableRateData) {
                    foreach ($tableRateData as $column => $data) {
                        $ifAlreadyExist = 0;
                        $supersetId = null;
                        $validator = Validator::make($data, [
                            'seller_id' => 'nullable|numeric',
                            'country' => 'nullable|string',
                            'shipping_type' => 'string|required',
                            'region' => 'nullable|string',
                            'zip_from' => 'nullable',
                            'zip_to' => 'required_with:zip_from',
                            'weight_to' => 'required|numeric|min:0.0001',
                            'weight_from' => 'required',
                            'price' => 'numeric|required|min:0.0001',
                            'is_zip_range' => 'sometimes',
                            'zip_code' => 'nullable'
                        ]);

                        $data['marketplace_seller_id'] = $data['seller_id'];

                        $sellerId[$column+1] = $data['marketplace_seller_id'];

                        if ($validator->fails()) {

                            $failedRules[$column+1] = $validator->errors();

                            session()->flash('error', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.incorrect-format'));
                            redirect()->back();

                        } else {
                            $errorMsg = [];

                            if (isset($failedRules)) {
                                foreach ($failedRules as $coulmn => $fail) {
                                    if ($fail->first('seller_id')){
                                        $errorMsg[$coulmn] = $fail->first('seller_id');
                                    } else if ($fail->first('country')) {
                                        $errorMsg[$coulmn] = $fail->first('country');
                                    } else if ($fail->first('region')) {
                                        $errorMsg[$coulmn] = $fail->first('region');
                                    } else if ($fail->first('zip_from')) {
                                        $errorMsg[$coulmn] = $fail->first('zip_from');
                                    } else if ($fail->first('zip_to')) {
                                        $errorMsg[$coulmn] = $fail->first('zip_to');
                                    } else if ($fail->first('price')) {
                                        $errorMsg[$coulmn] = $fail->first('price');
                                    } else if ($fail->first('weight_from')) {
                                        $errorMsg[$coulmn] = $fail->first('weight_from');
                                    } else if ($fail->first('weight_to')) {
                                        $errorMsg[$coulmn] = $fail->first('weight_to');
                                    } else if ($fail->first('is_zip_range')) {
                                        $errorMsg[$coulmn] = $fail->first('is_zip_range');
                                    } else if ($fail->first('zip_code')) {
                                        $errorMsg[$coulmn] = $fail->first('zip_code');
                                    }
                                }

                                foreach ($errorMsg as $key => $msg) {
                                    $msg = str_replace(".", "", $msg);
                                    $message[] = $msg. ' at Row '  .$key . '.';
                                }

                                $finalMsg = implode(" ", $message);

                                session()->flash('error', $finalMsg);
                            } else {

                                $methodUpperCase = strtoupper($data['shipping_type']);

                                foreach ($shippingMethods as $methods) {

                                    $nameUpper = strtoupper($methods->name);

                                    if ($nameUpper == $methodUpperCase && $methods->status == 1) {
                                       $supersetId = $methods->id;
                                    }
                                }

                                if ($supersetId != null && in_array($data['marketplace_seller_id'],$sellerName) || $data['seller_id'] == 0 ) {
                                    $data['marketplace_tablerate_superset_id'] = $supersetId;
                                    $data['marketplace_seller_id'] = $data['seller_id'];

                                    if($data['marketplace_seller_id'] == 0) {
                                        $data['marketplace_seller_id'] = null;
                                    }

                                    if($supersetId != null) {

                                        $shippingRates = $this->shippingRateRepository->findWhere([
                                            'marketplace_seller_id' => $data['marketplace_seller_id'],
                                            'marketplace_tablerate_superset_id' => $data['marketplace_tablerate_superset_id']
                                        ]);

                                        foreach ($shippingRates as $shippingRate) {
                                            if ($shippingRate->zip_from <= $data['zip_from']
                                                && $shippingRate->zip_to >= $data['zip_to']
                                                && $data['weight_from'] >= $shippingRate->weight_from
                                                && $data['weight_to'] <= $shippingRate->weight_to

                                            ) {
                                                $ifAlreadyExist +=1;
                                                $exist = $ifAlreadyExist;
                                            }
                                        }

                                        if ($ifAlreadyExist > 0) {
                                            continue;
                                        } else {
                                            $this->shippingRateRepository->create($data);
                                        }
                                    } else {

                                        session()->flash('error', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.no-superset-found'));
                                        redirect()->back();
                                    }
                                } else if(!in_array($data['marketplace_seller_id'],$sellerName) && !$data['marketplace_seller_id'] == null) {

                                    session()->flash('error', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.seller-not-found'));
                                    redirect()->back();
                                } else {

                                    session()->flash('error', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.no-superset-found'));
                                    redirect()->back();
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                $failure = new Failure(1, 'rows', [0 => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.enough-row-error')]);

                session()->flash('error', $failure->errors()[0]);
            }
        }

        if ($exist > 0) {
            session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.some-weight-range-exist',
            ['name' => 'Shipping Rate']));
        } else {
            session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.upload-success', ['name' => 'Shipping Rate']));
        }

        return redirect()->route($this->_config['redirect']);
    }
}