<?php

namespace Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Imports\DataGridImport;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Validators\Failure;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\ShippingRateRepository as SellerShipping;
use Webkul\Core\Repositories\CountryRepository as Country;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;

/**
 * Marketplace Shipping Rate controller
 *
 * @author    Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ShippingRateController extends Controller
{
    /**
     * Contains route related configuration
     *
     *
     */
    protected $_config;

    /**
     * sellerRepository object
     *
     * @var array
     */
    protected $sellerRepository;

    /**
     * sellerShippingRepository object
     *
     * @var array
     */
    protected $sellerShipping;

    /**
     * countryRepository object
     *
     * @var array
     */
    protected $country;

    /**
     * supersetRepository object
     *
     * @var array
     */
    protected $supersetRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository $sellerRepository
     * @return void
     */
    public function __construct(
        SellerRepository $sellerRepository,
        SellerShipping $sellerShipping,
        Country $country,
        SupersetRepository $supersetRepository
    ) {
        $this->_config = request('_config');

        $this->sellerRepository = $sellerRepository;

        $this->sellerShipping = $sellerShipping;

        $this->country = $country;

        $this->supersetRepository = $supersetRepository;
    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $isSeller = $this->sellerRepository->isSeller(auth()->guard('customer')->user()->id);

        if (!$isSeller)
            return redirect()->route('marketplace.account.seller.create');


        $supersetMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $seller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $data = $this->sellerShipping->all()->where('marketplace_seller_id', $seller->id);

        $shippingMethods = $data->groupBy('marketplace_tablerate_superset_id');

        if (core()->getConfigData('sales.carriers.mptablerate.active')) {

            return view($this->_config['view'])->with('shippingMethods', $shippingMethods)->with('supersetMethods', $supersetMethods);
        } else {
            return redirect()->route('customer.profile.index');
        }
    }

    /**
     * Add Shipping Rate.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $countries = $this->country->all();

        return view($this->_config['view'])->with('countries', $countries)->with('shippingMethods', $shippingMethods);
    }

    /**
     * Store Shipping Rate
     *
     * @return response
     */
    public function store(Request $request)
    {


        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $sellerId = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $data = request()->all();

        $this->validate($request, [
            'shipping_type' => 'string|required',
            'country' => 'string|required',
            'region' => 'string|nullable',
            'weight_from' => 'required|numeric|min:0.0001',
            'weight_to' => 'required|numeric|min:' . $data['weight_from'],
            'zip_from' => 'string|required',
            'zip_to' => 'string|required',
            'price' => 'numeric|required|min:0.0001',
            'is_zip_range' => 'sometimes',
            'zip_code' => 'string|nullable',
        ]);

        $data = request()->all();

        $data['marketplace_seller_id'] = $sellerId->id;

        foreach ($shippingMethods as $method) {
            if ($method->id == $data['shipping_type']) {
                $data['marketplace_tablerate_superset_id'] = $method->id;
            }
        }

        $shippingRates = $this->sellerShipping->all();

        foreach ($shippingRates as $shippingRate) {
            if ($sellerId->id == $shippingRate->marketplace_seller_id) {
                if (
                    $data['weight_from'] >= $shippingRate->weight_from
                    && $data['weight_from'] <= $shippingRate->weight_to
                    && $data['marketplace_tablerate_superset_id'] == $shippingRate->marketplace_tablerate_superset_id
                ) {
                    if ($data['zip_from'] == $shippingRate->zip_from && $data['zip_to'] == $shippingRate->zip_to) {
                        session()->flash('error', trans(
                            'marketplace_tablerate_shipping::app.admin.shipping-rates.weight-range-exist',
                            ['name' => 'Shipping Rate']
                        ));

                        return back();
                    }
                }
            }
        }

        $superSet = $this->sellerShipping->create($data);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.create-success', ['name' => 'Shipping Rate']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Edit The Shipping Rate
     *
     * @return response
     */
    public function edit($id)
    {


        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $countries = $this->country->all();

        $shippingData = $this->sellerShipping->findOrFail($id);

        return view($this->_config['view'])->with('shippingData', $shippingData)->with('countries', $countries)->with('shippingMethods', $shippingMethods);
    }

    /**
     * update Shipping Rate
     *
     * @return response
     */
    public function update($id)
    {


        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $sellerId = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $data = request()->all();

        $this->validate(request(), [
            'shipping_type' => 'string|required',
            'country' => 'string|required',
            'region' => 'string|nullable',
            'weight_from' => 'required|numeric|min:0.0001',
            'weight_to' => 'required|numeric|min:' . $data['weight_from'],
            'zip_from' => 'string|required',
            'zip_to' => 'string|required',
            'price' => 'numeric|required|min:0.0001',
            'is_zip_range' => 'sometimes',
        ]);

        $data = request()->all();

        foreach ($shippingMethods as $method) {
            if ($method->id == $data['shipping_type']) {
                $data['marketplace_tablerate_superset_id'] = $method->id;
            }
        }

        $shippingRates = $this->sellerShipping->all();

        $this->sellerShipping->update($data, $id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.update-success', ['name' => 'Shipping Rate']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Delete Shipping Rate
     *
     * @return response
     */
    public function delete($id)
    {


        $this->sellerShipping->delete($id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.delete-success', ['name' => 'Shipping Rate']));

        return redirect()->back();
    }

    /**
     * Mass Delete Shipping Rate
     *
     * @return response
     */
    public function massDelete($key)
    {


        $data = $this->sellerShipping->all();

        $shippingMethods = $data->groupBY('marketplace_tablerate_superset_id');

        foreach ($shippingMethods as $keys => $Methods) {
            foreach ($Methods as  $method) {
                if ($method->marketplace_tablerate_superset_id == $key) {
                    $this->sellerShipping->delete($method->id);
                }
            }
        }

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.delete-success', ['name' => 'Shipping Rates']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     *  Dwonload The Sample csv file
     */
    public function sampleDownload()
    {


        $pathToFile = base_path() . "/packages/Webkul/MarketplaceTableRateShipping/src/Resources/views/sampledownload/sellershipping.csv";

        return response()->download($pathToFile);
    }

    /**
     * import Shipping Rate
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {


        $currentSeller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $shippingMethods = $this->supersetRepository->all();
        $valid_extension = ['csv'];
        $exist = 0;
        $file_extension[] = request()->file('file')->getClientOriginalExtension();

        if ($valid_extension !=  $file_extension) {
            session()->flash('error', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.upload-error'));
        } else {
            try {
                $excelData = (new DataGridImport)->toArray(request()->file('file'));

                foreach ($excelData as $tableRateData) {
                    foreach ($tableRateData as $column => $uploadData) {
                        $supersetId = null;
                        $ifAlreadyExist = 0;
                        $validator = Validator::make($uploadData, [
                            'country' => 'nullable|string',
                            'region' => 'nullable|string',
                            'shipping_type' => 'string|required',
                            'zip_from' => 'nullable|required',
                            'zip_to' => 'required_with:zip_from',
                            'weight_to' => 'required|numeric|min:0.0001',
                            'weight_from' => 'required',
                            'price' => 'required|min:0.0001',
                            'is_zip_range' => 'sometimes',
                            'zip_code' => 'nullable'
                        ]);

                        if ($validator->fails()) {
                            $failedRules[$column + 1] = $validator->errors();
                        }

                        if ($validator->fails()) {
                            session()->flash('error', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.incorrect-format'));
                            redirect()->back();
                        } else {
                            $errorMsg = [];

                            if (isset($failedRules)) {
                                foreach ($failedRules as $coulmn => $fail) {
                                    if ($fail->first('marketplace_seller_id')) {
                                        $errorMsg[$coulmn] = $fail->first('marketplace_seller_id');
                                    } else if ($fail->first('seller_name')) {
                                        $errorMsg[$coulmn] = $fail->first('seller_name');
                                    } else if ($fail->first('country')) {
                                        $errorMsg[$coulmn] = $fail->first('country');
                                    } else if ($fail->first('region')) {
                                        $errorMsg[$coulmn] = $fail->first('region');
                                    } else if ($fail->first('zip_from')) {
                                        $errorMsg[$coulmn] = $fail->first('zip_from');
                                    } else if ($fail->first('zip_to')) {
                                        $errorMsg[$coulmn] = $fail->first('zip_to');
                                    } else if ($fail->first('price')) {
                                        $errorMsg[$coulmn] = $fail->first('price');
                                    } else if ($fail->first('weight_from')) {
                                        $errorMsg[$coulmn] = $fail->first('weight_from');
                                    } else if ($fail->first('weight_to')) {
                                        $errorMsg[$coulmn] = $fail->first('weight_to');
                                    } else if ($fail->first('is_zip_range')) {
                                        $errorMsg[$coulmn] = $fail->first('is_zip_range');
                                    } else if ($fail->first('zip_code')) {
                                        $errorMsg[$coulmn] = $fail->first('zip_code');
                                    } else if ($fail->first('shipping_type')) {
                                        $errorMsg[$coulmn] = $fail->first('shipping_type');
                                    }
                                }

                                foreach ($errorMsg as $key => $msg) {
                                    $msg = str_replace(".", "", $msg);
                                    $message[] = $msg . ' at Row '  . $key . '.';
                                }
                                $finalMsg = implode(" ", $message);

                                session()->flash('error', $finalMsg);
                            } else {

                                $methodUpperCase = strtoupper($uploadData['shipping_type']);

                                foreach ($shippingMethods as $methods) {

                                    $nameUpper  = strtoupper($methods->name);

                                    if ($nameUpper == $methodUpperCase && $methods->status == 1) {
                                        $supersetId = $methods->id;
                                    }
                                }

                                $uploadData['marketplace_tablerate_superset_id'] = $supersetId;

                                if ($uploadData['is_zip_range'] == 'Yes') {
                                    $uploadData['is_zip_range'] = '0';
                                }

                                if ($uploadData['is_zip_range'] == 'No') {
                                    $uploadData['is_zip_range'] = '1';
                                }

                                $uploadData['marketplace_seller_id'] = $currentSeller->id;

                                if ($supersetId != null) {

                                    $shippingRates = $this->sellerShipping->findWhere([
                                        'marketplace_seller_id' => $currentSeller->id,
                                        'marketplace_tablerate_superset_id' => $uploadData['marketplace_tablerate_superset_id']
                                    ]);

                                    foreach ($shippingRates as $shippingRate) {
                                        if (
                                            $shippingRate->zip_from <= $uploadData['zip_from']
                                            && $shippingRate->zip_to >= $uploadData['zip_to']
                                            && $uploadData['weight_from'] >= $shippingRate->weight_from
                                            && $uploadData['weight_to'] <= $shippingRate->weight_to

                                        ) {
                                            $ifAlreadyExist += 1;
                                            $exist = $ifAlreadyExist;
                                        }
                                    }

                                    if ($ifAlreadyExist > 0) {
                                        continue;
                                    } else {
                                        $this->sellerShipping->create($uploadData);
                                    }
                                } else {

                                    session()->flash('error', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.no-superset-found'));
                                    redirect()->back();
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                $failure = new Failure(1, 'rows', [0 => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.enough-row-error')]);

                session()->flash('error', $failure->errors()[0]);
            }
        }

        if ($exist > 0) {
            session()->flash('error', trans(
                'marketplace_tablerate_shipping::app.admin.shipping-rates.some-weight-range-exist',
                ['name' => 'Shipping Rate']
            ));
        } else {
            session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.upload-success', ['name' => 'Shipping Rate']));
        }

        return redirect()->route($this->_config['redirect']);
    }
}
