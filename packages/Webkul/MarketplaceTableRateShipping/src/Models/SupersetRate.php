<?php

namespace Webkul\MarketplaceTableRateShipping\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\MarketplaceTableRateShipping\Contracts\SupersetRate as SupersetRateContract;
use Webkul\Marketplace\Models\SellerProxy;

class SupersetRate Extends Model implements SupersetRateContract
{
    protected $table = 'marketplace_tablerate_superset_rates';

    protected $fillable = [
        'marketplace_seller_id', 'marketplace_superset_id', 'price_from', 'price_to', 'shipping_type', 'price'
    ];

    public function seller()
    {
        return $this->belongsTo(SellerProxy::modelClass(), 'marketplace_seller_id');
    }

    public function superset()
    {
        return $this->belongsTo(SupersetProxy::modelClass(), 'marketplace_superset_id');
    }
}
