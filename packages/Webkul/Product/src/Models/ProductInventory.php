<?php

namespace Webkul\Product\Models;

use App\Models\ExtendWebkul\MarketplaceProduct;
use Illuminate\Database\Eloquent\Model;
use Webkul\Inventory\Models\InventorySourceProxy;
use Webkul\Product\Contracts\ProductInventory as ProductInventoryContract;

class ProductInventory extends Model implements ProductInventoryContract
{
    public $timestamps = false;

    protected $fillable = [
        'qty',
        'product_id',
        'inventory_source_id',
        'vendor_id',
    ];

    /**
     * Get the product attribute family that owns the product.
     */
    public function inventory_source()
    {
        return $this->belongsTo(InventorySourceProxy::modelClass());
    }

    /*
     * Return the SUM of al relevant marketplace_products
     */
    public function getQtyAttribute()
    {
        return MarketplaceProduct::where([
            ['product_id', '=', $this->product_id],
            ['marketplace_seller_id', '=', $this->vendor_id]]
        )->get()->pluck('qty')->sum();
    }

    /**
     * Get the product that owns the product inventory.
     */
    public function product()
    {
        return $this->belongsTo(ProductProxy::modelClass());
    }
}