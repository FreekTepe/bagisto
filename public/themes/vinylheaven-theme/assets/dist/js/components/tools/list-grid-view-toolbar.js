$(document).ready(function() {
    if ($(".toolbar-limits-select").length) {
        $.each($(".toolbar-limits-select"), function(select_index, select) {
            $(select).change(function(event) {
                window.location = $(this)
                    .children("option:selected")
                    .val();
            });
        });
    }

    if ($(".toolbar-order-select").length) {
        $.each($(".toolbar-order-select"), function(select_index, select) {
            $(select).change(function(event) {
                window.location = $(this)
                    .children("option:selected")
                    .val();
            });
        });
    }
});
