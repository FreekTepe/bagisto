$(document).ready(function() {
    //gallery images holder
    //gallery images navigator repsonsive breakingpoints => ((@media min-width) xs | sm 576px | md 768px | lg 992px | xl 1285px | "xxl" 1336px)
    var gallery_images_holder_slick_options = {
        asNavFor: "#gallery-images-navigator",
        slidesToShow: 1,
        rows: 0,
        slidesToScroll: 1,
        arrows: !1,
        fade: !0
    };
    var gallery_images_navigator_slick_options = {
        rows: 0,
        slidesToShow: 4,
        slidesToScroll: 1,
        vertical: !0,
        asNavFor: "#gallery-images-holder",
        dots: !1,
        focusOnSelect: !0,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    vertical: !1,
                    infinite: !0,
                    centerMode: !1,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerPadding: "0",
                    variableWidth: !0
                }
            }
        ]
    };

    $("#gallery-images-holder").slick(gallery_images_holder_slick_options);
    $("#gallery-images-navigator").slick(
        gallery_images_navigator_slick_options
    );
});
