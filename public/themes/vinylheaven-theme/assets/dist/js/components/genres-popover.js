$(document).ready(function() {
    $(".genres-image").each(function(i) {
        $(this).css("z-index", 999 - i);
    });

    $(".genres-list li").each(function() {
        var i = $(this).attr("id");
        $(this).hover(function(s) {
            $("." + i)["mouseenter" == s.type ? "addClass" : "removeClass"](
                "is-active"
            );
        });
    });
});
