$(document).ready(function() {
    $.each($(".navbar-collapse.has-transition"), function(
        collapse_index,
        collapse
    ) {
        $(collapse).on("show.bs.collapse", function() {
            if (!$($(collapse).data("trigger")).hasClass("open")) {
                $($(collapse).data("trigger")).addClass("open");
            }
        });

        $(collapse).on("hide.bs.collapse", function() {
            if ($($(collapse).data("trigger")).hasClass("open")) {
                $($(collapse).data("trigger")).removeClass("open");
            }
        });

        // $($(collapse).data("trigger")).on("mouseover", function() {
        //     $(collapse).collapse("show");
        // });

        // $(collapse).on("mouseout", function() {
        //     $(collapse).collapse("hide");
        // });
    });

    $("#navbar-currency-switcher").on("show.bs.dropdown", function() {
        if (!$("#navbar-currency").hasClass("open")) {
            $("#navbar-currency").addClass("open");
        }
    });

    $("#navbar-currency-switcher").on("hide.bs.dropdown", function() {
        if ($("#navbar-currency").hasClass("open")) {
            $("#navbar-currency").removeClass("open");
        }
    });
});
