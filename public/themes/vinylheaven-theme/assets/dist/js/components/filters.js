$(document).ready(function() {
    $(".js-filter").click(function() {
        $(".js-filter-area").toggleClass("is-active");
    });

    $(".js-fold-click").click(function(element) {
        element.preventDefault();
        $(this).toggleClass("hide");
        $(this)
            .next()
            .slideToggle();
    });

    $(".filter-condition").each(function(filter_index, filter) {
        $(filter).click(function(event) {
            window.location = $(this).data("url");
        });
    });

    $(".filter-sleeve-condition").each(function(filter_index, filter) {
        $(filter).click(function(event) {
            window.location = $(this).data("url");
        });
    });

    $(".filter-genre").each(function(filter_index, filter) {
        $(filter).click(function(event) {
            window.location = $(this).data("url");
        });
    });

    $(".filter-format").each(function(filter_index, filter) {
        $(filter).click(function(event) {
            window.location = $(this).data("url");
        });
    });

    $("#filter-clear-button").click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var location_url = "";

        if (
            $("#filter-price-min").val().length &&
            $("#filter-price-max").val().length
        ) {
            location_url =
                $("#filter-price-min")
                    .data("url")
                    .replace("%23price_min", $("#filter-price-min").val()) +
                "&price_max=" +
                $("#filter-price-max").val();
        } else if (
            $("#filter-price-min").val().length &&
            !$("#filter-price-max").val().length
        ) {
            location_url = $("#filter-price-min")
                .data("url")
                .replace("%23price_min", $("#filter-price-min").val());
        } else if (
            !$("#filter-price-min").val().length &&
            $("#filter-price-max").val().length
        ) {
            location_url = $("#filter-price-max")
                .data("url")
                .replace("%23price_max", $("#filter-price-max").val());
        }

        if (location_url.length) {
            window.location = location_url;
        }

        return false;
    });
});
